var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var username = localStorage['taikhoan'];
// let matram = localStorage.get('matram');
var macongty = localStorage['macongty_excel'];
var madienluc = localStorage['madienluc_excel'];
var matram = localStorage['matram_excel'];
var height_ = $(window).height()- 250;
// var height_ = window.height -300;
document.getElementById("excel_MA_DVIQLY").innerHTML = madienluc + "/" + matram;

/* ----- BAT DAU BIEN TAP -----*/
var bientapControl = 0;
var Array_DSKHACHHANG;
var oFile_COT;

// BIEN TAP 1
$(function () {
    oFile_COT = document.getElementById('luoidienhathe_input_dskhachhang');
    if (oFile_COT.addEventListener) {
        oFile_COT.addEventListener('change', filePicked_DSCOT, false);
    }
});

// Override --> Đọc dữ liệu từ file excel--> trả về json
function filePicked_DSCOT(oEvent) {

    // Get The File From The Input
    var oFile = oEvent.target.files[0];
    var sFilename = oFile.name;
    // Create A File Reader HTML5
    var reader = new FileReader();

    // Ready The Event For When A File Gets Selected
    reader.onload = function (e) {
        try {
            var data = e.target.result;
            var cfb = XLSX.CFB.read(data, { type: 'binary' });
            var wb = XLSX.parse_xlscfb(cfb);
            // Loop Over Each Sheet
            wb.SheetNames.forEach(function (sheetName) {

                // Obtain The Current Row As CSV
                var sCSV = XLSX.utils.make_csv(wb.Sheets[sheetName]);
                var oJS = XLSX.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);

                //
                $("#my_file_output").html(sCSV);
                $("#tbody-table-dscot").css('height',`${height_}`);
                for (var i = 0; i < oJS.length; i++) {
                    $("#tbody-table-dscot").append(
                        `<tr>
                        <td  style='text-align:center;width:  60px'>${(i + 1) + ""}</td>
                        <td  style='text-align:left;width: 130px'>${oJS[i].MA_DVIQLY}</td>
                        <td  style='text-align:left;width: 110px'>${oJS[i].MA_TRAM}</td>
                        <td  style='text-align:left;width: 120px'>${oJS[i].MA_LO}</td>
                        <td  style='text-align:left;width: 120px'>${oJS[i].MA_SOGCS}</td>
                        <td  style='text-align:left;width: 80px'>${replaceAll(oJS[i].STT, '\'', '')}</td>
                        <td  style='text-align:left;width:150px'>${oJS[i].MA_KHANG}</td>
                        <td  style='text-align:left;width: 180px'>${oJS[i].MA_DDO}</td>
                        <td  style='text-align:left;width: 395px'>${oJS[i].TEN_KHANG}</td>
                        <td  style='text-align:left;width: 350px'>${oJS[i].DCHI_KHANG}</td>
                        <td  style='text-align:left;width: 140px'>${replaceAll(oJS[i].DIEN_THOAI, '\'', '')}</td>
                        <td  style='text-align:left;width: 220px'>${replaceAll(oJS[i].EMAIL, '', '')}</td>
                        <td  style='text-align:left;width: 350px'>${oJS[i].DCHI_SDD}</td>
                        <td  style='text-align:left;width: 150px'>${replaceAll(oJS[i].X_SDD, '', '')}</td>
                        <td  style='text-align:left;width: 150px'>${replaceAll(oJS[i].Y_SDD, '', '')}</td>
                        <td  style='text-align:left;width: 150px'>${replaceAll(oJS[i].Z_SDD, '', '')}</td>
                        <td  style='text-align:left;width: 100px'>${replaceAll(oJS[i].SO_PHA, '', '')}</td>
                        <td  style='text-align:left;width: 60px'>${replaceAll(oJS[i].PHA, '', '')}</td>
                        <td  style='text-align:left;width: 100px'>${replaceAll(oJS[i].SO_HO, '', '')}</td>
                        <td  style='text-align:left;width: 180px'>${replaceAll(oJS[i].SO_COT, '\'', '')}</td>
                        <td  style='text-align:left;width: 140px'>${replaceAll(oJS[i].SO_THUNG, '', '')}</td>
                        <td  style='text-align:left;width: 150px'>${replaceAll(oJS[i].VTRI_THUNG, '', '')}</td>
                        <td  style='text-align:left;width: 150px'>${replaceAll(oJS[i].LOAI_THUNG, '', '')}</td>
                        <td  style='text-align:left;width: 120px'>${replaceAll(oJS[i].VTRI_CTO, '', '')}</td>
                        <td  style='text-align:left;width: 130px'>${replaceAll(oJS[i].VTRI_TREO, '', '')}</td>
                        <td  style='text-align:left;width: 120px'>${replaceAll(oJS[i].GIA_DIEN, '', '')}</td>
                        <td  style='text-align:left;width: 160px'>${oJS[i].MA_CLOAI_CTO}</td>
                        <td  style='text-align:left;width: 180px'>${oJS[i].NAM_SXUAT_CTO}</td>
                        <td  style='text-align:left;width: 100px'>${replaceAll(oJS[i].SO_CTO, '\'', '')}</td>
                        <td  style='text-align:left;width: 180px'>${oJS[i].NGAY_TREO_CTO}</td>
                        <td  style='text-align:left;width: 200px'>${oJS[i].NGAY_KDINH_CTO}</td>
                        <td  style='text-align:left;width: 120px'>${oJS[i].HS_NHAN}</td>
                        <td  style='text-align:left;width: 160px'>${replaceAll(oJS[i].MA_CLOAI_TI1, '', '')}</td>
                        <td  style='text-align:left;width: 180px'>${checkdata(oJS[i].NAM_SXUAT_TI1)}</td>
                        <td  style='text-align:left;width: 100px'>${replaceAll(oJS[i].SO_TI1, '\'', '')}</td>
                        <td  style='text-align:left;width: 180px'>${checkdata(oJS[i].NGAY_TREO_TI1)}</td>
                        <td  style='text-align:left;width: 200px'>${checkdata(oJS[i].NGAY_KDINH_TI1)}</td>
                        <td  style='text-align:left;width: 160px'>${replaceAll(oJS[i].MA_CLOAI_TI2, '', '')}</td>
                        <td  style='text-align:left;width: 180px'>${checkdata(oJS[i].NAM_SXUAT_TI2)}</td>
                        <td  style='text-align:left;width: 100px'>${replaceAll(oJS[i].SO_TI2, '\'', '')}</td>
                       
                        <td  style='text-align:left;width: 180px'>${checkdata(oJS[i].NGAY_TREO_TI2)}</td>
                        <td  style='text-align:left;width: 200px'>${checkdata(oJS[i].NGAY_KDINH_TI2)}</td>
                        <td  style='text-align:left;width: 160px'>${replaceAll(oJS[i].MA_CLOAI_TI3, '', '')}</td>
                        <td  style='text-align:left;width: 180px'>${checkdata(oJS[i].NAM_SXUAT_TI3)}</td>
                        <td  style='text-align:left;width: 100px'>${replaceAll(oJS[i].SO_TI3, '\'', '')}</td>
                        <td  style='text-align:left;width: 180px'>${checkdata(oJS[i].NGAY_TREO_TI3)}</td>
                        <td  style='text-align:left;width: 200px'>${checkdata(oJS[i].NGAY_KDINH_TI3)}</td>
                        <td  style='text-align:left;width: 120px'>${replaceAll(oJS[i].GHI_CHU, '', '')}</td>
                       
                   </tr>`
                    );
                }

                //
                Array_DSKHACHHANG = oJS;
            });
        } catch (e) {
            // khong dung dinh dang se thong bao
            alert("File excel THONGTIN_KHACHHANG không đúng.");
        }
    };
    reader.readAsBinaryString(oFile);
}

//1. Gửi dữ liệu cột về server
function GuiDuLieu_COT() {
    //
    if (CheckThongTinUpLoad(Array_DSKHACHHANG) == 1) {
        $("#loading").show();
        guiCot(Array_DSKHACHHANG, 0);
    }
}

function guiCot(m_Array_DSKHACHHANG, m_i) {
    //console.log("index: " + m_i);
    m_Array_DSKHACHHANG[m_i].MA_DVIQLY = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_DVIQLY);
    m_Array_DSKHACHHANG[m_i].MA_TRAM = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_TRAM);
    m_Array_DSKHACHHANG[m_i].MA_LO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_LO);
    m_Array_DSKHACHHANG[m_i].MA_SOGCS = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_SOGCS);
    m_Array_DSKHACHHANG[m_i].STT = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].STT);
    m_Array_DSKHACHHANG[m_i].MA_KHANG = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_KHANG);
    m_Array_DSKHACHHANG[m_i].MA_DDO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_DDO);
    m_Array_DSKHACHHANG[m_i].TEN_KHANG = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].TEN_KHANG);
    m_Array_DSKHACHHANG[m_i].DCHI_KHANG = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].DCHI_KHANG);
    m_Array_DSKHACHHANG[m_i].DIEN_THOAI = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].DIEN_THOAI);
    m_Array_DSKHACHHANG[m_i].EMAIL = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].EMAIL);
    m_Array_DSKHACHHANG[m_i].DCHI_SDD = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].DCHI_SDD);
    m_Array_DSKHACHHANG[m_i].X_SDD = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].X_SDD);
    m_Array_DSKHACHHANG[m_i].Y_SDD = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].Y_SDD);
    m_Array_DSKHACHHANG[m_i].Z_SDD = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].Z_SDD);
    m_Array_DSKHACHHANG[m_i].SO_PHA = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_PHA);
    m_Array_DSKHACHHANG[m_i].PHA = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].PHA);
    m_Array_DSKHACHHANG[m_i].SO_HO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_HO);
    m_Array_DSKHACHHANG[m_i].SO_COT = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_COT);
    m_Array_DSKHACHHANG[m_i].SO_THUNG = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_THUNG);
    m_Array_DSKHACHHANG[m_i].VTRI_THUNG = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].VTRI_THUNG);
    m_Array_DSKHACHHANG[m_i].LOAI_THUNG = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].LOAI_THUNG);
    m_Array_DSKHACHHANG[m_i].VTRI_CTO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].VTRI_CTO);
    m_Array_DSKHACHHANG[m_i].VTRI_TREO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].VTRI_TREO);
    m_Array_DSKHACHHANG[m_i].GIA_DIEN = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].GIA_DIEN);
    m_Array_DSKHACHHANG[m_i].MA_CLOAI_CTO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_CLOAI_CTO);
    m_Array_DSKHACHHANG[m_i].NAM_SXUAT_CTO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NAM_SXUAT_CTO);
    m_Array_DSKHACHHANG[m_i].SO_CTO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_CTO);
    m_Array_DSKHACHHANG[m_i].NGAY_TREO_CTO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_TREO_CTO);
    m_Array_DSKHACHHANG[m_i].NGAY_KDINH_CTO = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_KDINH_CTO);
    m_Array_DSKHACHHANG[m_i].HS_NHAN = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].HS_NHAN);
    m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI1 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI1);
    m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI1 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI1);
    m_Array_DSKHACHHANG[m_i].SO_TI1 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_TI1);
    m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI1 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI1);
    m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI1 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI1);
    m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI2 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI2);
    m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI2 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI2);
    m_Array_DSKHACHHANG[m_i].SO_TI2 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_TI2);
    m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI2 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI2);
    m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI2 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI2);
    m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI3 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI3);
    m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI3 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI3);
    m_Array_DSKHACHHANG[m_i].SO_TI3 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].SO_TI3);
    m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI3 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI3);
    m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI3 = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI3);
    m_Array_DSKHACHHANG[m_i].GHI_CHU = checkContentData_forPost(m_Array_DSKHACHHANG[m_i].GHI_CHU);
    //
    m_Array_DSKHACHHANG[m_i].STT = replaceAll(m_Array_DSKHACHHANG[m_i].STT, '\'', '');
    m_Array_DSKHACHHANG[m_i].DIEN_THOAI = replaceAll(m_Array_DSKHACHHANG[m_i].DIEN_THOAI, '\'', '');
    m_Array_DSKHACHHANG[m_i].SO_COT = replaceAll(m_Array_DSKHACHHANG[m_i].SO_COT, '\'', '');
    m_Array_DSKHACHHANG[m_i].SO_CTO = replaceAll(m_Array_DSKHACHHANG[m_i].SO_CTO, '\'', '');
    m_Array_DSKHACHHANG[m_i].SO_TI1 = replaceAll(m_Array_DSKHACHHANG[m_i].SO_TI1, '\'', '');
    m_Array_DSKHACHHANG[m_i].SO_TI2 = replaceAll(m_Array_DSKHACHHANG[m_i].SO_TI2, '\'', '');
    m_Array_DSKHACHHANG[m_i].SO_TI3 = replaceAll(m_Array_DSKHACHHANG[m_i].SO_TI3, '\'', '');
    if( m_Array_DSKHACHHANG[m_i].SO_PHA==''){
        m_Array_DSKHACHHANG[m_i].SO_PHA =0;
    }
    if( m_Array_DSKHACHHANG[m_i].SO_HO==''){
        m_Array_DSKHACHHANG[m_i].SO_HO =0;
    }
    if( m_Array_DSKHACHHANG[m_i].VTRI_THUNG==''){
        m_Array_DSKHACHHANG[m_i].VTRI_THUNG =0;
    }
    if( m_Array_DSKHACHHANG[m_i].VTRI_CTO==''){
        m_Array_DSKHACHHANG[m_i].VTRI_CTO =0;
    }
    if( m_Array_DSKHACHHANG[m_i].NAM_SXUAT_CTO==''){
        m_Array_DSKHACHHANG[m_i].NAM_SXUAT_CTO =0;
    } 
    if( m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI1==''){
        m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI1 =0;
    }
    if( m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI2==''){
        m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI2 =0;
    }
    if( m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI3==''){
        m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI3 =0;
    }

    //
    const body = {
        MA_DVIQLY: m_Array_DSKHACHHANG[m_i].MA_DVIQLY,
        MA_TRAM : m_Array_DSKHACHHANG[m_i].MA_TRAM, 
        MA_LO : m_Array_DSKHACHHANG[m_i].MA_LO, 
        MA_SOGCS : m_Array_DSKHACHHANG[m_i].MA_SOGCS, 
        STT : m_Array_DSKHACHHANG[m_i].STT, 
        MA_KHANG : m_Array_DSKHACHHANG[m_i].MA_KHANG, 
        MA_DDO : m_Array_DSKHACHHANG[m_i].MA_DDO, 
        TEN_KHANG : m_Array_DSKHACHHANG[m_i].TEN_KHANG, 
        DCHI_KHANG : m_Array_DSKHACHHANG[m_i].DCHI_KHANG, 
        DIEN_THOAI : m_Array_DSKHACHHANG[m_i].DIEN_THOAI, 
        EMAIL : m_Array_DSKHACHHANG[m_i].EMAIL, 
        DCHI_SDD : m_Array_DSKHACHHANG[m_i].DCHI_SDD, 
        X_SDD : m_Array_DSKHACHHANG[m_i].X_SDD, 
        Y_SDD : m_Array_DSKHACHHANG[m_i].Y_SDD, 
        Z_SDD : m_Array_DSKHACHHANG[m_i].Z_SDD, 
        SO_PHA : m_Array_DSKHACHHANG[m_i].SO_PHA, 
        PHA : m_Array_DSKHACHHANG[m_i].PHA, 
        SO_HO : m_Array_DSKHACHHANG[m_i].SO_HO, 
        SO_COT : m_Array_DSKHACHHANG[m_i].SO_COT.toUpperCase(), 
        SO_THUNG : m_Array_DSKHACHHANG[m_i].SO_THUNG, 
        VTRI_THUNG : m_Array_DSKHACHHANG[m_i].VTRI_THUNG, 
        LOAI_THUNG : m_Array_DSKHACHHANG[m_i].LOAI_THUNG, 
        VTRI_CTO : m_Array_DSKHACHHANG[m_i].VTRI_CTO,
        VTRI_TREO : m_Array_DSKHACHHANG[m_i].VTRI_TREO,
        GIA_DIEN : m_Array_DSKHACHHANG[m_i].GIA_DIEN,
        MA_CLOAI_CTO : m_Array_DSKHACHHANG[m_i].MA_CLOAI_CTO,
        NAM_SXUAT_CTO : m_Array_DSKHACHHANG[m_i].NAM_SXUAT_CTO,
        SO_CTO : m_Array_DSKHACHHANG[m_i].SO_CTO,
        NGAY_TREO_CTO : m_Array_DSKHACHHANG[m_i].NGAY_TREO_CTO,
        NGAY_KDINH_CTO : m_Array_DSKHACHHANG[m_i].NGAY_KDINH_CTO,
        HS_NHAN : m_Array_DSKHACHHANG[m_i].HS_NHAN,
        MA_CLOAI_TI1 : m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI1,
        NAM_SXUAT_TI1 : m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI1,
        SO_TI1 : m_Array_DSKHACHHANG[m_i].SO_TI1,
        NGAY_TREO_TI1 : m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI1,
        NGAY_KDINH_TI1 : m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI1,
        MA_CLOAI_TI2 : m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI2,
        NAM_SXUAT_TI2 : m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI2,
        SO_TI2 : m_Array_DSKHACHHANG[m_i].SO_TI2,
        NGAY_TREO_TI2 : m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI2,
        NGAY_KDINH_TI2 : m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI2,
        MA_CLOAI_TI3 : m_Array_DSKHACHHANG[m_i].MA_CLOAI_TI3,
        NAM_SXUAT_TI3 : m_Array_DSKHACHHANG[m_i].NAM_SXUAT_TI3,
        SO_TI3 : m_Array_DSKHACHHANG[m_i].SO_TI3,
        NGAY_TREO_TI3 : m_Array_DSKHACHHANG[m_i].NGAY_TREO_TI3,
        NGAY_KDINH_TI3 : m_Array_DSKHACHHANG[m_i].NGAY_KDINH_TI3,
        GHI_CHU : m_Array_DSKHACHHANG[m_i].GHI_CHU,
        NGUOI_NHAP : username,
        THAY_DOI : '$EXCEL'
    }
    //
    var url_InsertDSKhangT = "/api/HATHE_updateDSKhachhang_byTram";
    $.post(url_InsertDSKhangT, body, function (data) {
        //console.log("Test: "+m_Array_DSKHACHHANG[m_i].SO_COT.toUpperCase());
        $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_mtb_cmis_update_socot_cmis3?`
        + `ma_dviqly=${m_Array_DSKHACHHANG[m_i].MA_DVIQLY}`
        + `&ma_tram=${m_Array_DSKHACHHANG[m_i].MA_TRAM}`
        + `&ma_khang=${m_Array_DSKHACHHANG[m_i].MA_KHANG}`
        +`&ma_ddo=${m_Array_DSKHACHHANG[m_i].MA_DDO}`
        +`&so_cot=${m_Array_DSKHACHHANG[m_i].SO_COT.toUpperCase()}`
        +`&so_thung=${m_Array_DSKHACHHANG[m_i].SO_THUNG}`
        +`&vitri_treo=${m_Array_DSKHACHHANG[m_i].VTRI_TREO}`
        +`&sdt=${m_Array_DSKHACHHANG[m_i].DIEN_THOAI}`
        +`&email=${m_Array_DSKHACHHANG[m_i].EMAIL}`
        +`&stt=${m_Array_DSKHACHHANG[m_i].STT}`
        +`&ma_lo=${m_Array_DSKHACHHANG[m_i].MA_LO}`
        +`&ma_cto=${m_Array_DSKHACHHANG[m_i].SO_CTO}`
        +`&NGUOI_NHAP=${username}`
        +`&KEY=${GV_KEY_TOANDTB}`, function (data) {
        });
        m_i++;
        if (Array_DSKHACHHANG.length == m_i) {
            $("#loading").hide();
            document.getElementById('id_excelcot').style.display = 'none';
            alert("Bạn gửi dữ liệu khách hàng thành công.");
            setTimeout(clickDongy, 0);
        } else {
            guiCot(Array_DSKHACHHANG, m_i);
               // Gui CMIS
            //console.log("URL: "+ `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_mtb_cmis_update_socot?MA_DVIQLY=${m_Array_DSKHACHHANG[m_i].MA_DVIQLY}&ma_ddo=${m_Array_DSKHACHHANG[m_i].MA_DDO}&so_cot=${m_Array_DSKHACHHANG[m_i].SO_COT.toUpperCase()}&NGUOI_NHAP=${username}&KEY=${GV_KEY_TOANDTB}`);
        }
    });
}

//
function xacNhanGuiDuLieu() {
    try {
        if (Array_DSKHACHHANG.length == 0) {
            alert("Excel không có dữ liệu nào để gửi.");
        } else {
            document.getElementById('id_excelcot').style.display = 'block';
            var obj = $("#text_excelcot").text("Bạn có đồng ý gửi dữ liệu cho trạm " + matram + " này không?"
                + " \nLưu ý: \n1. Khi bạn đồng ý, toàn bộ dữ liệu cột của trạm này sẽ bị xóa hết và được đẩy dữ liệu mới nhất từ file Excel vào."
                + "\n 2. Khi gửi dữ liệu thành công, bạn vui lòng nhấn lại nút 'Xem' để xem dữ liệu mới gửi về."
                + " \n3. Chức năng này chỉ có Quản trị ở Điện lực mới sử dụng được."
                + " \n4. Số cột sẽ được đẩy thẳng lên CMIS.");
            obj.html(obj.html().replace(/\n/g, '<br/>'));
        }
    } catch (e) {
        alert("Excel không có dữ liệu nào để gửi.")
    }
}

function quayLai() {
    window.close();
}

function checkContentData_forPost(data) {
    if (data == undefined || data.trim() == '' || data.trim() == 'null' || data.trim() == 'NULL') {
        data = '';
    }
    //data = replaceAll(data, '/', '%2F');
    //data = replaceAll(data, ' ', '%20');
    //data = replaceAll(data, '-', '%2D');
    return data;
}
function checkContentData(data) {
    if (data == undefined || data.trim() == '' || data.trim() == 'null' || data.trim() == 'NULL') {
        data = '%20';
    }
    data = replaceAll(data, '/', '%2F');
    data = replaceAll(data, ' ', '%20');
    data = replaceAll(data, '-', '%2D');
    return data;
}

//Kiểm tra MA_DVIQLY va MA_TRAM của user có trùng với file excel không?
function CheckThongTinUpLoad(ArrayInfo) {
    var count_madienluc_failed = 0;
    var count_matram_failed = 0;
    var count_malo_failed = 0;
    var count_masogcs_failed = 0;
    var count_stt_failed = 0;
    var count_makhachhang_failed = 0;
    var count_madiemdo_failed = 0;
    var count_tenkhachhang_failed = 0;
    var count_diachikhachhang_failed = 0;
    var count_diachisudungdien_failed = 0;
    var count_sopha_failed = 0;
    var count_soho_failed = 0;
    var count_socot_failed = 0;
    var count_vitritreo_failed = 0;
    var count_machungloaicongto_failed = 0;
    var count_namsanxuatcto_failed = 0;
    var count_socongto_failed = 0;
    var count_ngaytreocongto_failed = 0;
    var count_ngaykinhdoanhcongto_failed = 0;
    var count_hesonhan_failed = 0;

    for (var i = 0; i < ArrayInfo.length; i++) {

        if (ArrayInfo[i].MA_DVIQLY != madienluc) {
            count_madienluc_failed++;
        }
        if (ArrayInfo[i].MA_TRAM != matram) {
            count_matram_failed++;
        }
        if (ArrayInfo[i].MA_LO == undefined || ArrayInfo[i].MA_LO == null || ArrayInfo[i].MA_LO.trim() == '') {
            count_malo_failed++;
        }
        if (ArrayInfo[i].MA_SOGCS == undefined || ArrayInfo[i].MA_SOGCS == null || ArrayInfo[i].MA_SOGCS.trim() == '') {
            count_masogcs_failed++;
        }
        if (ArrayInfo[i].STT == undefined || ArrayInfo[i].STT == null || ArrayInfo[i].STT.trim() == '') {
            count_stt_failed++;
        }
        if (ArrayInfo[i].MA_KHANG == undefined || ArrayInfo[i].MA_KHANG == null || ArrayInfo[i].MA_KHANG.trim() == '') {
            count_makhachhang_failed++;
        }
        if (ArrayInfo[i].MA_DDO == undefined || ArrayInfo[i].MA_DDO == null || ArrayInfo[i].MA_DDO.trim() == '') {
            count_madiemdo_failed++;
        }
        if (ArrayInfo[i].TEN_KHANG == undefined || ArrayInfo[i].TEN_KHANG == null || ArrayInfo[i].TEN_KHANG.trim() == '') {
            count_tenkhachhang_failed++;
        }
        if (ArrayInfo[i].DCHI_KHANG == undefined || ArrayInfo[i].DCHI_KHANG == null || ArrayInfo[i].DCHI_KHANG.trim() == '') {
            count_diachikhachhang_failed++;
        }
        if (ArrayInfo[i].SO_PHA == undefined || ArrayInfo[i].SO_PHA == null || ArrayInfo[i].SO_PHA.trim() == '') {
            count_sopha_failed++;
        }
        if (ArrayInfo[i].SO_HO == undefined || ArrayInfo[i].SO_HO == null || ArrayInfo[i].SO_HO.trim() == '') {
            count_soho_failed++;
        }
        if (ArrayInfo[i].SO_COT == undefined || ArrayInfo[i].SO_COT == null || ArrayInfo[i].SO_COT.trim() == '') {
            count_socot_failed++;
        }
        if (ArrayInfo[i].VTRI_TREO == undefined || ArrayInfo[i].VTRI_TREO == null || ArrayInfo[i].VTRI_TREO.trim() == '') {
            count_vitritreo_failed++;
        }
        if (ArrayInfo[i].MA_CLOAI_CTO == undefined || ArrayInfo[i].MA_CLOAI_CTO == null || ArrayInfo[i].MA_CLOAI_CTO.trim() == '') {
            count_machungloaicongto_failed++;
        }
        if (ArrayInfo[i].NAM_SXUAT_CTO == undefined || ArrayInfo[i].NAM_SXUAT_CTO == null || ArrayInfo[i].NAM_SXUAT_CTO.trim() == '') {
            count_namsanxuatcto_failed++;
        }
        if (ArrayInfo[i].SO_CTO == undefined || ArrayInfo[i].SO_CTO == null || ArrayInfo[i].SO_CTO.trim() == '') {
            count_socongto_failed++;
        }
        if (ArrayInfo[i].NGAY_TREO_CTO == undefined || ArrayInfo[i].NGAY_TREO_CTO == null || ArrayInfo[i].NGAY_TREO_CTO.trim() == '') {
            count_ngaytreocongto_failed++;
        }
        if (ArrayInfo[i].NGAY_KDINH_CTO == undefined || ArrayInfo[i].NGAY_KDINH_CTO == null || ArrayInfo[i].NGAY_KDINH_CTO.trim() == '') {
            count_ngaykinhdoanhcongto_failed++;
        }
        if (ArrayInfo[i].HS_NHAN == undefined || ArrayInfo[i].HS_NHAN == null || ArrayInfo[i].HS_NHAN.trim() == '') {
            count_hesonhan_failed++;
        }

    }

    if (count_madienluc_failed == 0 && count_matram_failed == 0 && count_malo_failed == 0 && count_masogcs_failed == 0 &&
        count_stt_failed == 0 && count_makhachhang_failed == 0 && count_madiemdo_failed == 0 && count_tenkhachhang_failed == 0 &&
        count_diachikhachhang_failed == 0 && count_diachisudungdien_failed == 0 && count_sopha_failed == 0 && count_soho_failed == 0 &&
        count_socot_failed == 0 && count_vitritreo_failed == 0 && count_machungloaicongto_failed == 0 && count_namsanxuatcto_failed == 0 &&
        count_socongto_failed == 0 && count_ngaytreocongto_failed == 0 && count_ngaykinhdoanhcongto_failed == 0 && count_hesonhan_failed == 0) {
        return 1;
    } else {
        var msg = "Số lượng MA_DVIQLY không hợp lí: " + count_madienluc_failed +
            "\n Số lượng MA_TRAM không hợp lí: " + count_matram_failed +
            "\n Số lượng MA_LO không hợp lí: " + count_malo_failed +
            "\n Số lượng MA_SOGCS không hợp lí: " + count_masogcs_failed +
            "\n Số lượng STT không hợp lí: " + count_stt_failed +
            "\n Số lượng MA_KHANG không hợp lí: " + count_makhachhang_failed +
            "\n Số lượng MA_DDO không hợp lí: " + count_madiemdo_failed +
            "\n Số lượng TEN_KHANG không hợp lí: " + count_tenkhachhang_failed +
            "\n Số lượng DCHI_KHANG không hợp lí: " + count_diachikhachhang_failed +
            "\n Số lượng DCHI_SDD không hợp lí: " + count_diachisudungdien_failed +
            "\n Số lượng SO_PHA không hợp lí: " + count_sopha_failed +
            "\n Số lượng SO_HO không hợp lí: " + count_soho_failed +
            "\n Số lượng SO_COT không hợp lí: " + count_socot_failed +
            "\n Số lượng VTRI_TREO không hợp lí: " + count_vitritreo_failed +
            "\n Số lượng MA_CLOAI_CTO không hợp lí: " + count_machungloaicongto_failed +
            "\n Số lượng NAM_SXUAT_CTO không hợp lí: " + count_namsanxuatcto_failed +
            "\n Số lượng SO_CTO không hợp lí: " + count_socongto_failed +
            "\n Số lượng NGAY_TREO_CTO không hợp lí: " + count_ngaytreocongto_failed +
            "\n Số lượng NGAY_KDINH_CTO không hợp lí: " + count_ngaykinhdoanhcongto_failed +
            "\n Số lượng HS_NHAN không hợp lí: " + count_hesonhan_failed +
            "\n Bạn vui lòng kiểm tra lại dữ liệu.";
        alert(msg);
    }

}

function replaceAll(str, find, replace) {
    if (str == undefined) {
        return '';
    } else {
        return str.replace(new RegExp(find, 'g'), replace);
    }
}

function clickDongy() {
    window.close();
}

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    //
    $("#loading").show();
    $.get(`${url}/api/HATHE_getDSExcelThongtinkhachhang_byTram/${madienluc.trim()}/${matram.trim()}`, function (data) {
        $("#loading").hide();
        //console.log(data.length + "---");
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
    });
}

function checkdata(thongtin) {
    if (thongtin == '0' || thongtin == '1900-01-01' || thongtin == '1/1/00') {
        return "";
    }
    else return thongtin;
}
