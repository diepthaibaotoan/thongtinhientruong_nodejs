GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matram;
var isDate = false;

$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);

// BIEN TAP CHE DO NGAY GIO 
document.querySelector("#today").valueAsDate = new Date();
document.getElementById("today").disabled = true;
var cb_date = document.querySelector("input[name=cb_date]");
cb_date.addEventListener('change', function () {
    if (this.checked) {
        isDate = true;
        document.getElementById("today").disabled = false;
        document.getElementById("today").style.backgroundColor = 'white';
        document.getElementById("congty").disabled = true;
        document.getElementById("dienluc").disabled = true;
        document.getElementById("tram").disabled = true;

    } else {
        isDate = false;
        document.getElementById("today").disabled = true;
        document.getElementById("today").style.backgroundColor = 'beige';
        document.getElementById("congty").disabled = false;
        document.getElementById("dienluc").disabled = false;
        document.getElementById("tram").disabled = false;
    }
});

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

$("#loading").hide();

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: dulieubaocao,
        columns: getColumns(dulieubaocao)
    });
}

// Click nut xem bao cao
function clickXembaocao() {
    $('#tongcong_colwillchangef').html('');
    $('#tongsocot_colwillchangef').html('');
    $('#tongsoanhcot_colwillchangef').html('');
    $('#tongsokhachhang_colwillchangef').html('');
    $('#tongsoanhkhachhang_colwillchangef').html('');

    if (isDate) {
        $("#dataTable").dataTable().fnDestroy();
        $("#tbody").empty();
        $('#colwillchangeh').html('Công Ty');
        $("#loading").show();
        //console.log("URL: "+`${url}/api/HATHE_getBCKhachhang/CPC/${GV_CAPTONG}`);
        $.get(`${url}/api/HATHE_getBCKhachhang_TheoNgay/${$('#today').val()}`, function (data) {
            dulieubaocao = data;
            $('#tongcong_colwillchangef').html('Tổng cộng:');
            var tongsokh = 0;
            var tongsokhkhongcosocot = 0;
            var tongsokhkhongcotoado = 0;
            var tongsokhkhongcoanh = 0;
            $.each(data, function (i, f) {
                tongsokh = tongsokh + f.SLUONG_KHANG;
                tongsokhkhongcosocot = tongsokhkhongcosocot + f.SLUONG_KHANGKHONGCOSOCOT;
                tongsokhkhongcotoado = tongsokhkhongcotoado + f.SLUONG_KHANGKHONGCOTOADOCOT;
                tongsokhkhongcoanh = tongsokhkhongcoanh + f.SLUONG_KHANGKHONGCOANH;
                //console.log("A: "+tongsocot);
                $("#tbody").append(
                    `<tr>
                        <td style='text-align:center'>${f.STT}</td>
                        <td>${f.TEN_DONVILOC}</td>
                        <td style='text-align:right'>${f.SLUONG_KHANG.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOSOCOT.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOTOADOCOT.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOANH.toLocaleString('en')}</td>
                    </tr>`
                );
            });
            $('#dataTable').DataTable({
                "columns": [
                    { "width": "3%" },
                    { "width": "25%" },
                    { "width": "18%" },
                    { "width": "18%" },
                    { "width": "18%" },
                    { "width": "18%" }
                ]
            });
            //
            $('#tongsocot_colwillchangef').html(tongsokh.toLocaleString('en'));
            $('#tongsoanhcot_colwillchangef').html(tongsokhkhongcosocot.toLocaleString('en'));
            $('#tongsokhachhang_colwillchangef').html(tongsokhkhongcotoado.toLocaleString('en'));
            $('#tongsoanhkhachhang_colwillchangef').html(tongsokhkhongcoanh.toLocaleString('en'));
            $("#loading").hide();
        });
    } else {
        if (macongty == null || macongty == '0') {
            $("#dataTable").dataTable().fnDestroy();
            $("#tbody").empty();
            $('#colwillchangeh').html('Công Ty');
            $("#loading").show();
            //console.log("URL: "+`${url}/api/HATHE_getBCKhachhang/CPC/${GV_CAPTONG}`);
            $.get(`${url}/api/HATHE_getBCKhachhang/CPC/${GV_CAPTONG}`, function (data) {
                dulieubaocao = data;
                $('#tongcong_colwillchangef').html('Tổng cộng:');
                var tongsokh = 0;
                var tongsokhkhongcosocot = 0;
                var tongsokhkhongcotoado = 0;
                var tongsokhkhongcoanh = 0;
                $.each(data, function (i, f) {
                    tongsokh = tongsokh + f.SLUONG_KHANG;
                    tongsokhkhongcosocot = tongsokhkhongcosocot + f.SLUONG_KHANGKHONGCOSOCOT;
                    tongsokhkhongcotoado = tongsokhkhongcotoado + f.SLUONG_KHANGKHONGCOTOADOCOT;
                    tongsokhkhongcoanh = tongsokhkhongcoanh + f.SLUONG_KHANGKHONGCOANH;
                    //console.log("A: "+tongsocot);
                    $("#tbody").append(
                        `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.TEN_DONVILOC}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANG.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOSOCOT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOTOADOCOT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOANH.toLocaleString('en')}</td>
                        </tr>`
                    );
                });
                $('#dataTable').DataTable({
                    "columns": [
                        { "width": "3%" },
                        { "width": "25%" },
                        { "width": "18%" },
                        { "width": "18%" },
                        { "width": "18%" },
                        { "width": "18%" }
                    ]
                });
                //
                $('#tongsocot_colwillchangef').html(tongsokh.toLocaleString('en'));
                $('#tongsoanhcot_colwillchangef').html(tongsokhkhongcosocot.toLocaleString('en'));
                $('#tongsokhachhang_colwillchangef').html(tongsokhkhongcotoado.toLocaleString('en'));
                $('#tongsoanhkhachhang_colwillchangef').html(tongsokhkhongcoanh.toLocaleString('en'));
                $("#loading").hide();
            });
        } else {
            if (madienluc == null || madienluc == '0') {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $('#colwillchangeh').html('Điện Lực');
                $("#loading").show();
                console.log("URL: " + `${url}/api/HATHE_getBCKhachhang/${macongty}/${GV_CAPCONGTY}`);
                $.get(`${url}/api/HATHE_getBCKhachhang/${macongty}/${GV_CAPCONGTY}`, function (data) {
                    dulieubaocao = data;
                    $('#tongcong_colwillchangef').html('Tổng cộng:');
                    var tongsokh = 0;
                    var tongsokhkhongcosocot = 0;
                    var tongsokhkhongcotoado = 0;
                    var tongsokhkhongcoanh = 0;
                    $.each(data, function (i, f) {
                        tongsokh = tongsokh + f.SLUONG_KHANG;
                        tongsokhkhongcosocot = tongsokhkhongcosocot + f.SLUONG_KHANGKHONGCOSOCOT;
                        tongsokhkhongcotoado = tongsokhkhongcotoado + f.SLUONG_KHANGKHONGCOTOADOCOT;
                        tongsokhkhongcoanh = tongsokhkhongcoanh + f.SLUONG_KHANGKHONGCOANH;
                        $("#tbody").append(
                            `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.TEN_DONVILOC}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANG.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOSOCOT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOTOADOCOT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOANH.toLocaleString('en')}</td>
                        </tr>`
                        );
                    });
                    $('#dataTable').DataTable({
                        // "scrollY": "200px",
                        // "scrollCollapse": true,
                        // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        "columns": [
                            { "width": "5%" },
                            { "width": "35%" },
                            { "width": "15%" },
                            { "width": "15%" },
                            { "width": "15%" },
                            { "width": "15%" }
                        ]
                    });
                    //
                    $('#tongsocot_colwillchangef').html(tongsokh.toLocaleString('en'));
                    $('#tongsoanhcot_colwillchangef').html(tongsokhkhongcosocot.toLocaleString('en'));
                    $('#tongsokhachhang_colwillchangef').html(tongsokhkhongcotoado.toLocaleString('en'));
                    $('#tongsoanhkhachhang_colwillchangef').html(tongsokhkhongcoanh.toLocaleString('en'));
                    $("#loading").hide();
                });
            } else {
                if (matram == null || matram == '0') {
                    $("#dataTable").dataTable().fnDestroy();
                    $("#tbody").empty();
                    $('#colwillchangeh').html('Trạm');
                    $("#loading").show();
                    $.get(`${url}/api/HATHE_getBCKhachhang/${madienluc}/${GV_CAPDIENLUC}`, function (data) {
                        dulieubaocao = data;
                        $('#tongcong_colwillchangef').html('Tổng cộng:');
                        var tongsokh = 0;
                        var tongsokhkhongcosocot = 0;
                        var tongsokhkhongcotoado = 0;
                        var tongsokhkhongcoanh = 0;
                        $.each(data, function (i, f) {
                            tongsokh = tongsokh + f.SLUONG_KHANG;
                            tongsokhkhongcosocot = tongsokhkhongcosocot + f.SLUONG_KHANGKHONGCOSOCOT;
                            tongsokhkhongcotoado = tongsokhkhongcotoado + f.SLUONG_KHANGKHONGCOTOADOCOT;
                            tongsokhkhongcoanh = tongsokhkhongcoanh + f.SLUONG_KHANGKHONGCOANH;
                            $("#tbody").append(
                                `<tr>
                                <td style='text-align:center'>${f.STT}</td>
                                <td>${f.TEN_DONVILOC}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANG.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOSOCOT.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOTOADOCOT.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOANH.toLocaleString('en')}</td>
                            </tr>`
                            );
                        });
                        $('#dataTable').DataTable({
                            // "scrollY": "200px",
                            // "scrollCollapse": true,
                            // "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                            "columns": [
                                { "width": "5%" },
                                { "width": "35%" },
                                { "width": "15%" },
                                { "width": "15%" },
                                { "width": "15%" },
                                { "width": "15%" }
                            ]
                        });
                        //
                        $('#tongsocot_colwillchangef').html(tongsokh.toLocaleString('en'));
                        $('#tongsoanhcot_colwillchangef').html(tongsokhkhongcosocot.toLocaleString('en'));
                        $('#tongsokhachhang_colwillchangef').html(tongsokhkhongcotoado.toLocaleString('en'));
                        $('#tongsoanhkhachhang_colwillchangef').html(tongsokhkhongcoanh.toLocaleString('en'));
                        $("#loading").hide();
                    });
                } else {
                    $("#dataTable").dataTable().fnDestroy();
                    $("#tbody").empty();
                    $('#colwillchangeh').html('Trạm');
                    $("#loading").show();
                    $.get(`${url}/api/HATHE_getBCKhachhang/${matram}/${GV_CAPTRAM}`, function (data) {
                        dulieubaocao = data;
                        $('#tongcong_colwillchangef').html('Tổng cộng:');
                        var tongsokh = 0;
                        var tongsokhkhongcosocot = 0;
                        var tongsokhkhongcotoado = 0;
                        var tongsokhkhongcoanh = 0;
                        $.each(data, function (i, f) {
                            tongsokh = tongsokh + f.SLUONG_KHANG;
                            tongsokhkhongcosocot = tongsokhkhongcosocot + f.SLUONG_KHANGKHONGCOSOCOT;
                            tongsokhkhongcotoado = tongsokhkhongcotoado + f.SLUONG_KHANGKHONGCOTOADOCOT;
                            tongsokhkhongcoanh = tongsokhkhongcoanh + f.SLUONG_KHANGKHONGCOANH;
                            $("#tbody").append(
                                `<tr>
                                <td style='text-align:center'>${f.STT}</td>
                                <td>${f.TEN_DONVILOC}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANG.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOSOCOT.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOTOADOCOT.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_KHANGKHONGCOANH.toLocaleString('en')}</td>
                            </tr>`
                            );
                        });
                        $('#dataTable').DataTable({
                            "columns": [
                                { "width": "5%" },
                                { "width": "35%" },
                                { "width": "15%" },
                                { "width": "15%" },
                                { "width": "15%" },
                                { "width": "15%" }
                            ]
                        });
                        //
                        $('#tongsocot_colwillchangef').html(tongsokh.toLocaleString('en'));
                        $('#tongsoanhcot_colwillchangef').html(tongsokhkhongcosocot.toLocaleString('en'));
                        $('#tongsokhachhang_colwillchangef').html(tongsokhkhongcotoado.toLocaleString('en'));
                        $('#tongsoanhkhachhang_colwillchangef').html(tongsokhkhongcoanh.toLocaleString('en'));
                        $("#loading").hide();
                    });
                }
            }
        }
    }

}
