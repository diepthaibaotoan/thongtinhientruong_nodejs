
function mapToImg(selector, filename="exportMap") {
    // console.log(document.querySelector(selector));
    // selector = `
    // <img draggable="false" alt="" role="presentation" src="https://maps.googleapis.com/maps/vt?pb=!1m5!1m4!1i17!2i104293!3i59170!4i256!2m3!1e0!2sm!3i453160658!3m9!2svi-VN!3sUS!5e18!12m1!1e68!12m3!1e37!2m1!1ssmartmaps!4e0!5m1!5f2&amp;key=AIzaSyA8g2zXBweswLuUXKWWVQzJpYhVSEY9PXc&amp;token=129560" style="width: 256px; height: 256px; user-select: none; border: 0px; padding: 0px; margin: 0px; max-width: none;"/>
    // `;
    // d = document.createElement('div'); 
    // d.innerHTML = selector;
    // selector = d;
    // console.log(selector);
    // let html = document.querySelector(selector);

    // let url = 'data:image/svg+xml,' + encodeURIComponent(`
    // <svg xmlns="http://www.w3.org/2000/svg" width="100" height="100">
    // <foreignObject width="100%" height="100%">
    // <div xmlns="http://www.w3.org/1999/xhtml">${html.outerHTML}</div>
    // </foreignObject>
    // </svg>`);
    // console.log(url);
    
    // filename = filename + '.png';
    // if(navigator.msSaveOrOpenBlob ){
    //     var blob = new Blob(['\ufeff', html], {
    //         type: 'image/png'
    //     });
    //     navigator.msSaveOrOpenBlob(blob, filename);
    // }else{
    //     // Create download link element
    //     var downloadLink = document.createElement("a");
    //     document.body.appendChild(downloadLink);
    //     // Create a link to the file
    //     downloadLink.href = url;
        
    //     // Setting the file name
    //     downloadLink.download = filename;
        
    //     //triggering the function
    //     //downloadLink.click();
    // }
    
    // document.body.removeChild(downloadLink);

    html2canvas(document.querySelector(selector), {useCORS: true, allowTaint: false}).then(canvas => {
        console.log(canvas);
        
        //document.body.appendChild(canvas)
        var downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);
        // Create a link to the file
        downloadLink.href = canvas.toDataURL();

        filename = filename ? filename + '.png' : 'image.png';
        // Setting the file name
        downloadLink.download = filename;

        //triggering the function
        downloadLink.click();
        document.body.removeChild(downloadLink);
    });
    // var is_safari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);
    // if(is_safari) {// Fix for Chrome
    //     var transform=$(".gm-style>div:first>div").css("transform");
    //     var comp=transform.split(","); //split up the transform matrix
    //     var mapleft=parseFloat(comp[4]); //get left value
    //     var maptop=parseFloat(comp[5]);  //get top value
    //     $(".gm-style>div:first>div").css({ //get the map container. not sure if stable
    //       "transform":"none",
    //       "left":mapleft,
    //       "top":maptop,
    //     });
    // }

    // html2canvas(selector).then(canvas => {

    //     filename = filename + ".png";
    //     if (/\bMSIE|Trident\b/.test(navigator.userAgent) && $.browser.version.match(/9.0|10.0|11.0/)) {//Only for IE 9, 10 and 11
    //         download_image_IE(canvas, filename);
    //     }
    //     else {
    //         $("body").append("<a id='downloadimg' download='" + filename + "' href='" + canvas.toDataURL("image/png").replace("image/png", "image/octet-stream") + "'><a/>");
    //     }
    //     if ($("#downloadimg").length > 0){
    //         $("#downloadimg")[0].click();
    //         $("#downloadimg")[0].remove();
    //     }

    //     if(is_safari) {// Fix for Chrome
    //         $(".gm-style>div:first>div").css({
    //         left:0,
    //         top:0,
    //         "transform":transform
    //         });
    //     }
    // });
}

// function download_image_IE(canvas, filename) {
//     if ($.browser.version.match(/9.0/)){ //Only for IE9
//         var w = window.open();
//         $(w.document.body).html('<img src="'+ canvas.toDataURL() +'"/>');
//     }
//     else{
//         var image = canvas.toDataURL();
//         image = image.substring(22); // remove data stuff
//         var byteString = atob(image);
//         var buffer = new ArrayBuffer(byteString.length);
//         var intArray = new Uint8Array(buffer);
//         for (var i = 0; i < byteString.length; i++) {
//             intArray[i] = byteString.charCodeAt(i);
//         }
//         var blob = new Blob([buffer], { type: "image/png" });
//         window.navigator.msSaveOrOpenBlob(blob, filename);
//     }
// }