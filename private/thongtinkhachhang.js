//var url = "http://localhost:3004";
var url = "http://ttht.cpc.vn:3004";
// AAAAA
var jsonDonvi;
var macongty, madienluc, matram;
var jsonKhachhang;


//
$("#tieude").text("CMIS");
$("#isBatsocot").hide();
$("#batsocot").hide();
$("#dongboCMIS").show();

function getitem(makh) {
    var khachhangObj = jsonKhachhang.find(function (obj) { return obj.MA_KHANG === makh });
    // I
    $("#left_makhang").text(khachhangObj.MA_KHANG); //1
    $("#left_tenkhang").text(khachhangObj.TEN_KHANG); //2
    $("#left_diachi").text(khachhangObj.DCHI_KHANG); //3
    $("#left_sdt").text(khachhangObj.DIEN_THOAI); //4
    $("#left_email").text(khachhangObj.EMAIL); //5
    // II
    $("#left_maddo").text(khachhangObj.MA_DDO); //6
    $("#left_dchisdd").text(khachhangObj.DCHI_SDD); //7
    if (khachhangObj.X_SDD == '' || khachhangObj.X_SDD == null) {
        //
    } else {
        $("#left_toadosdd").text(khachhangObj.X_SDD + ", " + khachhangObj.Y_SDD + ", " + khachhangObj.Z_SDD); //8
    }
    $("#left_sopha").text(khachhangObj.SO_PHA); //9
    $("#left_pha").text(khachhangObj.PHA); //10
    $("#left_soho").text(khachhangObj.SO_HO); //11
    $("#left_sogcs").text(khachhangObj.SO_GCS); //12
    $("#left_stt").text(khachhangObj.STT); //13
    $("#left_matram").text(khachhangObj.MA_TRAM); //14
    $("#left_malo").text(khachhangObj.MA_LO); //15
    $("#left_socot").text(khachhangObj.SO_COT); //16
    if (khachhangObj.X_COT == '' || khachhangObj.X_COT == null) {
        //
    } else {
        $("#left_toadocot").text(khachhangObj.X_COT + ", " + khachhangObj.Y_COT + ", " + khachhangObj.Z_COT); //8
    }
}

function init(madonvi, matram) {

    // 1
    isHiensocot = false;
    $("#isBatsocot").prop("checked", false);
    $("#ul_khachhang").empty();
    $("#ul_hinhanh").empty();

    //
    $("#loading").show();
    $("#ul_khachhang").empty();
    console.log("URL: " + `${url}/api/get_DS_Thongtinkhachhang_byTram/${madonvi}/${matram}`);
    $.get(`${url}/api/get_DS_Thongtinkhachhang_byTram/${madonvi}/${matram}`, function (data) {
        $("#tieudeI").text("I. Thông Tin Khách Hàng: " + data.length + " KH");
        jsonKhachhang = data;
        $.each(data, function (i, f) {
            $("#ul_khachhang").append(
                `<li onclick="getitem('${f.MA_KHANG}')">
                    <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_KHANG}</b></a><br>
                    <a>${f.TEN_KHANG}</a><br>
                    <a>${f.DCHI_KHANG}</a><br>
                </li>
                <div style="background-color:rgb(50,190,166);height:1px"></div>`
            );
        });
        $("#loading").hide();
    });

}

/* BAT DAU AAAAA:  PHAN HEADER */
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    console.log("URL: " + `${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

//
var cb_hiensocot = document.querySelector("input[name=cb_hiensocot]");
cb_hiensocot.addEventListener('change', function () {
    if (this.checked) {
        isHiensocot = true;
        bochuyendoiZoom(map.getZoom());
        for (var i = 0; i < marker_array.length; i++) {
            marker_array[i].setIcon({
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: marker_array[i].getIcon().fillColor,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                socot: marker_array[i].getIcon().socot
            });

            //
            if (marker_array[i].lkcot2 == THUOCTINH_TRAM) {
                marker_array[i].label = { color: '#FF0000', fontSize: cochu + 'px', text: marker_array[i].matram };
                marker_array[i].icon.labelOrigin = new google.maps.Point(0, -2);
            } else {
                marker_array[i].label = { color: '#000000', fontSize: cochu + 'px', text: marker_array[i].socot };
                marker_array[i].icon.labelOrigin = new google.maps.Point(0, -2);
            }
        }
    } else {
        isHiensocot = false;
        bochuyendoiZoom(map.getZoom());
        for (var i = 0; i < marker_array.length; i++) {
            marker_array[i].setIcon({
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: marker_array[i].getIcon().fillColor,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                socot: marker_array[i].getIcon().socot
            });

            //
            if (marker_array[i].lkcot2 == THUOCTINH_TRAM) {
                marker_array[i].label = { color: '#FF0000', fontSize: cochu + 'px', text: marker_array[i].matram };
                marker_array[i].icon.labelOrigin = new google.maps.Point(0, -2);
            } else {
                marker_array[i].label = { color: '#FF00FF', fontSize: '0px', text: marker_array[i].socot };
                marker_array[i].icon.labelOrigin = new google.maps.Point(0, -2);
            }
        }
    }
});

// Click nut xem bao cao
function clickXembaocao() {
    if (macongty == null || macongty == '0') {
        alert("Mời bạn chọn Công ty")
    } else {
        if (madienluc == null || madienluc == '0') {
            alert("Mời bạn chọn Điện lực.");
        } else {
            if (matram == null || matram == '0') {
                alert("Mời bạn chọn Trạm.");
            } else {
                init(madienluc, matram);
            }
        }
    }
}

// w3_open
document.getElementById("main").style.marginLeft = "17.7%";
document.getElementById("mySidebar").style.width = "17.7%";
document.getElementById("mySidebar").style.display = "block";
//document.getElementById("map").style.width = '85%';

function w3_close() {
    document.getElementById("main").style.marginLeft = "0%";
    document.getElementById("mySidebar").style.display = "none";
    //document.getElementById("openNav").style.display = "inline-block";
    //document.getElementById("map").style.width = '100%';
}
/* KET THUC AAAAA:  PHAN HEADER */

function clickDongboCMIS() {
    $("#loading").show();
    if (macongty == null || macongty == '0') {
        alert("Mời bạn chọn Công ty")
    } else {
        if (madienluc == null || madienluc == '0') {
            alert("Mời bạn chọn Điện lực.");
        } else {
            if (matram == null || matram == '0') {
                alert("Mời bạn chọn Trạm.");
            } else {
                console.log("URL: " + `${url}/api/mtb_delete_khachhang_by_matram/${madienluc}/${matram}/dungtoan`);
                $.get(`${url}/api/mtb_delete_khachhang_by_matram/${madienluc}/${matram}/dungtoan`, function (data) {
                    if (data == 'OK') {
                        //
                        $.getJSON(`${url}/api/mtb_get_khachhang_by_ma_tram_full/${madienluc}/${matram}/dungtoan`, function (data) {
                            //console.log("DULIEUKHACHHANG: "+data);
                            data.forEach(function (khangObj) {
                                $.post(`${url}/api/update_DS_Thongtinkhachhang`, {
                                    MA_DVIQLY: convert_khongchonull(khangObj.MA_DVIQLY),
                                    MA_TRAM: convert_khongchonull(khangObj.MA_TRAM),
                                    MA_LO: convert_khongchonull(khangObj.MA_LO),
                                    MA_SOGCS: convert_khongchonull(khangObj.MA_SOGCS),
                                    STT: convert_khongchonull(khangObj.STT),
                                    MA_KHANG: convert_khongchonull(khangObj.MA_KHANG),
                                    MA_DDO: convert_khongchonull(khangObj.MA_DDO),
                                    TEN_KHANG: convert_khongchonull(khangObj.TEN_KHANG),
                                    DCHI_KHANG: convert_khongchonull(khangObj.DCHI_KHANG),
                                    DIEN_THOAI: convert_khongchonull(khangObj.DIEN_THOAI),
                                    EMAIL: convert_khongchonull(khangObj.EMAIL),
                                    DCHI_SDD: convert_khongchonull(khangObj.DCHI_SDD),
                                    X_SDD: convert_khongchonull(khangObj.X_SDD),
                                    Y_SDD: convert_khongchonull(khangObj.Y_SDD),
                                    Z_SDD: convert_khongchonull(khangObj.Z_SDD),
                                    SO_PHA: convert_khongchonull(khangObj.SO_PHA),
                                    PHA: convert_khongchonull(khangObj.PHA),
                                    SO_HO: convert_khongchonull(khangObj.SO_HO),
                                    SO_COT: convert_khongchonull(khangObj.SO_COT),
                                    SO_THUNG: convert_khongchonull(khangObj.SO_THUNG),
                                    VTRI_THUNG: convert_khongchonull(khangObj.VTRI_THUNG),
                                    LOAI_THUNG: convert_khongchonull(khangObj.LOAI_THUNG),
                                    VTRI_CTO: convert_khongchonull(khangObj.VTRI_CTO),
                                    VTRI_TREO: convert_khongchonull(khangObj.VTRI_TREO),
                                    GIA_DIEN: convert_khongchonull(khangObj.GIA_DIEN),
                                    MA_CLOAI_CTO: convert_khongchonull(khangObj.MA_CLOAI_CTO),
                                    NAM_SXUAT_CTO: convert_khongchonull(khangObj.NAM_SXUAT_CTO),
                                    SO_CTO: convert_khongchonull(khangObj.SO_CTO),
                                    NGAY_TREO_CTO: convert_khongchonull(khangObj.NGAY_TREO_CTO),
                                    NGAY_KDINH_CTO: convert_khongchonull(khangObj.NGAY_KDINH_CTO),
                                    HS_NHAN: convert_khongchonull(khangObj.HS_NHAN),
                                    MA_CLOAI_TI1: convert_khongchonull(khangObj.MA_CLOAI_TI1),
                                    NAM_SXUAT_TI1: convert_khongchonull(khangObj.NAM_SXUAT_TI1),
                                    SO_TI1: convert_khongchonull(khangObj.SO_TI1),
                                    NGAY_TREO_TI1: convert_khongchonull(khangObj.NGAY_TREO_TI1),
                                    NGAY_KDINH_TI1: convert_khongchonull(khangObj.NGAY_KDINH_TI1),
                                    MA_CLOAI_TI2: convert_khongchonull(khangObj.MA_CLOAI_TI2),
                                    NAM_SXUAT_TI2: convert_khongchonull(khangObj.NAM_SXUAT_TI2),
                                    SO_TI2: convert_khongchonull(khangObj.SO_TI2),
                                    NGAY_TREO_TI2: convert_khongchonull(khangObj.NGAY_TREO_TI2),
                                    NGAY_KDINH_TI2: convert_khongchonull(khangObj.NGAY_KDINH_TI2),
                                    MA_CLOAI_TI3: convert_khongchonull(khangObj.MA_CLOAI_TI3),
                                    NAM_SXUAT_TI3: convert_khongchonull(khangObj.NAM_SXUAT_TI3),
                                    SO_TI3: convert_khongchonull(khangObj.SO_TI3),
                                    NGAY_TREO_TI3: convert_khongchonull(khangObj.NGAY_TREO_TI3),
                                    NGAY_KDINH_TI3: convert_khongchonull(khangObj.NGAY_KDINH_TI3),
                                    GHI_CHU: convert_khongchonull(khangObj.GHI_CHU),
                                    NGAY_KHOITAO: convert_khongchonull(khangObj.NGAY_KHOITAO),
                                    NGUOI_NHAP: khangObj.NGUOI_NHAP,
                                    NGAY_NHAP: convert_khongchonull(khangObj.NGAY_NHAP),
                                    KEY: 'toandtb'
                                }, function (result) {
                                    console.log("hhh" + result);
                                })
                            })
                            $("#loading").hide();
                        })
                    } else {
                        //
                    }
                });
            }
        }
    }
}

function convert_namthangngay(thoigian) {
    if (thoigian == null || thoigian == '') {
        return 1900 / 01 / 01
    } else {
        var chuoi = thoigian.split("T");
        var chuoitach = chuoi[0].split("-");
        return chuoitach[0], chuoitach[1], chuoitach[2];
    }
}

function convert_khongchonull(chuoiguivao) {
    if (chuoiguivao == null) {
        return encodeURI('');
    } else {
        return encodeURI(chuoiguivao);
    }
}