var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var macongty, madienluc;
var nhanvien;
var jsonNhanVien;
var madonvicuanguoidung = '';
var m_taikhoan = localStorage['taikhoan'];

$('#loading').hide();
$('#table_GD').hide();
$('#table_NV').hide();
$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

    // Kiem tra user
    $.get(`${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`, function (data) {
        $.each(data, function (i, f) {
            madonvicuanguoidung = f.MA_DVIQLY;
            if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
                $("#congty").select2().val(madonvicuanguoidung);
                $("#congty").trigger('change');
                $("#congty").prop('disabled', true);
            } else if (madonvicuanguoidung.length == 6) {
                if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 2));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                } else {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 4));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                }
            }
        });
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);

    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$('#dienluc').on('change', function () {
    madienluc = this.value;
});

function taoDuLieuChoBang(data) {
    $("#dataTable").dataTable().fnDestroy();
    $("#tbody").empty();
    var index = 1;
    $.each(data, function (i, f) {
        $("#tbody").append(
            `<tr>
                    <td style='text-align:center'>${index++}</td>
                    <td style='text-align:center'>${f.MA_DVIQLY}</td>
                    <td style='text-align:left'>${f.MA_NVIEN}</td>
                    <td style='text-align:left'>${f.TEN_NVIEN}</td>
                    <td style='text-align:left'>${f.CHUC_VU}</td>
                    <td style='text-align:center'>
                         <input class="col-lg-10"  style="display: table;margin: 0 auto" type="text" id="Input_TaikhoanAD_${i}" placeholder="Nhập Tài Khoản AD">
                    </td>
                    <td style='text-align:center'>
                        <button  class='button_xem' style="display: table;margin: 0 auto" onclick="getItem_NhanVien(${i},'${f.MA_NVIEN}')">Xác nhận</button>
                    </td>
                </tr>`
        );
        if (f.TEN_DNHAP != undefined && f.TEN_DNHAP != null && f.TEN_DNHAP.trim() != "") {
            $(`#Input_TaikhoanAD_${i}`).val(f.TEN_DNHAP);
        }

    });
    $('#dataTable').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "30%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "10%" }
        ]
    });
}

function clickGetData_GiamDocDienLuc() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#TTCT_tieude").text("");
    $("#TTCT_tieude").text("Thông Tin Giám Đốc");
    $("#table_NV").hide();
    $("#table_GD").show();
    var manhanvien = madienluc + '_GD';
    $("#tenuser").text(manhanvien);

    $("#loading").show();
    $.get(`${url}/api/TTCT_getThongtinGiamDoc_ByMaDienLuc/${madienluc}`, function (data) {
        if (data.length > 0) {
            $.each(data, function (i, f) {
                $("#TTCTdialog_Tengiamdoc").val(f.TEN_NVIEN);
                $("#TTCTdialog_TaikhoanAD").val(f.TEN_DNHAP);
            });
        }
        else {
            $("#TTCTdialog_Tengiamdoc").val('');
            $("#TTCTdialog_TaikhoanAD").val('');
        }
        $("#loading").hide();
    });

}

function sendData_GiamDocDienLuc() {
    if ($("#TTCTdialog_Tengiamdoc").val().trim() == '') {
        alert("Mời bạn nhập tên giám đốc");
        return;
    }

    if ($("#TTCTdialog_TaikhoanAD").val().trim() == '') {
        alert("Mời bạn nhập vào tài khoản AD");
        return;
    }

    var manhanvien = madienluc + '_GD';
    var tengiamdoc = $("#TTCTdialog_Tengiamdoc").val().trim();
    var taikhoan = $("#TTCTdialog_TaikhoanAD").val().trim();
    tengiamdoc = tengiamdoc.replace(/ /g, '%20');

    $("#loading").show();
    $.get(`${url}/api/TTCT_updateThongtinGiamDoc_ByMaDienLuc/${madienluc}/${manhanvien}/${tengiamdoc}/${taikhoan}`, function (data) {
        $("#loading").hide();
        GF_SHOWTOAST_LONG("Gửi dữ liệu thành công.", "250px");
    });
}

function clickGetData_NhanVienDienLuc() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $("#TTCT_tieude").text("");
    $("#TTCT_tieude").text("Thông Tin Nhân Viên");
    $.get(`${url}/api/TTCT_getThongtinNhanVien_ByMaDienLuc/${madienluc}`, function (data) {
        $("#table_GD").hide();
        taoDuLieuChoBang(data);
        $("#table_NV").show();
        $("#loading").hide();
    });
}

function getItem_NhanVien(index, data) {
    var taikhoan_Ad = $(`#Input_TaikhoanAD_${index}`).val();
    $("#loading").show();
    if (taikhoan_Ad != '') {
        $("#loading").show();
        $.get(`${url}/api/TTCT_updateThongtinNhanVien_ByMaDienLuc/${madienluc}/${GF_checkContentData(data)}/${taikhoan_Ad}`, function (data) {
            $("#loading").hide();
            GF_SHOWTOAST_LONG("Gửi thông tin thành công.", "250px");
        });
    }
    else {
        alert("Vui lòng nhập vào tài khoản AD");
    }
}

function clickDongboCMIS() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#treothao_dialog").show();
}

function clickLayCMIS_DMTram() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMTram_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Trạm thành công.");
    });

}

function clickLayCMIS_DMVitritreo() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMVtritreo_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Vị trí treo thành công.");
    });

}

function clickLayCMIS_DMChungloaicongto() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMChungloacto_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Chủng loại công tơ thành công.");
    });
}

function clickLayCMIS_DMSogcs() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMSogcs_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Sổ ghi chỉ số thành công.");
    });
}

function clickLayCMIS_DMLydotreothao() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMLydotreothao?KEY=${GV_KEY_TOANDTB}`, function (data) {

        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Lý do treo tháo thành công.");
    });
}

function clickLayCMIS_DMLoaiDdo() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMLoaiddo?KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
         alert("Bạn đồng bộ danh mục Loại điểm đo thành công.");
    });
}

function clickLayCMIS_DMNVien() {
    if (madienluc == undefined || madienluc == '' || madienluc == '0') {
        alert("Bạn chưa chọn Điện lực");
        return;
    }

    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMNhanvien?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Nhân viên thành công.");
    });
}


