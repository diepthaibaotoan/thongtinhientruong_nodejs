var url = GV_URL;
var h = $("#mySidebar").height();
$("#ul_khachhang").height(h - 188);
$("#hinhanh").height(h - 106);

// AAAAA
var jsonDonvi, jsonKhachhangbandau, jsonKhachhang, jsonHinnhanh_cotbandau, jsonHinnhanh_cot, jsonHinnhanh_khang;
var macongty, madienluc, matram;
var CAPTONG = 1, CAPCONGTY = 2, CAPDIENLUC = 3, CAPTRAM = 4;
var madonvicuanguoidung = '';
var vitrimarker_click;
var stt_marker;

// BBBBBB
var zoom = 17;
var cochu = 10;
var cochu_chotram = 10;
var rong = 24;
var cao = 34;
var m_rong = 24;
var m_cao = 34;
var chucach = -6;
var isHiensocot;
var isBientap;
var isBientapExcel;
var tron_scale, tron_stroke;
var map;
var marker;
var markerss = [];
var list_zoomImage_Cot = [];
var index_zoomImage_Cot_current = 0;
var list_zoomImage_Khang = [];
var index_zoomImage_Khang_current = 0;

//
var hashMapMarker_truoc = new Map();
var hashMapMarker = new Map();
var arrayMarker = [];
var arrayPoly = [];
var hashMapPoly = new Map();
var cotgoc;
var listCothathe;
var hinh_giotnuoc = 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0';
//
var bounds;
var northEast;
var southWest;

var mylat = 16.055994;
var mylng = 108.186822;

// Luc moi vo
$("#tieude").text("Mất An Toàn");
$("#dongboCMIS").hide();
$("#text_ngaylaycuoi").hide();
$("#dongboEMEC").hide();
// 

$("#btn_xuatexcel").hide();
$("#socot").hide();
$("#isSocot").hide();
$("#isBientap").hide();
$("#bientap").hide();
$("#isBientapExcel").hide();
$("#bientapexcel").hide();
$("#isBandonhiet").prop("checked", true);


$("#tram").hide();
w3_close();



// console.log("URL: " + `${url}/api/BTHT_getVitringuyhiem/PC05AA`);
// //Lúc đầu vô khởi tạo HeatMap


//Lúc đầu vô khởi tạo HeatMap
$.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/PC11`, function (data) {
    var X, Y;
    data.forEach(function (cotObj) {
        X = cotObj.X_COT;
        Y = cotObj.Y_COT;
    })
    HeatMapInit('', X, Y, 7, true, true);
});

function bochuyendoiZoom(bochuyendoi_zoom) {
    if (bochuyendoi_zoom == 17) {
        tron_scale = 5;
        tron_stroke = 1;
        cochu = 10;
        cochu_chotram = 10;
    } else if (bochuyendoi_zoom > 17) {
        tron_scale = (bochuyendoi_zoom - 17) * 2 + 5;
        tron_stroke = 1;
        cochu = (bochuyendoi_zoom - 17) * 3 + 10;
        cochu_chotram = (bochuyendoi_zoom - 17) * 3 + 10;
    } else if (bochuyendoi_zoom == 15) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0;
    } else if (bochuyendoi_zoom > 15 && bochuyendoi_zoom < 17) {
        tron_scale = (bochuyendoi_zoom - 17) * 1.5 + 5;
        tron_stroke = 0.5;
        cochu_chotram = 10;
        cochu = 0;
    } else if (bochuyendoi_zoom < 15 && bochuyendoi_zoom > 13) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0
    } else if (bochuyendoi_zoom == 13) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0
    } else if (bochuyendoi_zoom < 13) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0
    }
}

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: listCothathe,
        columns: getColumns(listCothathe)
    });
}

//Ham xuat dia diem khi chon khach hang
function getdiadiem(makh, event) {
    removeMarkers();
    event.stopPropagation();
    var khachhangObj = jsonKhachhang.find(function (obj) { return obj.MA_KHANG === makh });
    if (khachhangObj.X_COT == null || khachhangObj.X_COT == '') {
        alert("Tọa độ điểm chưa có. Xin vui lòng nhập số liệu.");
    } else {
        var center = new google.maps.LatLng(khachhangObj.X_COT, khachhangObj.Y_COT);
        var image = {
            url: 'https://png.icons8.com/color/48/000000/filled-flag.png',

            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(10, 43)
        };
        // using global variable:
        var marker1 = new google.maps.Marker({
            position: center,
            icon: image,
            draggable: true,
            map: map
        });
        markerss.push(marker1);
        map.panTo(center);

    }
}
//Ham xoa marker
function removeMarkers() {
    for (i = 0; i < markerss.length; i++) {
        markerss[i].setMap(null);
    }
}
function getitem(makh) {
    $(".search-wrapper").hide();
    document.getElementById('id01').style.display = 'block';
    var khachhangObj = jsonKhachhang.find(function (obj) { return obj.MA_KHANG === makh });

    // I
    $("#dialog_makhang").val(khachhangObj.MA_KHANG); //1
    $("#dialog_tenkhang").val(khachhangObj.TEN_KHANG); //2
    $("#dialog_diachikhang").val(khachhangObj.DCHI_KHANG); //3
    $("#dialog_dienthoai").val(khachhangObj.DIEN_THOAI); //4
    $("#dialog_email").val(khachhangObj.EMAIL); //5
    $("#dialog_ghichu").text(khachhangObj.GHI_CHU); //6
    $("#dialog_nguoigui").val(khachhangObj.NGUOI_NHAP); //7
    $("#dialog_ngaygui").val(khachhangObj.NGAY_NHAP); //8
    var m_taikhoan = localStorage['taikhoan'];
    $("#dialog_nguoilay").val(m_taikhoan); //9
    $("#dialog_ngaylay").val(khachhangObj.NGAY_KHOITAO); //10

    // II
    $("#dialog_maddo").val(khachhangObj.MA_DDO); //11
    $("#dialog_diachisdd").val(khachhangObj.DCHI_SDD); //12
    if (khachhangObj.X_SDD == '' || khachhangObj.X_SDD == null) {
        //
    } else {
        $("#dialog_toadosdd").val(khachhangObj.X_SDD + ", " + khachhangObj.Y_SDD); //13
    }
    $("#dialog_sopha").val(khachhangObj.SO_PHA); //14
    $("#dialog_pha").val(khachhangObj.PHA); //15
    $("#dialog_soho").val(khachhangObj.SO_HO); //16
    $("#dialog_sogcs").val(khachhangObj.MA_SOGCS); //17
    $("#dialog_stt").val(khachhangObj.STT); //18
    $("#dialog_tram").val(khachhangObj.MA_TRAM); //19
    $("#dialog_malo").val(khachhangObj.MA_LO); //20
    $("#dialog_socot").val(khachhangObj.SO_COT); //21
    if (khachhangObj.X_COT == '' || khachhangObj.X_COT == null) {
        //
    } else {
        $("#dialog_toadocot").val(khachhangObj.X_COT + ", " + khachhangObj.Y_COT); //22
    }
    $("#dialog_sothung").val(khachhangObj.SO_THUNG); //23
    $("#dialog_vtrithung").val(khachhangObj.VTRI_THUNG); //24
    $("#dialog_loaithung").val(khachhangObj.LOAI_THUNG); //25
    $("#dialog_vtricto").val(khachhangObj.VTRI_CTO); //26
    $("#dialog_vtritreo").val(khachhangObj.VTRI_TREO); //27
    $("#dialog_giadien").val(khachhangObj.GIA_DIEN); //28

    // III
    $("#dialog_macongto").val(khachhangObj.MA_CLOAI_CTO + khachhangObj.NAM_SXUAT_CTO + khachhangObj.SO_CTO); //29
    $("#dialog_chungloai").val(khachhangObj.MA_CLOAI_CTO); //30
    $("#dialog_namsanxuat").val(khachhangObj.NAM_SXUAT_CTO); //31
    $("#dialog_socongto").val(khachhangObj.SO_CTO); //32
    $("#dialog_ngaytreo").val(khachhangObj.NGAY_TREO_CTO); //33
    $("#dialog_ngaykiemdinh").val(khachhangObj.NGAY_KDINH_CTO); //34
    $("#dialog_hesonhan").val(khachhangObj.GIA_DIEN); //35

    $("#dialog_mati1").val(khachhangObj.MA_CLOAI_TI1 + khachhangObj.NAM_SXUAT_TI1 + khachhangObj.SO_TI1); //36
    $("#dialog_chungloaiti1").val(khachhangObj.MA_CLOAI_TI1); //37
    $("#dialog_namsanxuatti1").val(khachhangObj.NAM_SXUAT_TI1); //38
    $("#dialog_soti1").val(khachhangObj.SO_TI1); //39
    $("#dialog_ngaytreoti1").val(khachhangObj.NGAY_KDINH_TI1); //40
    $("#dialog_ngaykiemdinhti1").val(khachhangObj.NGAY_KDINH_TI1); //41

    $("#dialog_mati2").val(khachhangObj.MA_CLOAI_TI2 + khachhangObj.NAM_SXUAT_TI2 + khachhangObj.SO_TI2); //36
    $("#dialog_chungloaiti2").val(khachhangObj.MA_CLOAI_TI2); //37
    $("#dialog_namsanxuatti2").val(khachhangObj.NAM_SXUAT_TI2); //38
    $("#dialog_soti2").val(khachhangObj.SO_TI2); //39
    $("#dialog_ngaytreoti2").val(khachhangObj.NGAY_KDINH_TI2); //40
    $("#dialog_ngaykiemdinhti2").val(khachhangObj.NGAY_KDINH_TI2); //41

    $("#dialog_mati3").val(khachhangObj.MA_CLOAI_TI3 + khachhangObj.NAM_SXUAT_TI3 + khachhangObj.SO_TI3); //36
    $("#dialog_chungloaiti3").val(khachhangObj.MA_CLOAI_TI3); //37
    $("#dialog_namsanxuatti3").val(khachhangObj.NAM_SXUAT_TI3); //38
    $("#dialog_soti3").val(khachhangObj.SO_TI3); //39
    $("#dialog_ngaytreoti3").val(khachhangObj.NGAY_KDINH_TI3); //40
    $("#dialog_ngaykiemdinhti3").val(khachhangObj.NGAY_KDINH_TI3); //41

    // Phan ho so dien tu
    $("#ul_hosodientu").empty();
    $('#obj_hosodientu').attr("data", null);
    $('#obj_hosodientu').show();
    //console.log("URL: " +`${url}/api/get_DS_Hosodientu_byMakhang/${khachhangObj.MA_DVIQLY}/${khachhangObj.MA_TRAM}/${khachhangObj.MA_KHANG}`);
    $.get(`${url}/api/get_DS_Hosodientu_byMakhang/${khachhangObj.MA_DVIQLY}/${khachhangObj.MA_TRAM}/${khachhangObj.MA_KHANG}`, function (data) {
        $("#tieudesohoa").text(khachhangObj.MA_KHANG + ": " + data.length + " hồ sơ");

        $.each(data, function (i, f) {
            $("#ul_hosodientu").append(
                `<li class="li_hosodientu" onclick="getHosochitiet(this, '${f.MA_KHANG}', '${f.TEN_HINH}')">
                    <a>${f.TEN_HINH}</a>
                </li>
                <div style="background-color:rgb(50,190,166);height:1px"></div>`
            );

            //
        });
    });

    // Phan anh cua khach hang
    $("#anhkhachhang").empty();
    //console.log("URL: " + `${url}/api/HATHE_getDSHinhanh_byKhang/${marker.icon.madviqly}/%20/HA_THE/${marker.icon.matram}/${marker.icon.socot.split("/").join("%2f")}`);
    $.get(`${url}/api/HATHE_getDSHinhanh_byKhang/${khachhangObj.MA_DVIQLY}/${khachhangObj.MA_TRAM}/${khachhangObj.MA_KHANG}`, function (data) {
        jsonHinnhanh_khang = data;
        $.each(jsonHinnhanh_khang, function (i, f) {
            $("#anhkhachhang").append(
                `<div class="gallery_khachhang" onclick="getHinhanhchitiet(${i}, ${1})">
                 <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                </div>`
            );
        });
    });
}
//Ham co gian nut search
function searchToggle(obj, evt) {

    var container = $(obj).closest('.search-wrapper');

    if (!container.hasClass('active')) {
        container.addClass('active');
        evt.preventDefault();
    }
    else if (container.hasClass('active') && $(obj).closest('.input-holder').length == 0) {
        container.removeClass('active');
        // clear input
        container.find('.search-input').val('');
        // clear and hide result container when we press close
        container.find('.result-container').fadeOut(100, function () { $(this).empty(); });
    }
}

//Ham Autocomlete cho google map version1
function initAutocomplete() {
    initMap();
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function (place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            var icon = {
                url: 'https://png.icons8.com/ultraviolet/80/000000/marker.png',
                size: new google.maps.Size(80, 80),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(40, 40)
            };

            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));

            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }


        });
        map.fitBounds(bounds);
    });
    //google.maps.event.addDomListener(window, 'load', initAutocomplete);
}
//google.maps.event.addDomListener(window, 'load', initAutocomplete);
// thuanbq
function HeatMapInit(dieukien, X, Y, zoom, heatmap, satellite) {

    //
    if (satellite == true) {
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(X, Y),
            zoom: zoom,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });
    } else {
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(X, Y),
            zoom: zoom
        });
    }
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));
    layer = new google.maps.FusionTablesLayer({
        map: map,
        heatmap: { enabled: heatmap },
        query: {
            select: "col3",
            from: "1EpndCe5BcF6VAihd61GkHO_prfn0oUD6yo_br4DM",
            where: dieukien
        },
        options: {
            styleId: 2,
            templateId: 2
        }
    });

    //when the map zoom changes, resize the icon based on the zoom level so the marker covers the same geographic area
    google.maps.event.addListener(map, 'zoom_changed', function () {
        //
    });


}

//Lắng nghe thay đổi chọn lựa bản đồ nhiệt
$("#isBandonhiet").change(function () {
    var bandonhiet = document.getElementById('isBandonhiet');
    var dieukien = '';
    if (bandonhiet.checked == true) {
        //Thể hiện bản đồ nhiệt theo từng cấp
        //Nếu chưa chọn mã công ty --> thể hiện bản đồ nhiệt cho tổng miền trung
        if (macongty != null && macongty != '0') {

            //Điều kiện khi select 
            if (matram == null || matram == '0' && madienluc == null || madienluc == '0') {
                dieukien = "'MA_DVIQLY' like '" + macongty + "%'";
            } else if (matram == null || matram == '0') {
                dieukien = "'MA_DVIQLY' = '" + madienluc + "'";
            }
            else {
                dieukien = "'MA_DVIQLY' = '" + madienluc + "'" + "AND 'MA_TRAM' = '" + matram + "'";
            }

            //Tạo và di chuyển map tới tọa độ trung tâm vị trí 
            if (matram == null || matram == '0' && madienluc == null || madienluc == '0') {
                $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/${macongty}`, function (data) {
                    var X, Y;
                    var i = 0;
                    data.forEach(function (cotObj) {
                        X = cotObj.X_COT;
                        Y = cotObj.Y_COT;
                        i++;
                    })
                    if (i == 0) {
                        alert("Công ty này không có dữ liệu");
                    } else {
                        HeatMapInit(dieukien, X, Y, 10, true, false);
                    }
                });
            } else if (matram == null || matram == '0') {
                //Cấp điện lực: Khong co ban do nhiet
            } else {
                // Cap Tram: khong co ban do nhiet
            }
        } else {
            $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/PC11`, function (data) {
                var X, Y;
                data.forEach(function (cotObj) {
                    X = cotObj.X_COT;
                    Y = cotObj.Y_COT;
                })
                HeatMapInit(dieukien, X, Y, 7, true, false);
            });
        }
    } else {
        //Nếu ở cấp công ty thì cho hiện hột marker
        if (macongty != null && macongty != '0') {
            dieukien = "'MA_DVIQLY' like '" + macongty + "%'";
            $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/${macongty}`, function (data) {
                var X, Y;
                var i = 0;
                data.forEach(function (cotObj) {
                    X = cotObj.X_COT;
                    Y = cotObj.Y_COT;
                    i++;
                })
                if (i == 0) {
                    alert("Công ty này không có dữ liệu");
                } else {
                    HeatMapInit(dieukien, X, Y, 10, false, false);
                }
            });
        } else {
            //Lúc đầu vô khởi tạo bản đồ hột marker cho tổng miền trung
            $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/PC11`, function (data) {
                var X, Y;
                data.forEach(function (cotObj) {
                    X = cotObj.X_COT;
                    Y = cotObj.Y_COT;
                })
                HeatMapInit('', X, Y, 7, false, false);
            });
        }
    }
});

$(".button_timkiemkhachhang").on('click', function () {
    //Get
    var str_thongtinkhachhang = $('.search_thongtinkhachhang_input').val();
    str_thongtinkhachhang = GF_CONVERTTOENGLISH(str_thongtinkhachhang);

    if (str_thongtinkhachhang.length > 4) {
        var maTramKhachHang = '';
        var capdonvi;
        if (matram == null || matram == '0') {
            maTramKhachHang = '%20';
            capdonvi = 1;
        } else {
            maTramKhachHang = matram;
            capdonvi = 2;
        }
        $("#ul_khachhang").empty();
        $.get(`${url}/api/HATHE_getDSThongtinkhachhang_byTimkiem/${madienluc}/${maTramKhachHang}/${capdonvi}/${str_thongtinkhachhang}`, function (data) {
            var i = 0;
            console.log("hihi: " + data.length);
            jsonKhachhangbandau = data;
            jsonKhachhang = jsonKhachhangbandau;
            data.forEach(function (khachhangObj) {
                $("#ul_khachhang").append(
                    `<li onclick="getitem('${khachhangObj.MA_KHANG}')">
                        <a style="font-size:12.3px">
                                <b>${khachhangObj.MA_KHANG}</b>
                                <button aria-label="chi duong" id="searchbox-directions" class="searchbox-directions" data-toggle="tooltip" data-placement="right" title="Chỉ đường" onclick="getdiadiem('${khachhangObj.MA_KHANG}',event)"></button>
                        </a><br>
                        <a>${khachhangObj.TEN_KHANG}</a><br>
                        <a>${khachhangObj.DCHI_KHANG}</a><br>
                        </li>
                        <div style="background-color:rgb(50,190,166);height:1px"></div>`
                );
            })
        });
    } else {
        alert("Thông tin tìm kiếm cần lớn hơn 4 kí tự");

        // $("#ul_khachhang").empty();
    }
});



//
function initMap() {
    var options;
    if (madienluc == null || madienluc == '0') {
        zoom = 14;
    } else {
        if (matram == null || matram == '0') {
            zoom = 16;
            GF_SHOWTOAST_LONG("Bạn nhấn chuột phải vào cột chứa TBA để xem chi tiết một trạm.", 500);
        } else {
            zoom = 17;
        }
    }
    options = {
        zoom: zoom,
        center: { lat: mylat, lng: mylng }
    }

    map = new google.maps.Map(document.getElementById('map'), options);


    bochuyendoiZoom(map.getZoom());
    if (madienluc == null || madienluc == '0') {

    } else {

        if (matram == null || matram == '0') {
            $("#tieudeI").text("I. Số Lượng Vị Trí Bất Thường: 0");
            $("#tieudeII").text("II. Thông Tin Vị Trí Bất Thường: ");
            $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: ");
            //
            $("#I_toadoX").hide();
            $("#I_toadoY").hide();

            $("#isBandonhiet").hide(); //5
            $("#bandonhiet").hide(); //6
            $("#isBandonhiet").prop("checked", false);


            //LAY DS_COT
            $.get(`${url}/api/HATHE_getDS_BTHT_DS_VITRIBATTHUONG_byMadviqly/${madienluc}`, function (data) {

                var index = 0;
                $("#tieudeI").text("I. Số Lượng Vị Trí Bất Thường: "+data.length);

                data.forEach(function (vitriObj) {
                    if(index==0){
                        moveToLocation(vitriObj.X_VITRI,vitriObj.Y_VITRI);
                    }
                    addMarker_BTHT(vitriObj, index);
                    index++;
                })
            });

        }
    }
}

function addMarker_BTHT(vitriObj, i) {

    marker = new google.maps.Marker({
        position: { lat: parseFloat(vitriObj.X_VITRI), lng: parseFloat(vitriObj.Y_VITRI) },
        map: map,
        draggable: false,
        label: { color: 'black', fontSize: '' + cochu + 'px', text: ' ' },
    });

    marker.label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: vitriObj.MA_VITRI };
    hashMapMarker.set(i, marker);
    arrayMarker.push(marker);

    var icon = {
        path: google.maps.SymbolPath.CIRCLE,
        scale: tron_scale,
        fillColor: GV_COLOR_DOVIENDEN,
        fillOpacity: 1,
        strokeWeight: tron_stroke,
        origin: new google.maps.Point(0, 0), // origin
        anchor: new google.maps.Point(0, 0), // anchor
        labelOrigin: new google.maps.Point(0, -2),
        madviqly: vitriObj.MA_DVIQLY,
        id: vitriObj.MA_VITRI
    };
    marker.setIcon(icon);

    // Allow each marker to have an info window    
    google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {

            // toandtb
            vitrimarker_click = i;
            //
            if (hashMapMarker_truoc.size == 1) {
                hashMapMarker_truoc.get(0).setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: tron_scale,
                    fillColor: hashMapMarker_truoc.get(0).getIcon().fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -2),
                    madviqly: hashMapMarker_truoc.get(0).getIcon().madviqly, //1
                    id: hashMapMarker_truoc.get(0).getIcon().id, //2
                });
            }

            //
            marker.setIcon({
                path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                scale: tron_scale,
                fillColor: marker.getIcon().fillColor,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                madviqly: marker.getIcon().madviqly, //1
                id: marker.getIcon().id, //2

            });
            marker.label.fontSize = cochu + 'px';
            marker.icon.labelOrigin = new google.maps.Point(0, -6);
            hashMapMarker_truoc.set(0, marker);
            w3_open();
            
            
            $("#tieudeII").text("II. Thông Tin Vị Trí Bất Thường");
            $("#II_MA_VITRI").show();
            $("#II_SO_COT").show();
            $("#II_toadoX").show();
            $("#II_toadoY").show();
            $("#II_MA_DOITUONG").show();
            $("#II_MA_NOIDUNG").show();
            $("#II_CAP_DO").show();
            $("#II_NGAY_NHAP").show();

            $("#II_MA_VITRI").text("Mã vị trí: "+ vitriObj.MA_VITRI);
            $("#II_SO_COT").text("Số cột: "+ vitriObj.SO_COT);
            $("#II_X_VITRI").text("Vĩ độ: "+ vitriObj.X_VITRI);
            $("#II_Y_VITRI").text("Kinh độ: "+ vitriObj.Y_VITRI);
            $("#II_MA_DOITUONG").text("Mã đối tượng: "+ vitriObj.MA_DOITUONG);
            $("#II_MA_NOIDUNG").text("Nội dung: "+ vitriObj.MA_NOIDUNG);
            $("#II_CAP_DO").text("Cấp độ: "+ vitriObj.CAP_DO);
            $("#II_NGAY_NHAP").text("Ngày nhập: "+ vitriObj.NGAY_NHAP.substring(0,vitriObj.NGAY_NHAP.indexOf("T")));
            
            $("#hinhanh").empty();
            $.get(`${url}/api/BTHT_HATHE_getDSHinhanh_bySocot/${vitriObj.MA_DVIQLY}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${vitriObj.MA_TRAM}/${vitriObj.SO_COT.split("/").join("%2f")}`, function (data) {
                jsonHinnhanh_cot = data;
                $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cot.length );
                $.each(jsonHinnhanh_cot, function (i, f) {
                    $("#hinhanh").append(
                        `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                            <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_BATTHUONG/${f.TEN_HINHANH}">
                        </div>`
                    );
                });
            });
        }

    })(marker, i));
}


function val2key(val, array) {
    for (var i = 0; i < array.size; i++) {
        //console.log("KEY1: "+array.get(i).icon.socot);
        if (array.get(i).icon.socot == val) {
            //console.log("KEYA: "+array.get(i).icon.socot);
            return i;
        }
    }
}

function moveToLocation(lat, lng) {
    var center = new google.maps.LatLng(lat, lng);
    // using global variable:
    map.panTo(center);
}

/* BAT DAU AAAAA:  PHAN HEADER */
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);


$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

    // Kiem tra user
    var m_taikhoan = localStorage['taikhoan'];
    //console.log("AAA: "+ `${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`);
    $.get(`${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`, function (data) {
        $.each(data, function (i, f) {
            madonvicuanguoidung = f.MA_DVIQLY;
            if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
                $("#congty").select2().val(madonvicuanguoidung);
                $("#congty").trigger('change');
                $("#congty").prop('disabled', true);
            } else if (madonvicuanguoidung.length == 6) {
                if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 2));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                } else {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 4));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                }
            }
        });
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});


$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;

});



function w3_open() {
    document.getElementById("main").style.marginLeft = "17.7%";
    document.getElementById("mySidebar").style.width = "17.7%";
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("map").style.width = '82.3%';
}

function w3_close() {
    document.getElementById("main").style.marginLeft = "0%";
    document.getElementById("mySidebar").style.display = "none";
    //document.getElementById("openNav").style.display = "inline-block";
    document.getElementById("map").style.width = '100%';
}
/* KET THUC AAAAA:  PHAN HEADER */

// HIEN SO COT
var cb_hiensocot = document.querySelector("input[name=cb_hiensocot]");
cb_hiensocot.addEventListener('change', function () {
    if (this.checked) {
        isHiensocot = true;
        if ((matram == null || matram == '0') && map.getZoom() <= 15) {
            // khong lam chi
        } else {
            bochuyendoiZoom(map.getZoom());
            for (var i = 0; i < hashMapMarker.size; i++) {
                hashMapMarker.get(i).setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: tron_scale,
                    fillColor: hashMapMarker.get(i).icon.fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -2),
                    madviqly: hashMapMarker.get(i).icon.madviqly, //1
                    matram: hashMapMarker.get(i).icon.matram, //2
                    tentram: hashMapMarker.get(i).icon.tentram, //3
                    socot: hashMapMarker.get(i).icon.socot, //4
                    lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                    ishien: 1 //5
                });

                //
                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else {
                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                }
            }
        }
    } else {
        isHiensocot = false;
        if ((matram == null || matram == '0') && map.getZoom() <= 15) {
            // khong lam chi
        } else {
            bochuyendoiZoom(map.getZoom());
            for (var i = 0; i < hashMapMarker.size; i++) {
                hashMapMarker.get(i).setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: tron_scale,
                    fillColor: hashMapMarker.get(i).icon.fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -2),
                    madviqly: hashMapMarker.get(i).icon.madviqly, //1
                    matram: hashMapMarker.get(i).icon.matram, //2
                    tentram: hashMapMarker.get(i).icon.tentram, //3
                    socot: hashMapMarker.get(i).icon.socot, //4
                    lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                    ishien: 0 //6
                });

                //
                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else {
                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                }
            }
        }
    }

});


// Click nut xem bao cao
function clickXembaocao() {
    if (macongty == null || macongty == '0') {

    } else if (madienluc == null || madienluc == '0') {

    } else {
        initMap();
        $(".search-wrapper").css({ 'right': 'auto', 'left': '480px' });
        // $(".search-wrapper.active.input-holder").css('left','50%');
        // $(".search-wrapper.active.close").css('right','-260px');
        jsonKhachhang = '';
        jsonKhachhangbandau = '';
        jsonHinnhanh_cot = '';
        jsonHinnhanh_khang = '';
        jsonHinnhanh_cotbandau = '';
        vitrimarker_click = '';
        w3_open();
    }

  
}

function initZoomImage_Cot(index_image) {
    //Zoom in and Zoom out image
    list_zoomImage_Cot[index_image] = document.getElementById(`myImage_zoom_Cot${index_image}`);
    if (list_zoomImage_Cot[index_image].addEventListener) {
        // IE9, Chrome, Safari, Opera
        list_zoomImage_Cot[index_image].addEventListener("mousewheel", MouseWheelHandler_Cot, false);
        // Firefox
        list_zoomImage_Cot[index_image].addEventListener("DOMMouseScroll", MouseWheelHandler_Cot, false);
    }
    // IE 6/7/8
    else list_zoomImage_Cot[index_image].attachEvent("onmousewheel", MouseWheelHandler_Cot);
}

function initZoomImage_Khang(index_image) {
    //Zoom in and Zoom out image
    list_zoomImage_Khang[index_image] = document.getElementById(`myImage_zoom_Khang${index_image}`);
    if (list_zoomImage_Khang[index_image].addEventListener) {
        // IE9, Chrome, Safari, Opera
        list_zoomImage_Khang[index_image].addEventListener("mousewheel", MouseWheelHandler_Khang, false);
        // Firefox
        list_zoomImage_Khang[index_image].addEventListener("DOMMouseScroll", MouseWheelHandler_Khang, false);
    }
    // IE 6/7/8
    else list_zoomImage_Khang[index_image].attachEvent("onmousewheel", MouseWheelHandler_Khang);
}

function MouseWheelHandler_Cot(e) {

    // cross-browser wheel delta
    var e = window.event || e; // old IE support
    var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
    list_zoomImage_Cot[index_zoomImage_Cot_current].style.width = Math.max(50, Math.min(900, list_zoomImage_Cot[index_zoomImage_Cot_current].width + (30 * delta))) + "px";
    list_zoomImage_Cot[index_zoomImage_Cot_current].style.height = Math.max(50, Math.min(1000, list_zoomImage_Cot[index_zoomImage_Cot_current].height + (30 * delta))) + "px";

    console.log(list_zoomImage_Cot[index_zoomImage_Cot_current].style.width + "---" + list_zoomImage_Cot[index_zoomImage_Cot_current].height);
    return false;
}

function MouseWheelHandler_Khang(e) {

    // cross-browser wheel delta
    var e = window.event || e; // old IE support
    var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
    list_zoomImage_Khang[index_zoomImage_Khang_current].style.width = Math.max(50, Math.min(900, list_zoomImage_Khang[index_zoomImage_Khang_current].width + (30 * delta))) + "px";
    list_zoomImage_Khang[index_zoomImage_Khang_current].style.height = Math.max(50, Math.min(1000, list_zoomImage_Khang[index_zoomImage_Khang_current].height + (30 * delta))) + "px";

    console.log(list_zoomImage_Khang[index_zoomImage_Khang_current].style.width + "---" + list_zoomImage_Khang[index_zoomImage_Khang_current].height);
    return false;
}

// BAT DAU: Xem hinh anh cot chi tiet
function getHinhanhchitiet(index, cotorkhang) {

    document.getElementById('id_image').style.display = 'block';
    var n = 1;
    $("#hinhanhslide").empty();
    $("#dotanh").empty();
    if (cotorkhang == 0) {
        //Lấy vị trí index thứ ảnh
        index_zoomImage_Cot_current = index;
        var index_image = 0;
        $.each(jsonHinnhanh_cot, function (i, f) {
            $("#hinhanhslide").append(
                `<div  class="mySlides" style="height:auto; width:auto">
                        <img id='myImage_zoom_Cot${index_image}' src="https://ktat.cpc.vn/${f.TEN_ANH.substring(3, f.TEN_ANH.length)}" style="height:80%; width:auto">                
                </div>`
            );
            $("#dotanh").append(
                `<span class="dot" onclick="currentSlide(${n++})"></span>`
            );

            //Lấy vị trí index ảnh để zoom
            initZoomImage_Cot(index_image);
            index_image++;

        })

    } else if (cotorkhang == 1) {
        //Lấy vị trí index thứ ảnh
        index_zoomImage_Khang_current = index;
        var index_image = 0;
        $.each(jsonHinnhanh_khang, function (i, f) {
            $("#hinhanhslide").append(
                `<div class="mySlides" style="height:auto; width:auto">
                    <img id='myImage_zoom_Khang${index_image}' src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}" style="height:80%; width:auto">                
                </div>`
            );

            $("#dotanh").append(
                `<span class="dot" onclick="currentSlide(${n++})"></span>`
            );

            //Lấy vị trí index ảnh để zoom
            initZoomImage_Khang(index_image);
            index_image++;
        });
    }

    //
    currentSlide(index + 1, cotorkhang);
}

function getHosochitiet(ctrl, ma_khang, ten_hinh) {
    var macongty_sohoa = "";
    var madienluc_sohoa = ""
    if (ma_khang.substring(0, 2) == 'PP' || ma_khang.substring(0, 2) == 'PQ') {
        macongty_sohoa = ma_khang.substring(0, 2);
        madienluc_sohoa = ma_khang.substring(0, 6);
    } else {
        var macongty_sohoa = ma_khang.substring(0, 4);
        var madienluc_sohoa = ma_khang.substring(0, 6);
    }

    var url = "";
    if (macongty == "PC01") {
        url = "http://10.126.0.22/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC02") {
        url = "http://10.125.184.2/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC03") {
        url = "http://10.124.0.9/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PP") {
        url = "http://10.123.99.15:8888/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC05") {
        url = "http://10.122.0.17/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC06") {
        url = "http://qnpc.evncpc.vn/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC07") {
        url = "http://qnpc.evncpc.vn/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC08") {
        url = "http://10.119.0.8/HoSo_KhachHang_DienTu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PQ") {
        url = "http://file.khpc.vn/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC10") {
        url = "http://10.117.0.38/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC11") {
        url = "http://10.116.0.10/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC12") {
        url = "http://10.115.0.14/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC13") {
        url = "http://10.114.0.8/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    }

    //console.log("MACONGTY: "+ macongty);
    //console.log("MADIENLUC: "+ madienluc);
    //console.log("URL: " + url);
    $('#obj_hosodientu').attr("data", url);
    $('#obj_hosodientu').show();

    //
    var list = document.getElementById("ul_hosodientu").getElementsByTagName('li');
    for (i = 0; i < list.length; i++) {
        list[i].style.background = '#ffffff';
    }

    ctrl.style.background = '#ddd';
}

// script cho slide ảnh
var slideIndex = 0;
function plusSlides(n) {

    showSlides(slideIndex += n);

    //
    if (cotorkhang == 0) {
        //Lấy vị trí index ảnh để zoom
        index_zoomImage_Cot_current = slideIndex - 1;
        initZoomImage_Cot(index_zoomImage_Cot_current);

        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_cot.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    } else if (cotorkhang == 1) {
        //Lấy vị trí index ảnh để zoom
        index_zoomImage_Khang_current = slideIndex - 1;
        initZoomImage_Khang(index_zoomImage_Khang_current);

        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_khang.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    }
}

var cotorkhang;
function currentSlide(n, m_cotorkhang) {

    cotorkhang = m_cotorkhang
    showSlides(slideIndex = n, cotorkhang);

    //
    if (cotorkhang == 0) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_cot.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    } else if (cotorkhang == 1) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_khang.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    }

}

//
function showSlides(n, m_cotorkhang) {
    if (m_cotorkhang == 0) {
        $("#tenhinhanh").text(jsonHinnhanh_cot[n - 1].TEN_HINHANH);
    } else if (m_cotorkhang == 1) {
        $("#tenhinhanh").text(jsonHinnhanh_khang[n - 1].TEN_HINHANH);
    }

    //
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" image_active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " image_active";
}


