GF_PHANMENU();
var url = GV_URL;
var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matramnguon;
$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tramnguon").empty();
$("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);

$("#nguoikiemtra").empty();
$("#nguoikiemtra").append(`<option value="0">-- Chọn Người Kiểm Tra --</option>`);

$("#ngaykiemtra").empty();
$("#ngaykiemtra").append(`<option value="0">-- Chọn Ngày Kiểm Tra --</option>`);

$("#tbt_id").empty();
$("#tbt_id").append(`<option value="0">-- Nhập TBT_ID --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tramnguon").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm Nguồn"
});

$("#nguoikiemtra").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Người Kiểm Tra"
});

$("#ngaykiemtra").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Ngày Kiểm Tra"
});

$("#tbt_id").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Ngày Kiểm Tra"
});


$("#nguoikiemtra").append(`<option value="1">Bùi Quang Thuận</option>`);
$("#nguoikiemtra").append(`<option value="2">Diệp Thái Bảo Toàn</option>`);
$("#nguoikiemtra").append(`<option value="3">Nguyễn Văn Điệp</option>`);
$("#nguoikiemtra").append(`<option value="4">Ronaldo yêu Messi</option>`);

$("#ngaykiemtra").append(`<option value="1">10/07/1997</option>`);
$("#tbt_id").append(`<option value="1">1234567</option>`);


$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matramnguon = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matramnguon = '0';
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
    $.get(`${url}/api/TRUNGTHE_getDMTramnguon_byMadviqly/${madienluc.trim()}`, function (data) {
        $("#tramnguon").empty();
        $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
        $.each(data, function (i, f) {
            $("#tramnguon").append(`<option value="${f.MA_TRAMNGUON}">${f.MA_TRAMNGUON} - ${f.TEN_TRAMNGUON}</option>`);
        });
    });
});

$('#tramnguon').on('change', function () {
    matramnguon = this.value;
});

//chay mac dinh lan dau
//$('#colwillchangeh').html('Công Ty');
$("#loading").hide();


// Click nut xem bao cao
function clickXembaocao() {
    if (macongty == null || macongty == '0') {

    } else {
        if (madienluc == null || madienluc == '0') {

        } else {
            if (matramnguon == null || matramnguon == '0') {

            } else {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $('#colwillchangeh').html('Trạm');
                $("#loading").show();
                $.get(`${url}/api/TRUNGTHE_getDMXuattuyen_byTramnguon/${madienluc}/${matramnguon}`, function (data) {
                    $.each(data, function (i, f) {
                        $("#tbody").append(
                            `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.MA_DVIQLY}</td>
                            <td style='text-align:left'>${f.MA_XUATTUYEN}</td>
                            <td style='text-align:left'>Cháy TBA</td>
                            <td style='text-align:right'>10/07/1997</td>
                            <td style='text-align:left'>Bùi Quang Thuận</td>
                            <td style='text-align:left'>CPCITC</td>
                        </tr>`
                        );
                    });
                    // $('#dataTable').DataTable({
                    //     "columns": [
                    //         { "width": "5%" },
                    //         { "width": "35%" },
                    //         { "width": "5%" },
                    //         { "width": "5%" },
                    //         { "width": "5%" },
                    //         { "width": "5%" }
                    //     ]
                    // });
                    $("#loading").hide();
                });
            }
        }
    }
}


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}