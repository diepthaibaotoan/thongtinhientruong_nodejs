GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var macongty, madienluc, matram;
var json_soluongtram;
var isDate = false;


$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);

// BIEN TAP CHE DO NGAY GIO 
document.querySelector("#today").valueAsDate = new Date();
document.getElementById("today").disabled = true;
var cb_date = document.querySelector("input[name=cb_date]");
cb_date.addEventListener('change', function () {
    if (this.checked) {
        isDate = true;
        document.getElementById("today").disabled = false;
        document.getElementById("today").style.backgroundColor = 'white';
        document.getElementById("congty").disabled = true;
        document.getElementById("dienluc").disabled = true;
        document.getElementById("tram").disabled = true;

    } else {
        isDate = false;
        document.getElementById("today").disabled = true;
        document.getElementById("today").style.backgroundColor = 'beige';
        document.getElementById("congty").disabled = false;
        document.getElementById("dienluc").disabled = false;
        document.getElementById("tram").disabled = false;
    }
});
$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);

    });

});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});

$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

$("#loading").hide();

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: json_soluongtram,
        columns: getColumns(json_soluongtram)
    });
}

// Click nut xem bao cao
function clickXembaocao() {

    madonviquanly_tam = '%20';
    matram_tam = '%20';
    capdonvi = '%20';
    if (macongty == null || macongty == '0') {
        capdonvi = 1;
        madonviquanly_tam = 'CPC';
    } else {

        if (madienluc == null || madienluc == '0') {
            capdonvi = 2;
            madonviquanly_tam = macongty;
        } else {
            if (matram == null || matram == '0') {
                capdonvi = 3;
                madonviquanly_tam = madienluc;
            } else {
                capdonvi = 4;
                madonviquanly_tam = madienluc;
                matram_tam = matram;
            }
        }


    }


    //Nếu bật chế độ ngày!!!!
    if (isDate) {
        $("#loading").show();
        $.get(`${url}/api/HATHE_getBCKhachhang_TramHoanThanh_TheoNgay/${$('#today').val()}`, function (data) {
            jsonKhachhang = data;
            taodulieuchobang(jsonKhachhang);
            $("#loading").hide();
        });
    } else {
        $("#loading").show();
        console.log("URL: "+ `${url}/api/HATHE_getBCKhachhang_TramHoanThanh/${madonviquanly_tam}/${matram_tam}/${capdonvi}`);
        $.get(`${url}/api/HATHE_getBCKhachhang_TramHoanThanh/${madonviquanly_tam}/${matram_tam}/${capdonvi}`, function (data) {
            json_soluongtram = data;
            taodulieuchobang(json_soluongtram);
            $("#loading").hide();
        });
    }


}


function taodulieuchobang(data) {
    $("#dataTable").dataTable().fnDestroy();
    $("#tbody").empty();
    $('#colwillchangeh').html('Trạm');
    var tong_SLUONG_TRAM_XUATTUCMIS = 0;
    var tong_SLUONG_TRAM_DUOCRASOAT = 0;
    $.each(data, function (i, f) {
        tong_SLUONG_TRAM_XUATTUCMIS = tong_SLUONG_TRAM_XUATTUCMIS + f.SLUONG_TRAM_XUATTUCMIS;
        tong_SLUONG_TRAM_DUOCRASOAT = tong_SLUONG_TRAM_DUOCRASOAT + f.SLUONG_TRAM_DUOCRASOAT;
        var tyle_phantram;
        if (f.SLUONG_TRAM_DUOCRASOAT != 0) {
            tyle_phantram = (f.SLUONG_TRAM_DUOCRASOAT / f.SLUONG_TRAM_XUATTUCMIS) * 100;
            tyle_phantram = tyle_phantram.toFixed(2);
        } else {
            tyle_phantram = 0;
        }
        $("#tbody").append(
            `<tr>
                <td style='text-align:center'>${f.STT}</td>
                <td style='text-align:left'>${f.TEN_DONVI}</td>
                <td style='text-align:right'>${f.SLUONG_TRAM_XUATTUCMIS.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_TRAM_DUOCRASOAT.toLocaleString('en')}</td>
                <td style='text-align:right'>${tyle_phantram + "%"}</td>
            </tr>`
        );
    });

    $('#dataTable').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "29%" },
            { "width": "22%" },
            { "width": "22%" },
            { "width": "22%" }
        ]
    });

    $('#tendonvi_colwillchangef').html('Tổng cộng:');
    $('#tongsoluongtramtrenttht_colwillchangef').html(tong_SLUONG_TRAM_XUATTUCMIS.toLocaleString('en'));
    $('#tongsoluongtramdarasoat_colwillchangef').html(tong_SLUONG_TRAM_DUOCRASOAT.toLocaleString('en'));
}


