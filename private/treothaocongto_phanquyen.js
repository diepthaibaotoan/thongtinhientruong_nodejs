GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var jsonUser;
var userObj_Duyet;
var macongty, madienluc, matram;
var macongty_user, madienluc_user;
var madonvicuanguoidung = '';
var madonvimoi = '';
var is_Madonvimoi;

$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);

$("#congty_user").empty();
$("#congty_user").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc_user").empty();
$("#dienluc_user").append(`<option value="0">-- Tất cả Điện lực --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    //
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

    // Kiem tra user
    var m_taikhoan = localStorage['taikhoan'];
    $.get(`${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`, function (data) {
        $.each(data, function (i, f) {
            madonvicuanguoidung = f.MA_DVIQLY;
            if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
                $("#congty").select2().val(madonvicuanguoidung);
                $("#congty").trigger('change');
                $("#congty").prop('disabled', true);
            } else if (madonvicuanguoidung.length == 6) {
                if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 2));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                } else {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 4));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                }
            }
        });

        //Khởi tạo lần đầu
        clickXembaocao();

    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);

    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$('#dienluc').on('change', function () {
    madienluc = this.value;
});

//cho dialog duyet
$.getJSON('/assets/DM_DONVI_PHANQUYEN.json', function (data) {

    // cho dialog duyệt
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty_user").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty_user").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc_user").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});

$('#congty_user').on('change', function () {
    macongty_user = this.value;
    madienluc_user = '0';
    $("#dienluc_user").empty();
    $("#dienluc_user").append(`<option value="0">-- Tất cả Điện lực --</option>`);

    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty_user)
            $("#dienluc_user").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
})

$('#dienluc_user').on('change', function () {
    madienluc_user = this.value;
});

//Ẩn quyền lựa chọn đơn vị cho user khi chưa chọn mới
$('#congty_user').prop("disabled", true);
$('#dienluc_user').prop("disabled", true);

$("#loading").hide();


function hienThiThanhChonMoi() {

    if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
        $("#congty_user").select2().val(madonvicuanguoidung);
        $("#congty_user").trigger('change');
        $("#congty_user").prop('disabled', true);
    } else if (madonvicuanguoidung.length == 6) {
        if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
            $("#congty_user").select2().val(madonvicuanguoidung.substring(0, 2));
            $("#congty_user").trigger('change');
            $("#congty_user").prop('disabled', true);
            //
            $("#dienluc_user").select2().val(madonvicuanguoidung);
            $("#dienluc_user").trigger('change');
            $("#dienluc_user").prop('disabled', true);
        } else {
            $("#congty_user").select2().val(madonvicuanguoidung.substring(0, 4));
            $("#congty_user").trigger('change');
            $("#congty_user").prop('disabled', true);
            //
            $("#dienluc_user").select2().val(madonvicuanguoidung);
            $("#dienluc_user").trigger('change');
            $("#dienluc_user").prop('disabled', true);
        }
    }
}

// Click nut xem bao cao
function clickXembaocao() {

    $("#loading").show();
    var chuoidauvao = '%20';
    if (macongty == null || macongty == '0') {
        chuoidauvao = '%20';
    } else {
        if (madienluc == null || madienluc == '0') { // cap cong ty
            chuoidauvao = macongty;
        } else {
            chuoidauvao = madienluc; //cap dien luc
        }
    }

    //Lấy dữ liệu service và đổ vào bảng
    $.get(`${url}/api/TTCT_getDSNGUOIDUNG_byMadviqly/${chuoidauvao}`, function (data) {
        jsonUser = data;
        taodulieuchobang(data);
    });
}

function taodulieuchobang(data) {
    $("#dataTable").dataTable().fnDestroy();
    $("#tbody").empty();
    $.each(data, function (i, f) {

        if (f.MA_DVIQLY == null) {
            f.MA_DVIQLY = "";
        }

        if (f.TEN_DVIQLY == null) {
            f.TEN_DVIQLY = "";
        }
        if (f.TEN_DANGNHAP == null) {
            f.TEN_DANGNHAP = "";
        }
        if (f.tenkhaisinh == null) {
            f.tenkhaisinh = "";
        }

        if (f.CHUC_NANG_TTCT == null) {
            f.CHUC_NANG_TTCT = "";
        }

        $("#tbody").append(
            `<tr >
                        <td style='text-align:center'>${f.STT}</td>
                        <td style='text-align:left'>${f.MA_DVIQLY + " - " + f.TEN_DVIQLY}</td>
                        <td style='text-align:left'>${f.TEN_DANGNHAP}</td>
                        <td style='text-align:left'>${f.tenkhaisinh}</td>
                        <td style='text-align:center'>
                            <p id="ADMIN_TTCT_PHANQUYEN${f.TEN_DANGNHAP}"></p>
                        </td>
                        <td style='text-align:center'>
                            <p id="ADMIN_TTCT_PHANCONGNHIEMVU${f.TEN_DANGNHAP}"></p>
                        </td>
                        <td style='text-align:center'>
                            <p id="ADMIN_TTCT_THUCHIENNHIEMVU${f.TEN_DANGNHAP}"></p>
                        </td>
                        <td style='text-align:center'>
                                <button class='button_xem' style="display: table;margin: 0 auto" onclick="xacNhanPhanQuyen('${f.TEN_DANGNHAP}')">Duyệt</button>
                        </td>
            </tr>`
        );

        //Check Phan Quyen
        if (f.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT_PHANQUYEN) > -1) {
            // document.getElementById("admin" + f.TEN_DANGNHAP).HTML = "X";
            var paragraph = document.getElementById("ADMIN_TTCT_PHANQUYEN" + f.TEN_DANGNHAP);
            paragraph.textContent += "X";
        }
        //Check DANG KY TREO THAO
        if (f.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT_PHANCONGNHIEMVU) > -1) {
            var paragraph = document.getElementById("ADMIN_TTCT_PHANCONGNHIEMVU" + f.TEN_DANGNHAP);
            paragraph.textContent += "X";
        }
        //Check THAO TAC HIEN TRUONG
        if (f.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT_THUCHIENNHIEMVU) > -1) {
            var paragraph = document.getElementById("ADMIN_TTCT_THUCHIENNHIEMVU" + f.TEN_DANGNHAP);
            paragraph.textContent += "X";
        }


    });
    $('#dataTable').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "20%" },
            { "width": "7%" },
            { "width": "7%" },
            { "width": "7%" },
            { "width": "9%" }
        ]
    });
    $("#loading").hide();
}

function xacNhanPhanQuyen(tendangnhap) {

    //Set lại mặc định cho các ô chọn
    document.getElementById("ADMIN_TTCT_PHANQUYEN").checked = false;
    document.getElementById("ADMIN_TTCT_PHANCONGNHIEMVU").checked = false;
    document.getElementById("ADMIN_TTCT_THUCHIENNHIEMVU").checked = false;
    //Ẩn quyền lựa chọn đơn vị cho user khi chưa chọn mới
    $('#congty_user').prop("disabled", true);
    $('#dienluc_user').prop("disabled", true);
    $("#congty_user").select2().val(0);
    $("#congty_user").trigger('change');
    $("#dienluc_user").select2().val(0);
    $("#dienluc_user").trigger('change');
    madonvimoi = "";
    hienThiThanhChonMoi();

    userObj_Duyet = jsonUser.find(function (obj) { return obj.TEN_DANGNHAP === tendangnhap });
    //Tên user 
    $("#tenuser").text(tendangnhap);
    $("#tendonvi").text(userObj_Duyet.TEN_DVIQLY);


    //Check ADMIN_TTCT_PHANQUYEN
    if (userObj_Duyet.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT_PHANQUYEN) > -1) {
        document.getElementById("ADMIN_TTCT_PHANQUYEN").checked = true;
    }
    //Check ADMIN_TTCT_PHANCONGNHIEMVU
    if (userObj_Duyet.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT_PHANCONGNHIEMVU) > -1) {
        document.getElementById("ADMIN_TTCT_PHANCONGNHIEMVU").checked = true;
    }
    //Check ADMIN_TTCT_THUCHIENNHIEMVU
    if (userObj_Duyet.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT_THUCHIENNHIEMVU) > -1) {
        document.getElementById("ADMIN_TTCT_THUCHIENNHIEMVU").checked = true;
    }

    //bắt sự kiện khi chọn quyền
    $("#ADMIN_TTCT_PHANQUYEN").change(function () {
        var checkboxuser = document.getElementById("ADMIN_TTCT_PHANQUYEN");
        if (checkboxuser.checked == true) {
            userObj_Duyet.CHUC_NANG_TTCT += ADMIN_TTCT_PHANQUYEN;
        } else {
            userObj_Duyet.CHUC_NANG_TTCT = GF_REPLACEALL(userObj_Duyet.CHUC_NANG_TTCT, ADMIN_TTCT_PHANQUYEN, "");
        }
    });

    //
    $("#ADMIN_TTCT_PHANCONGNHIEMVU").change(function () {
        var checkboxuser = document.getElementById("ADMIN_TTCT_PHANCONGNHIEMVU");
        if (checkboxuser.checked == true) {
            userObj_Duyet.CHUC_NANG_TTCT += ADMIN_TTCT_PHANCONGNHIEMVU;
        } else {
            userObj_Duyet.CHUC_NANG_TTCT = GF_REPLACEALL(userObj_Duyet.CHUC_NANG_TTCT, ADMIN_TTCT_PHANCONGNHIEMVU, "");
        }
    });

    //
    $("#ADMIN_TTCT_THUCHIENNHIEMVU").change(function () {
        var checkboxuser = document.getElementById("ADMIN_TTCT_THUCHIENNHIEMVU");
        if (checkboxuser.checked == true) {
            userObj_Duyet.CHUC_NANG_TTCT += ADMIN_TTCT_THUCHIENNHIEMVU;
        } else {
            userObj_Duyet.CHUC_NANG_TTCT = GF_REPLACEALL(userObj_Duyet.CHUC_NANG_TTCT, ADMIN_TTCT_THUCHIENNHIEMVU, "");
        }
    });

    document.getElementById('id_duyet').style.display = 'block';
}

function GuiDuLieu_PhanQuyen() {

    if (is_Madonvimoi) {
        if (macongty_user == '' || macongty_user == '0') {
            //Mã này thì khong thay đổi đơn vị quản lý
            madonvimoi = GV_KHONGTHAYDOI_MADVIQLY_TTCT;
        } else {
            if (madienluc_user == '' || madienluc_user == '0') {
                madonvimoi = macongty_user;
            } else {
                madonvimoi = madienluc_user;
            }
        }
    } else {
        //Mã này thì khong thay đổi đơn vị quản lý
        madonvimoi = GV_KHONGTHAYDOI_MADVIQLY_TTCT;
    }

    if (userObj_Duyet.CHUC_NANG_TTCT.trim() == '' || userObj_Duyet.CHUC_NANG_TTCT == null) {
        userObj_Duyet.CHUC_NANG_TTCT = '%20';
    }
    $("#loading").show();
    //console.log("URL: " + `${url}/api/TTCT_PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full/${madonvimoi}/${userObj_Duyet.TEN_DANGNHAP}/${userObj_Duyet.CHUC_NANG_TTCT}/${localStorage['taikhoan']}`);
    $.get(`${url}/api/TTCT_PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full/${madonvimoi}/${userObj_Duyet.TEN_DANGNHAP}/${userObj_Duyet.CHUC_NANG_TTCT}/${localStorage['taikhoan']}`, function (data) {
        //
        document.getElementById('id_duyet').style.display = 'none';
        $("#loading").hide();
        GF_SHOWTOAST_LONG("Phân quyền thành công.", "250px");
        clickXembaocao();
    });
}
