GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var macongty, madienluc, matram;
var jsonkhachhang;
var isDate = false;

$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);


$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

$("#loading").hide();

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: jsonKhachhang,
        columns: getColumns(jsonKhachhang)
    });
}

// Click nut xem bao cao
function clickXembaocao() {

    madonviquanly_tam = '%20';
    matram_tam = '%20';
    capdonvi = '%20';
    if (macongty == null || macongty == '0') {
        capdonvi = 1;
        madonviquanly_tam = 'CPC';
        alert("Chỉ được chọn ở cấp trạm.");
        return;
    } else {
        if (madienluc == null || madienluc == '0') {
            capdonvi = 2;
            madonviquanly_tam = macongty;
            alert("Chỉ được chọn ở cấp trạm.");
            return;
        } else {
            if (matram == null || matram == '0') {
                capdonvi = 3;
                madonviquanly_tam = madienluc;
                // alert("Chỉ được chọn ở cấp trạm.");
                // return;
            } else {
                capdonvi = 4;
                madonviquanly_tam = madienluc;
                matram_tam = matram;
                
                //console.log("matram: " + matram);
                // if (matram != null && matram != '0') {
                //     $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSHosodientu_byTram?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&KEY=${GV_KEY_TOANDTB}`, function (data) {
                //         //console.log("HATHE_setDSHosodientu_byTram: " + data);
                //     });

                //     $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSHosodientu_byTram_Soluong?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&KEY=${GV_KEY_TOANDTB}`, function (data) {
                //         //console.log("HATHE_setDSHosodientu_byTram: " + data);
                //     });
                // }
            }
        }
    }

    //Load du lieu webservice
    $("#loading").show();
    $.get(`${url}/api/HATHE_getBCKhachhang_ThieuAnhCongTo/${madonviquanly_tam}/${matram_tam}/${capdonvi}`, function (data) {
        jsonKhachhang = data;
        taodulieuchobang(jsonKhachhang);
        $("#loading").hide();
    });
}

function taodulieuchobang(data) {
    $("#dataTable").dataTable().fnDestroy();
    $("#tbody").empty();

    $.each(data, function (i, f) {

        $("#tbody").append(
            `<tr>
            <td style='text-align:center'>${i + 1}</td>
            <td style='text-align:left'>${f.MA_DVIQLY}</td>
            <td style='text-align:left'>${f.MA_TRAM}</td>
            <td style='text-align:left'>${f.MA_KHANG}</td>
            <td style='text-align:left'>${f.TEN_KHANG}</td>
            <td style='text-align:left'>${f.DCHI_SDD}</td>
            <td style='text-align:left'>${f.SO_COT}</td>
            </tr>`
        );
    });

    $('#dataTable').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "20%" },
            { "width": "15%" }
        ]
    });

}

