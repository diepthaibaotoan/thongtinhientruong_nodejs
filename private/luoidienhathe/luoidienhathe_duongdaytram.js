function DDT_showTotalLines(array) {
    var n = array.length;
    var matrix = [];
    var hash = new Array();

    //init matrix
    matrix = DDT_fill2DimensionsArray(matrix, n,n);

    //set hash
    for (var i = 0; i < n; i++) {
        var tupple = {
            pos: i,
            name: array[i].SO_COT,
            associates: array[i].LK_COT1
        };
        hash.push(tupple);
    }

    //find associates in matrix
    for (var i = 0; i < n; i++) {
        var associates = hash[i].associates.split('$');
        for (var j = 0; j < associates.length; j++) {
            var pos = DDT_getPosInHashes(associates[j], hash);
            if (pos != -1) matrix[i][pos] = 1;
        }
    }

    //split half matix
    for (var i = 0; i < n; i++) {
        for (var j = 0; j < i; j++) {
            if(matrix[i][j] == 1) {
                matrix[i][j] = 0;
                matrix[j][i] = 1;
            }
        }
    }

    //calculate total distant
    var sum = 0;
    for (var i = 0; i < n; i++) {
        for (var j = i; j < n; j++) {
            if(matrix[i][j] == 1) {
                var distance = GF_distanceBetweenTwoPoints(array[i].X_COT,array[i].Y_COT,array[j].X_COT,array[j].Y_COT,"K");
                sum += (distance * 1000);
            }
        }
    }

    return sum.toFixed(2);
}

function DDT_getPosInHashes(name, hash) {
    for (var i = 0; i < hash.length; i++)
        if (hash[i].name == name) return hash[i].pos;
    return -1;
}

function DDT_fill2DimensionsArray(matrix, rows, columns) {
    for (var i = 0; i < rows; i++) {
        matrix.push([0])
        for (var j = 0; j < columns; j++) {
            matrix[i][j] = 0;
        }
    }

    return matrix;
}
