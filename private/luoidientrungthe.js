var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);
var h = $("#mySidebar").height();
$("#ul_thietbi").height(h - 106);
$("#hinhanh").height(h - 106);
// AAAAA
var jsonDonvi, jsonKhachhangbandau, jsonKhachhang, jsonHinnhanh_cotbandau, jsonHinnhanh_cot, jsonHinnhanh_khang;
var macongty, madienluc, matramnguon, maxuattuyen;
var CAPTONG = 1, CAPCONGTY = 2, CAPDIENLUC = 3, CAPTRAMNGUON = 4, CAPXUATTUYEN = 5;
var madonvicuanguoidung = '';
var vitrimarker_click;
var stt_marker;

// BBBBBB
var zoom = 17;
var cochu = 10;
var cochu_chotram = 10;
var rong = 24;
var cao = 34;
var m_rong = 24;
var m_cao = 34;
var chucach = -6;
var isHiensocot;
var isBientap;
var isBientapExcel;
var tron_scale, tron_stroke;
var map;
var marker;

//
var hashMapMarker_truoc = new Map();
var hashMapMarker = new Map();
var arrayMarker = [];
var arrayPoly = [];
var hashMapPoly = new Map();
var tramda_clickdouble = new Array();
var cotgoc;
var listCottrungthe;
var hinh_giotnuoc = 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0';
//
var bounds;
var northEast;
var southWest;

var mylat = 16.055994;
var mylng = 108.186822;

// Luc moi vo
$("#tieude").text("Trung Thế");
$("#isSocot").show();
$("#socot").show();
$("#isBientap").hide();
$("#bientap").hide();
$("#isBientapExcel").hide();
$("#bientapexcel").hide();
$('#search-cot').hide();
$("#isBandonhiet").prop("checked", true);
w3_close();

//console.log("URL: "+`${url}/api/TRUNGTHE_getDSThietbicuacot_byMavitri/PQ0600/PQ0600_DZ_VT_0044870`)
//Lúc đầu vô khởi tạo HeatMap
$.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/PC11`, function (data) {
    var X, Y;
    data.forEach(function (cotObj) {
        X = cotObj.X_COT;
        Y = cotObj.Y_COT;
    })
    HeatMapInit('', X, Y, 7, true, true);
});

function bochuyendoiZoom(bochuyendoi_zoom) {
    if (bochuyendoi_zoom == 17) {
        tron_scale = 5;
        tron_stroke = 1;
        cochu = 10;
        cochu_chotram = 10;
    } else if (bochuyendoi_zoom > 17) {
        tron_scale = (bochuyendoi_zoom - 17) * 2 + 5;
        tron_stroke = 1;
        cochu = (bochuyendoi_zoom - 17) * 3 + 10;
        cochu_chotram = (bochuyendoi_zoom - 17) * 3 + 10;
    } else if (bochuyendoi_zoom == 15) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0;
    } else if (bochuyendoi_zoom > 15 && bochuyendoi_zoom < 17) {
        tron_scale = (bochuyendoi_zoom - 17) * 1.5 + 5;
        tron_stroke = 0.5;
        cochu_chotram = 10;
        cochu = 0;
    } else if (bochuyendoi_zoom < 15 && bochuyendoi_zoom > 13) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0
    } else if (bochuyendoi_zoom == 13) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0
    } else if (bochuyendoi_zoom < 13) {
        tron_scale = 3;
        tron_stroke = 0.3;
        cochu_chotram = 10;
        cochu = 0
    }
}

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    if (maxuattuyen != null && maxuattuyen != '0') {

        //
        if (localStorage['taikhoan'] == null || localStorage['taikhoan'] == '' || localStorage['taikhoan'] == 'undefined') {
            $("#isBientapExcel").prop("checked", false);
            alert("Bạn chưa đăng nhập.");
        } else {
            //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
            $("#loading").show();
            $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${localStorage['taikhoan']}`, function (data) {
                if (data.length != 0) {
                    data.forEach(function (nguoidungObj) {
                        if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_PMIS_EXCUTE) > -1) {
                            if (macongty == null || macongty == '0') {
                                alert("Mời bạn chọn Công ty")
                            } else {
                                if (madienluc == null || madienluc == '0') {
                                    alert("Mời bạn chọn Điện lực.");
                                } else {
                                    if (maxuattuyen == null || maxuattuyen == '0') {
                                        alert("Mời bạn chọn Xuất tuyến.");
                                    } else {
                                        $("#id11").show();
                                    }
                                }
                            }
                        } else {
                            alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Trung Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
                        }
                    })
                } else {
                    alert("Tài khoản này không tồn tại.");
                }
                $("#loading").hide();
            });
        }
    } else {
        GF_SHOWTOAST_LONG("Chỉ có xem ở cấp Xuất tuyến mới có thể xuất Excel.", 400);
    }
}
function clickXuatExcelBandoluoidien() {

    //
    $("#loading").show();
    $.get(`${url}/api/TRUNGTHE_getDSExcelBandoluoidien_byXuattuyen/${madienluc.trim()}/${matramnguon}/${maxuattuyen}`, function (data) {
        $("#loading").hide();
        //console.log(data.length + "---");
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
        $("#id11").hide();
    });
}
function clickXuatExcelDuongdanhinhanh() {

    //
    $("#loading").show();
    $.get(`${url}/api/TRUNGTHE_getDSExcelDuongdanhinhanh_byXuattuyen/${madienluc.trim()}/${matramnguon.trim()}/${maxuattuyen.trim()}`, function (data) {
        $("#loading").hide();
        //console.log(data.length + "---");
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
    });
    $("#id11").hide();
}

// thuanbq
function HeatMapInit(dieukien, X, Y, zoom, heatmap, satellite) {

    //
    if (satellite == true) {
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(X, Y),
            zoom: zoom,
            mapTypeId: google.maps.MapTypeId.HYBRID
        });
    } else {
        map = new google.maps.Map(document.getElementById('map'), {
            center: new google.maps.LatLng(X, Y),
            zoom: zoom
        });
    }
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend-open'));
    map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.getElementById('googft-legend'));
    layer = new google.maps.FusionTablesLayer({
        map: map,
        heatmap: { enabled: heatmap },
        query: {
            select: "col3",
            from: "1EpndCe5BcF6VAihd61GkHO_prfn0oUD6yo_br4DM",
            where: dieukien
        },
        options: {
            styleId: 2,
            templateId: 2
        }
    });

    //when the map zoom changes, resize the icon based on the zoom level so the marker covers the same geographic area
    google.maps.event.addListener(map, 'zoom_changed', function () {
        //
    });

    //Tạo nút hiển thị tọa độ hiện tại
    addYourLocationButton(map);
}

//Lắng nghe thay đổi chọn lựa bản đồ nhiệt
$("#isBandonhiet").change(function () {
    var bandonhiet = document.getElementById('isBandonhiet');
    var dieukien = '';
    if (bandonhiet.checked == true) {
        //Thể hiện bản đồ nhiệt theo từng cấp
        //Nếu chưa chọn mã công ty --> thể hiện bản đồ nhiệt cho tổng miền trung
        if (macongty != null && macongty != '0') {

            //Điều kiện khi select 
            if (matramnguon == null || matramnguon == '0' && madienluc == null || madienluc == '0') {
                dieukien = "'MA_DVIQLY' like '" + macongty + "%'";
            } else if (matramnguon == null || matramnguon == '0') {
                dieukien = "'MA_DVIQLY' = '" + madienluc + "'";
            }
            else {
                dieukien = "'MA_DVIQLY' = '" + madienluc + "'" + "AND 'MA_TRAM' = '" + matram + "'";
            }

            //Tạo và di chuyển map tới tọa độ trung tâm vị trí 
            if (matramnguon == null || matramnguon == '0' && madienluc == null || madienluc == '0') {
                $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/${macongty}`, function (data) {
                    var X, Y;
                    var i = 0;
                    data.forEach(function (cotObj) {
                        X = cotObj.X_COT;
                        Y = cotObj.Y_COT;
                        i++;
                    })
                    if (i == 0) {
                        alert("Công ty này không có dữ liệu");
                    } else {
                        HeatMapInit(dieukien, X, Y, 10, true, false);
                    }
                });
            } else if (matramnguon == null || matramnguon == '0') {
                //Cấp điện lực: Khong co ban do nhiet
            } else {
                // Cap Trạm nguồn & Xuất tuyến chính: khong co ban do nhiet
            }
        } else {
            $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/PC11`, function (data) {
                var X, Y;
                data.forEach(function (cotObj) {
                    X = cotObj.X_COT;
                    Y = cotObj.Y_COT;
                })
                HeatMapInit(dieukien, X, Y, 7, true, false);
            });
        }
    } else {
        //Nếu ở cấp công ty thì cho hiện hột marker
        if (macongty != null && macongty != '0') {
            dieukien = "'MA_DVIQLY' like '" + macongty + "%'";
            $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/${macongty}`, function (data) {
                var X, Y;
                var i = 0;
                data.forEach(function (cotObj) {
                    X = cotObj.X_COT;
                    Y = cotObj.Y_COT;
                    i++;
                })
                if (i == 0) {
                    alert("Công ty này không có dữ liệu");
                } else {
                    HeatMapInit(dieukien, X, Y, 10, false, false);
                }
            });
        } else {
            //Lúc đầu vô khởi tạo bản đồ hột marker cho tổng miền trung
            $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/PC11`, function (data) {
                var X, Y;
                data.forEach(function (cotObj) {
                    X = cotObj.X_COT;
                    Y = cotObj.Y_COT;
                })
                HeatMapInit('', X, Y, 7, false, false);
            });
        }
    }
});

//
function initMap() {
    var options;
    if (madienluc == null || madienluc == '0') {
        zoom = 14;
    } else {
        if (matramnguon == null || matramnguon == '0' || maxuattuyen == null || maxuattuyen == '0') {
            zoom = 16;
        } else {
            zoom = 17;
        }
    }
    options = {
        zoom: zoom,
        center: { lat: mylat, lng: mylng }
    }

    map = new google.maps.Map(document.getElementById('map'), options);

    // Listen for click on map
    google.maps.event.addListener(map, 'click', function (event) {
        if (isBientap) {

        } else {
            vitrimarker_click = '';
            for (var i = 0; i < hashMapMarker.size; i++) {

                hashMapMarker.get(i).setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: tron_scale,
                    fillColor: hashMapMarker.get(i).icon.fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -2),
                    madviqly: hashMapMarker.get(i).icon.madviqly,
                    matramnguon: hashMapMarker.get(i).icon.matramnguon,
                    maxuattuyen: hashMapMarker.get(i).icon.maxuattuyen,
                    matram: hashMapMarker.get(i).icon.matram,
                    tentram: hashMapMarker.get(i).icon.tentram,
                    socot: hashMapMarker.get(i).icon.socot,
                    lkcot2: hashMapMarker.get(i).icon.lkcot2,
                    ishien: hashMapMarker.get(i).icon.ishien
                });

                //
                if (isHiensocot) {
                    if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                        if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                            hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                            hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                        } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                            hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                            hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                        } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                            hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: +  cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                            hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                        }
                    } else {
                        hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: + cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    }
                } else {
                    if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                        if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                            hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                            hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                        } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                            hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                            hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                        } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                            hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                            hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                        }
                    } else {
                        hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    }
                }
            }

            //
            if (madienluc == null || madienluc == '0') {
                //
            } else {
                if (matramnguon == null || matramnguon == '0') {

                    // Chon toi chinh xac toi xuat tuyen chinh
                    $("#I_maxuattuyen").show();
                    $("#I_maxuattuyen").text("Trạm công cộng: Đang cập nhập");
                    $("#I_maduongday").show();
                    $("#I_maduongday").text("Trạm chuyên dùng: Đang cập nhập");
                    $("#I_tenduongday").hide();
                    $("#I_mavitri").hide();
                    $("#I_tenvitri").hide();
                    $("#I_thongtinsocot").hide();
                    $("#I_toadoX").hide();
                    $("#I_toadoY").hide();
                    $("#I_loaicot").hide();
                    $("#I_chieucao").hide();
                    $("#I_nhasanxuat").hide();
                    $("#search-cot").show();

                    // // Lay so luong cot
                    // console.log(`${url}/api/TRUNGTHE_getSLCot_byMadviqly/${madienluc}`);
                    // $.get(`${url}/api/TRUNGTHE_getSLCot_byMadviqly/${madienluc}`, function (data) {
                    //     $.each(data, function (i, f) {
                    //         $("#tieudeI").text("I. Cột Trung Thế: " + f.SLUONG_COT.toLocaleString('en'));
                    //         $("#I_maxuattuyen").text("Trạm công cộng (Đã thu thập): " + f.SLUONG_TRAMCONGCONG);
                    //         $("#I_maduongday").text("Trạm chuyên dùng (Đã thu thập): " + f.SLUONG_TRAMCHUYENDUNG);
                    //     });
                    // });


                    // // Thiet lap lai tu dau
                    // $("#ul_thietbi").empty();
                    // $("#tieudeII").text("II. Thông Tin Thiết Bị: " + jsonKhachhangbandau.length.toLocaleString('en') + "");
                    // jsonKhachhang = jsonKhachhangbandau;
                    // var demthietbi = 0;
                    // $.each(jsonKhachhang, function (i, f) {
                    //     demthietbi++;
                    //     if (demthietbi < 500) {
                    //         $("#ul_thietbi").append(
                    //             `<li onclick="getitem('${f.MA_LOAITHIETBI}')>          
                    //               <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_LOAITHIETBI}</b><b style="float:right">PMIS</b></a><br>
                    //               <a>${f.MA_THIETBI}</a><br>
                    //               <a>${f.TEN_THIETBI}</a><br>
                    //              </li>
                    //              <div style="background-color:rgb(50,190,166);height:1px"></div>`
                    //         );
                    //     }
                    // });

                    // $("#hinhanh").empty();
                    // $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length.toLocaleString('en') + "");
                    // jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
                    // var demhinhanh = 0;
                    // $.each(jsonHinnhanh_cot, function (i, f) {
                    //     demhinhanh++;
                    //     if (demhinhanh < 500) {
                    //         $("#hinhanh").append(
                    //             `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                    //               <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                    //               </div>`
                    //         );
                    //     }
                    // });
                } else {
                    if (maxuattuyen == null || maxuattuyen == '0') {
                        // DOI KU DAT
                    } else {

                        // Chon toi chinh xac toi xuat tuyen chinh
                        $("#I_maxuattuyen").show();
                        $("#I_maxuattuyen").text("Trạm công cộng: Đang cập nhập");
                        $("#I_maduongday").show();
                        $("#I_maduongday").text("Trạm chuyên dùng: Đang cập nhập");
                        $("#I_tenduongday").hide();
                        $("#I_mavitri").hide();
                        $("#I_tenvitri").hide();
                        $("#I_thongtinsocot").hide();
                        $("#I_toadoX").hide();
                        $("#I_toadoY").hide();
                        $("#I_loaicot").hide();
                        $("#I_chieucao").hide();
                        $("#I_nhasanxuat").hide();
                        $("#search-cot").show();

                        // Lay so luong cot
                        //console.log(`${url}/api/HATHE_getSLThongtinkhachhang_byMadviqly/${madienluc}`);
                        $.get(`${url}/api/TRUNGTHE_getSLCot_byMaxuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
                            $.each(data, function (i, f) {
                                $("#tieudeI").text("I. Cột Trung Thế: " + f.SLUONG_COT.toLocaleString('en'));
                                $("#I_maxuattuyen").text("Trạm công cộng (Đã thu thập): " + f.SLUONG_TRAMCONGCONG);
                                $("#I_maduongday").text("Trạm chuyên dùng (Đã thu thập): " + f.SLUONG_TRAMCHUYENDUNG);
                            });
                        });


                        // Thiet lap lai tu dau
                        $("#ul_thietbi").empty();
                        $("#tieudeII").text("II. Thông Tin Thiết Bị: " + jsonKhachhangbandau.length.toLocaleString('en') + "");
                        jsonKhachhang = jsonKhachhangbandau;
                        var demthietbi = 0;
                        $.each(jsonKhachhang, function (i, f) {
                            demthietbi++;
                            if (demthietbi < 500) {
                                $("#ul_thietbi").append(
                                    `<li onclick="getitem('${f.MA_LOAITHIETBI}')>          
                                     <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_LOAITHIETBI}</b><b style="float:right">PMIS</b></a><br>
                                     <a>${f.MA_THIETBI}</a><br>
                                     <a>${f.TEN_THIETBI}</a><br>
                                    </li>
                                    <div style="background-color:rgb(50,190,166);height:1px"></div>`
                                );
                            }
                        });

                        //////////////////////////
                        $.get(`${url}/api/TRUNGTHE_getSLHinhanh_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
                            $.each(data, function (i, f) {
                                $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + f.SO_LUONG.toLocaleString('en') + "");
                            });
                        });
                        $("#hinhanh").empty();
                        $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length.toLocaleString('en') + "");
                        jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
                        $.each(jsonHinnhanh_cot, function (i, f) {
                            $("#hinhanh").append(
                                `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                                     <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                                     </div>`
                            );

                        });
                    }
                }
            }
        }
    });

    //when the map zoom changes, resize the icon based on the zoom level so the marker covers the same geographic area
    google.maps.event.addListener(map, 'zoom_changed', function () {
        if (isBientap) {
            bochuyendoiZoom(map.getZoom());
            for (var i = 0; i < hashMapMarker.size; i++) {
                //
                hashMapMarker.get(i).setDraggable(true);
                hashMapMarker.get(i).setIcon({
                    path: hinh_giotnuoc,
                    scale: tron_scale / 8,
                    fillColor: hashMapMarker.get(i).icon.fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -50),
                    madviqly: hashMapMarker.get(i).icon.madviqly, //1
                    matramnguon: hashMapMarker.get(i).icon.matramnguon, //2
                    maxuattuyen: hashMapMarker.get(i).icon.maxuattuyen, //3
                    matram: hashMapMarker.get(i).icon.matram, //4
                    tentram: hashMapMarker.get(i).icon.tentram, //5
                    socot: hashMapMarker.get(i).icon.socot, //6
                    lkcot2: hashMapMarker.get(i).icon.lkcot2, //7
                    ishien: 1 //8
                });

                //
                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.matramnguon };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                } else {
                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                }
            }
        } else {
            //
            if (macongty == null || macongty == '0') {
                // Tong
                bochuyendoiZoom(map.getZoom()); // khong cpo truong hop nao xay ra o day
            } else if (madienluc == null || madienluc == '0') {
                // Cong ty 
                bochuyendoiZoom(map.getZoom()); // khong cpo truong hop nao xay ra o day
            } else {
                // Dien luc
                if (map.getZoom() >= 16) {
                    clearOverlays();
                    bochuyendoiZoom(map.getZoom());
                    getMarker_lonhonbang16();
                } else if (map.getZoom() >= 15 && map.getZoom() < 16) {
                    clearOverlays();
                    bochuyendoiZoom(map.getZoom());
                    getMarker_doan15khoang16();
                } else {
                    clearOverlays();
                    bochuyendoiZoom(map.getZoom());
                    getMarker_nhohon15();
                }
            }
        }
    });

    google.maps.event.addListener(map, 'dragstart', function () {
        if (isBientap) {
            // khong lam chi ca
        } else {
            //
            if (macongty == null || macongty == '0') {
                //
            } else if (madienluc == null || madienluc == '0') {
                //
            } else {
                clearOverlays();
            }
        }
    });

    google.maps.event.addListener(map, 'dragend', function () {
        if (isBientap) {
            // khong lam chi ca
        } else {

            //
            if (macongty == null || macongty == '0') {
                //
            } else if (madienluc == null || madienluc == '0') {
                //
            } else {
                if (map.getZoom() >= 16) {
                    getMarker_lonhonbang16();
                } else if (map.getZoom() >= 15 && map.getZoom() < 16) {
                    getMarker_doan15khoang16();
                } else if (map.getZoom() < 15) {
                    getMarker_nhohon15();
                }
                //
                tramda_clickdouble  = [];
            }
        }
    });
    bochuyendoiZoom(map.getZoom());
    if (madienluc == null || madienluc == '0') {
        $("#isBientap").hide();
        $("#bientap").hide();
        $("#isBientapExcel").hide();
        $("#bientapexcel").hide();
        //
        $("#isBandonhiet").show();
        $("#bandonhiet").show();
    } else {
        // Chon xem tat ca tram nguon cua 1 dien luc:
        // Chi hien ra tram cong cong voi chuyen dung.
        // Khi kich vo tram moi hien ra duong day, hinh anh cua tram do
        if (matramnguon == null || matramnguon == '0') {
            // Chon toi chinh xac toi xuat tuyen chinh
            $("#tieudeI").text("I. Thông Tin Cột Trung Thế: 0");
            $("#tieudeII").text("II. Thông Tin Thiết Bị: 0");
            $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: 0");
            //
            $("#I_maxuattuyen").show();
            $("#I_maxuattuyen").text("Trạm công cộng: Đang cập nhập");
            $("#I_maduongday").show();
            $("#I_maduongday").text("Trạm chuyên dùng: Đang cập nhập");
            $("#I_tenduongday").hide();
            $("#I_mavitri").hide();
            $("#I_tenvitri").hide();
            $("#I_thongtinsocot").hide();
            $("#I_toadoX").hide();
            $("#I_toadoY").hide();
            $("#I_loaicot").hide();
            $("#I_chieucao").hide();
            $("#I_nhasanxuat").hide();

            //
            $("#isBientap").show(); // 1
            $("#bientap").show();
            $("#isBientap").prop("checked", false);
            isBientap = false;
            $("#isBientap").prop("disabled", false);
            $("#bientap").prop("disabled", false);

            $("#isBientapExcel").show(); // 2
            $("#bientapexcel").show();
            $("#isBientapExcel").prop("checked", false);
            isBientapExcel = false;
            $("#isBientapExcel").prop("disabled", false);
            $("#bientapexcel").prop("disabled", false);

            $("#socot").show(); // 3 
            $("#isSocot").show();
            $("#isSocot").prop("checked", false);
            isHiensocot = false;
            $("#isSocot").prop("disabled", false);
            $("#socot").prop("disabled", false);

            $("#isBandonhiet").hide(); //5
            $("#bandonhiet").hide(); //6
            $("#isBandonhiet").prop("checked", false);

            //
            $("#loading").show();
            // console.log("URL: "+ `${url}/api/TRUNGTHE_getDS1Cot_byMadviqly/${madienluc}`);
            $.get(`${url}/api/TRUNGTHE_getDS1Cot_byMadviqly/${madienluc}`, function (data) {

                //
                data.forEach(function (cotObj) {

                    // Di chuyen mac dinh cot dau tien
                    moveToLocation(parseFloat(cotObj.X_COT), parseFloat(cotObj.Y_COT));
                    clearOverlays();
                    if (zoom >= 16) {
                        getMarker_lonhonbang16();
                    }
                })

                // Lay so luong cot
                //console.log(`${url}/api/TRUNGTHE_getSLCot_byMadviqly/${madienluc}`);
                $.get(`${url}/api/TRUNGTHE_getSLCot_byMadviqly/${madienluc}`, function (data) {
                    $.each(data, function (i, f) {
                        $("#tieudeI").text("I. Cột Trung Thế: " + f.SLUONG_COT.toLocaleString('en'));
                        $("#I_maxuattuyen").text("Trạm công cộng (Đã thu thập): " + f.SLUONG_TRAMCONGCONG);
                        $("#I_maduongday").text("Trạm chuyên dùng (Đã thu thập): " + f.SLUONG_TRAMCHUYENDUNG);
                    });
                });

                $("#ul_thietbi").empty();
                //console.log("URL: "+`${url}/api/TRUNGTHE_getDSThietbicuacotPMIS_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`);
                $.get(`${url}/api/TRUNGTHE_getDSThietbicuacotPMIS_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
                    $("#tieudeII").text("II. Thông Tin Thiết Bị: " + data.length.toLocaleString('en') + "");
                    var demthietbi = 0;
                    $.each(data, function (i, f) {
                        jsonKhachhangbandau = data;
                        jsonKhachhang = jsonKhachhangbandau;
                        demthietbi++;
                        if (demthietbi < 500) {
                            $("#ul_thietbi").append(
                                `<li onclick="getitem('${f.MA_LOAITHIETBI}')>          
                                     <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_LOAITHIETBI}</b><b style="float:right">PMIS</b></a><br>
                                     <a>${f.MA_THIETBI}</a><br>
                                     <a>${f.TEN_THIETBI}</a><br>
                                 </li>
                                 <div style="background-color:rgb(50,190,166);height:1px"></div>`
                            );
                        }
                    });
                });


                /////////////////      
                $.get(`${url}/api/TRUNGTHE_getSLHinhanh_byMadviqly/${madienluc}`, function (data) {
                    $.each(data, function (i, f) {
                        $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + f.SO_LUONG.toLocaleString('en') + "");
                    });
                });

                // Lay so luong hinh anh
                $("#hinhanh").empty();
                console.log("URL: " + `${url}/api/TRUNGTHE_getDSHinhanh_byMadviqly/${madienluc}`);
                $.get(`${url}/api/TRUNGTHE_getDSHinhanh_byMadviqly/${madienluc}`, function (data) {
                    $.each(data, function (i, f) {
                        jsonHinnhanh_cotbandau = data;
                        jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
                        $("#hinhanh").append(
                            `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                         <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                        </div>`
                        );

                    });
                });
                $("#loading").hide();
            });
        } else {
            if (maxuattuyen == null || maxuattuyen == '0') {
                //
                //alert("BBBB");
            } else {
                // Chon toi chinh xac toi xuat tuyen chinh
                $("#tieudeI").text("I. Thông Tin Cột Trung Thế: 0");
                $("#tieudeII").text("II. Thông Tin Thiết Bị: 0");
                $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: 0");
                //
                $("#I_maxuattuyen").show();
                $("#I_maxuattuyen").text("Trạm công cộng: Đang cập nhập");
                $("#I_maduongday").show();
                $("#I_maduongday").text("Trạm chuyên dùng: Đang cập nhập");
                $("#I_tenduongday").hide();
                $("#I_mavitri").hide();
                $("#I_tenvitri").hide();
                $("#I_thongtinsocot").hide();
                $("#I_toadoX").hide();
                $("#I_toadoY").hide();
                $("#I_loaicot").hide();
                $("#I_chieucao").hide();
                $("#I_nhasanxuat").hide();

                //
                $("#isBientap").show(); // 1
                $("#bientap").show();
                $("#isBientap").prop("checked", false);
                isBientap = false;
                $("#isBientap").prop("disabled", false);
                $("#bientap").prop("disabled", false);

                $("#isBientapExcel").show(); // 2
                $("#bientapexcel").show();
                $("#isBientapExcel").prop("checked", false);
                isBientapExcel = false;
                $("#isBientapExcel").prop("disabled", false);
                $("#bientapexcel").prop("disabled", false);

                $("#socot").show(); // 3 
                $("#isSocot").show();
                $("#isSocot").prop("checked", false);
                isHiensocot = false;
                $("#isSocot").prop("disabled", false);
                $("#socot").prop("disabled", false);

                $("#isBandonhiet").hide(); //5
                $("#bandonhiet").hide(); //6
                $("#isBandonhiet").prop("checked", false);

                //
                $("#loading").show();
                //console.log("URL: "+ `${url}/api/TRUNGTHE_getDS1Cot_byMaxuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`);
                $.get(`${url}/api/TRUNGTHE_getDS1Cot_byMaxuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {

                    //
                    data.forEach(function (cotObj) {

                        // Di chuyen mac dinh cot dau tien
                        moveToLocation(parseFloat(cotObj.X_COT), parseFloat(cotObj.Y_COT));
                        clearOverlays();
                        if (zoom >= 16) {
                            getMarker_lonhonbang16();
                        }
                    })

                    // Lay so luong cot
                    // console.log(`${url}/api/TRUNGTHE_getSLCot_byMaxuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`);
                    $.get(`${url}/api/TRUNGTHE_getSLCot_byMaxuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
                        $.each(data, function (i, f) {
                            $("#tieudeI").text("I. Cột Trung Thế: " + f.SLUONG_COT.toLocaleString('en'));
                            $("#I_maxuattuyen").text("Trạm công cộng (Đã thu thập): " + f.SLUONG_TRAMCONGCONG);
                            $("#I_maduongday").text("Trạm chuyên dùng (Đã thu thập): " + f.SLUONG_TRAMCHUYENDUNG);
                        });
                    });

                    //
                    $("#ul_thietbi").empty();
                    console.log("URL: " + `${url}/api/TRUNGTHE_getDSThietbicuacotPMIS_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`);
                    $.get(`${url}/api/TRUNGTHE_getDSThietbicuacotPMIS_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
                        $("#tieudeII").text("II. Thông Tin Thiết Bị: " + data.length.toLocaleString('en') + "");
                        var demthietbi = 0;
                        $.each(data, function (i, f) {
                            jsonKhachhangbandau = data;
                            jsonKhachhang = jsonKhachhangbandau;
                            demthietbi++;
                            if (demthietbi < 500) {
                                $("#ul_thietbi").append(
                                    `<li onclick="getitem('${f.MA_LOAITHIETBI}')>          
                                        <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_LOAITHIETBI}</b><b style="float:right">PMIS</b></a><br>
                                        <a>${f.MA_THIETBI}</a><br>
                                        <a>${f.TEN_THIETBI}</a><br>
                                    </li>
                                    <div style="background-color:rgb(50,190,166);height:1px"></div>`
                                );
                            }
                        });
                    });


                    /////////////////      
                    $.get(`${url}/api/TRUNGTHE_getSLHinhanh_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
                        $.each(data, function (i, f) {
                            $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + f.SO_LUONG.toLocaleString('en') + "");
                        });
                    });

                    // Lay so luong hinh anh
                    $("#hinhanh").empty();
                    $.get(`${url}/api/TRUNGTHE_getDSHinhanh_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
                        $.each(data, function (i, f) {
                            jsonHinnhanh_cotbandau = data;
                            jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
                            $("#hinhanh").append(
                                `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                                 <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                                </div>`
                            );

                        });
                    });
                    $("#loading").hide();
                });
            }
        }
    }

    //Tạo nút hiển thị tọa độ hiện tại
    addYourLocationButton(map);
}

function addYourLocationButton(map) {
    var controlDiv = document.createElement('div');

    var firstChild = document.createElement('button');
    firstChild.style.backgroundColor = '#fff';
    firstChild.style.border = 'none';
    firstChild.style.outline = 'none';
    firstChild.style.width = '28px';
    firstChild.style.height = '28px';
    firstChild.style.borderRadius = '2px';
    firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
    firstChild.style.cursor = 'pointer';
    firstChild.style.marginLeft = '10px';
    firstChild.style.padding = '0px';
    firstChild.title = 'Your Location';
    controlDiv.appendChild(firstChild);

    var secondChild = document.createElement('div');
    secondChild.style.margin = '5px';
    secondChild.style.width = '18px';
    secondChild.style.height = '18px';
    secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-1x.png)';
    secondChild.style.backgroundSize = '180px 18px';
    secondChild.style.backgroundPosition = '0px 0px';
    secondChild.style.backgroundRepeat = 'no-repeat';
    secondChild.id = 'you_location_img';
    firstChild.appendChild(secondChild);

    firstChild.addEventListener('click', function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    title: 'Bạn đang ở đây'
                });
                var infowindow = new google.maps.InfoWindow({
                    content: 'Bạn đang ở đây',
                    position: pos
                });
                // Khi click vào Marker thì hiển thị
                infowindow.open(map, marker);
                moveToLocation(pos.lat, pos.lng);
            });
        } else {
            // Browser doesn't support Geolocation
        }
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(controlDiv);
}

// Hien thi thong tin moi cot
function ShowInfoMarker(map, marker, i) {

    $.get(`${url}/api/HATHE_getTentram_ByMatram/${marker.icon.madviqly}/${marker.icon.matram}`, function (data) {
        $.each(data, function (i, f) {
            X_COT = marker.getPosition().lat();
            Y_COT = marker.getPosition().lng();
            var myLatlng = new google.maps.LatLng(X_COT, Y_COT);

            var infowindow = new google.maps.InfoWindow({
                content: '<p class="ic-linkSupportRed">Tên trạm: ' + f.TEN_TRAM + '</p></br>' +
                    '<p class="ic-linkSupportRed">Mã trạm : ' + f.MA_TRAM + '</p>',
                position: myLatlng
            });


            marker = new google.maps.Marker({
                position: myLatlng,
                //map: map
            });

            // Khi click vào Marker thì hiển thị
            infowindow.open(map, marker);
        });
    });

}

function ShowInfoCot(cotObj) {
    var I_loaicot = QuyDoiI_loaicot(cotObj.LOAI_COT);
    var I_chieucao = QuyDoiI_chieucao(cotObj.CHIEU_CAO);
    var I_nhasanxuat = QuyDoiI_nhasanxuat(cotObj.NHA_SANXUAT)
    $("#I_matram").text("Mã trạm: " + cotObj.MA_TRAM);
    $("#I_maduongday").text("ID Đ.dây: " + cotObj.MA_DUONGDAY);
    $("#I_tenduongday").text("Tên Đ.dây: " + cotObj.TEN_DUONGDAY);
    $("#I_mavitri").text("ID V.trí: " + cotObj.MA_VITRI);
    $("#I_tenvitri").text("Tên V.trí: " + cotObj.TEN_VITRI);
    $("#I_thongtinsocot").text("Số cột: " + cotObj.SO_COT);
    $("#I_toadoX").text("Vĩ độ: " + cotObj.X_COT);
    $("#I_toadoY").text("Kinh độ: " + cotObj.Y_COT);
    $("#I_loaicot").text("Loại cột: " + I_loaicot);
    $("#I_chieucao").text("Chiều cao: " + I_chieucao);
    $("#I_nhasanxuat").text("Nhà sản xuất: " + I_nhasanxuat);
}

function QuyDoiI_loaicot(I_loaicot) {
    I_loaicot = I_loaicot.trim();
    if (I_loaicot == '' || I_loaicot == undefined) {
        return 'Không có thông tin';
    }
    var loai = new Array("", "Bê tông li tâm", "Bê tông chữ H", "Thép ống", "Gỗ", "Bê tông li tâm dự ứng lực", "Bê tông vuông", "Sắt",
        "Pylone", "Tháp thép");

    // console.log(I_loaicot+" :"+  loai[parseInt(I_loaicot)]+" hehe ");
    return loai[parseInt(I_loaicot)];
}

function QuyDoiI_chieucao(I_chieucao) {
    I_chieucao = I_chieucao.trim();

    if (I_chieucao == '' || I_chieucao == undefined) {
        return 'Không có thông tin';
    }
    var loai = new Array("", "Nhỏ hơn 7.5m", "8.4m", "8.5m", "10m", "10.5m", "12m", "14m",
        "16m", "18m", "20m");
    if (parseInt(I_chieucao) <= 10) {
        return loai[parseInt(I_chieucao)];
    } else {
        var h = parseInt(I_chieucao) + 10;
        return h + "m";
    }
}

function QuyDoiI_nhasanxuat(I_nhasanxuat) {
    I_nhasanxuat = I_nhasanxuat.trim();

    if (I_nhasanxuat == '' || I_nhasanxuat == undefined) {
        return 'Không có thông tin';
    }
    var loai = new Array("", "ABB", "Công ty CP TBĐ Sài Gòn", "CT TNHH KTĐ Việt Hùng",
        "Đại Long", "Đông Anh", "Đạt Hưng", "Tuấn Ân",
        "Việt Á", "Vinacomin", "Địa Phong", "Phương Minh", "Thiên Tân");
    return loai[parseInt(I_nhasanxuat)];
}

function GF_CONVERTTOENGLISH(stringInput) {
    var a_character = new Array("á", "à", "ả", "ã", "ạ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ");
    var A_character = new Array("A", "Á", "À", "Ả", "Ã", "Ạ", "Ă", "Ă", "Ằ", "Ẳ", "Ẵ", "Ặ", "Â", "Ấ", "Ầ", "Ẩ", "Ẫ", "Ậ");
    var d_character = new Array("đ");
    var D_character = new Array("D", "Đ");
    var e_character = new Array("é", "è", "ẽ", "ẹ", "ê", "ế", "ề", "ể", "ễ", "ệ");
    var E_character = new Array("E", "É", "È", "Ẽ", "Ẹ", "Ê", "Ế", "Ề", "Ể", "Ễ", "Ệ");
    var i_character = new Array("í", "ì", "ỉ", "ĩ", "ị");
    var I_character = new Array("I", "Í", "Ì", "Ỉ", "Ĩ", "Ị");
    var o_character = new Array("ó", "ò", "ỏ", "õ", "ọ", "ô", "ố", "ồ", "ổ", "ỗ", "ộ", "ơ", "ớ", "ờ", "ở", "ỡ", "ợ");
    var O_character = new Array("O", "Ó", "Ò", "Ỏ", "Õ", "Ọ", "Ô", "Ố", "Ồ", "ổ", "Ỗ", "Ộ", "Ơ", "Ớ", "Ờ", "Ở", "Ỡ", "Ợ");
    var u_character = new Array("ú", "ù", "ủ", "ũ", "ụ", "ư", "ứ", "ừ", "ử", "ữ", "ự");
    var U_character = new Array("U", "Ú", "Ù", "Ủ", "Ũ", "Ụ", "Ư", "Ứ", "Ừ", "Ử", "Ữ", "Ự");
    var y_character = new Array("ý", "ỳ", "ỷ", "ỹ", "ỵ");
    var Y_character = new Array("Y", "Ý", "Ỳ", "Ỷ", "Ỹ", "Ỵ");

    for (var i = 0; i < a_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, a_character[i], "a");
    }
    for (var i = 0; i < d_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, d_character[i], "d");
    }
    for (var i = 0; i < e_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, e_character[i], "e");
    }
    for (var i = 0; i < i_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, i_character[i], "i");
    }
    for (var i = 0; i < o_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, o_character[i], "o");
    }
    for (var i = 0; i < u_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, u_character[i], "u");
    }
    for (var i = 0; i < y_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, y_character[i], "y");
    }

    for (var i = 0; i < A_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, A_character[i], "a");
    }
    for (var i = 0; i < D_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, D_character[i], "d");
    }
    for (var i = 0; i < E_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, E_character[i], "e");
    }
    for (var i = 0; i < I_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, I_character[i], "i");
    }
    for (var i = 0; i < O_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, O_character[i], "o");
    }
    for (var i = 0; i < U_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, U_character[i], "u");
    }
    for (var i = 0; i < Y_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, Y_character[i], "y");
    }
    stringInput = stringInput.toLowerCase();
    return stringInput;
}

/* ----- BAT DAU BIEN TAP -----*/
var bientapControl = 0;
var Array_DSCOT;
var Array_DSHINHANH;
var oFile_COT;
var oFile_HINHANH;
var oFile_ImageUpload;

// BIEN TAP: TAO NUT BIÊN TẬP GIAO DIỆN 
function CenterControl_BIENTAPGIAODIEN(controlDiv, map, control_temp, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div) {

    //create button DSCot in google map when choose option "Biên tập"
    var controlUILUU = document.createElement('button');
    controlUILUU.style.borderRadius = '5px 5px 5px 5px';
    controlUILUU.style.border= '1px solid brown';
    controlUILUU.style.margin = '5px';
    controlUILUU.style.padding = '0px';
    controlUILUU.style.paddingTop = '1.8px';
    controlUILUU.style.width = '85px';
    controlUILUU.style.height = '35px';
    controlUILUU.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUILUU.style.textAlign = 'center';
    controlUILUU.style.fontSize = '13px';
    controlUILUU.style.fontWeight = 'bold';
    //controlUICOT.style.color = '#FFF';
    controlUILUU.innerHTML = 'Lưu';
    controlUILUU.onclick = function () {

    };
    controlUILUU.onmouseover = function () {
        controlUILUU.style.backgroundColor = 'aquamarine';
    };
    controlUILUU.onmouseleave = function () {
        controlUILUU.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUILUU);

    //create button DSHinh in google map when choose option "Biên tập"
    var controlUIHUY = document.createElement('button');
    controlUIHUY.style.borderRadius = '5px 5px 5px 5px';
    controlUIHUY.style.border= '1px solid brown';
    controlUIHUY.style.margin = '5px';
    controlUIHUY.style.padding = '0px';
    controlUIHUY.style.paddingTop = '1.8px';
    controlUIHUY.style.width = '85px';
    controlUIHUY.style.height = '35px';
    controlUIHUY.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIHUY.style.textAlign = 'center';
    controlUIHUY.style.fontSize = '13px';
    controlUIHUY.style.fontWeight = 'bold';
    controlUIHUY.style.color = 'brown';
    //controlUIHINH.style.color = '#FFF';
    controlUIHUY.innerHTML = 'Hủy';
    controlUIHUY.onclick = function () {
        controlDiv.appendChild(controlUICOT_div);
        controlDiv.appendChild(controlUIBTGIAODIEN_div);
        controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
     
        //centerControlDiv_BienTapGiaoDien = document.createElement('div');
        control_temp.click();
        //   control_temp.checked = false;
        isBientap = false;
    };
    controlUIHUY.onmouseover = function () {
        controlUIHUY.style.backgroundColor = 'aquamarine';
    };
    controlUIHUY.onmouseleave = function () {
        controlUIHUY.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIHUY);

    //create button DSImage in google map when choose option "Biên tập"
    var controlUIHDSD = document.createElement('button');
    controlUIHDSD.style.borderRadius = '5px 5px 5px 5px';
    controlUIHDSD.style.border= '1px solid brown';
    controlUIHDSD.style.margin = '5px';
    controlUIHDSD.style.padding = '0px';
    controlUIHDSD.style.paddingTop = '1.8px';
    controlUIHDSD.style.width = '85px';
    controlUIHDSD.style.height = '35px';
    controlUIHDSD.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIHDSD.style.textAlign = 'center';
    controlUIHDSD.style.fontSize = '13px';
    controlUIHDSD.style.fontWeight = 'bold';
    controlUIHDSD.style.color = 'brown';
    controlUIHDSD.innerHTML = 'HDSD';
    controlUIHDSD.onclick = function () {

    };
    controlUIHDSD.onmouseover = function () {
        controlUIHDSD.style.backgroundColor = 'aquamarine';
    };
    controlUIHDSD.onmouseleave = function () {
        controlUIHDSD.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIHDSD);

}

// BIEN TAP: TAO NUT BIÊN TẬP GIAO DIỆN EXCEL
function CenterControl(controlDiv, map, control_temp, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div) {

    //create button DSCot in google map when choose option "Biên tập"
    var controlUICOT = document.createElement('button');
    controlUICOT.style.borderRadius = '5px 5px 5px 5px';
    controlUICOT.style.border= '1px solid brown';
    controlUICOT.style.margin = '5px';
    controlUICOT.style.padding = '0px';
    controlUICOT.style.paddingTop = '1.8px';
    controlUICOT.style.width = '85px';
    controlUICOT.style.height = '35px';
    controlUICOT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUICOT.style.textAlign = 'center';
    controlUICOT.style.fontSize = '13px';
    controlUICOT.style.fontWeight = 'bold';
    //controlUICOT.style.color = '#FFF';
    controlUICOT.innerHTML = 'Excel Cột';
    controlUICOT.onclick = function () {
        localStorage['macongty_excel'] = macongty;
        localStorage['madienluc_excel'] = madienluc;
        localStorage['matramnguon_excel'] = matramnguon;
        localStorage['maxuattuyen_excel'] = maxuattuyen;
        var win = window.open("/luoidientrungthe_uploaddscot", '_blank');
        win.focus();
    };
    controlUICOT.onmouseover = function () {
        controlUICOT.style.backgroundColor = 'aquamarine';
    };
    controlUICOT.onmouseleave = function () {
        controlUICOT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUICOT);

    //create button DSHinh in google map when choose option "Biên tập"
    var controlUIHINH = document.createElement('button');
    controlUIHINH.style.borderRadius = '5px 5px 5px 5px';
    controlUIHINH.style.border= '1px solid brown';
    controlUIHINH.style.margin = '5px';
    controlUIHINH.style.padding = '0px';
    controlUIHINH.style.paddingTop = '1.8px';
    controlUIHINH.style.width = '85px';
    controlUIHINH.style.height = '35px';
    controlUIHINH.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIHINH.style.textAlign = 'center';
    controlUIHINH.style.fontSize = '13px';
    controlUIHINH.style.fontWeight = 'bold';
    //controlUIHINH.style.color = '#FFF';
    controlUIHINH.innerHTML = 'Excel Hình';
    controlUIHINH.onclick = function () {
        // document.getElementById('id09').style.display = 'block';
        // alert("HINHANH");
        localStorage['macongty_excel'] = macongty;
        localStorage['madienluc_excel'] = madienluc;
        localStorage['matramnguon_excel'] = matramnguon;
        localStorage['maxuattuyen_excel'] = maxuattuyen;
        var win = window.open("/luoidientrungthe_uploaddslinkhinhanh", '_blank');
        win.focus();
        // document.location.href = "/luoidienhathe_uploaddslinkhinhanh";
    };
    controlUIHINH.onmouseover = function () {
        controlUIHINH.style.backgroundColor = 'aquamarine';
    };
    controlUIHINH.onmouseleave = function () {
        controlUIHINH.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIHINH);

    //create button DSImage in google map when choose option "Biên tập"
    var controlUIImage = document.createElement('button');
    controlUIImage.style.borderRadius = '5px 5px 5px 5px';
    controlUIImage.style.border= '1px solid brown';
    controlUIImage.style.margin = '5px';
    controlUIImage.style.padding = '0px';
    controlUIImage.style.paddingTop = '1.8px';
    controlUIImage.style.width = '85px';
    controlUIImage.style.height = '35px';
    controlUIImage.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIImage.style.textAlign = 'center';
    controlUIImage.style.fontSize = '13px';
    controlUIImage.style.fontWeight = 'bold';
    //controlUIImage.style.color = '#FFF';
    controlUIImage.innerHTML = 'Hình Ảnh';
    controlUIImage.onclick = function () {

        var win = window.open("/luoidientrungthe_uploadhinhanh", '_blank');
        win.focus();
    };
    controlUIImage.onmouseover = function () {
        controlUIImage.style.backgroundColor = 'aquamarine';
    };
    controlUIImage.onmouseleave = function () {
        controlUIImage.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIImage);

    //create button HSSD in google map when choose option "Biên tập"
    var controlUIHDSD = document.createElement('button');
    controlUIHDSD.style.borderRadius = '5px 5px 5px 5px';
    controlUIHDSD.style.border= '1px solid brown';
    controlUIHDSD.style.margin = '5px';
    controlUIHDSD.style.padding = '0px';
    controlUIHDSD.style.paddingTop = '1.8px';
    controlUIHDSD.style.width = '85px';
    controlUIHDSD.style.height = '35px';
    controlUIHDSD.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIHDSD.style.textAlign = 'center';
    controlUIHDSD.style.fontSize = '13px';
    controlUIHDSD.style.fontWeight = 'bold';
    controlUIHDSD.style.color = 'brown';
    controlUIHDSD.innerHTML = 'HDSD';
    controlUIHDSD.onclick = function () {
        var win = window.open("http://10.72.96.92/Mobile_GIS/evncpc/evncpc_ttht/7.%20H%C6%B0%E1%BB%9Bng%20d%E1%BA%ABn%20x%E1%BB%AD%20l%C3%BD%20khi%20g%E1%BB%ADi%20d%E1%BB%AF%20li%E1%BB%87u%20l%E1%BB%97i%20b%E1%BA%B1ng%20file%20Excel.mp4", '_blank');
        win.focus();
    };
    controlUIHDSD.onmouseover = function () {
        controlUIHDSD.style.backgroundColor = 'aquamarine';
    };
    controlUIHDSD.onmouseleave = function () {
        controlUIHDSD.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIHDSD);
    var controlUIOUT = document.createElement('button');
    controlUIOUT.style.borderRadius = '5px 5px 5px 5px';
    controlUIOUT.style.border = '1px solid brown';
    controlUIOUT.style.margin = '5px';
    controlUIOUT.style.padding = '0px';
    controlUIOUT.style.paddingTop = '1.8px';
    controlUIOUT.style.width = '85px';
    controlUIOUT.style.height = '35px';
    controlUIOUT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIOUT.style.textAlign = 'center';
    controlUIOUT.style.fontSize = '13px';
    controlUIOUT.style.fontWeight = 'bold';
    controlUIOUT.style.color = 'brown';
    controlUIOUT.innerHTML = 'Thoát';
    controlUIOUT.onmouseover=function(){
        controlUIOUT.style.backgroundColor = 'aquamarine';
        controlUIOUT.style.color = 'brown';
    };
    controlUIOUT.onmouseleave=function(){
        controlUIOUT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        controlUIOUT.style.color = 'brown';
    };
    controlUIOUT.onclick = function () {
        while (controlDiv.firstChild) {
            // console.log("CHan qua di");
            controlDiv.removeChild(controlDiv.firstChild);
        }

        controlDiv.appendChild(controlUICOT_div);
        controlDiv.appendChild(controlUIBTGIAODIEN_div);
        controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
      
        //centerControlDiv_BienTapGiaoDien = document.createElement('div');
        control_temp.click();

        isBientapExcel = false;
    };
    controlDiv.appendChild(controlUIOUT);
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
/* ----- KET THUC BIEN TAP -----*/

// Add Marker Function
function addMarker(cotObj, i) {

    //
    if (isHiensocot) {
        marker = new google.maps.Marker({
            position: { lat: parseFloat(cotObj.X_COT), lng: parseFloat(cotObj.Y_COT) },
            map: map,
            draggable: false,
            label: { color: 'black', fontSize: '' + cochu + 'px', text: cotObj.SO_COT },
            ishien: 1
        });
    } else {
        marker = new google.maps.Marker({
            position: { lat: parseFloat(cotObj.X_COT), lng: parseFloat(cotObj.Y_COT) },
            map: map,
            draggable: false,
            label: { color: 'black', fontSize: '' + cochu + 'px', text: ' ' },
        });

        //
        if (cotObj.LK_COT2 == GV_THUOCTINH_TRAMNGUON) {
            marker.label = { color: GV_COLOR_XANHVIENDEN, fontSize: + cochu_chotram + 'px', text: cotObj.MA_TRAMNGUON };
        } else if (cotObj.LK_COT2 == GV_THUOCTINH_TRAMCONGCONG) {
            marker.label = { color: GV_COLOR_TIMVIENDEN, fontSize: + cochu_chotram + 'px', text: cotObj.SO_COT + ": " + cotObj.MA_TRAM + " - " + cotObj.TEN_TRAM };
        } else if (cotObj.LK_COT2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
            marker.label = { color: GV_COLOR_HONGVIENDEN, fontSize: + cochu_chotram + 'px', text: cotObj.SO_COT + ": " + cotObj.MA_TRAM + " - " + cotObj.TEN_TRAM };
        } else {
            marker.label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: cotObj.SO_COT };
        }
    }
    hashMapMarker.set(i, marker);
    arrayMarker.push(marker);

    // Check for customicon
    switch (cotObj.LK_COT2) {
        case GV_THUOCTINH_COT:
            switch (cotObj.TINH_CHAT) {
                case GV_THUOCTINH_COTHATHE:
                    var icon = {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: GV_COLOR_VANGVIENDEN,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: cotObj.MA_DVIQLY,
                        matramnguon: cotObj.MA_TRAMNGUON,
                        maxuattuyen: cotObj.MA_XUATTUYEN,
                        matram: cotObj.MA_TRAM,
                        tentram: cotObj.TEN_TRAM,
                        socot: cotObj.SO_COT,
                        lkcot2: cotObj.LK_COT2,
                        ishien: 0,
                        mavitri: cotObj.MA_VITRI
                    };
                    marker.setIcon(icon);
                    break;
                case GV_THUOCTINH_COTHATHETRUNGTHE_KETHOP:
                    var icon = {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: GV_COLOR_CAMVIENDEN,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: cotObj.MA_DVIQLY,
                        matramnguon: cotObj.MA_TRAMNGUON,
                        maxuattuyen: cotObj.MA_XUATTUYEN,
                        matram: cotObj.MA_TRAM,
                        tentram: cotObj.TEN_TRAM,
                        socot: cotObj.SO_COT,
                        lkcot2: cotObj.LK_COT2,
                        ishien: 0,
                        mavitri: cotObj.MA_VITRI
                    };
                    marker.setIcon(icon);
                    break;
                case GV_THUOCTINH_TUDIENNGAMHATHE:
                    var icon = {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: GV_COLOR_XAMNHATVIENDEN,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: cotObj.MA_DVIQLY,
                        matramnguon: cotObj.MA_TRAMNGUON,
                        maxuattuyen: cotObj.MA_XUATTUYEN,
                        matram: cotObj.MA_TRAM,
                        tentram: cotObj.TEN_TRAM,
                        socot: cotObj.SO_COT,
                        lkcot2: cotObj.LK_COT2,
                        ishien: 0,
                        mavitri: cotObj.MA_VITRI
                    };
                    marker.setIcon(icon);
                    break;
                case GV_THUOCTINH_COTTRUNGTHE:
                    var icon = {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: GV_COLOR_DOVIENDEN,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: cotObj.MA_DVIQLY,
                        matramnguon: cotObj.MA_TRAMNGUON,
                        maxuattuyen: cotObj.MA_XUATTUYEN,
                        matram: cotObj.MA_TRAM,
                        tentram: cotObj.TEN_TRAM,
                        socot: cotObj.SO_COT,
                        loaicot: cotObj.LOAI_COT,
                        chieucao: cotObj.CHIEU_CAO,
                        lkcot2: cotObj.LK_COT2,
                        ishien: 0,
                        mavitri: cotObj.MA_VITRI,
                    };
                    marker.setIcon(icon);
                    break;
                case GV_THUOCTINH_RMU:
                    var icon = {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: GV_COLOR_XAMDAMVIENDEN,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: cotObj.MA_DVIQLY,
                        matramnguon: cotObj.MA_TRAMNGUON,
                        maxuattuyen: cotObj.MA_XUATTUYEN,
                        matram: cotObj.MA_TRAM,
                        tentram: cotObj.TEN_TRAM,
                        socot: cotObj.SO_COT,
                        loaicot: cotObj.LOAI_COT,
                        chieucao: cotObj.CHIEU_CAO,
                        lkcot2: cotObj.LK_COT2,
                        ishien: 0,
                        mavitri: cotObj.MA_VITRI,
                    };
                    marker.setIcon(icon);
                    break;
            }
            break;
        case GV_THUOCTINH_TRAMNGUON:
            var icon = {
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: GV_COLOR_XANHVIENDEN,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                madviqly: cotObj.MA_DVIQLY,
                matramnguon: cotObj.MA_TRAMNGUON,
                maxuattuyen: cotObj.MA_XUATTUYEN,
                matram: cotObj.MA_TRAM,
                tentram: cotObj.TEN_TRAM,
                socot: cotObj.SO_COT,
                lkcot2: cotObj.LK_COT2,
                ishien: 0,
                mavitri: cotObj.MA_VITRI
            };
            marker.setIcon(icon);
            break;
        case GV_THUOCTINH_TRAMCONGCONG:
            var icon = {
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: GV_COLOR_TIMVIENDEN,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                madviqly: cotObj.MA_DVIQLY,
                matramnguon: cotObj.MA_TRAMNGUON,
                maxuattuyen: cotObj.MA_XUATTUYEN,
                matram: cotObj.MA_TRAM,
                tentram: cotObj.TEN_TRAM,
                socot: cotObj.SO_COT,
                lkcot2: cotObj.LK_COT2,
                ishien: 0,
                mavitri: cotObj.MA_VITRI
            };
            marker.setIcon(icon);
            break;
        case GV_THUOCTINH_TRAMCHUYENDUNG:
            var icon = {
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: GV_COLOR_HONGVIENDEN,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                madviqly: cotObj.MA_DVIQLY,
                matramnguon: cotObj.MA_TRAMNGUON,
                maxuattuyen: cotObj.MA_XUATTUYEN,
                matram: cotObj.MA_TRAM,
                tentram: cotObj.TEN_TRAM,
                socot: cotObj.SO_COT,
                lkcot2: cotObj.LK_COT2,
                ishien: 0,
                mavitri: cotObj.MA_VITRI,
            };
            marker.setIcon(icon);
            break;
        case GV_THUOCTINH_DCU:
            // 
            var icon = {
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: GV_COLOR_XANHLACAYVIENDEN,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                madviqly: cotObj.MA_DVIQLY,
                matramnguon: cotObj.MA_TRAMNGUON,
                maxuattuyen: cotObj.MA_XUATTUYEN,
                matram: cotObj.MA_TRAM,
                tentram: cotObj.TEN_TRAM,
                socot: cotObj.SO_COT,
                lkcot2: cotObj.LK_COT2,
                ishien: 0,
                mavitri: cotObj.MA_VITRI,
            };
            marker.setIcon(icon);
            break;
        case GV_THUOCTINH_ROUTER:
            var icon = {
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: GV_COLOR_XANHDATROIVIENDEN,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                madviqly: cotObj.MA_DVIQLY,
                matramnguon: cotObj.MA_TRAMNGUON,
                maxuattuyen: cotObj.MA_XUATTUYEN,
                matram: cotObj.MA_TRAM,
                tentram: cotObj.TEN_TRAM,
                socot: cotObj.SO_COT,
                lkcot2: cotObj.LK_COT2,
                ishien: 0,
                mavitri: cotObj.MA_VITRI,
            };
            marker.setIcon(icon);
            break;
        default:
            var icon = {
                path: google.maps.SymbolPath.CIRCLE,
                scale: tron_scale,
                fillColor: GV_COLOR_MAUDEN,
                fillOpacity: 1,
                strokeWeight: tron_stroke,
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0), // anchor
                labelOrigin: new google.maps.Point(0, -2),
                madviqly: cotObj.MA_DVIQLY,
                matramnguon: cotObj.MA_TRAMNGUON,
                maxuattuyen: cotObj.MA_XUATTUYEN,
                matram: cotObj.MA_TRAM,
                tentram: cotObj.TEN_TRAM,
                socot: cotObj.SO_COT,
                lkcot2: cotObj.LK_COT2,
                ishien: 0,
                mavitri: cotObj.MA_VITRI,
            };
            marker.setIcon(icon);
    }

    // Allow each marker to have an info window    
    google.maps.event.addListener(marker, 'click', (function (marker, i) {
        return function () {
            if (isBientap) {
                alert("Bạn đang ở chế độ chỉnh sửa.");
            } else {

                // toandtb
                vitrimarker_click = i;
                //
                if (hashMapMarker_truoc.size == 1) {
                    hashMapMarker_truoc.get(0).setIcon({
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: hashMapMarker_truoc.get(0).getIcon().fillColor,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: hashMapMarker_truoc.get(0).getIcon().madviqly, //1
                        matramnguon: hashMapMarker_truoc.get(0).getIcon().matramnguon, //2
                        maxuattuyen: hashMapMarker_truoc.get(0).getIcon().maxuattuyen, //3
                        matram: hashMapMarker_truoc.get(0).getIcon().matram, //4
                        tentram: hashMapMarker_truoc.get(0).getIcon().tentram, //5
                        socot: hashMapMarker_truoc.get(0).getIcon().socot, //6
                        lkcot2: hashMapMarker_truoc.get(0).getIcon().lkcot2, //7
                        ishien: hashMapMarker_truoc.get(0).getIcon().ishien, // 8
                        mavitri: hashMapMarker_truoc.get(0).getIcon().mavitri, //9
                    });
                }

                //
                marker.setIcon({
                    path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                    scale: tron_scale,
                    fillColor: marker.getIcon().fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -2),
                    madviqly: marker.getIcon().madviqly, //1
                    matramnguon: marker.getIcon().matramnguon, //2
                    maxuattuyen: marker.getIcon().maxuattuyen, //3
                    matram: marker.getIcon().matram, //4
                    tentram: marker.getIcon().tentram, //5
                    socot: marker.getIcon().socot, //6
                    lkcot2: marker.getIcon().lkcot2, //7
                    ishien: 1, //8,
                    mavitri: marker.getIcon().mavitri, //9
                });
                marker.label.fontSize = cochu + 'px';
                marker.icon.labelOrigin = new google.maps.Point(0, -6);
                hashMapMarker_truoc.set(0, marker);
                w3_open();

                //
                $("#tieudeI").text("I. Thông Tin Cột Trung Thế: 1");
                $("#ul_thietbi").empty();
                $.get(`${url}/api/TRUNGTHE_getDSThietbicuacot_byMavitri/${cotObj.MA_DVIQLY}/${cotObj.MA_VITRI}`, function (data) {
                    $("#tieudeII").text("II. Thông tin thiết bị: " + data.length + "");
                    $.each(data, function (i, f) {
                        jsonKhachhang = data;
                        $("#ul_thietbi").append(
                            `<li onclick="getitem('${f.MA_LOAITHIETBI}')>          
                                <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_LOAITHIETBI}</b><b style="float:right">PMIS</b></a><br>
                                <a>${f.MA_THIETBI}</a><br>
                                <a>${f.TEN_THIETBI}</a><br>
                            </li>
                            <div style="background-color:rgb(50,190,166);height:1px"></div>`
                        );
                    });
                });

                //
                var matram_tam;
                if (marker.icon.matram == null || marker.icon.matram == "" || marker.icon.matram == undefined) {
                    matram_tam = "%20";
                } else {
                    matram_tam = marker.icon.matram;
                }

                //console.log("URL: " + `${url}/api/TRUNGTHE_getDSHinhanh_bySocot/${marker.icon.madviqly}/${marker.icon.matramnguon}/${marker.icon.maxuattuyen}/${matram_tam}/${marker.icon.socot.split("/").join("%2f")}`);
                $("#hinhanh").empty();
                $.get(`${url}/api/TRUNGTHE_getDSHinhanh_bySocot/${marker.icon.madviqly}/${marker.icon.matramnguon}/${marker.icon.maxuattuyen}/${matram_tam}/${marker.icon.socot.split("/").join("%2f")}`, function (data) {
                    $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + data.length + "");
                    jsonHinnhanh_cot = data;
                    $.each(jsonHinnhanh_cot, function (i, f) {
                        $("#hinhanh").append(
                            `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                              <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                             </div>`
                        );
                    });
                });
            }

            ShowInfoCot(cotObj);
            //
            if (madienluc == null || madienluc == '0') {
                zoom = 14;
            } else {
                if (matramnguon == null || matramnguon == '0') {
                    //$("#I_matram").show();
                    //$("#I_thongtinsocot").show();
                    //$("#I_toadoX").show();
                    //$("#I_toadoY").show();
                    //$("#I_loaicot").show();
                    //$("#I_chieucao").show();
                    //$("#I_nhasanxuat").show();
                } else {
                    if (maxuattuyen == null || maxuattuyen == '0') {

                    } else {
                        $("#I_maxuattuyen").hide();
                        $("#I_maduongday").show();
                        $("#I_tenduongday").show();
                        $("#I_mavitri").show();
                        $("#I_tenvitri").show();
                        $("#I_thongtinsocot").show();
                        $("#I_toadoX").show();
                        $("#I_toadoY").show();
                        $("#I_loaicot").show();
                        $("#I_chieucao").show();
                        $("#I_nhasanxuat").show();
                    }
                }
            }
        }
    })(marker, i));

    google.maps.event.addListener(marker, 'rightclick', (function (marker, i) {
        return function () {
            if (isBientap) {
                alert("Bạn đang ở chế độ chỉnh sửa.");
            } else {
                // 
                if (madienluc == null || madienluc == '0') {
                    //
                } else { // chon nhieu tram trong 1 dien luc             
                    if (matramnguon == null || matramnguon == '0' || maxuattuyen == null || maxuattuyen == '0') {
                        // Khong lam chi ca
                        //if (marker.icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                        //    ShowInfoMarker(map, marker, i);
                        //}

                    } else { 
                        // truong hop chon chinh xac toi tram
                        var is_clickroi = false;
                        for (var i = 0; i < tramda_clickdouble.length; i++) {
                            if (tramda_clickdouble[i] == marker.icon.matram) {
                                is_clickroi = true;
                            }
                        }
                        //
                        if (is_clickroi) {
                            //
                            if (marker.icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG || marker.icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                                // toandtb
                                vitrimarker_click = i;
                                //
                                if (hashMapMarker_truoc.size == 1) {
                                    hashMapMarker_truoc.get(0).setIcon({
                                        path: google.maps.SymbolPath.CIRCLE,
                                        scale: tron_scale,
                                        fillColor: hashMapMarker_truoc.get(0).getIcon().fillColor,
                                        fillOpacity: 1,
                                        strokeWeight: tron_stroke,
                                        origin: new google.maps.Point(0, 0), // origin
                                        anchor: new google.maps.Point(0, 0), // anchor
                                        labelOrigin: new google.maps.Point(0, -2),
                                        madviqly: hashMapMarker_truoc.get(0).getIcon().madviqly, //1
                                        matram: hashMapMarker_truoc.get(0).getIcon().matram, //2
                                        tentram: hashMapMarker_truoc.get(0).getIcon().tentram, //3
                                        socot: hashMapMarker_truoc.get(0).getIcon().socot, //4
                                        lkcot2: hashMapMarker_truoc.get(0).getIcon().lkcot2, //5
                                        ishien: hashMapMarker_truoc.get(0).getIcon().ishien,//6
                                        mavitri: hashMapMarker_truoc.get(0).getIcon().mavitri
                                    });
                                }

                                //
                                marker.setIcon({
                                    path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                                    scale: tron_scale,
                                    fillColor: marker.getIcon().fillColor,
                                    fillOpacity: 1,
                                    strokeWeight: tron_stroke,
                                    origin: new google.maps.Point(0, 0), // origin
                                    anchor: new google.maps.Point(0, 0), // anchor
                                    labelOrigin: new google.maps.Point(0, -2),
                                    madviqly: marker.getIcon().madviqly, //1
                                    matram: marker.getIcon().matram, //2
                                    tentram: marker.getIcon().tentram, //3
                                    socot: marker.getIcon().socot, //4
                                    lkcot2: marker.getIcon().lkcot2, //5
                                    ishien: 1, //5,
                                    mavitri: marker.getIcon().mavitri
                                });
                                marker.label.fontSize = cochu + 'px';
                                marker.icon.labelOrigin = new google.maps.Point(0, -6);
                                hashMapMarker_truoc.set(0, marker);
                                w3_open();

                                //
                                $("#ul_thietbi").empty();
                                $.get(`${url}/api/get_DS_Thongtinkhachhang_byTram/${marker.icon.madviqly}/${marker.icon.matram}`, function (data) {
                                    $("#tieudeI").text("I. Thông Tin Thiết Bị: " + data.length + " KH");
                                    $.each(data, function (i, f) {
                                        jsonKhachhang = data;
                                        $("#ul_thietbi").append(
                                            `<li onclick="getitem('${f.MA_KHANG}')">
                                             <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_KHANG}</b></a><br>
                                             <a>${f.TEN_KHANG}</a><br>
                                             <a>${f.DCHI_KHANG}</a><br>
                                         </li>
                                         <div style="background-color:rgb(50,190,166);height:1px"></div>`
                                        );
                                    });
                                });

                                //
                                $("#hinhanh").empty();
                                //console.log("URL: " + `${url}/api/HATHE_getDSHinhanh_bySocot/${marker.icon.madviqly}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${marker.icon.matram}/${marker.icon.socot.split("/").join("%2f")}`);
                                $.get(`${url}/api/HATHE_getDSHinhanhchocot_byTram/${marker.icon.madviqly}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${marker.icon.matram}`, function (data) {
                                    $("#tieudeII").text("II. Hình Ảnh Hiện Trường: " + data.length + " ảnh");
                                    jsonHinnhanh_cot = data;
                                    $.each(jsonHinnhanh_cot, function (i, f) {
                                        $("#hinhanh").append(
                                            `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                                             <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                                         </div>`
                                        );
                                    });
                                });
                            } else {
                                // khong lam chi ca. vi khong phai cot chua tram
                            }
                        } else {
                            if (marker.icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG || marker.icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                                // toandtb
                                vitrimarker_click = i;
                                //
                                if (hashMapMarker_truoc.size == 1) {
                                    hashMapMarker_truoc.get(0).setIcon({
                                        path: google.maps.SymbolPath.CIRCLE,
                                        scale: tron_scale,
                                        fillColor: hashMapMarker_truoc.get(0).getIcon().fillColor,
                                        fillOpacity: 1,
                                        strokeWeight: tron_stroke,
                                        origin: new google.maps.Point(0, 0), // origin
                                        anchor: new google.maps.Point(0, 0), // anchor
                                        labelOrigin: new google.maps.Point(0, -2),
                                        madviqly: hashMapMarker_truoc.get(0).getIcon().madviqly, //1
                                        matram: hashMapMarker_truoc.get(0).getIcon().matram, //2
                                        tentram: hashMapMarker_truoc.get(0).getIcon().tentram, //3
                                        socot: hashMapMarker_truoc.get(0).getIcon().socot, //4
                                        lkcot2: hashMapMarker_truoc.get(0).getIcon().lkcot2, //5
                                        ishien: hashMapMarker_truoc.get(0).getIcon().ishien, //6,
                                        mavitri: hashMapMarker_truoc.get(0).getIcon().mavitri
                                    });
                                }

                                //
                                marker.setIcon({
                                    path: google.maps.SymbolPath.BACKWARD_CLOSED_ARROW,
                                    scale: tron_scale,
                                    fillColor: marker.getIcon().fillColor,
                                    fillOpacity: 1,
                                    strokeWeight: tron_stroke,
                                    origin: new google.maps.Point(0, 0), // origin
                                    anchor: new google.maps.Point(0, 0), // anchor
                                    labelOrigin: new google.maps.Point(0, -2),
                                    madviqly: marker.getIcon().madviqly, //1
                                    matram: marker.getIcon().matram, //2
                                    tentram: marker.getIcon().tentram, //3
                                    socot: marker.getIcon().socot, //4
                                    lkcot2: marker.getIcon().lkcot2, //5
                                    ishien: 1, //5
                                    mavitri: marker.getIcon().mavitri
                                });
                                marker.label.fontSize = cochu + 'px';
                                marker.icon.labelOrigin = new google.maps.Point(0, -6);
                                hashMapMarker_truoc.set(0, marker);
                                w3_open();
                                // Click vo thi hien cot cua tram ra
                                $("#loading").show();
                                tramda_clickdouble.push(marker.icon.matram);
                                //console.log(`${url}/api/HATHE_getDSCot_byTramhathe/${madienluc}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${matram}`);
                                $.getJSON(`${url}/api/HATHE_getDSCot_byTramhathe/${madienluc}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${marker.icon.matram}`, function (data) {
                                    data.forEach(function (cotObj) {
                                        if (cotObj.LK_COT2 != GV_THUOCTINH_TRAMCONGCONG) {
                                            stt_marker++;
                                            addMarker(cotObj, stt_marker);
                                        }

                                        //
                                        if (cotObj.LK_COT1 == "") {
                                            // Khong lam chi ca
                                        } else {
                                            var mangsocot = cotObj.LK_COT1.split('$');
                                            for (var j = 0; j < mangsocot.length; j++) {
                                                var cotnoi = data.find(function (obj) { return obj.SO_COT === mangsocot[j]; });
                                                if (cotnoi != null) {
                                                    //alert(cotnoi.SO_COT);
                                                    if (cotObj.MA_TRAM == cotnoi.MA_TRAM) {
                                                        addLine(cotObj, i, cotnoi);
                                                    }
                                                }
                                            }
                                        }
                                    })

                                    $("#ul_thietbi").empty();
                                    $.get(`${url}/api/get_DS_Thongtinkhachhang_byTram/${marker.icon.madviqly}/${marker.icon.matram}`, function (data) {
                                        $("#tieudeI").text("I. Thông Tin Thiết Bị: " + data.length + " KH");
                                        $.each(data, function (i, f) {
                                            jsonKhachhang = data;
                                            $("#ul_thietbi").append(
                                                `<li onclick="getitem('${f.MA_KHANG}')">
                                             <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_KHANG}</b></a><br>
                                             <a>${f.TEN_KHANG}</a><br>
                                             <a>${f.DCHI_KHANG}</a><br>
                                         </li>
                                         <div style="background-color:rgb(50,190,166);height:1px"></div>`
                                            );
                                        });
                                    });

                                    $("#hinhanh").empty();
                                    //console.log("URL: " + `${url}/api/HATHE_getDSHinhanh_bySocot/${marker.icon.madviqly}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${marker.icon.matram}/${marker.icon.socot.split("/").join("%2f")}`);
                                    $.get(`${url}/api/HATHE_getDSHinhanhchocot_byTram/${marker.icon.madviqly}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${marker.icon.matram}`, function (data) {
                                        $("#tieudeII").text("II. Hình Ảnh Hiện Trường: " + data.length + " ảnh");
                                        jsonHinnhanh_cot = data;
                                        $.each(jsonHinnhanh_cot, function (i, f) {
                                            $("#hinhanh").append(
                                                `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                                             <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                                         </div>`
                                            );
                                        });
                                    });
                                    $("#loading").hide();
                                });
                            } else {
                                // khong lam chi ca
                            }
                        }
                    }
                }
            }
        }
    })(marker, i));

    // Drag 1 
    google.maps.event.addListener(marker, 'dragstart', (function (marker, i) {
        return function () {
            if (isBientap) {
                cotgoc = new Object(listCottrungthe.find(function (obj) { return obj.SO_COT === marker.icon.socot; }));
            } else {
                alert("dragstart.");
            }
        }
    })(marker, i));

    // Drag 2 
    google.maps.event.addListener(marker, 'drag', (function (marker, i) {
        return function () {
            if (isBientap) {
                dichchuyen(cotgoc, i, marker);
            } else {
                alert("drag.");
            }
        }
    })(marker, i));

    // Drag 3 
    google.maps.event.addListener(marker, 'dragend', (function (marker, i) {
        return function () {
            if (isBientap) {
                hashMapMarker.set(i, marker);
                listCottrungthe[i].X_COT = marker.getPosition().lat();
                listCottrungthe[i].Y_COT = marker.getPosition().lng();
                // chua luu nen khong set l
            } else {
                alert("dragend.");
            }
        }
    })(marker, i));
}

// Add duong day: taoLienketCot_1_1
function addLine(cotObjgoc, vitri_cotdau, cotObjnoi) {
    var points = [new google.maps.LatLng(parseFloat(cotObjgoc.X_COT), parseFloat(cotObjgoc.Y_COT)), new google.maps.LatLng(parseFloat(cotObjnoi.X_COT), parseFloat(cotObjnoi.Y_COT))];
    var polyline = new google.maps.Polyline({ map: map, path: points, strokeColor: GV_COLOR_DOVIENDEN, strokeWeight: 1, strokeOpacity: 1 });
    //
    var key = vitri_cotdau + "_" + cotObjnoi.SO_COT;
    //console.log("AAA: " + key);
    arrayPoly.push(polyline);
    hashMapPoly.set(key, polyline);
}

//
function dichchuyen(cotObjgoc, vitri_cotdichuyen, marker) {

    // thuan
    var mangsocot = cotgoc.LK_COT1.split('$');
    for (var i = 0; i < mangsocot.length; i++) {
        var cotnoi = listCottrungthe.find(function (obj) { return obj.SO_COT === mangsocot[i]; });
        if (cotnoi != null) {
            var key_thuan = vitri_cotdichuyen + "_" + mangsocot[i];
            var thuan = hashMapPoly.get(key_thuan);
            //
            if (thuan != null) {
                hashMapPoly.get(key_thuan).setMap(null);
                var points_new = [new google.maps.LatLng(parseFloat(marker.getPosition().lat()), parseFloat(marker.getPosition().lng())), new google.maps.LatLng(parseFloat(cotnoi.X_COT), parseFloat(cotnoi.Y_COT))];
                hashMapPoly.set(key_thuan, new google.maps.Polyline({ map: map, path: points_new, strokeColor: GV_COLOR_DOVIENDEN, strokeWeight: 1, strokeOpacity: 1 }));
            }
        }
    }

    // nghich
    var mangsocot_nghich = cotgoc.LK_COT3.split('$');
    for (var j = 0; j < mangsocot_nghich.length; j++) {
        var cotnoi = listCottrungthe.find(function (obj) { return obj.SO_COT === mangsocot_nghich[j]; });
        if (cotnoi != null) {
            var key = val2key(cotnoi.SO_COT, hashMapMarker);
            //console.log("KEY: "+key);
            var key_nghich = key + "_" + cotObjgoc.SO_COT;
            var nghich = hashMapPoly.get(key_nghich);
            //
            if (nghich != null) {
                hashMapPoly.get(key_nghich).setMap(null);
                var points_newnghich = [new google.maps.LatLng(parseFloat(marker.getPosition().lat()), parseFloat(marker.getPosition().lng())), new google.maps.LatLng(parseFloat(cotnoi.X_COT), parseFloat(cotnoi.Y_COT))];
                hashMapPoly.set(key_nghich, new google.maps.Polyline({ map: map, path: points_newnghich, strokeColor: GV_COLOR_DOVIENDEN, strokeWeight: 1, strokeOpacity: 1 }));
            }
        }
    }
}

function val2key(val, array) {
    for (var i = 0; i < array.size; i++) {
        //console.log("KEY1: "+array.get(i).icon.socot);
        if (array.get(i).icon.socot == val) {
            //console.log("KEYA: "+array.get(i).icon.socot);
            return i;
        }
    }
}

// khong dung
function replaceLine(cotObjgoc, x_moi, y_moi, cotObjnoi) {
    var points1 = [new google.maps.LatLng(parseFloat(cotObjgoc.X_COT), parseFloat(cotObjgoc.Y_COT)), new google.maps.LatLng(parseFloat(cotObjnoi.X_COT), parseFloat(cotObjnoi.Y_COT))];
    var polyline1 = new google.maps.Polyline({ map: map, path: points1, strokeColor: GV_COLOR_TIMVIENDEN, strokeWeight: 1, strokeOpacity: 1 });

    var points2 = [new google.maps.LatLng(parseFloat(x_moi), parseFloat(y_moi)), new google.maps.LatLng(parseFloat(cotObjnoi.X_COT), parseFloat(cotObjnoi.Y_COT))];
    var polyline2 = new google.maps.Polyline({ map: map, path: points2, strokeColor: GV_COLOR_DOVIENDEN, strokeWeight: 1, strokeOpacity: 1 });

    cotgoctam.X_COT = x_moi;
    cotgoctam.Y_COT = y_moi;
}

function moveToLocation(lat, lng) {
    var center = new google.maps.LatLng(lat, lng);
    // using global variable:
    map.panTo(center);
}

/* BAT DAU AAAAA:  PHAN HEADER */
$("#congtydienluc").empty();
$("#congtydienluc").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tramnguon").empty();
$("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn & Xuất tuyến chính --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congtydienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

    // Kiem tra user
    var m_taikhoan = localStorage['taikhoan'];
    //console.log("AAA: "+ `${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`);
    $.get(`${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`, function (data) {
        $.each(data, function (i, f) {
            madonvicuanguoidung = f.MA_DVIQLY;
            if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
                $("#congtydienluc").select2().val(madonvicuanguoidung);
                $("#congtydienluc").trigger('change');
                $("#congtydienluc").prop('disabled', true);
            } else if (madonvicuanguoidung.length == 6) {
                if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
                    $("#congtydienluc").select2().val(madonvicuanguoidung.substring(0, 2));
                    $("#congtydienluc").trigger('change');
                    $("#congtydienluc").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                } else {
                    $("#congtydienluc").select2().val(madonvicuanguoidung.substring(0, 4));
                    $("#congtydienluc").trigger('change');
                    $("#congtydienluc").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                }
            }
        });
    });
});

$("#congtydienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});

$("#tramnguon").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm nguồn"
});

$("#xuattuyen").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Xuất tuyến chính"
});

$('#congtydienluc').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matramnguon = '0';
    maxuattuyen = '0';
    //
    $("#dienluc").empty();
    $("#dienluc").append(`<option style='font-weight:bold'value="0">-- Tất cả Điện lực --</option>`);
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option style='font-weight:bold'value="0">-- Tất cả Trạm nguồn & Xuất tuyến chính --</option>`);
    //
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty) {
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}"><b>${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</b></option>`);
        }
    });
});

$('#dienluc').on('change', function () {
    $("#isBientap").hide();
    $("#bientap").hide();
    madienluc = this.value;
    matramnguon = '0';
    maxuattuyen = '0';
    //
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn & Xuất tuyến chính --</option>`);
    var matam = '';
    $.get(`${url}/api/TRUNGTHE_getDMXuattuyen_byDviqly/${madienluc.trim()}`, function (data) {
        $.each(data, function (i, f) {
            if (matam != f.MA_TRAMNGUON) {
                matam = f.MA_TRAMNGUON;
                $("#tramnguon").append(`<optgroup value="TRAM_NGUON" style="font-weight:bold"  label="${f.MA_TRAMNGUON}- ${f.TEN_TRAMNGUON}">${f.MA_TRAMNGUON} - ${f.TEN_TRAMNGUON}</optgroup>`);
                var giatri = f.MA_TRAMNGUON + "$" + f.MA_XUATTUYEN;
                $("#tramnguon").append(`<option value="${giatri}">${f.MA_XUATTUYEN} - ${f.TEN_XUATTUYEN}</option>`);
            } else {
                var giatri = f.MA_TRAMNGUON + "$" + f.MA_XUATTUYEN;
                $("#tramnguon").append(`<option value="${giatri}">${f.MA_XUATTUYEN} - ${f.TEN_XUATTUYEN}</option>`);
            }
        });
    });
});

$(".select2-results__options").select2({
    width: '100px',
    dropdownAutoWidth: true,
});
$('#tramnguon').on('change', function () {
    var chuoi = this.value.split("$");
    matramnguon = chuoi[0];
    maxuattuyen = chuoi[1];
    //console.log("ABC: " + matramnguon + ", " + maxuattuyen);
});

function w3_open() {
    document.getElementById("main").style.marginLeft = "17.7%";
    document.getElementById("mySidebar").style.width = "17.7%";
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("map").style.width = '82.3%';
}

function w3_close() {
    document.getElementById("main").style.marginLeft = "0%";
    document.getElementById("mySidebar").style.display = "none";
    //document.getElementById("openNav").style.display = "inline-block";
    document.getElementById("map").style.width = '100%';
}
/* KET THUC AAAAA:  PHAN HEADER */

// HIEN SO COT
var cb_hiensocot = document.querySelector("input[name=cb_hiensocot]");
// cb_hiensocot.addEventListener('change', function () {
//     if (this.checked) {
//         isHiensocot = true;
//         if (map.getZoom() <= 15) {
//             // khong lam chi
//         } else {
//             bochuyendoiZoom(map.getZoom());
//             for (var i = 0; i < hashMapMarker.size; i++) {
//                 hashMapMarker.get(i).setIcon({
//                     path: google.maps.SymbolPath.CIRCLE,
//                     scale: tron_scale,
//                     fillColor: hashMapMarker.get(i).icon.fillColor,
//                     fillOpacity: 1,
//                     strokeWeight: tron_stroke,
//                     origin: new google.maps.Point(0, 0), // origin
//                     anchor: new google.maps.Point(0, 0), // anchor
//                     labelOrigin: new google.maps.Point(0, -2),
//                     madviqly: hashMapMarker.get(i).icon.madviqly, //1
//                     matram: hashMapMarker.get(i).icon.matram, //2
//                     tentram: hashMapMarker.get(i).icon.tentram, //3
//                     socot: hashMapMarker.get(i).icon.socot, //4
//                     lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
//                     ishien: 1 //5
//                 });

//                 //
//                 if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 } else {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 }
//             }
//         }
//     } else {
//         isHiensocot = false;
//         if (map.getZoom() <= 15) {
//             // khong lam chi
//         } else {
//             bochuyendoiZoom(map.getZoom());
//             for (var i = 0; i < hashMapMarker.size; i++) {
//                 hashMapMarker.get(i).setIcon({
//                     path: google.maps.SymbolPath.CIRCLE,
//                     scale: tron_scale,
//                     fillColor: hashMapMarker.get(i).icon.fillColor,
//                     fillOpacity: 1,
//                     strokeWeight: tron_stroke,
//                     origin: new google.maps.Point(0, 0), // origin
//                     anchor: new google.maps.Point(0, 0), // anchor
//                     labelOrigin: new google.maps.Point(0, -2),
//                     madviqly: hashMapMarker.get(i).icon.madviqly, //1
//                     matram: hashMapMarker.get(i).icon.matram, //2
//                     tentram: hashMapMarker.get(i).icon.tentram, //3
//                     socot: hashMapMarker.get(i).icon.socot, //4
//                     lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
//                     ishien: 0 //6
//                 });

//                 //
//                 if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.matramnguon };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 } else {
//                     hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
//                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//                 }
//             }
//         }
//     }

//     // Thiet lap lai tu dau
//     // $("#ul_thietbi").empty();
//     // $("#tieudeII").text("II. Thông Tin Thiết Bị: " + jsonKhachhangbandau.length.toLocaleString('en') + "");
//     // jsonKhachhang = jsonKhachhangbandau;
//     // var demthietbi = 0;
//     // $.each(jsonKhachhang, function (i, f) {
//     //     demthietbi++;
//     //     if (demthietbi < 500) {
//     //         $("#ul_thietbi").append(
//     //             `<li onclick="getitem('${f.MA_LOAITHIETBI}')>          
//     //                 <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_LOAITHIETBI}</b><b style="float:right">PMIS</b></a><br>
//     //                 <a>${f.MA_THIETBI}</a><br>
//     //                 <a>${f.TEN_THIETBI}</a><br>
//     //             </li>
//     //             <div style="background-color:rgb(50,190,166);height:1px"></div>`
//     //         );
//     //     }
//     // });

//     // //////////////////////////
//     // $.get(`${url}/api/TRUNGTHE_getSLHinhanh_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
//     //     $.each(data, function (i, f) {
//     //         $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + f.SO_LUONG.toLocaleString('en') + "");
//     //     });
//     // });
//     // $("#hinhanh").empty();
//     // $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length.toLocaleString('en') + "");
//     // jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
//     // $.each(jsonHinnhanh_cot, function (i, f) {
//     //     $("#hinhanh").append(
//     //         `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
//     //              <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
//     //              </div>`
//     //     );

//     // });
// });
function CenterControlGiaodienchinh(controlDiv, map) {

    /* BAT DAU: Hien So Cot */
    var controlUICOT_div = document.createElement('div');
    controlUICOT_div.id = 'GNC_hienthicot';
    controlUICOT_div.style.cssFloat = 'left';
    var controlUICOT_GDC = document.createElement('input');
    controlUICOT_GDC.type = 'checkbox';
    controlUICOT_GDC.id = 'isSocot';
    controlUICOT_GDC.name = 'cb_hiensocot';
    controlUICOT_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUICOT_GDC.style.width = '18px';
    controlUICOT_GDC.style.height = '25px';
    controlUICOT_GDC.style.textAlign = 'center';
    controlUICOT_GDC.style.fontSize = '18px';
    controlUICOT_GDC.style.fontWeight = 'bold';

    var controlUICOT_GDC_label = document.createElement('label');
    controlUICOT_GDC_label.htmlFor = 'isSocot';
    controlUICOT_GDC_label.id = 'socot';
    controlUICOT_GDC_label.style.margin = '2px 0px 0px 27px';
    controlUICOT_GDC_label.style.cursor = 'pointer';
    controlUICOT_GDC_label.innerHTML = 'Hiện số cột';

    //add div hien so cot vao map
    controlUICOT_div.onmouseover = function () {
        controlUICOT_div.style.backgroundColor = 'aquamarine';
    };
    controlUICOT_div.onmouseleave = function () {
        if(isHiensocot){
            controlUICOT_div.style.backgroundColor = 'aquamarine';
        } else {
            controlUICOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        }
    };
    controlUICOT_div.appendChild(controlUICOT_GDC);
    controlUICOT_div.appendChild(controlUICOT_GDC_label);
    controlDiv.appendChild(controlUICOT_div);
    controlUICOT_GDC.addEventListener('change', function () {
        if (this.checked) {
            controlUICOT_div.style.backgroundColor = ' aquamarine';
            isHiensocot = true;
            if (map.getZoom() <= 15) {
                // khong lam chi
            } else {
                bochuyendoiZoom(map.getZoom());
                for (var i = 0; i < hashMapMarker.size; i++) {
                    hashMapMarker.get(i).setIcon({
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: hashMapMarker.get(i).icon.fillColor,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: hashMapMarker.get(i).icon.madviqly, //1
                        matram: hashMapMarker.get(i).icon.matram, //2
                        tentram: hashMapMarker.get(i).icon.tentram, //3
                        socot: hashMapMarker.get(i).icon.socot, //4
                        lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                        ishien: 1 //5
                    });
    
                    //
                    if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else {
                        hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    }
                }
            }
        } else {
            isHiensocot = false;
            controlUICOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
            if (map.getZoom() <= 15) {
                // khong lam chi
            } else {
                bochuyendoiZoom(map.getZoom());
                for (var i = 0; i < hashMapMarker.size; i++) {
                    hashMapMarker.get(i).setIcon({
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: hashMapMarker.get(i).icon.fillColor,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: hashMapMarker.get(i).icon.madviqly, //1
                        matram: hashMapMarker.get(i).icon.matram, //2
                        tentram: hashMapMarker.get(i).icon.tentram, //3
                        socot: hashMapMarker.get(i).icon.socot, //4
                        lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                        ishien: 0 //6
                    });
    
                    //
                    if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.matramnguon };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else {
                        hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    }
                }
            }
        }
    
        // Thiet lap lai tu dau
        // $("#ul_thietbi").empty();
        // $("#tieudeII").text("II. Thông Tin Thiết Bị: " + jsonKhachhangbandau.length.toLocaleString('en') + "");
        // jsonKhachhang = jsonKhachhangbandau;
        // var demthietbi = 0;
        // $.each(jsonKhachhang, function (i, f) {
        //     demthietbi++;
        //     if (demthietbi < 500) {
        //         $("#ul_thietbi").append(
        //             `<li onclick="getitem('${f.MA_LOAITHIETBI}')>          
        //                 <a id="MA_KHANG" style="font-size:12.3px"><b>${f.MA_LOAITHIETBI}</b><b style="float:right">PMIS</b></a><br>
        //                 <a>${f.MA_THIETBI}</a><br>
        //                 <a>${f.TEN_THIETBI}</a><br>
        //             </li>
        //             <div style="background-color:rgb(50,190,166);height:1px"></div>`
        //         );
        //     }
        // });
    
        // //////////////////////////
        // $.get(`${url}/api/TRUNGTHE_getSLHinhanh_byXuattuyen/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {
        //     $.each(data, function (i, f) {
        //         $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + f.SO_LUONG.toLocaleString('en') + "");
        //     });
        // });
        // $("#hinhanh").empty();
        // $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length.toLocaleString('en') + "");
        // jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
        // $.each(jsonHinnhanh_cot, function (i, f) {
        //     $("#hinhanh").append(
        //         `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
        //              <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
        //              </div>`
        //     );
    
        // });
    });
    
    /* KET THUC: Hien So Cot */

    /* BAT DAU: Bien tap giao dien */
    var controlUIBTGIAODIEN_div = document.createElement('div');
    controlUIBTGIAODIEN_div.id = 'GND_bientap';
    controlUIBTGIAODIEN_div.style.cssFloat = 'left';
    var controlUIBTGIAODIEN_GDC = document.createElement('input');
    controlUIBTGIAODIEN_GDC.type = 'checkbox';
    controlUIBTGIAODIEN_GDC.id = 'isBientap';
    controlUIBTGIAODIEN_GDC.name = 'cb_bientap';
    controlUIBTGIAODIEN_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUIBTGIAODIEN_GDC.style.width = '18px';
    controlUIBTGIAODIEN_GDC.style.height = '25px';
    controlUIBTGIAODIEN_GDC.style.textAlign = 'center';
    controlUIBTGIAODIEN_GDC.style.fontSize = '18px';
    controlUIBTGIAODIEN_GDC.style.fontWeight = 'bold';

    var controlUIBTGIAODIEN_GDC_label = document.createElement('label');
    controlUIBTGIAODIEN_GDC_label.htmlFor = 'isBientap';
    controlUIBTGIAODIEN_GDC_label.id = 'bientap';
    controlUIBTGIAODIEN_GDC_label.style.margin = '2px 0px 0px 6px';
    controlUIBTGIAODIEN_GDC_label.style.cursor = 'pointer';
    controlUIBTGIAODIEN_GDC_label.innerHTML = 'Biên tập giao diện';

    //
    controlUIBTGIAODIEN_div.onmouseover = function () {
        controlUIBTGIAODIEN_div.style.backgroundColor = 'aquamarine';
    };
    controlUIBTGIAODIEN_div.onmouseleave = function () {
        controlUIBTGIAODIEN_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlUIBTGIAODIEN_div.appendChild(controlUIBTGIAODIEN_GDC);
    controlUIBTGIAODIEN_div.appendChild(controlUIBTGIAODIEN_GDC_label);
    controlDiv.appendChild(controlUIBTGIAODIEN_div);
    controlUIBTGIAODIEN_GDC.addEventListener('change', function () {

        if (this.checked) {
            var m_taikhoan = localStorage['taikhoan'];
            var m_matkhau = localStorage['matkhau'];
    
            //tạo 3 nút biên tập
            // var centerControl = new CenterControl_BIENTAPGIAODIEN(centerControlDiv_BienTapGiaoDien, map);
            // centerControlDiv_BienTapGiaoDien.index = 1;
            // map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapGiaoDien);
    
            //
            if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
                $("#isBientap").prop("checked", false);
                alert("Bạn chưa đăng nhập.");
            } else {
                //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
                $("#loading").show();
                $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
                    if (data.length != 0) {
                        data.forEach(function (nguoidungObj) {
                            if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_PMIS_EXCUTE) !== -1) {
                                if (macongty == null || macongty == '0') {
                                    alert("Mời bạn chọn Công ty.")
                                } else {
                                    if (madienluc == null || madienluc == '0') {
                                        alert("Mời bạn chọn Điện lực.");
                                    } else {
                                        if (matramnguon == null || matramnguon == '0') {
                                            alert("Mời bạn chọn Trạm nguồn.");
                                        } else {
                                            if (maxuattuyen == null || maxuattuyen == '0') {
                                                alert("Mời bạn chọn Xuất tuyến chính.");
                                            } else {
                                                //
                                                w3_close();
                                                GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Giao Diện.", 250);
    
                                                //
                                                //$("#socot").prop("disabled", true);
                                               // $("#isSocot").prop("disabled", true);
                                                isHiensocot = false;
                                               // cb_hiensocot.checked = false;
                                               controlUICOT_GDC_label.click();
                                                //
                                                //$("#bientapexcel").prop("disabled", true);
                                                //$("#isBientapExcel").prop("disabled", true);
                                                isBientapExcel = false;
                                               // cb_bientapexcel.checked = false;
    
                                                //
                                                isBientap = true;
                                                while (controlDiv.firstChild) {
                                                    // console.log("CHan qua di");
                                                    controlDiv.removeChild(controlDiv.firstChild);
                                                }
                                                var centerControl_giaodien = new CenterControl_BIENTAPGIAODIEN(centerControlDiv_BienTapGiaoDien, map, controlUIBTGIAODIEN_GDC, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div);
                                                centerControlDiv_BienTapGiaoDien.index = 1;
                                                map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapGiaoDien);
    
                                                controlUIBTGIAODIEN_GDC.checked = true;
                                                bientapControl = 1;
    
                                                //
                                                bochuyendoiZoom(map.getZoom());
                                                for (var i = 0; i < hashMapMarker.size; i++) {
                                                    //
                                                    hashMapMarker.get(i).setDraggable(true);
                                                    hashMapMarker.get(i).setIcon({
                                                        path: hinh_giotnuoc,
                                                        scale: tron_scale / 8,
                                                        fillColor: hashMapMarker.get(i).icon.fillColor,
                                                        fillOpacity: 1,
                                                        strokeWeight: tron_stroke,
                                                        origin: new google.maps.Point(0, 0), // origin
                                                        anchor: new google.maps.Point(0, 0), // anchor
                                                        labelOrigin: new google.maps.Point(0, -50),
                                                        madviqly: hashMapMarker.get(i).icon.madviqly, //1
                                                        matram: hashMapMarker.get(i).icon.matram, //2
                                                        tentram: hashMapMarker.get(i).icon.tentram, //3
                                                        socot: hashMapMarker.get(i).icon.socot, //4
                                                        lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                                                        ishien: 1 //5
                                                    });
    
                                                    //
                                                    if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                                                        hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                                                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                                                    } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                                                        hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                                                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                                                    } else {
                                                        hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                                                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                $("#isBientap").prop("checked", false);
                                alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Trung Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
                            }
                        })
                    } else {
                        $("#isBientap").prop("checked", false);
                        alert("Tài khoản này không tồn tại.");
                    }
                    $("#loading").hide();
                });
            }
        } else {
            //hủy ba nút biên tập
            while (centerControlDiv_BienTapGiaoDien.firstChild) {
                // console.log("CHan qua di");
                centerControlDiv_BienTapGiaoDien.removeChild(centerControlDiv_BienTapGiaoDien.firstChild);
            }
            controlDiv.appendChild(controlUICOT_div);
            controlDiv.appendChild(controlUIBTGIAODIEN_div);
            controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
            centerControlDiv_BienTapGiaoDien = document.createElement('div');
    
            //
            w3_open();
            GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Giao Diện.", 250);
    
            //
            //$("#socot").prop("disabled", false);
            //$("#isSocot").prop("disabled", false);
            //isBientap = false;
            //cb_hiensocot.checked = false;
    
            //
            // $("#bientapexcel").prop("disabled", false);
            // $("#isBientapExcel").prop("disabled", false);
            // isBientapExcel = false;
            // cb_bientapexcel.checked = false;
    
            //
            isBientap = false;
            bochuyendoiZoom(map.getZoom());
            $("#tieudeI").text("I. Thông Tin Cột Hạ Thế: " + hashMapMarker.size + "");
            for (var i = 0; i < hashMapMarker.size; i++) {
                hashMapMarker.get(i).setDraggable(false);
                hashMapMarker.get(i).setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: tron_scale,
                    fillColor: hashMapMarker.get(i).icon.fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -2),
                    madviqly: hashMapMarker.get(i).icon.madviqly, //1
                    matram: hashMapMarker.get(i).icon.matram, //2
                    tentram: hashMapMarker.get(i).icon.tentram, //3
                    socot: hashMapMarker.get(i).icon.socot, //4
                    lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                    ishien: 0 //6
                });
    
                //
                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else {
                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                }
            }
    
            // Thiet lap lai tu dau
            $("#ul_thietbi").empty();
            $("#tieudeII").text("II. Thông Tin Thiết Bị: " + jsonKhachhangbandau.length + "");
            // jsonKhachhang = jsonKhachhangbandau;
            // $.each(jsonKhachhang, function (i, f) {
            //     $("#ul_thietbi").append(
            //         `<li onclick="getitem('${f.MA_KHANG}')">
            //         <a style="font-size:12.3px"><b>${f.MA_KHANG}</b></a><br>
            //         <a>${f.TEN_KHANG}</a><br>
            //         <a>${f.DCHI_KHANG}</a><br>
            //     </li>
            //     <div style="background-color:rgb(50,190,166);height:1px"></div>`
            //     );
            // });
    
            $("#hinhanh").empty();
            $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length + "");
            jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
            $.each(jsonHinnhanh_cot, function (i, f) {
                $("#hinhanh").append(
                    `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                 <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
                </div>`
                );
            });
        }
    });
    
    // controlUIBTGIAODIEN_GDC.addEventListener('change', function () {

    //     if (this.checked) {
    //         var m_taikhoan = localStorage['taikhoan'];
    //         var m_matkhau = localStorage['matkhau'];

    //         //
    //         if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
    //             $("#isBientap").prop("checked", false);
    //             alert("Bạn chưa đăng nhập.");
    //         } else {
    //             //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
    //             $("#loading").show();
    //             $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
    //                 if (data.length != 0) {
    //                     data.forEach(function (nguoidungObj) {
    //                         if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_CMIS_EXCUTE) !== -1) {
    //                             if (macongty == null || macongty == '0') {
    //                                 alert("Mời bạn chọn Công ty.")
    //                             } else {
    //                                 if (madienluc == null || madienluc == '0') {
    //                                     alert("Mời bạn chọn Điện lực.");
    //                                 } else {
    //                                     if (matram == null || matram == '0') {
    //                                         alert("Mời bạn chọn Trạm.");
    //                                     } else {
    //                                         //
    //                                         w3_close();
    //                                         GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Giao Diện.", 250);

    //                                         isHiensocot = false;
    //                                         isBientapExcel = false;
    //                                         isXemDuongDay = false;
    //                                         isTinhKhoangCachCot = false;
    //                                         isDoiTramChoCot = false;
    //                                         controlUICOT_GDC.checked = true
    //                                         controlUICOT_GDC_label.click();
    //                                         controlUIKHOANGCACHCOT_GDC.checked = true
    //                                         controlUIKHOANGCACHCOT_GDC_label.click();
    //                                         //
    //                                         isBientap = true;
    //                                         while (controlDiv.firstChild) {
    //                                             // console.log("CHan qua di");
    //                                             controlDiv.removeChild(controlDiv.firstChild);
    //                                         }
    //                                         var centerControl_giaodien = new CenterControlGiaoDien(centerControlDiv_BienTapGiaoDien, map, controlUIBTGIAODIEN_GDC, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div, controlUIDUONGDICOT_div, controlUIKHOANGCACHCOT_div);
    //                                         centerControlDiv_BienTapGiaoDien.index = 1;
    //                                         map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapGiaoDien);

    //                                         controlUIBTGIAODIEN_GDC.checked = true;
    //                                         bientapControl = 1;

    //                                         //
    //                                         bochuyendoiZoom(map.getZoom());
    //                                         for (var i = 0; i < hashMapMarker.size; i++) {
    //                                             //
    //                                             hashMapMarker.get(i).setDraggable(true);
    //                                             hashMapMarker.get(i).setIcon({
    //                                                 path: hinh_giotnuoc,
    //                                                 scale: tron_scale / 8,
    //                                                 fillColor: hashMapMarker.get(i).icon.fillColor,
    //                                                 fillOpacity: 1,
    //                                                 strokeWeight: tron_stroke,
    //                                                 origin: new google.maps.Point(0, 0), // origin
    //                                                 anchor: new google.maps.Point(0, 0), // anchor
    //                                                 labelOrigin: new google.maps.Point(0, -50),
    //                                                 madviqly: hashMapMarker.get(i).icon.madviqly, //1
    //                                                 matram: hashMapMarker.get(i).icon.matram, //2
    //                                                 tentram: hashMapMarker.get(i).icon.tentram, //3
    //                                                 socot: hashMapMarker.get(i).icon.socot, //4
    //                                                 lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
    //                                                 ishien: 1, //6
    //                                                 iscot: hashMapMarker.get(i).icon.iscot //7
    //                                             });

    //                                             //
    //                                             if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
    //                                                 hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
    //                                                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
    //                                             } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
    //                                                 hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
    //                                                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
    //                                             } else {
    //                                                 hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
    //                                                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
    //                                             }
    //                                         }
    //                                     }
    //                                 }
    //                             }
    //                         } else {
    //                             $("#isBientap").prop("checked", false);
    //                             alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Hạ Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
    //                         }
    //                     })
    //                 } else {
    //                     $("#isBientap").prop("checked", false);
    //                     alert("Tài khoản này không tồn tại.");
    //                 }
    //                 $("#loading").hide();
    //             });
    //         }
    //     } else {
    //         //hủy ba nút biên tập
    //         while (centerControlDiv_BienTapGiaoDien.firstChild) {
    //             // console.log("CHan qua di");
    //             centerControlDiv_BienTapGiaoDien.removeChild(centerControlDiv_BienTapGiaoDien.firstChild);
    //         }

    //         controlDiv.appendChild(controlUICOT_div);
    //         controlDiv.appendChild(controlUIBTGIAODIEN_div);
    //         controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
    //         controlDiv.appendChild(controlUIDUONGDICOT_div);
    //         controlDiv.appendChild(controlUIKHOANGCACHCOT_div);
    //         controlDiv.appendChild(controlUIDOITRAMCHOCOT_div);
    //         centerControlDiv_BienTapGiaoDien = document.createElement('div');
    //         //
    //         w3_open();
    //         GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Giao Diện.", 250);
    //         isBientap = false;
    //         bochuyendoiZoom(map.getZoom());
    //         $("#tieudeI").text("I. Thông Tin Cột Hạ Thế: " + hashMapMarker.size + "");
    //         for (var i = 0; i < hashMapMarker.size; i++) {
    //             hashMapMarker.get(i).setDraggable(false);
    //             hashMapMarker.get(i).setIcon({
    //                 path: google.maps.SymbolPath.CIRCLE,
    //                 scale: tron_scale,
    //                 fillColor: hashMapMarker.get(i).icon.fillColor,
    //                 fillOpacity: 1,
    //                 strokeWeight: tron_stroke,
    //                 origin: new google.maps.Point(0, 0), // origin
    //                 anchor: new google.maps.Point(0, 0), // anchor
    //                 labelOrigin: new google.maps.Point(0, -2),
    //                 madviqly: hashMapMarker.get(i).icon.madviqly, //1
    //                 matram: hashMapMarker.get(i).icon.matram, //2
    //                 tentram: hashMapMarker.get(i).icon.tentram, //3
    //                 socot: hashMapMarker.get(i).icon.socot, //4
    //                 lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
    //                 ishien: 0, //6
    //                 iscot: hashMapMarker.get(i).icon.iscot, //7
    //             });

    //             //
    //             if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
    //                 hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
    //                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
    //             } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
    //                 hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
    //                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
    //             } else {
    //                 hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
    //                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
    //             }
    //         }

    //         // Thiet lap lai tu dau
    //         $("#ul_khachhang").empty();
    //         $("#tieudeII").text("II. Thông Tin Khách Hàng: " + jsonKhachhangbandau.length + "");
    //         jsonKhachhang = jsonKhachhangbandau;
    //         $.each(jsonKhachhang, function (i, f) {
    //             $("#ul_khachhang").append(
    //                 `<li onclick="getitem('${f.MA_KHANG}')">
    //                     <a style="font-size:12.3px">
    //                     <b>${f.MA_KHANG}</b>
                        
    //                     <button aria-label="chi duong" id="icon-box-directions" class="icon-box-directions" data-toggle="tooltip" data-placement="right" title="Chỉ đường" onclick="findDirectionKhachHang('${f.MA_KHANG}',event)"></button>
    //                     <button aria-label="chi duong" id="searchbox-directions" class="searchbox-directions" data-toggle="tooltip" data-placement="right" title="Tìm vị trí" onclick="getdiadiem('${f.MA_KHANG}',event)"></button>
    //                     <button  id="display-client" class="display-client" data-toggle="tooltip" data-placement="right"  onclick="getmaKH('${f.MA_KHANG}',event)">CPM</button>
    //                     </a><br>
    //                     <a>${f.TEN_KHANG}</a><br>
    //                     <a>${f.DCHI_KHANG}</a><br>
    //                 </li>
    //                 <div style="background-color:rgb(50,190,166);height:1px"></div>`
    //             );
    //         });

    //         $("#hinhanh").empty();
    //         $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length + "");
    //         jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
    //         $.each(jsonHinnhanh_cot, function (i, f) {
    //             $("#hinhanh").append(
    //                 `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
    //                  ${displaydatefromdb(f.NGAY_NHAP)}${checknull_anh(f.UNGDUNG_SUDUNG)}
    //                 <img style="height:${height_}"  src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
    //         </div>`
    //             );
    //         });
    //     }
    // });
    /* KET THUC: Bien tap giao dien */

    /* BAT DAU: Bien tap Excel */
    var controlUIBTGIAODIENEXCEL_div = document.createElement('div');
    controlUIBTGIAODIENEXCEL_div.id = 'GND_bientapexcel';
    controlUIBTGIAODIENEXCEL_div.style.cssFloat = 'left';
    var controlUIBTGIAODIENEXCEL_GDC = document.createElement('input');
    controlUIBTGIAODIENEXCEL_GDC.type = 'checkbox';
    controlUIBTGIAODIENEXCEL_GDC.id = 'isBientapExcel';
    controlUIBTGIAODIENEXCEL_GDC.name = 'cb_bientapexcel';
    controlUIBTGIAODIENEXCEL_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUIBTGIAODIENEXCEL_GDC.style.width = '18px';
    controlUIBTGIAODIENEXCEL_GDC.style.height = '25px';
    controlUIBTGIAODIENEXCEL_GDC.style.textAlign = 'center';
    controlUIBTGIAODIENEXCEL_GDC.style.fontSize = '18px';
    controlUIBTGIAODIENEXCEL_GDC.style.fontWeight = 'bold';

    var controlUIBTGIAODIENEXCEL_GDC_label = document.createElement('label');
    controlUIBTGIAODIENEXCEL_GDC_label.htmlFor = 'isBientapExcel';
    controlUIBTGIAODIENEXCEL_GDC_label.id = 'bientapexcel';
    controlUIBTGIAODIENEXCEL_GDC_label.style.margin = '2px 0px 0px 15px';
    controlUIBTGIAODIENEXCEL_GDC_label.innerHTML = 'Biên tập EXCEL';
    controlUIBTGIAODIENEXCEL_GDC_label.style.cursor = 'pointer';

    //
    controlUIBTGIAODIENEXCEL_div.onmouseover = function () {
        controlUIBTGIAODIENEXCEL_div.style.backgroundColor = 'aquamarine';
    };
    controlUIBTGIAODIENEXCEL_div.onmouseleave = function () {
        controlUIBTGIAODIENEXCEL_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlUIBTGIAODIENEXCEL_div.appendChild(controlUIBTGIAODIENEXCEL_GDC);
    controlUIBTGIAODIENEXCEL_div.appendChild(controlUIBTGIAODIENEXCEL_GDC_label);
    controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
    controlUIBTGIAODIENEXCEL_GDC.addEventListener('change', function () {

        if (this.checked) {
            var m_taikhoan = localStorage['taikhoan'];
            var m_matkhau = localStorage['matkhau'];
    
            //
            if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
                $("#isBientapExcel").prop("checked", false);
                alert("Bạn chưa đăng nhập.");
            } else {
                //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
                $("#loading").show();
                $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
                    if (data.length != 0) {
                        data.forEach(function (nguoidungObj) {
                            if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_PMIS_EXCUTE) !== -1) {
                                if (macongty == null || macongty == '0') {
                                    alert("Mời bạn chọn Công ty.")
                                } else {
                                    if (madienluc == null || madienluc == '0') {
                                        alert("Mời bạn chọn Điện lực.");
                                    } else {
                                        if (matramnguon == null || matramnguon == '0') {
                                            alert("Mời bạn chọn Trạm nguồn.");
                                        } else {
                                            if (maxuattuyen == null || maxuattuyen == '0') {
                                                alert("Mời bạn chọn Xuất tuyến chính.");
                                            } else {
                                                //
                                                isHiensocot = false;
                                                isBientap = false;
                                                controlUICOT_GDC.checked = true
                                                controlUICOT_GDC_label.click();
                                                w3_close();
                                                GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Excel.", 250);
                                                //
                                
    
                                                //
                                                isBientapExcel = true;
    
                                                //
                                                while (controlDiv.firstChild) {
                                                    // console.log("CHan qua di");
                                                    controlDiv.removeChild(controlDiv.firstChild);
                                                }
                                                var centerControl = new CenterControl(centerControlDiv_BienTapEXCEL, map, controlUIBTGIAODIENEXCEL_GDC, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div);
                                                centerControlDiv_BienTapEXCEL.index = 1;
                                                map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapEXCEL);
    
                                                controlUIBTGIAODIENEXCEL_GDC.checked = true;
                                                bientapControl = 1;
                                            }
                                        }
                                    }
                                }
                            } else {
                                $("#isBientapExcel").prop("checked", false);
                                alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Trung Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
                            }
                        })
                    } else {
                        $("#isBientapExcel").prop("checked", false);
                        alert("Tài khoản này không tồn tại.");
                    }
                    $("#loading").hide();
                });
            }
        } else {
    
            //
            w3_open();
            GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Excel.", 250);
            //
            
    
            //
            isBientapExcel = false;
    
            while (centerControlDiv_BienTapEXCEL.firstChild) {
                // console.log("CHan qua di");
                centerControlDiv_BienTapEXCEL.removeChild(centerControlDiv_BienTapEXCEL.firstChild);
            }
            controlDiv.appendChild(controlUICOT_div);
            controlDiv.appendChild(controlUIBTGIAODIEN_div);
            controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
            centerControlDiv_BienTapEXCEL = document.createElement('div');
            bientapControl = 0;
        }
    });
    // controlUIBTGIAODIENEXCEL_GDC.addEventListener('change', function () {

    //     if (this.checked) {
    //         var m_taikhoan = localStorage['taikhoan'];
    //         var m_matkhau = localStorage['matkhau'];

    //         //
    //         if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
    //             $("#isBientapExcel").prop("checked", false);
    //             alert("Bạn chưa đăng nhập.");
    //         } else {
    //             //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
    //             $("#loading").show();
    //             $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
    //                 if (data.length != 0) {
    //                     data.forEach(function (nguoidungObj) {
    //                         if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_CMIS_EXCUTE) !== -1) {
    //                             if (macongty == null || macongty == '0') {
    //                                 alert("Mời bạn chọn Công ty")
    //                             } else {
    //                                 if (madienluc == null || madienluc == '0') {
    //                                     alert("Mời bạn chọn Điện lực.");
    //                                 } else {
    //                                     if (matram == null || matram == '0') {
    //                                         alert("Mời bạn chọn Trạm.");
    //                                     } else {
    //                                         //
    //                                         isHiensocot = false;
    //                                         isBientap = false;
    //                                         isXemDuongDay = false;
    //                                         isTinhKhoangCachCot = false;
    //                                         isDoiTramChoCot = false;
    //                                         controlUICOT_GDC.checked = true
    //                                         controlUICOT_GDC_label.click();
    //                                         controlUIKHOANGCACHCOT_GDC.checked = true
    //                                         controlUIKHOANGCACHCOT_GDC_label.click();
    //                                         //
    //                                         w3_close();
    //                                         GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Excel.", 250);

    //                                         //
    //                                         isBientapExcel = true;
    //                                         while (controlDiv.firstChild) {
    //                                             // console.log("CHan qua di");
    //                                             controlDiv.removeChild(controlDiv.firstChild);
    //                                         }
    //                                         var centerControl = new CenterControl(centerControlDiv_BienTapEXCEL, map, controlUIBTGIAODIENEXCEL_GDC, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div, controlUIDUONGDICOT_div, controlUIKHOANGCACHCOT_div);
    //                                         centerControlDiv_BienTapEXCEL.index = 1;
    //                                         map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapEXCEL);

    //                                         controlUIBTGIAODIENEXCEL_GDC.checked = true;
    //                                         bientapControl = 1;
    //                                     }
    //                                 }
    //                             }
    //                         } else {
    //                             $("#isBientapExcel").prop("checked", false);
    //                             alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Hạ Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
    //                         }
    //                     })
    //                 } else {
    //                     $("#isBientapExcel").prop("checked", false);
    //                     alert("Tài khoản này không tồn tại.");
    //                 }
    //                 $("#loading").hide();
    //             });
    //         }
    //     } else {

    //         //
    //         w3_open();
    //         GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Excel.", 250);
    //         isBientapExcel = false;

    //         while (centerControlDiv_BienTapEXCEL.firstChild) {
    //             // console.log("CHan qua di");
    //             centerControlDiv_BienTapEXCEL.removeChild(centerControlDiv_BienTapEXCEL.firstChild);
    //         }
    //         controlDiv.appendChild(controlUICOT_div);
    //         controlDiv.appendChild(controlUIBTGIAODIEN_div);
    //         controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
    //         controlDiv.appendChild(controlUIDUONGDICOT_div);
    //         controlDiv.appendChild(controlUIKHOANGCACHCOT_div);
    //         controlDiv.appendChild(controlUIDOITRAMCHOCOT_div);
    //         centerControlDiv_BienTapEXCEL = document.createElement('div');
    //         bientapControl = 0;
    //         $("#tinhkhoangcachcot").prop("disabled", false);
    //         $("#isTinhKhoangCachCot").prop("disabled", false);
    //         isTinhKhoangCachCot = false;
    //         $("#xemduongday").prop("disabled", false);
    //         $("#isXemDuongDay").prop("disabled", false);
    //         isXemDuongDay = false;
    //     }
    // });
    /* KET THUC: Bien tap Excel */


}
// BIEN TAP LUOI DIEN

var centerControlDiv_BienTapGiaoDien = document.createElement('div');
// cb_bientap.addEventListener('change', function () {

//     if (this.checked) {
//         var m_taikhoan = localStorage['taikhoan'];
//         var m_matkhau = localStorage['matkhau'];

//         //tạo 3 nút biên tập
//         // var centerControl = new CenterControl_BIENTAPGIAODIEN(centerControlDiv_BienTapGiaoDien, map);
//         // centerControlDiv_BienTapGiaoDien.index = 1;
//         // map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapGiaoDien);

//         //
//         if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
//             $("#isBientap").prop("checked", false);
//             alert("Bạn chưa đăng nhập.");
//         } else {
//             //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
//             $("#loading").show();
//             $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
//                 if (data.length != 0) {
//                     data.forEach(function (nguoidungObj) {
//                         if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_PMIS_EXCUTE) !== -1) {
//                             if (macongty == null || macongty == '0') {
//                                 alert("Mời bạn chọn Công ty.")
//                             } else {
//                                 if (madienluc == null || madienluc == '0') {
//                                     alert("Mời bạn chọn Điện lực.");
//                                 } else {
//                                     if (matramnguon == null || matramnguon == '0') {
//                                         alert("Mời bạn chọn Trạm nguồn.");
//                                     } else {
//                                         if (maxuattuyen == null || maxuattuyen == '0') {
//                                             alert("Mời bạn chọn Xuất tuyến chính.");
//                                         } else {
//                                             //
//                                             w3_close();
//                                             GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Giao Diện.", 250);

//                                             //
//                                             $("#socot").prop("disabled", true);
//                                             $("#isSocot").prop("disabled", true);
//                                             isHiensocot = false;
//                                             cb_hiensocot.checked = false;

//                                             //
//                                             $("#bientapexcel").prop("disabled", true);
//                                             $("#isBientapExcel").prop("disabled", true);
//                                             isBientapExcel = false;
//                                             cb_bientapexcel.checked = false;

//                                             //
//                                             isBientap = true;

//                                             //
//                                             bochuyendoiZoom(map.getZoom());
//                                             for (var i = 0; i < hashMapMarker.size; i++) {
//                                                 //
//                                                 hashMapMarker.get(i).setDraggable(true);
//                                                 hashMapMarker.get(i).setIcon({
//                                                     path: hinh_giotnuoc,
//                                                     scale: tron_scale / 8,
//                                                     fillColor: hashMapMarker.get(i).icon.fillColor,
//                                                     fillOpacity: 1,
//                                                     strokeWeight: tron_stroke,
//                                                     origin: new google.maps.Point(0, 0), // origin
//                                                     anchor: new google.maps.Point(0, 0), // anchor
//                                                     labelOrigin: new google.maps.Point(0, -50),
//                                                     madviqly: hashMapMarker.get(i).icon.madviqly, //1
//                                                     matram: hashMapMarker.get(i).icon.matram, //2
//                                                     tentram: hashMapMarker.get(i).icon.tentram, //3
//                                                     socot: hashMapMarker.get(i).icon.socot, //4
//                                                     lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
//                                                     ishien: 1 //5
//                                                 });

//                                                 //
//                                                 if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
//                                                     hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
//                                                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
//                                                 } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
//                                                     hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
//                                                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
//                                                 } else {
//                                                     hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
//                                                     hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
//                                                 }
//                                             }
//                                         }
//                                     }
//                                 }
//                             }
//                         } else {
//                             $("#isBientap").prop("checked", false);
//                             alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Trung Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
//                         }
//                     })
//                 } else {
//                     $("#isBientap").prop("checked", false);
//                     alert("Tài khoản này không tồn tại.");
//                 }
//                 $("#loading").hide();
//             });
//         }
//     } else {
//         //hủy ba nút biên tập
//         while (centerControlDiv_BienTapGiaoDien.firstChild) {
//             // console.log("CHan qua di");
//             centerControlDiv_BienTapGiaoDien.removeChild(centerControlDiv_BienTapGiaoDien.firstChild);
//         }
//         centerControlDiv_BienTapGiaoDien = document.createElement('div');

//         //
//         w3_open();
//         GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Giao Diện.", 250);

//         //
//         $("#socot").prop("disabled", false);
//         $("#isSocot").prop("disabled", false);
//         isHiensocot = false;
//         cb_hiensocot.checked = false;

//         //
//         $("#bientapexcel").prop("disabled", false);
//         $("#isBientapExcel").prop("disabled", false);
//         isBientapExcel = false;
//         cb_bientapexcel.checked = false;

//         //
//         isBientap = false;
//         bochuyendoiZoom(map.getZoom());
//         $("#tieudeI").text("I. Thông Tin Cột Hạ Thế: " + hashMapMarker.size + "");
//         for (var i = 0; i < hashMapMarker.size; i++) {
//             hashMapMarker.get(i).setDraggable(false);
//             hashMapMarker.get(i).setIcon({
//                 path: google.maps.SymbolPath.CIRCLE,
//                 scale: tron_scale,
//                 fillColor: hashMapMarker.get(i).icon.fillColor,
//                 fillOpacity: 1,
//                 strokeWeight: tron_stroke,
//                 origin: new google.maps.Point(0, 0), // origin
//                 anchor: new google.maps.Point(0, 0), // anchor
//                 labelOrigin: new google.maps.Point(0, -2),
//                 madviqly: hashMapMarker.get(i).icon.madviqly, //1
//                 matram: hashMapMarker.get(i).icon.matram, //2
//                 tentram: hashMapMarker.get(i).icon.tentram, //3
//                 socot: hashMapMarker.get(i).icon.socot, //4
//                 lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
//                 ishien: 0 //6
//             });

//             //
//             if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
//                 hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
//                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//             } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
//                 hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
//                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//             } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
//                 hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
//                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//             } else {
//                 hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
//                 hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
//             }
//         }

//         // Thiet lap lai tu dau
//         $("#ul_thietbi").empty();
//         $("#tieudeII").text("II. Thông Tin Thiết Bị: " + jsonKhachhangbandau.length + "");
//         // jsonKhachhang = jsonKhachhangbandau;
//         // $.each(jsonKhachhang, function (i, f) {
//         //     $("#ul_thietbi").append(
//         //         `<li onclick="getitem('${f.MA_KHANG}')">
//         //         <a style="font-size:12.3px"><b>${f.MA_KHANG}</b></a><br>
//         //         <a>${f.TEN_KHANG}</a><br>
//         //         <a>${f.DCHI_KHANG}</a><br>
//         //     </li>
//         //     <div style="background-color:rgb(50,190,166);height:1px"></div>`
//         //     );
//         // });

//         $("#hinhanh").empty();
//         $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length + "");
//         jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
//         $.each(jsonHinnhanh_cot, function (i, f) {
//             $("#hinhanh").append(
//                 `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
//              <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
//             </div>`
//             );
//         });
//     }
// });

// BIEN TAP LUOI DIEN EXCEL
var centerControlDiv_BienTapEXCEL = document.createElement('div');
// cb_bientapexcel.addEventListener('change', function () {

//     if (this.checked) {
//         var m_taikhoan = localStorage['taikhoan'];
//         var m_matkhau = localStorage['matkhau'];

//         //
//         if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
//             $("#isBientapExcel").prop("checked", false);
//             alert("Bạn chưa đăng nhập.");
//         } else {
//             //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
//             $("#loading").show();
//             $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
//                 if (data.length != 0) {
//                     data.forEach(function (nguoidungObj) {
//                         if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_PMIS_EXCUTE) !== -1) {
//                             if (macongty == null || macongty == '0') {
//                                 alert("Mời bạn chọn Công ty.")
//                             } else {
//                                 if (madienluc == null || madienluc == '0') {
//                                     alert("Mời bạn chọn Điện lực.");
//                                 } else {
//                                     if (matramnguon == null || matramnguon == '0') {
//                                         alert("Mời bạn chọn Trạm nguồn.");
//                                     } else {
//                                         if (maxuattuyen == null || maxuattuyen == '0') {
//                                             alert("Mời bạn chọn Xuất tuyến chính.");
//                                         } else {
//                                             //
//                                             w3_close();
//                                             GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Excel.", 250);
//                                             //
//                                             $("#bientap").prop("disabled", true);
//                                             $("#isBientap").prop("disabled", true);
//                                             isBientap = false;
//                                             cb_bientap.checked = false;

//                                             //
//                                             isBientapExcel = true;

//                                             //
//                                             var centerControl = new CenterControl(centerControlDiv_BienTapEXCEL, map);
//                                             centerControlDiv_BienTapEXCEL.index = 1;
//                                             map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapEXCEL);
//                                             bientapControl = 1;
//                                         }
//                                     }
//                                 }
//                             }
//                         } else {
//                             $("#isBientapExcel").prop("checked", false);
//                             alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Trung Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
//                         }
//                     })
//                 } else {
//                     $("#isBientapExcel").prop("checked", false);
//                     alert("Tài khoản này không tồn tại.");
//                 }
//                 $("#loading").hide();
//             });
//         }
//     } else {

//         //
//         w3_open();
//         GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Excel.", 250);
//         //
//         $("#bientap").prop("disabled", false);
//         $("#isBientap").prop("disabled", false);
//         isBientap = false;
//         cb_bientap.checked = false;

//         //
//         isBientapExcel = false;

//         while (centerControlDiv_BienTapEXCEL.firstChild) {
//             // console.log("CHan qua di");
//             centerControlDiv_BienTapEXCEL.removeChild(centerControlDiv_BienTapEXCEL.firstChild);
//         }
//         centerControlDiv_BienTapEXCEL = document.createElement('div');
//         bientapControl = 0;
//     }
// });

// Click nut xem bao cao
function clickXembaocao() {
    $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${localStorage['taikhoan']}`, function (data) {
        if (data.length != 0) {
            data.forEach(function (nguoidungObj) {
                if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_PMIS_READ) > -1
                    || nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_PMIS_EXCUTE) > -1) {

                    if (macongty == null || macongty == '0') {
                        $("#socot").hide();
                        $("#isSocot").hide();
                        $("#isBientap").hide();
                        $("#bientap").hide();
                        $("#isBientapExcel").hide();
                        $("#bientapexcel").hide();
                        $("#isBandonhiet").show();
                        $("#bandonhiet").show();
                        $("#isBandonhiet").prop("checked", false);
                        w3_close();

                        //
                        $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/PC11`, function (data) {
                            var X, Y;
                            data.forEach(function (cotObj) {
                                X = cotObj.X_COT;
                                Y = cotObj.Y_COT;
                            })
                            HeatMapInit('', X, Y, 7, true, false);
                        });
                    } else if (madienluc == null || madienluc == '0') {

                        //
                        $("#socot").hide();
                        $("#isSocot").hide();
                        $("#isBientap").hide();
                        $("#bientap").hide();
                        $("#isBientapExcel").hide();
                        $("#bientapexcel").hide();
                        $("#isBandonhiet").show();
                        $("#bandonhiet").show();
                        $('#search-cot').show();
                        $("#isBandonhiet").prop("checked", false);
                        w3_close();

                        //
                        dieukien = "'MA_DVIQLY' like '" + macongty + "%'";
                        $.get(`${url}/api/HATHE_getDMToadocongty_byDviqly/${macongty}`, function (data) {
                            var X, Y;
                            var i = 0;
                            data.forEach(function (cotObj) {
                                X = cotObj.X_COT;
                                Y = cotObj.Y_COT;
                                i++;
                            })
                            if (i == 0) {
                                alert("Công ty này không có dữ liệu");
                            } else {
                                HeatMapInit(dieukien, X, Y, 10, false, false);
                            }
                        });
                    } else {
                        initMap();
                        $('#search-cot').show();
                        var centerControlDiv_giaodienchinh = document.createElement('div');
                        var centerControl = new CenterControlGiaodienchinh(centerControlDiv_giaodienchinh, map);
                        centerControlDiv_giaodienchinh.index = 1;
                        map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_giaodienchinh);
                        bientapControl = 1;
                        tramda_clickdouble = new Array();
                        jsonKhachhang = '';
                        jsonKhachhangbandau = '';
                        jsonHinnhanh_cot = '';
                        jsonHinnhanh_cotbandau = '';
                        vitrimarker_click = '';
                        w3_open();
                    }

                    //hủy 3 button cot-hinhanh-image đi
                    // centerControlDiv_BienTapEXCEL = document.createElement('div');
                    // centerControlDiv_BienTapGiaoDien = document.createElement('div');
                } else {
                    alert("Tài khoản của bạn không có xem Lưới Điện Trung Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
                }
            })
        } else {
            alert("Tài khoản này không tồn tại.");
        }
        $("#loading").hide();
    });
}

// BAT DAU: Xem hinh anh cot chi tiet
function getHinhanhchitiet(index, cotorkhang) {
    document.getElementById('id_image').style.display = 'block';
    var n = 1;
    $("#hinhanhslide").empty();
    $("#dotanh").empty();
    if (cotorkhang == 0) {
        $.each(jsonHinnhanh_cot, function (i, f) {
            $("#hinhanhslide").append(
                `<div class="mySlides" style="height:auto; width:auto">
                        <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}" style="height:80%; width:auto">                
                </div>`
            );
            $("#dotanh").append(
                `<span class="dot" onclick="currentSlide(${n++})"></span>`
            );
        });
    } else if (cotorkhang == 1) {
        $.each(jsonHinnhanh_khang, function (i, f) {
            $("#hinhanhslide").append(
                `<div class="mySlides" style="height:auto; width:auto">
                    <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}" style="height:80%; width:auto">                
                </div>`
            );
            $("#dotanh").append(
                `<span class="dot" onclick="currentSlide(${n++})"></span>`
            );
        });
    }

    //
    currentSlide(index + 1, cotorkhang);
}
// KET THUC: Xem hinh anh chi tiet

function getHosochitiet(ctrl, ma_khang, ten_hinh) {
    var macongty_sohoa = "";
    var madienluc_sohoa = ""
    if (ma_khang.substring(0, 2) == 'PP' || ma_khang.substring(0, 2) == 'PQ') {
        macongty_sohoa = ma_khang.substring(0, 2);
        madienluc_sohoa = ma_khang.substring(0, 6);
    } else {
        var macongty_sohoa = ma_khang.substring(0, 4);
        var madienluc_sohoa = ma_khang.substring(0, 6);
    }

    var url = "";
    if (macongty == "PC01") {
        url = "http://10.126.0.22/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC02") {
        url = "http://10.125.184.2/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC03") {
        url = "http://10.124.0.9/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PP") {
        url = "http://10.123.99.15:8888/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC05") {
        url = "http://10.122.0.17/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC06") {
        url = "http://qnpc.evncpc.vn/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC07") {
        url = "http://qnpc.evncpc.vn/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC08") {
        url = "http://10.119.0.8/HoSo_KhachHang_DienTu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PQ") {
        url = "http://file.khpc.vn/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC10") {
        url = "http://10.117.0.38/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC11") {
        url = "http://10.116.0.10/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC12") {
        url = "http://10.115.0.14/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC13") {
        url = "http://10.114.0.8/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    }

    //console.log("MACONGTY: "+ macongty);
    //console.log("MADIENLUC: "+ madienluc);
    //console.log("URL: " + url);
    $('#obj_hosodientu').attr("data", url);
    $('#obj_hosodientu').show();

    //
    var list = document.getElementById("ul_hosodientu").getElementsByTagName('li');
    for (i = 0; i < list.length; i++) {
        list[i].style.background = '#ffffff';
    }

    ctrl.style.background = '#ddd';
}

// script cho slide ảnh
var slideIndex = 0;
function plusSlides(n) {
    showSlides(slideIndex += n);
    //
    if (cotorkhang == 0) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_cot.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    } else if (cotorkhang == 1) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_khang.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    }
}

var cotorkhang;
function currentSlide(n, m_cotorkhang) {
    cotorkhang = m_cotorkhang
    showSlides(slideIndex = n, cotorkhang);
    //
    if (cotorkhang == 0) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_cot.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    } else if (cotorkhang == 1) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_khang.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    }
}

//
function showSlides(n, m_cotorkhang) {
    if (m_cotorkhang == 0) {
        $("#tenhinhanh").text(jsonHinnhanh_cot[n - 1].TEN_HINHANH);
    } else if (m_cotorkhang == 1) {
        $("#tenhinhanh").text(jsonHinnhanh_khang[n - 1].TEN_HINHANH);
    }

    //
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" image_active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " image_active";
}

// A1
function getMarker_lonhonbang16() {
    bounds = map.getBounds();
    northEast = bounds.getNorthEast();
    southWest = bounds.getSouthWest();
    var i = 0;
    $("#loading").show();
    //console.log("URL: " + `${url}/api/TRUNGTHE_getDSCot_byXuattuyenXY/${madienluc}/${matramnguon}/${maxuattuyen}/${northEast.lat()}/${northEast.lng()}/${southWest.lat()}/${southWest.lng()}`);
    $.get(`${url}/api/TRUNGTHE_getDSCot_byXuattuyenXY/${madienluc}/${matramnguon}/${maxuattuyen}/${northEast.lat()}/${northEast.lng()}/${southWest.lat()}/${southWest.lng()}`, function (data) {

        //
        listCottrungthe = data;
        hashMapMarker = new Map();
        hashMapPoly = new Map();
        arrayMarker = [];
        arrayPoly = [];
        //
        data.forEach(function (cotObj) {

            addMarker(cotObj, i);
            if (cotObj.LK_COT1 == "") {
                // Khong lam chi ca
            } else {
                var mangsocot = cotObj.LK_COT1.split('$');
                for (var j = 0; j < mangsocot.length; j++) {
                    var cotnoi = data.find(function (obj) { return (obj.SO_COT === mangsocot[j] && obj.MA_TRAM === cotObj.MA_TRAM); });
                    if (cotnoi != null) {
                        addLine(cotObj, i, cotnoi);
                    }
                }
            }
            i++;
        })
        dieuchinhZoomchoTram();
        $("#loading").hide();
    });
}

function getMarker_doan15khoang16() {
    bounds = map.getBounds();
    northEast = bounds.getNorthEast();
    southWest = bounds.getSouthWest();
    var i = 0;
    $("#loading").show();
    cochu_chotram = 0;
    $.get(`${url}/api/TRUNGTHE_getDSCot_byXuattuyenXY/${madienluc}/${matramnguon}/${maxuattuyen}/${northEast.lat()}/${northEast.lng()}/${southWest.lat()}/${southWest.lng()}`, function (data) {

        //
        listCottrungthe = data;
        hashMapMarker = new Map();
        hashMapPoly = new Map();
        arrayMarker = [];
        arrayPoly = [];
        //
        data.forEach(function (cotObj) {

            //
            if (cotObj.LK_COT2 == GV_THUOCTINH_TRAMNGUON || cotObj.LK_COT2 == GV_THUOCTINH_TRAMCONGCONG || cotObj.LK_COT2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                addMarker(cotObj, i);
                i++;
            }

            //
            if (cotObj.LK_COT1 == "") {
                // Khong lam chi ca
            } else {
                var mangsocot = cotObj.LK_COT1.split('$');
                for (var j = 0; j < mangsocot.length; j++) {
                    var cotnoi = data.find(function (obj) { return (obj.SO_COT === mangsocot[j] && obj.MA_TRAM === cotObj.MA_TRAM); });
                    if (cotnoi != null) {
                        addLine(cotObj, i, cotnoi);
                    }
                }
            }
        })
        $("#loading").hide();
    });
}

function getMarker_nhohon15() {
    bounds = map.getBounds();
    northEast = bounds.getNorthEast();
    southWest = bounds.getSouthWest();
    var i = 0;
    $("#loading").show();
    cochu_chotram = 0;
    $.get(`${url}/api/TRUNGTHE_getDSCot_byLkcot2/${madienluc}/${matramnguon}/${maxuattuyen}`, function (data) {

        //
        listCottrungthe = data;
        hashMapMarker = new Map();
        hashMapPoly = new Map();
        arrayMarker = [];
        arrayPoly = [];
        //
        data.forEach(function (cotObj) {
            addMarker(cotObj, i);
            i++;
        })
        // Add a marker clusterer to manage the markers.
        //var markerCluster = new MarkerClusterer(map, arrayMarker,
        //    {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

        $("#loading").hide();
    });
}

// A
function clearOverlays() {
    for (var i = 0; i < arrayMarker.length; i++) {
        arrayMarker[i].setMap(null);
    }
    arrayMarker.length = 0;

    for (var i = 0; i < arrayPoly.length; i++) {
        arrayPoly[i].setMap(null);
    }
    arrayPoly.length = 0;
}

function dieuchinhZoomchoTram() {
    for (var i = 0; i < hashMapMarker.size; i++) {
        hashMapMarker.get(i).setIcon({
            path: hashMapMarker.get(i).icon.path,
            scale: tron_scale,
            fillColor: hashMapMarker.get(i).icon.fillColor,
            fillOpacity: 1,
            strokeWeight: tron_stroke,
            origin: new google.maps.Point(0, 0), // origin
            anchor: new google.maps.Point(0, 0), // anchor
            labelOrigin: new google.maps.Point(0, -2),
            madviqly: hashMapMarker.get(i).icon.madviqly,
            matramnguon: hashMapMarker.get(i).icon.matramnguon,
            maxuattuyen: hashMapMarker.get(i).icon.maxuattuyen,
            matram: hashMapMarker.get(i).icon.matram,
            tentram: hashMapMarker.get(i).icon.tentram,
            socot: hashMapMarker.get(i).icon.socot,
            lkcot2: hashMapMarker.get(i).icon.lkcot2,
            ishien: hashMapMarker.get(i).icon.ishien
        });

        //
        if (isHiensocot) {
            if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.matramnguon };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                }
            } else {
                hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: + cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
            }
        } else {
            if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG || hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMNGUON) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_XANHVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.matramnguon };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: + cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                }
            } else {
                if (hashMapMarker.get(i).icon.ishien == 1) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: + cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else {
                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                }
            }
        }

        //
        if ("" + i == vitrimarker_click) {
            hashMapMarker.get(i).label.fontSize = cochu + 'px';
            hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -6);
        }
    }
}
//Ham co gian nut search
function searchToggle(obj, evt) {

    var container = $(obj).closest('.search-wrapper');

    if (!container.hasClass('active')) {
        container.addClass('active');
        evt.preventDefault();
        $('#search-cot').hide();
    }
    else if (container.hasClass('active') && $(obj).closest('.input-holder').length == 0) {
        container.removeClass('active');
        // clear input
        container.find('.search-input').val('');
        // clear and hide result container when we press close
        container.find('.result-container').fadeOut(100, function () { $(this).empty(); });
        $('#search-cot').show();
    }
}
var array_dscot = [];
var dsCot_recent;
//ham search cot
$(".search-input-cot").on('change keydown paste input', function () {
    var str_thongtincot = $('.search-input-cot').val();
    str_thongtincot = str_thongtincot.toUpperCase();
    if (str_thongtincot.length == 0) {
        $("ul").remove("#ul_socot");
        $(".input-holder-cot").after(
            `<ul id="ul_socot" style="overflow-y: scroll;overflow-x: hidden;height:200px; width:250px;left: 35%;position: absolute;z-index: 10; background: #FFF;"></ul>`
        );
        array_dscot.forEach(function (i) {

            $("#ul_socot").append(
                `<li onclick="getdiadiemcot('${i}')">
                        <a style="font-size:12.3px">
                        <b>${i}</b>
                        </a>
                </li>
                <div style="background-color:rgb(50,190,166);height:1px;width:330px;"></div>`
            );
        });
    } else {
        $("ul").remove("#ul_socot");
        $(".input-holder-cot").after(
            `<ul id="ul_socot" style="overflow-y: scroll;overflow-x: hidden;height:200px; width:250px;left: 35%;position: absolute;z-index: 10; background: #FFF;"></ul>`
        );
        array_dscot.forEach(function (i) {
            if (i.includes(str_thongtincot)) {
                console.log(i.toString());
                $("#ul_socot").append(
                    `<li onclick="getdiadiemcot('${i}')">
                            <a style="font-size:12.3px">
                            <b>${i}</b>
                            </a>
                    </li>
                    <div style="background-color:rgb(50,190,166);height:1px;width:330px;"></div>`
                );
            }
        })
    }
})
//ham search cot
function searchcotToggle(obj, evt) {
    dsCot_recent=listCottrungthe;
    var container = $(obj).closest('.search-cot');

    if (!container.hasClass('active')) {
        container.addClass('active');

        $(".input-holder-cot").after(
            `<ul id="ul_socot" style="overflow-y: scroll;overflow-x: hidden;height:200px; width:250px;left: 35%;position: absolute;z-index: 10; background: #FFF;"></ul>`
        );
        dsCot_recent.forEach(function (cotObj) {
            if (array_dscot.length == 0) {
                array_dscot.push(`${cotObj.SO_COT}`);
            }
            else {
                if (array_dscot.includes(`${cotObj.SO_COT}`) == false) {
                    array_dscot.push(`${cotObj.SO_COT}`);
                }
            }


        });
        console.log(array_dscot.length);
        array_dscot.forEach(function (i) {
            $("#ul_socot").append(
                `<li onclick="getdiadiemcot('${i}')">
                        <a style="font-size:12.3px">
                        <b>${i}</b>
                        </a>
                </li>
                <div style="background-color:rgb(50,190,166);height:1px;width:330px;"></div>`
            );
        })
    }
    else if (container.hasClass('active')) {
        console.log("alalallalala");
        var str_thongtincot = $('.search-input-cot').val();
        str_thongtincot = str_thongtincot.toUpperCase();
        console.log(str_thongtincot);
        if (str_thongtincot.length > 0) {
            array_dscot.forEach(function (i) {
                if (str_thongtincot === i) {
                    $("ul").remove("#ul_socot");
                    getdiadiemcot(i);
                    $('.search-input-cot').val(i.toString());
                }
            })
        }
    }

}
function searchcotdisable(obj, evt) {

    var container = $(obj).closest('.search-cot');
    if (container.hasClass('active')) {
        console.log("kikikikikiki");
        $("ul").remove("#ul_socot");
        console.log("kakakak");
        container.removeClass('active');
        // clear input
        container.find('.search-cot').val('');
        // clear and hide result container when we press close
        container.find('.result-container').fadeOut(100, function () { $(this).empty(); });


    }
}
function Deletearray_dscot() {
    for (var i = 0; i < array_dscot.length; i++) {
        array_dscot.pop();
    }

}
var markerss=[];
//search thong tin cot
function getdiadiemcot(socot) {
    var container = $(".search-icon-cot").closest('.search-cot');
   
    if (container.hasClass('active')) {
        $("#ul_socot").remove();
    }
    $('.search-input-cot').val(socot);

    removeMarkers();

    var cotObj = dsCot_recent.find(function (obj) { return obj.SO_COT === socot });
    if (cotObj.X_COT == null || cotObj.X_COT == '') {
        alert("Tọa độ điểm chưa có. Xin vui lòng nhập số liệu.");
    } else {
        var center = new google.maps.LatLng(cotObj.X_COT, cotObj.Y_COT);
        var image = {
            url: 'https://png.icons8.com/color/48/000000/filled-flag.png',

            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(10, 43)
        };
        // using global variable:
        var marker1 = new google.maps.Marker({
            position: center,
            icon: image,
            draggable: true,
            map: map
        });
        markerss.push(marker1);
        map.panTo(center);

    }
    document.getElementById('search-wrapper').style.display = 'block';
    document.getElementById('search-cot').style.display = 'block';
}
function removeMarkers() {
    for (i = 0; i < markerss.length; i++) {
        markerss[i].setMap(null);
    }
}
