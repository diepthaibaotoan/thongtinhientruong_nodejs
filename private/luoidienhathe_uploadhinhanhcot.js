var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);
var madviqly, tendviqly, matram, tentram, socot, tenkhachhang, taikhoan;


$(document).ready(function () {
    madviqly = localStorage['madviqly'];
    tendviqly = localStorage['tendviqly'];
    matram = localStorage['matram'];
    tentram = localStorage['tentram'];
    socot = localStorage['socot'];
    taikhoan = localStorage['taikhoan'];
    document.getElementById("text_madviqly").innerHTML = madviqly + " - " + tendviqly;
    document.getElementById("text_matram").innerHTML = matram + " - " + tentram;
    document.getElementById("text_socot").innerHTML = socot;
});

/* ----- BAT DAU BIEN TAP -----*/
$('#loading').hide();
$(function () {
    $(':input[type="submit"]').prop('disabled', true);

    // Multiple images preview in browser
    var imagesPreview = function (input, placeToInsertImagePreview) {
        if (input.files) {
            $(".responsive-parent").empty();

            var filesAmount = input.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    $($.parseHTML('<img class="img-gallery">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }

            $('input[name="madviqly"]').attr('value', madviqly);
            $('input[name="matram"]').attr('value', matram);
            $('input[name="socot"]').attr('value', socot);
            $('input[name="taikhoan"]').attr('value', taikhoan);
            $(':input[type="submit"]').prop('disabled', false);

        }
    };

    $('#gallery-photo-add').on('change', function () {
        imagesPreview(this, 'div.responsive-parent');
    });
});

function quayLai() {
    window.close();
}

function guiDuLieu() {
    document.getElementById('id01_excel').style.display = 'block';
}
