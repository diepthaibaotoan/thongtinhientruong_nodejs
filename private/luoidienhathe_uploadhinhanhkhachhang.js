var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);
var madviqly, matram, tentram, makhachhang, tenkhachhang, taikhoan;


$(document).ready(function () {
    madviqly = localStorage['madviqly'];
    matram = localStorage['matram'];
    tentram = localStorage['tentram'];
    makhachhang = localStorage['makhachhang'];
    tenkhachhang = localStorage['tenkhachhang'];
    taikhoan = localStorage['taikhoan'];
    document.getElementById("text_tenkhachhang").innerHTML = tenkhachhang;
    document.getElementById("text_matram").innerHTML = matram + " - " + tentram;
    document.getElementById("text_makhachhang").innerHTML = makhachhang;
});

/* ----- BAT DAU BIEN TAP -----*/
$('#loading').hide();
$(function () {
    $(':input[type="submit"]').prop('disabled', true);

    // Multiple images preview in browser
    var imagesPreview = function (input, placeToInsertImagePreview) {
        if (input.files) {
            $(".responsive-parent").empty();

            var filesAmount = input.files.length;
            for (var i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function (event) {
                    $($.parseHTML('<img class="img-gallery">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }

            $('input[name="madviqly"]').attr('value', madviqly);
            $('input[name="matram"]').attr('value', matram);
            $('input[name="makhachhang"]').attr('value', makhachhang);
            $('input[name="taikhoan"]').attr('value', taikhoan);
            $(':input[type="submit"]').prop('disabled', false);

        }
    };

    $('#gallery-photo-add').on('change', function () {
        imagesPreview(this, 'div.responsive-parent');
    });
});

function quayLai() {
    window.close();
}

function guiDuLieu() {
    document.getElementById('id01_excel').style.display = 'block';
}
