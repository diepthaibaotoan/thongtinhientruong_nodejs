var url = GV_URL;

/* ----- BAT DAU BIEN TAP -----*/
var bientapControl = 0;
var Array_DSCOT;
var Array_DSHINHANH;
var oFile_COT;
var oFile_HINHANH;
var oFile_ImageUpload;
$('#loading').hide();
$(function () {
    $(':input[type="submit"]').prop('disabled', true);

    // Multiple images preview in browser
    var imagesPreview = function (input, placeToInsertImagePreview) {

        if (input.files) {
            $(".responsive-parent").empty();

            var filesAmount = input.files.length;
            var soluong_anhdung = 0;
            var soluong_annhsai = 0;
            for (var i = 0; i < filesAmount; i++) {
                var isCorrectImg = input.files[i].name[6];
                if (isCorrectImg != '_') {
                    soluong_annhsai++;

                } else {
                    // console.log(i + "--dung");
                    soluong_anhdung++;
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        $($.parseHTML('<img class="img-gallery">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
            if(soluong_annhsai==0){
                $(':input[type="submit"]').prop('disabled', false);
                //GF_SHOWTOAST_LONG("Ảnh được load thành công.",300);
                GF_SHOWTOAST_LONG("Thành công.",300);
            }else if(soluong_anhdung == 0){
                $(':input[type="submit"]').prop('disabled', true);
                //GF_SHOWTOAST_LONG("Không có ảnh nào hợp lệ",300);
                GF_SHOWTOAST_LONG("Thất bại.",300);

            }else {
                $(':input[type="submit"]').prop('disabled', false);
                //var error = "Chỉ có "+soluong_anhdung+" ảnh hợp lệ";
                var error = "Chỉ có "+soluong_anhdung+" ảnh đúng";
                GF_SHOWTOAST_LONG(error,300);
            }
        }
    };

    $('#gallery-photo-add').on('change', function () {
        imagesPreview(this, 'div.responsive-parent');
    });
});

function quayLai() {
    window.close();
}

function clickDongy() {
    window.close();
}

function guiDuLieu() {
    // alert("THANH CONG");
    document.getElementById('id01_excel').style.display = 'block';
}
