var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matram;
var nhanvien;
var jsonKhachhang;
var list_KhachHangDuocChon = [];
var madonvicuanguoidung = '';
var m_taikhoan = localStorage['taikhoan'];
var chonTatCa = false;

$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

    // Kiem tra user
    $.get(`${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`, function (data) {
        $.each(data, function (i, f) {
            madonvicuanguoidung = f.MA_DVIQLY;
            if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
                $("#congty").select2().val(madonvicuanguoidung);
                $("#congty").trigger('change');
                $("#congty").prop('disabled', true);
            } else if (madonvicuanguoidung.length == 6) {
                if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 2));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                } else {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 4));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                }
            }
        });

        //Khởi tạo lần đầu
        clickTaoBangDuLieu();
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});

$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

$("#loading").hide();

function taoDuLieuChoBang(data) {
    $("#dataTable").dataTable().fnDestroy();
    $("#tbody").empty();

    var index = 1;
    $.each(data, function (i, f) {
        var str = "";
        var ischeckbox = false;
        if (f.BUOC_TTHAO == TTCT_TRANGTHAIBINHTHUONG) {
            str = "Chưa phân công"
        } else if (f.BUOC_TTHAO > TTCT_TRANGTHAIBINHTHUONG && f.BUOC_TTHAO < TTCT_TRANGTHAIGUICMIS) {
            str = "Đã phân công"
        } else if (f.BUOC_TTHAO == TTCT_TRANGTHAIGUICMIS) {
            str = "Chờ gửi CMIS"
        } else if (f.BUOC_TTHAO == TTCT_TRANGTHAIHOANTHANHGUICMIS) {
            ischeckbox = true;
            str = `<label class="container" style="padding-left:10px">
                        <input id="${f.MA_DDO}" type="checkbox" onclick="chonKhachHang('${f.MA_DDO}')">Hoàn thành
                        <span class="checkmark"></span>
                        </label>`;
        }
        $("#tbody").append(
            `<tr>
                    <td style='text-align:center'>${index++}</td>
                    <td style='text-align:center'>
                        ${str}
                    </td>
                    <td style='text-align:left'>${f.MA_DDO}</td>
                    <td style='text-align:left'>${f.TEN_KHANG}</td>
                    <td style='text-align:left'>${f.DCHI_SDD}</td>
                    <td style='text-align:left'>${f.SO_CTO}</td>
                    <td style='text-align:left'>${f.MA_SOGCS + "-" + f.STT}</td>
                    <td style='text-align:right'>${f.NGUOI_DUOCPHANCONG}</td>
                </tr>`
        );
        if (chonTatCa) {
            if (ischeckbox) {
                var temp = document.getElementById(jsonKhachhang[i].MA_DDO);
                temp.checked = true;
            }
        }
    });

    $('#dataTable').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "10%" },
            { "width": "10%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "5%" },
            { "width": "5%" },
            { "width": "5%" }
        ]
    });
}

// Click nut xem bao cao
function clickTaoBangDuLieu() {
    if (macongty == null || macongty == '0') {
    } else {
        if (madienluc == null || madienluc == '0') {
        } else {
            if (matram == null || matram == '0') {
            } else {
                $("#loading").show();
                $.get(`${url}/api/TTCT_getDSKhachHangDeThongKe_byTram/${madienluc}/${matram}`, function (data) {
                    jsonKhachhang = data;
                    taoDuLieuChoBang(jsonKhachhang);
                    $("#loading").hide();
                });
            }
        }
    }
}

function chonKhachHang(madiemdo) {
    var checkBox_ = document.getElementById(madiemdo);
    var khachhangObj = jsonKhachhang.find(function (obj) { return obj.MA_DDO === madiemdo });
    if (checkBox_.checked == true) {
        list_KhachHangDuocChon.push(khachhangObj);
    } else {
        list_KhachHangDuocChon = GF_removeFunction(list_KhachHangDuocChon, "MA_DDO", madiemdo);
    }
}

function chonTatCaKhachHang() {
    var checkBox_ = document.getElementById('isChonTatCaKhachHang');

    if (jsonKhachhang == undefined || jsonKhachhang.length == 0) {
        alert("Không có khách hàng để chọn");
        checkBox_.checked = false;
    } else {
        if (checkBox_.checked == true) {
            chonTatCa = true;
            list_KhachHangDuocChon = new Array();
            for (var i = 0; i < jsonKhachhang.length; i++) {
                if (jsonKhachhang[i].BUOC_TTHAO == 8)
                    list_KhachHangDuocChon.push(jsonKhachhang[i]);
            }
        } else {
            chonTatCa = false;
            list_KhachHangDuocChon = new Array();
        }
        taoDuLieuChoBang(jsonKhachhang);
    }
}

function xacNhan() {
    if (list_KhachHangDuocChon == undefined || list_KhachHangDuocChon.length == 0) {
        alert("Bạn chưa chọn khách hàng nào");
    } else {
        xacNhanGuiDuLieu();
    }
}

function xacNhanGuiDuLieu() {
    try {
        if (list_KhachHangDuocChon.length == 0) {
            alert("Không có dữ liệu nào để xác nhận.");
        } else {
            document.getElementById('id_dktt').style.display = 'block';
            var obj = $("#text_dktt").text("Bạn có đồng ý đồng bộ hóa cho " + list_KhachHangDuocChon.length +
                " khách hàng đã hoàn thiện quy trình treo tháo công tơ này không?");
            obj.html(obj.html().replace(/\n/g, '<br/>'));
        }
    } catch (e) {
        alert("Không có dữ liệu nào để xác nhận.")
    }
}

function guiDanhSachDongBoLaiKhachHang(jsonArrays, m_i) {
    if (jsonArrays.length > 0) {
        $.get(`${url}/api/TTCT_updateBuocTreoThaoCongToVeBinhThuong/${jsonArrays[m_i].MA_DDO}/${TTCT_TRANGTHAIBINHTHUONG}`, function (data) {
            m_i++;
            if (jsonArrays.length == m_i) {
                alert("Bạn gửi dữ liệu thành công.");
                reload();
            } else {
                guiDanhSachDongBoLaiKhachHang(jsonArrays, m_i);
            }
        });
    } else {
        $("#loading").hide();
        reload();
    }
}

function Dongy() {
    $("#loading").show();
    guiDanhSachDongBoLaiKhachHang(list_KhachHangDuocChon, 0);
}

function Huy() {
    document.getElementById('id_dktt').style.display = 'none';
}

function reload() {
    $("#loading").hide();
    document.getElementById('isChonTatCaKhachHang').checked = false;
    chonTatCa = false;
    clickTaoBangDuLieu();
    document.getElementById('id_dktt').style.display = 'none';
    list_KhachHangDuocChon = new Array();
}
