// KEY
var GV_KEY_TOANDTB = "toandtb";
var GV_URL = "https://ttht.cpc.vn:8080";

// BANG MA MAU
var GV_COLOR_VANGVIENDEN = "#FFFF00";
var GV_COLOR_DOVIENDEN = "#FF0000";
var GV_COLOR_CAMVIENDEN = "#FF8C00";
var GV_COLOR_XANHDATROIVIENDEN = "#00FFFF";
var GV_COLOR_XANHLACAYVIENDEN = "#00FF00";
var GV_COLOR_TIMVIENDEN = "#9900FF";
var GV_COLOR_HONGVIENDEN = "#FF66FF";
var GV_COLOR_XAMNHATVIENDEN = "#BBBBBB";
var GV_COLOR_XAMDAMVIENDEN = "#777777";
var GV_COLOR_XANHVIENDEN = "#0000FF";
var GV_COLOR_MAUDEN = "#000000";
var GV_COLOR_MAUTRANG = "#FFFFFF";
var GV_COLOR_MAUDO = "#FF0000";

var GV_ARRAY_COLOR = ['#FF0000' , '#0000FF', '#9900FF', '#009900', '#000000', '#FF3E96', '#008080']; // DO, XANH, TIM, XANH LA CAY, DA

// THUOC TINH
var GV_THUOCTINH_COTHATHE = 'PH';
var GV_THUOCTINH_COTHATHETRUNGTHE_KETHOP = 'PTH';
var GV_THUOCTINH_COT = 'P';
var GV_THUOCTINH_COTTRUNGTHE = 'PT';
var GV_THUOCTINH_TRAMCONGCONG = 'SPUBLIC';
var GV_THUOCTINH_TRAMCHUYENDUNG = 'SPRIVATE';
var GV_THUOCTINH_DCU = 'D';
var GV_THUOCTINH_ROUTER = 'R';
var GV_THUOCTINH_TUDIENNGAMHATHE = 'TDNHT';
var GV_THUOCTINH_RMU = 'TRMU';
var GV_THUOCTINH_TRAMNGUON = 'SMID';

// Danh trong bao cao
var GV_CAPTONG = 1, GV_CAPCONGTY = 2, GV_CAPDIENLUC = 3, GV_CAPTRAM = 4;
var GV_NGUOIDUNG_QUYENTTHT1 = 'TTKH_ADMIN_CAPDIENLUC';
var GV_NGUOIDUNG_QUYENTTHT2 = 'ADMIN_TTHT';

var ADMIN_PHANQUYEN = "ADMIN_PHANQUYEN";
var ADMIN_TTHT = "ADMIN_TTHT";
var ADMIN_TTCT = "ADMIN_TTCT";
var ADMIN_TTHT_CMIS_READ = 'ADMIN_TTHT_CMIS_READ';
var ADMIN_TTHT_CMIS_EXCUTE = 'ADMIN_TTHT_CMIS_EXCUTE';
var ADMIN_TTHT_PMIS_READ = 'ADMIN_TTHT_PMIS_READ';
var ADMIN_TTHT_PMIS_EXCUTE = 'ADMIN_TTHT_PMIS_EXCUTE';

//Treo Thao Cong To
var ADMIN_TTCT_PHANQUYEN = 'ADMIN_TTCT_PHANQUYEN';
var ADMIN_TTCT_PHANCONGNHIEMVU = 'ADMIN_TTCT_PHANCONGNHIEMVU';
var ADMIN_TTCT_THUCHIENNHIEMVU = 'ADMIN_TTCT_THUCHIENNHIEMVU';

var TTCT_TRANGTHAIBINHTHUONG = 0;
var TTCT_TRANGTHAIPHANCONGNHIEMVU = 1;
var TTCT_TRANGTHAIGUICMIS = 7;
var TTCT_TRANGTHAIHOANTHANHGUICMIS = 8;

var TTCT_CONGTODANGTREO = 'DANGTREO';
var TTCT_CONGTODATHAO = 'DATHAO';

var GV_KHONGTHAYDOI_MADVIQLY = 'TTHT';
var GV_KHONGTHAYDOI_MADVIQLY_TTCT = 'TTCT';

//
var GV_CHUOI_MATRAMNGUON = "%20";
var GV_CHUOI_MAXUATTUYEN = "HA_THE";
var GV_CHUOI_MATRAM = "%20";

//
var GV_TYPEIMAGE_KH = 'KH';
var GV_TYPEIMAGE_COT = 'COT';

//Quản lý cap
var GV_CHUOI_KINHDOANH = "KD";
var GV_CHUOI_QUANLYCAP = "QLC";
var GV_CHUOI_SUACHUALON = "SCL";
var GV_CHUOI_QUANLYCAP_XAYDUNGCOBAN = "XDCB";

//Tọa độ GG MAP CTDL
var TD_QUANGBINH_X = 17.502818;
var TD_QUANGBINH_Y = 106.620006;

var TD_QUANGTRI_X = 16.835573;
var TD_QUANGTRI_Y = 107.108602;

var TD_HUE_X = 16.465287;
var TD_HUE_Y = 107.590393;

var TD_DANANG_X = 16.063726;
var TD_DANANG_Y = 108.212205;

var TD_QUANGNAM_X = 15.662436;
var TD_QUANGNAM_Y = 108.466810;

var TD_QUANGNGAI_X = 15.128819;
var TD_QUANGNGAI_Y = 108.784050;

var TD_BINHDINH_X = 13.783719;
var TD_BINHDINH_Y = 109.230523;

var TD_PHUYEN_X = 13.091077;
var TD_PHUYEN_Y = 109.307410;

var TD_KHANHHOA_X = 12.256446;
var TD_KHANHHOA_Y = 109.196857;

var TD_GIALAI_X = 13.977588;
var TD_GIALAI_Y = 108.001489;

var TD_KONTUM_X = 14.355361;
var TD_KONTUM_Y = 108.002981;

var TD_DAKLAK_X = 12.680660;
var TD_DAKLAK_Y = 108.044015;

var TD_DAKNONG_X = 12.004319;
var TD_DAKNONG_Y = 107.683949;

var TRANGTHAIBINHTHUONG = 0;
var TRANGTHAIDANGKY = 1;
var TRANGTHAIDUYETDANGKY = 2;
var TRANGTHAIDUYETTREOTHAO = 3;


//
function callcolor(name_color) {
    switch (name_color) {
        case "color_vangvienden":
            return "#FFFF00";
            break;
        case "color_dovienden":
            return "#FF0000";
            break;
        default:
            return "FF0000";
    }
}

//
function GF_SLEEP(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

function GR_WAIT(ms) {
    var start = new Date().getTime();
    var end = start;
    while (end < start + ms) {
        end = new Date().getTime();
    }
}

//
function GF_FORMATNUMBER(n, x, s, c) {
    var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')',
        num = this.toFixed(Math.max(0, ~~n));

    return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

function GF_REPLACEALL(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

//Hàm in màu đoạn text theo kí tự bắt đầu và kết thúc
function GF_MAKECOLORTEXT(startCharacter, finishCharacter, stringInput) {
    var stringcopy = stringInput;
    var start = -1;
    var finish = -1;
    while (true) {
        start = stringcopy.indexOf(startCharacter);
        finish = stringcopy.indexOf(finishCharacter);
        // console.log(start+"---"+finish);
        if (start < finish && start != -1) {
            var strFind = stringcopy.substring(start, finish + 1);
            var strThongBao = strFind.toLowerCase();
            var strReplace = "";
            if (strThongBao.indexOf("thông báo") > -1) {
                strReplace = "<i style='color:red'>" + strFind + "</i>";
            } else {
                strReplace = "<i style='color:blue'>" + strFind + "</i>";
            }
            stringInput = GF_REPLACEALL(stringInput, strFind, strReplace);
            var stringcopy = stringcopy.substring(finish + 2, stringcopy.length);
        } else {
            break;
        }
    }

    stringInput = GF_REPLACEALL(stringInput, '@Thông báo:', "<i style='color:red'>@Thông báo:</i>");
    return stringInput;
}


function GF_CHATAREA_TINNHAN(data, taikhoan) {
    //console.log("Test: "+data);
    var ds_tinnhan = "";
    data.forEach(function (item) {

        item.NGAY_NHAP = item.NGAY_NHAP.replace("T", " ");
        var ngay = item.NGAY_NHAP.substring(0, 16);
        item.NOI_DUNG = item.NOI_DUNG.trim();
        item.NOI_DUNG = GF_MAKECOLORTEXT('@', ':', item.NOI_DUNG);

        //
        if (item.chucdanh == null) {
            item.chucdanh = "";
        }
        if (item.tenpb == null) {
            item.tenpb = "";
        }
        if (item.tendv == null) {
            item.tendv = "";
        }

        if (item.chucdanh.indexOf("Trưởng Phòng") > -1) {
            item.chucdanh = "TP";
        }
        if (item.chucdanh.indexOf("Phó Phòng") > -1) {
            item.chucdanh = "PP";
        }
        if (item.chucdanh.indexOf("Chuyên viên") > -1) {
            item.chucdanh = "CV";
        }
        if (item.chucdanh.indexOf("Công nhân") > -1) {
            item.chucdanh = "CN";
        }
        if (item.chucdanh.indexOf("Nhân viên") > -1) {
            item.chucdanh = "NV";
        }
        if (item.chucdanh.indexOf("Cán sự") > -1) {
            item.chucdanh = "CS";
        }
        if (item.chucdanh.indexOf("Phó Tổng giám đốc") > -1) {
            item.chucdanh = "PTGĐ";
        }
        if (item.chucdanh.indexOf("Phó Trưởng Khoa") > -1) {
            item.chucdanh = "PTK";
        }

        item.tendv = GF_REPLACEALL(item.tendv, "Tổng Công ty Điện lực miền Trung", "Tổng Công ty");
        item.tendv = GF_REPLACEALL(item.tendv, "Công ty CNTT Điện lực miền Trung ", "CPCITC");
        item.tendv = GF_REPLACEALL(item.tendv, "Công ty TNHH MTV Điện lực", "PC");
        item.tendv = GF_REPLACEALL(item.tendv, "Công ty Điện lực", "PC");
        item.tendv = GF_REPLACEALL(item.tendv, "Công ty", "CT");
        item.tendv = item.tendv.trim();

        var thongtinuser = item.chucdanh + ". " + item.tenkhaisinh + ", " + item.tenpb + " - " + item.tendv;
        if (item.tenkhaisinh == null) {
            thongtinuser = item.TEN_DANGNHAP;
        }
        thongtinuser.trim();

        if (item.TEN_DANGNHAP == taikhoan) {
            if (item.TEN_DANGNHAP == 'toandtb' || item.TEN_DANGNHAP == 'thuanbq') {
                ds_tinnhan = ds_tinnhan +
                    "<div class='backgroundAreaChatADMIN'>" +
                    "<p class='outset'>" +
                    "<p class='alignright'>" + ngay + "</p>" +
                    "</p>" +
                    "<p class='alignrightChatContentADMIN'>" + item.NOI_DUNG + "</p>" +
                    "<div style='clear: both;'></div>" +
                    "</div>";
            } else {
                ds_tinnhan = ds_tinnhan +
                    "<div class='backgroundAreaChatUSER'>" +
                    "<p class='outset'>" +
                    "<p class='alignright'>" + ngay + "</p>" +
                    "</p>" +
                    "<p class='alignrightChatContentUSER'>" + item.NOI_DUNG + "</p>" +
                    "<div style='clear: both;'></div>" +
                    "</div>";
            }

        } else {
            if (item.TEN_DANGNHAP == 'toandtb' || item.TEN_DANGNHAP == 'thuanbq' || item.TEN_DANGNHAP == 'IT') {
                // item.TEN_DANGNHAP+='(ADMIN)';

                if (item.TEN_DANGNHAP == 'thuanbq') {
                    thongtinuser = 'Bùi Quang Thuận';
                }
                if (item.TEN_DANGNHAP == 'toandtb') {
                    thongtinuser = 'Diệp Thái Bảo Toàn';
                }
                ds_tinnhan = ds_tinnhan +
                    "<div class='backgroundAreaChatADMIN'>" +
                    "<p class='outset'>" +
                    "<p class='alignleftIDAD'>" + thongtinuser + ",       " + ngay + "</p>" +
                    "</p>" +
                    "<p class='alignleftChatContentADMIN'>" + item.NOI_DUNG + "  " + "</p>" +
                    "<div style='clear: both;'></div>" +
                    "</div>";
            } else {
                ds_tinnhan = ds_tinnhan +
                    "<div>" +
                    "<p class='outset'>" +
                    "<p class='alignleftID'>" + thongtinuser + ",       " + ngay + "</p>" +
                    "</p>" +
                    "<p class='alignleftChatContentAll'>" + item.NOI_DUNG + "</p>" +
                    "<div style='clear: both;'></div>" +
                    "</div>";
            }
        }
    });

    return ds_tinnhan;
}


function imageExists(url, callback) {
    var img = new Image();
    img.onload = function () { callback(true); };
    img.onerror = function () { callback(false); };
    img.src = url;
}

function GF_DANHSACHNGUOIDUNGONLINE(data) {
    var ds_nguoidungonline = "";
    data.forEach(function (item) {
        // var img = '<span><img class="some-style" src="https://image-eoffice.cpc.vn/Content/images/avatar/' + item.TEN_DANGNHAP + '.jpeg"' + '></span>';
        var imgAvatar;
        var imageUrl;
        imageUrl = 'https://image-eoffice.cpc.vn/Content/images/avatar/' + item.TEN_DANGNHAP + '.jpeg';

        //Set TK "thuanbq"
        if (item.TEN_DANGNHAP == 'thuanbq') {
            imageUrl = 'https://image-eoffice.cpc.vn/Content/images/avatar/thuanbq.jpg';
        }
        imgAvatar = '<span><img class="some-style" src="' + imageUrl + '"></span>';

        var tenuser;
        if (item.tenkhaisinh != null) {
            //Nếu tên quá dài thì ...
            if (item.tenkhaisinh.length > 20) {
                tenuser = '<span>  ' + item.tenkhaisinh.substring(0, 14) + "..." + '</span>';
            } else {
                tenuser = '<span>  ' + item.tenkhaisinh + '</span>';
            }
        } else {
            tenuser = '<span>  ' + item.TEN_DANGNHAP + '</span>';
        }

        //Set TK "thuanbq"
        if (item.TEN_DANGNHAP == 'thuanbq') {
            tenuser = '<span>  ' + "Bùi Quang Thuận" + '</span>';
        }

        var thietbi = '';
        if (item.THIET_BI.indexOf('Window') > -1) {
            thietbi = '<span class="thietbi"><img class="some-style-thietbi-window" src="https://cdn.iconscout.com/public/images/icon/premium/png-512/windows-laptop-client-39408d140235d40a-512x512.png" ></span>';
        } else if (item.THIET_BI.indexOf('Android') > -1) {
            thietbi = '<span class="thietbi"><img class="some-style-thietbi-android" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR3F2VnYoMUvRW4UqNowJjyHFv6Xfa8bOYcTP15FMTwPHDW8tLQ" ></span>';
        }

        ds_nguoidungonline = ds_nguoidungonline +
            "<div class='box-user-online'>" +
            imgAvatar +
            tenuser +
            thietbi +
            "</div>";
    });
    return ds_nguoidungonline;
}

//Chuyển tiếng việt có dấu sang không dấu
function GF_CONVERTTOENGLISH(str) {
    str.toString("utf8");
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
    str = str.replace(/đ/g, "d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g, " ");
    str = str.replace(/ + /g, " ");
    str = str.trim();
    return str;
}

//Hàm lấy thông tin người dùng cần tìm kiếm
function GF_TIMCHUOITHONGTIN(startCharacter, finishCharacter, stringInput) {
    var start = stringInput.indexOf(startCharacter);
    var finish = stringInput.indexOf(finishCharacter);

    var strFind = '';
    if (start < finish && start != -1) {
        strFind = stringInput.substring(start + 1, finish - 1);
        return strFind;
    }
    return strFind;
}


//Hàm showToast 
/* <button onclick="showToast()">Show Snackbar</button>
<div id="snackbar">Some text some message..</div> */
function GF_SHOWTOAST_LONG(chuoi, chieudai) {
    document.getElementById("snackbar").innerHTML = chuoi;
    var x = document.getElementById("snackbar");
    x.className = "show";
    x.style.width = chieudai;
    x.style.left = (screen.width - chieudai) / 2;
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
}

function GF_SHOWTOAST_SORT(chuoi, chieudai) {
    document.getElementById("snackbar").innerHTML = chuoi;
    var x = document.getElementById("snackbar");
    x.className = "show";
    x.style.width = chieudai;
    x.style.left = (screen.width - chieudai) / 2;
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 1000);
}

function GF_PHANMENU() {
    // Dang nhap tu dong lan dau
    var taikhoan = localStorage['taikhoan'];
    var matkhau = localStorage['matkhau'];
    if (taikhoan == '') {
        $("#menu_phanquyen").hide();
    } else {
        $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${taikhoan}`, function (data) {
            if (data.length != 0) {
                data.forEach(function (nguoidungObj) {
                    if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_PHANQUYEN) !== -1) {
                        $("#menu_phanquyen").show();
                    } else {
                        $("#menu_phanquyen").hide();
                    }
                })
            } else {
            }
        });
    }
}

function GF_distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var radlon1 = Math.PI * lon1 / 180
    var radlon2 = Math.PI * lon2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") { dist = dist * 1.609344 }
    if (unit == "N") { dist = dist * 0.8684 }
    return dist;
}

function battatgiaodienchinh(taikhoan) {
    if (taikhoan == undefined
        || taikhoan == null
        || taikhoan == '') {
        $(".header").hide();
        $("#mySidebar").hide();
        $("#id_thongtin").hide();
        $("#main").hide();
        $("#wrapper").hide();
        $("#giaodien_trangchu").hide();
        $(".card-header").hide();
    } else {
        // $(".header").show();
        // $("#mySidebar").show();
        // $("#id_thongtin").show();
        // $("#main").show();
    }
}

function GF_distanceBetweenTwoPoints(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1 / 180
    var radlat2 = Math.PI * lat2 / 180
    var radlon1 = Math.PI * lon1 / 180
    var radlon2 = Math.PI * lon2 / 180
    var theta = lon1 - lon2
    var radtheta = Math.PI * theta / 180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180 / Math.PI
    dist = dist * 60 * 1.1515
    if (unit == "K") { dist = dist * 1.609344 }
    if (unit == "N") { dist = dist * 0.8684 }
    return dist;
}

function GF_checkContentData(data) {
    if (data == undefined || data.trim() == '' || data.trim() == 'null' || data.trim() == 'NULL') {
        data = '%20';
    }
    data = GF_REPLACEALL(data, '/', '%2F');
    data = GF_REPLACEALL(data, ' ', '%20');
    data = GF_REPLACEALL(data, '-', '%2D');
    return data;
}


function GF_getTencuakhachhang(data) {
    var res = data.split(" ");
    return res[res.length - 1];
}

function GF_isValidString(str) {
    if (str == null || str == undefined || str.trim() == '') return false;
    return true;
}

function GF_removeFunction(myObjects, prop, value) {
    var newArray = myObjects.filter(function (val) {
        return val[prop] !== value;
    });
    return newArray;
}

function GF_isObjectExistInList(list, obj) {
    for (var i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }
    return false;
}

async function GF_getBase64(file) {
    return await new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
}

function GF_shuffle(arra1) {
    let ctr = arra1.length;
    let temp;
    let index;

    // While there are elements in the array
    while (ctr > 0) {
        // Pick a random index
        index = Math.floor(Math.random() * ctr);
        // Decrease ctr by 1
        ctr--;
        // And swap the last element with it
        temp = arra1[ctr];
        arra1[ctr] = arra1[index];
        arra1[index] = temp;
    }
    return arra1;
}