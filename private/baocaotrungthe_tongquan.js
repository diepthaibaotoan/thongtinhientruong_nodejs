GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matramnguon;
$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tramnguon").empty();
$("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tramnguon").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matramnguon = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matramnguon = '0';
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
    $.get(`${url}/api/TRUNGTHE_getDMTramnguon_byMadviqly/${madienluc.trim()}`, function (data) {
        $("#tramnguon").empty();
        $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
        $.each(data, function (i, f) {
            $("#tramnguon").append(`<option value="${f.MA_TRAMNGUON}">${f.MA_TRAMNGUON} - ${f.TEN_TRAMNGUON}</option>`);
        });
    });
});

$('#tramnguon').on('change', function () {
    matramnguon = this.value;
});

//chay mac dinh lan dau
$('#colwillchangeh').html('Công Ty');
$("#loading").show();
$.get(`${url}/api/TRUNGTHE_getBCTongquan/CPC/${GV_CHUOI_MATRAMNGUON}/${GV_CAPTONG}`, function (data) {
    $('#tongcong_colwillchangef').html('Tổng cộng:');
    var tongsocot = 0;
    var tongsoanhcot = 0;
    var tongsocotlkduongdayPMIS = 0;
    var tongsoanhkhachhang = 0;
    $.each(data, function (i, f) {
        tongsocot = tongsocot + f.SLUONG_COT;
        tongsoanhcot = tongsoanhcot + f.SLUONG_HINHANHCOT;
        tongsocotlkduongdayPMIS = tongsocotlkduongdayPMIS + f.SLUONG_COTLKDUONGDAYPMIS;
        tongsoanhkhachhang = tongsoanhkhachhang + f.SLUONG_COTLKVITRIPMIS;
        $("#tbody").append(
            `<tr>
                <td style='text-align:center'>${f.STT}</td>
                <td>${f.TEN_DONVILOC}</td>
                <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_HINHANHCOT.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_COTLKDUONGDAYPMIS.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_COTLKVITRIPMIS.toLocaleString('en')}</td>
            </tr>`
        );
    });
    $('#dataTable').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "35%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" }
        ]
    });
    //
    $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
    $('#tongsoanhcot_colwillchangef').html(tongsoanhcot.toLocaleString('en'));
    $('#tongsokhachhang_colwillchangef').html(tongsocotlkduongdayPMIS.toLocaleString('en'));
    $('#tongsoanhkhachhang_colwillchangef').html(tongsoanhkhachhang.toLocaleString('en'));
    $("#loading").hide();
});

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: dulieubaocao,
        columns: getColumns(dulieubaocao)
    });
}

// Click nut xem bao cao
function clickXembaocao() {

    //
    $('#tongcong_colwillchangef').html('');
    $('#tongsocot_colwillchangef').html('');
    $('#tongsoanhcot_colwillchangef').html('');
    $('#tongsokhachhang_colwillchangef').html('');
    $('#tongsoanhkhachhang_colwillchangef').html('');
    if (macongty == null || macongty == '0') {
        $("#dataTable").dataTable().fnDestroy();
        $("#tbody").empty();
        $('#colwillchangeh').html('Công Ty');
        $("#loading").show();
        //console.log("URL: "`${url}/api/TRUNGTHE_getBCTongquan/CPC/${GV_CAPTONG}` );
        $.get(`${url}/api/TRUNGTHE_getBCTongquan/CPC/${GV_CHUOI_MATRAMNGUON}/${GV_CAPTONG}`, function (data) {
            dulieubaocao = data;
            $('#tongcong_colwillchangef').html('Tổng cộng:');
            var tongsocot = 0;
            var tongsoanhcot = 0;
            var tongsocotlkduongdayPMIS = 0;
            var tongsoanhkhachhang = 0;
            $.each(data, function (i, f) {
                tongsocot = tongsocot + f.SLUONG_COT;
                tongsoanhcot = tongsoanhcot + f.SLUONG_HINHANHCOT;
                tongsocotlkduongdayPMIS = tongsocotlkduongdayPMIS + f.SLUONG_COTLKDUONGDAYPMIS;
                tongsoanhkhachhang = tongsoanhkhachhang + f.SLUONG_COTLKVITRIPMIS;
                //console.log("A: "+tongsocot);
                $("#tbody").append(
                    `<tr>
                        <td style='text-align:center'>${f.STT}</td>
                        <td>${f.TEN_DONVILOC}</td>
                        <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_HINHANHCOT.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTLKDUONGDAYPMIS.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTLKVITRIPMIS.toLocaleString('en')}</td>
                    </tr>`
                );
            });
            $('#dataTable').DataTable({
                "columns": [
                    { "width": "3%" },
                    { "width": "25%" },
                    { "width": "18%" },
                    { "width": "18%" },
                    { "width": "18%" },
                    { "width": "18%" }
                ]
            });
            //
            $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
            $('#tongsoanhcot_colwillchangef').html(tongsoanhcot.toLocaleString('en'));
            $('#tongsokhachhang_colwillchangef').html(tongsocotlkduongdayPMIS.toLocaleString('en'));
            $('#tongsoanhkhachhang_colwillchangef').html(tongsoanhkhachhang.toLocaleString('en'));
            $("#loading").hide();
        });
    } else {
        if (madienluc == null || madienluc == '0') {
            $("#dataTable").dataTable().fnDestroy();
            $("#tbody").empty();
            $('#colwillchangeh').html('Điện Lực');
            $("#loading").show();
            $.get(`${url}/api/TRUNGTHE_getBCTongquan/${macongty}/${GV_CHUOI_MATRAMNGUON}/${GV_CAPCONGTY}`, function (data) {
                $('#tongcong_colwillchangef').html('Tổng cộng:');
                var tongsocot = 0;
                var tongsoanhcot = 0;
                var tongsocotlkduongdayPMIS = 0;
                var tongsoanhkhachhang = 0;
                $.each(data, function (i, f) {
                    tongsocot = tongsocot + f.SLUONG_COT;
                    tongsoanhcot = tongsoanhcot + f.SLUONG_HINHANHCOT;
                    tongsocotlkduongdayPMIS = tongsocotlkduongdayPMIS + f.SLUONG_COTLKDUONGDAYPMIS;
                    tongsoanhkhachhang = tongsoanhkhachhang + f.SLUONG_COTLKVITRIPMIS;
                    $("#tbody").append(
                        `<tr>
                        <td style='text-align:center'>${f.STT}</td>
                        <td>${f.TEN_DONVILOC}</td>
                        <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_HINHANHCOT.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTLKDUONGDAYPMIS.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTLKVITRIPMIS.toLocaleString('en')}</td>
                    </tr>`
                    );
                });
                $('#dataTable').DataTable({
                    "columns": [
                        { "width": "5%" },
                        { "width": "35%" },
                        { "width": "15%" },
                        { "width": "15%" },
                        { "width": "15%" },
                        { "width": "15%" }
                    ]
                });
                //
                $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
                $('#tongsoanhcot_colwillchangef').html(tongsoanhcot.toLocaleString('en'));
                $('#tongsokhachhang_colwillchangef').html(tongsocotlkduongdayPMIS.toLocaleString('en'));
                $('#tongsoanhkhachhang_colwillchangef').html(tongsoanhkhachhang.toLocaleString('en'));
                $("#loading").hide();
            });
        } else {
            if (matramnguon == null || matramnguon == '0') {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $('#colwillchangeh').html('Trạm nguồn');
                $("#loading").show();
                $.get(`${url}/api/TRUNGTHE_getBCTongquan/${madienluc}/${GV_CHUOI_MATRAMNGUON}/${GV_CAPDIENLUC}`, function (data) {
                    $('#tongcong_colwillchangef').html('Tổng cộng:');
                    var tongsocot = 0;
                    var tongsoanhcot = 0;
                    var tongsocotlkduongdayPMIS = 0;
                    var tongsoanhkhachhang = 0;
                    $.each(data, function (i, f) {
                        tongsocot = tongsocot + f.SLUONG_COT;
                        tongsoanhcot = tongsoanhcot + f.SLUONG_HINHANHCOT;
                        tongsocotlkduongdayPMIS = tongsocotlkduongdayPMIS + f.SLUONG_COTLKDUONGDAYPMIS;
                        tongsoanhkhachhang = tongsoanhkhachhang + f.SLUONG_COTLKVITRIPMIS;
                        $("#tbody").append(
                            `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.TEN_DONVILOC}</td>
                            <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_HINHANHCOT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTLKDUONGDAYPMIS.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTLKVITRIPMIS.toLocaleString('en')}</td>
                        </tr>`
                        );
                    });
                    $('#dataTable').DataTable({
                        "columns": [
                            { "width": "5%" },
                            { "width": "35%" },
                            { "width": "15%" },
                            { "width": "15%" },
                            { "width": "15%" },
                            { "width": "15%" }
                        ]
                    });
                    //
                    $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
                    $('#tongsoanhcot_colwillchangef').html(tongsoanhcot.toLocaleString('en'));
                    $('#tongsokhachhang_colwillchangef').html(tongsocotlkduongdayPMIS.toLocaleString('en'));
                    $('#tongsoanhkhachhang_colwillchangef').html(tongsoanhkhachhang.toLocaleString('en'));
                    $("#loading").hide();
                });
            } else {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $('#colwillchangeh').html('Trạm nguồn');
                $("#loading").show();
                //console.log("URL:"+ `${url}/api/TRUNGTHE_getBCTongquan/${madienluc}/${matramnguon}/${GV_CAPTRAM}`);
                $.get(`${url}/api/TRUNGTHE_getBCTongquan/${madienluc}/${matramnguon}/${GV_CAPTRAM}`, function (data) {
                    $('#tongcong_colwillchangef').html('Tổng cộng:');
                    var tongsocot = 0;
                    var tongsoanhcot = 0;
                    var tongsocotlkduongdayPMIS = 0;
                    var tongsoanhkhachhang = 0;
                    $.each(data, function (i, f) {
                        tongsocot = tongsocot + f.SLUONG_COT;
                        tongsoanhcot = tongsoanhcot + f.SLUONG_HINHANHCOT;
                        tongsocotlkduongdayPMIS = tongsocotlkduongdayPMIS + f.SLUONG_COTLKDUONGDAYPMIS;
                        tongsoanhkhachhang = tongsoanhkhachhang + f.SLUONG_COTLKVITRIPMIS;
                        $("#tbody").append(
                            `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.TEN_DONVILOC}</td>
                            <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_HINHANHCOT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTLKDUONGDAYPMIS.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTLKVITRIPMIS.toLocaleString('en')}</td>
                        </tr>`
                        );
                    });
                    $('#dataTable').DataTable({
                        "columns": [
                            { "width": "5%" },
                            { "width": "35%" },
                            { "width": "15%" },
                            { "width": "15%" },
                            { "width": "15%" },
                            { "width": "15%" }
                        ]
                    });
                    //
                    $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
                    $('#tongsoanhcot_colwillchangef').html(tongsoanhcot.toLocaleString('en'));
                    $('#tongsokhachhang_colwillchangef').html(tongsocotlkduongdayPMIS.toLocaleString('en'));
                    $('#tongsoanhkhachhang_colwillchangef').html(tongsoanhkhachhang.toLocaleString('en'));
                    $("#loading").hide();
                });
            }
        }
    }
}
