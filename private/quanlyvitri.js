var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var socket = io(GV_URL);
var h = $("#mySidebar").height();
$("#ul_khachhang").height(h - 148);
$("#hinhanh").height(h - 106);

// AAAAA
var jsonDonvi, jsonKhachhangbandau, jsonKhachhang, jsonHinnhanh_cotbandau, jsonHinnhanh_cot, jsonHinnhanh_khang;
var macongty, madienluc, matram;
var CAPTONG = 1, CAPCONGTY = 2, CAPDIENLUC = 3, CAPTRAM = 4;
var madonvicuanguoidung = '';
var vitrimarker_click;
var stt_marker;

// BBBBBB
var zoom = 17;
var cochu = 10;
var rong = 24;
var cao = 34;
var m_rong = 24;
var m_cao = 34;
var chucach = -6;
var isHiensocot;
var isBientap;
var isBientapExcel;
var tron_scale, tron_stroke;
var map;
var marker;

//
var arrayMarker = [];
var arrayPoly = [];
var hashMapPoly = new Map();
var tramda_clickdouble = new Array();
var cotgoc;
var listCothathe;
var hinh_giotnuoc = 'M 0,0 C -2,-20 -10,-22 -10,-30 A 10,10 0 1,1 10,-30 C 10,-22 2,-20 0,0 z M -2,-30 a 2,2 0 1,1 4,0 2,2 0 1,1 -4,0';
//
var bounds;
var northEast;
var southWest;

var mylat = 16.055994;
var mylng = 108.186822;
var isHeadtoMap = false;
var isMaptoHead = false;

// Luc moi vo
$("#tieude").text("Demo");
$("#dongboCMIS").hide();
$("#text_ngaylaycuoi").hide();
$("#dongboEMEC").hide();
// 
$("#socot").hide();
$("#isSocot").hide();
$("#isBientap").hide();
$("#bientap").hide();
$("#isBientapExcel").hide();
$("#bientapexcel").hide();
$("#isBandonhiet").prop("checked", true);
$("#loading").hide();
w3_close();

// 2. Lang nghe server tra ve nguoi dang nhap
socket.on("Server-send-username", function (data) {
    for (var i = 0; i < arrayMarker.length; i++) {
        arrayMarker[i].setMap(null);
    }
    arrayMarker.length = 0;
    //
    data.forEach(function (f) {
        if(f.THIET_BI=='Android'){
            addMarker(f);
        }
    })
    GF_SHOWTOAST_SORT("Có "+arrayMarker .length+" thiết bị online.", 250);
});


//
function initMap() {
    var options;
    zoom = 7;
    options = {
        zoom: zoom,
        center: { lat: mylat, lng: mylng }
    }
    map = new google.maps.Map(document.getElementById('map'), options);
    moveToLocation(mylat, mylng);
}

/* ----- BAT DAU BIEN TAP -----*/
var bientapControl = 0;
var Array_DSCOT;
var Array_DSHINHANH;
var oFile_COT;
var oFile_HINHANH;
var oFile_ImageUpload;

// BIEN TAP: TAO NUT BIÊN TẬP GIAO DIỆN 
function CenterControl_BIENTAPGIAODIEN(controlDiv, map) {

    //create button DSCot in google map when choose option "Biên tập"
    var controlUILUU = document.createElement('button');
    controlUILUU.style.borderRadius = '5px 5px 5px 5px';
    controlUILUU.style.margin = '5px';
    controlUILUU.style.padding = '0px';
    controlUILUU.style.paddingTop = '1.8px';
    controlUILUU.style.width = '85px';
    controlUILUU.style.height = '35px';
    controlUILUU.style.backgroundColor = 'rgb(50,190,166)';
    controlUILUU.style.textAlign = 'center';
    controlUILUU.style.fontSize = '13px';
    controlUILUU.style.fontWeight = 'bold';
    //controlUICOT.style.color = '#FFF';
    controlUILUU.innerHTML = 'Lưu';
    controlUILUU.onclick = function () {

    };
    controlDiv.appendChild(controlUILUU);

    //create button DSHinh in google map when choose option "Biên tập"
    var controlUIHUY = document.createElement('button');
    controlUIHUY.style.borderRadius = '5px 5px 5px 5px';
    controlUIHUY.style.margin = '5px';
    controlUIHUY.style.padding = '0px';
    controlUIHUY.style.paddingTop = '1.8px';
    controlUIHUY.style.width = '85px';
    controlUIHUY.style.height = '35px';
    controlUIHUY.style.backgroundColor = 'rgb(50,190,166)';
    controlUIHUY.style.textAlign = 'center';
    controlUIHUY.style.fontSize = '13px';
    controlUIHUY.style.fontWeight = 'bold';
    //controlUIHINH.style.color = '#FFF';
    controlUIHUY.innerHTML = 'Hủy';
    controlUIHUY.onclick = function () {

    };
    controlDiv.appendChild(controlUIHUY);

    //create button DSImage in google map when choose option "Biên tập"
    var controlUIHDSD = document.createElement('button');
    controlUIHDSD.style.borderRadius = '5px 5px 5px 5px';
    controlUIHDSD.style.margin = '5px';
    controlUIHDSD.style.padding = '0px';
    controlUIHDSD.style.paddingTop = '1.8px';
    controlUIHDSD.style.width = '85px';
    controlUIHDSD.style.height = '35px';
    controlUIHDSD.style.backgroundColor = 'brown';
    controlUIHDSD.style.textAlign = 'center';
    controlUIHDSD.style.fontSize = '13px';
    controlUIHDSD.style.fontWeight = 'bold';
    controlUIHDSD.style.color = 'whitesmoke';
    controlUIHDSD.innerHTML = 'HDSD';
    controlUIHDSD.onclick = function () {

    };
    controlDiv.appendChild(controlUIHDSD);

}

// BIEN TAP: TAO NUT BIÊN TẬP GIAO DIỆN EXCEL
function CenterControl(controlDiv, map) {

    //create button DSCot in google map when choose option "Biên tập"
    var controlUICOT = document.createElement('button');
    controlUICOT.style.borderRadius = '5px 5px 5px 5px';
    controlUICOT.style.margin = '5px';
    controlUICOT.style.padding = '0px';
    controlUICOT.style.paddingTop = '1.8px';
    controlUICOT.style.width = '85px';
    controlUICOT.style.height = '35px';
    controlUICOT.style.backgroundColor = 'rgb(50,190,166)';
    controlUICOT.style.textAlign = 'center';
    controlUICOT.style.fontSize = '13px';
    controlUICOT.style.fontWeight = 'bold';
    //controlUICOT.style.color = '#FFF';
    controlUICOT.innerHTML = 'Excel Cột';
    controlUICOT.onclick = function () {
        localStorage['macongty_excel'] = macongty;
        localStorage['madienluc_excel'] = madienluc;
        localStorage['matram_excel'] = matram;
        var win = window.open("/luoidienhathe_uploaddscot", '_blank');
        win.focus();
    };
    controlDiv.appendChild(controlUICOT);

    //create button DSHinh in google map when choose option "Biên tập"
    var controlUIHINH = document.createElement('button');
    controlUIHINH.style.borderRadius = '5px 5px 5px 5px';
    controlUIHINH.style.margin = '5px';
    controlUIHINH.style.padding = '0px';
    controlUIHINH.style.paddingTop = '1.8px';
    controlUIHINH.style.width = '85px';
    controlUIHINH.style.height = '35px';
    controlUIHINH.style.backgroundColor = 'rgb(50,190,166)';
    controlUIHINH.style.textAlign = 'center';
    controlUIHINH.style.fontSize = '13px';
    controlUIHINH.style.fontWeight = 'bold';
    //controlUIHINH.style.color = '#FFF';
    controlUIHINH.innerHTML = 'Excel Hình';
    controlUIHINH.onclick = function () {
        // document.getElementById('id09').style.display = 'block';
        // alert("HINHANH");
        localStorage['macongty_excel'] = macongty;
        localStorage['madienluc_excel'] = madienluc;
        localStorage['matram_excel'] = matram;
        var win = window.open("/luoidienhathe_uploaddslinkhinhanh", '_blank');
        win.focus();
        // document.location.href = "/luoidienhathe_uploaddslinkhinhanh";
    };
    controlDiv.appendChild(controlUIHINH);

    //create button DSImage in google map when choose option "Biên tập"
    var controlUIImage = document.createElement('button');
    controlUIImage.style.borderRadius = '5px 5px 5px 5px';
    controlUIImage.style.margin = '5px';
    controlUIImage.style.padding = '0px';
    controlUIImage.style.paddingTop = '1.8px';
    controlUIImage.style.width = '85px';
    controlUIImage.style.height = '35px';
    controlUIImage.style.backgroundColor = 'rgb(50,190,166)';
    controlUIImage.style.textAlign = 'center';
    controlUIImage.style.fontSize = '13px';
    controlUIImage.style.fontWeight = 'bold';
    //controlUIImage.style.color = '#FFF';
    controlUIImage.innerHTML = 'Hình Ảnh';
    controlUIImage.onclick = function () {

        var win = window.open("/luoidienhathe_uploadhinhanh", '_blank');
        win.focus();
    };
    controlDiv.appendChild(controlUIImage);

    //create button HSSD in google map when choose option "Biên tập"
    var controlUIHDSD = document.createElement('button');
    controlUIHDSD.style.borderRadius = '5px 5px 5px 5px';
    controlUIHDSD.style.margin = '5px';
    controlUIHDSD.style.padding = '0px';
    controlUIHDSD.style.paddingTop = '1.8px';
    controlUIHDSD.style.width = '85px';
    controlUIHDSD.style.height = '35px';
    controlUIHDSD.style.backgroundColor = 'brown';
    controlUIHDSD.style.textAlign = 'center';
    controlUIHDSD.style.fontSize = '13px';
    controlUIHDSD.style.fontWeight = 'bold';
    controlUIHDSD.style.color = 'whitesmoke';
    controlUIHDSD.innerHTML = 'HDSD';
    controlUIHDSD.onclick = function () {
        var win = window.open("http://10.72.96.92/Mobile_GIS/evncpc/evncpc_ttht/7.%20H%C6%B0%E1%BB%9Bng%20d%E1%BA%ABn%20x%E1%BB%AD%20l%C3%BD%20khi%20g%E1%BB%ADi%20d%E1%BB%AF%20li%E1%BB%87u%20l%E1%BB%97i%20b%E1%BA%B1ng%20file%20Excel.mp4", '_blank');
        win.focus();
    };
    controlDiv.appendChild(controlUIHDSD);
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}
/* ----- KET THUC BIEN TAP -----*/

// Add Marker Function
function addMarker(f) {
    marker = new google.maps.Marker({
        position: { lat: parseFloat(f.LAT), lng: parseFloat(f.LONG) },
        map: map,
        draggable: false,
        //animation: google.maps.Animation.DROP,
        label: { color: 'red', fontSize: '12px', text: f.TEN_DANGNHAP },
    });
    var icon = {
        path: google.maps.SymbolPath.CIRCLE,
        scale: 4,
        fillColor: GV_COLOR_VANGVIENDEN,
        fillOpacity: 1.2,
        strokeWeight: 0.8,
        origin: new google.maps.Point(0, 0), // origin
        anchor: new google.maps.Point(0, 0), // anchor
        labelOrigin: new google.maps.Point(0, -3),
    };
    marker.setIcon(icon);
    arrayMarker.push(marker);
}

function val2key(val, array) {
    for (var i = 0; i < array.size; i++) {
        //console.log("KEY1: "+array.get(i).icon.socot);
        if (array.get(i).icon.socot == val) {
            //console.log("KEYA: "+array.get(i).icon.socot);
            return i;
        }
    }
}

// khong dung
function replaceLine(cotObjgoc, x_moi, y_moi, cotObjnoi) {
    var points1 = [new google.maps.LatLng(parseFloat(cotObjgoc.X_COT), parseFloat(cotObjgoc.Y_COT)), new google.maps.LatLng(parseFloat(cotObjnoi.X_COT), parseFloat(cotObjnoi.Y_COT))];
    var polyline1 = new google.maps.Polyline({ map: map, path: points1, strokeColor: GV_COLOR_TIMVIENDEN, strokeWeight: 1, strokeOpacity: 1 });

    var points2 = [new google.maps.LatLng(parseFloat(x_moi), parseFloat(y_moi)), new google.maps.LatLng(parseFloat(cotObjnoi.X_COT), parseFloat(cotObjnoi.Y_COT))];
    var polyline2 = new google.maps.Polyline({ map: map, path: points2, strokeColor: GV_COLOR_DOVIENDEN, strokeWeight: 1, strokeOpacity: 1 });

    cotgoctam.X_COT = x_moi;
    cotgoctam.Y_COT = y_moi;
}

function moveToLocation(lat, lng) {
    var center = new google.maps.LatLng(lat, lng);
    // using global variable:
    map.panTo(center);
}

/* BAT DAU AAAAA:  PHAN HEADER */
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

    // Kiem tra user
    var m_taikhoan = localStorage['taikhoan'];
    //console.log("AAA: "+ `${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`);
    $.get(`${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`, function (data) {
        $.each(data, function (i, f) {
            madonvicuanguoidung = f.MA_DVIQLY;
            //console.log("AAA: "+ madonvicuanguoidung);
            if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
                $("#congty").select2().val(madonvicuanguoidung);
                $("#congty").trigger('change');
                $("#congty").prop('disabled', true);
            } else if (madonvicuanguoidung.length == 6) {
                if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 2));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                } else {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 4));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                }
            }
        });
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    //
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/HATHE_getDMTram_byMadviqly/${madienluc.trim()}`, function (data) {
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

function w3_open() {
    document.getElementById("main").style.marginLeft = "17.7%";
    document.getElementById("mySidebar").style.width = "17.7%";
    document.getElementById("mySidebar").style.display = "block";
    document.getElementById("map").style.width = '82.3%';
}

function w3_close() {
    document.getElementById("main").style.marginLeft = "0%";
    document.getElementById("mySidebar").style.display = "none";
    //document.getElementById("openNav").style.display = "inline-block";
    document.getElementById("map").style.width = '100%';
}
/* KET THUC AAAAA:  PHAN HEADER */


// BIEN TAP LUOI DIEN EXCEL
var cb_bientapexcel = document.querySelector("input[name=cb_bientapexcel]");
var centerControlDiv_BienTapEXCEL = document.createElement('div');
cb_bientapexcel.addEventListener('change', function () {

    if (this.checked) {
        var m_taikhoan = localStorage['taikhoan'];
        var m_matkhau = localStorage['matkhau'];

        //
        $.get(`${url}/api/get_DangnhapTTKH_adminDienluc/${m_taikhoan}/${m_matkhau}/TTKH_ADMIN_CAPDIENLUC`, function (data) {
            if (data.length) {
                $("#loading").show();
                if (macongty == null || macongty == '0') {
                    alert("Mời bạn chọn Công ty")
                } else {
                    if (madienluc == null || madienluc == '0') {
                        alert("Mời bạn chọn Điện lực.");
                    } else {
                        if (matram == null || matram == '0') {
                            alert("Mời bạn chọn Trạm.");
                        } else {
                            //
                            w3_close();
                            GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Excel.", 250);
                            //
                            $("#bientap").prop("disabled", true);
                            $("#isBientap").prop("disabled", true);
                            isBientap = false;
                            cb_bientap.checked = false;

                            //
                            isBientapExcel = true;

                            //
                            var centerControl = new CenterControl(centerControlDiv_BienTapEXCEL, map);
                            centerControlDiv_BienTapEXCEL.index = 1;
                            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapEXCEL);
                            bientapControl = 1;
                        }
                    }
                }
            } else {
                $("#isBientapExcel").prop("checked", false);
                alert("Tài khoản của bạn không phải là Admin cấp Điện lực.\n Vui lòng liên hệ Công ty để lấy mã vạch phân quyền trên thiết bị di động thông minh.")
            }
            $("#loading").hide();
        });
    } else {

        //
        w3_open();
        GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Excel.", 250);
        //
        $("#bientap").prop("disabled", false);
        $("#isBientap").prop("disabled", false);
        isBientap = false;
        cb_bientap.checked = false;

        //
        isBientapExcel = false;

        while (centerControlDiv_BienTapEXCEL.firstChild) {
            // console.log("CHan qua di");
            centerControlDiv_BienTapEXCEL.removeChild(centerControlDiv_BienTapEXCEL.firstChild);
        }
        centerControlDiv_BienTapEXCEL = document.createElement('div');
        bientapControl = 0;
    }
});

// Click nut xem bao cao
function clickXembaocao() {
    if (macongty == null || macongty == '0') {
        $("#socot").hide();
        $("#isSocot").hide();
        $("#isBientap").hide();
        $("#bientap").hide();
        $("#isBientapExcel").hide();
        $("#bientapexcel").hide();
        $("#isBandonhiet").show();
        $("#bandonhiet").show();
        $("#isBandonhiet").prop("checked", false);
        w3_close();
    } else if (madienluc == null || madienluc == '0') {

        //
        $("#socot").hide();
        $("#isSocot").hide();
        $("#isBientap").hide();
        $("#bientap").hide();
        $("#isBientapExcel").hide();
        $("#bientapexcel").hide();
        $("#isBandonhiet").show();
        $("#bandonhiet").show();
        $("#isBandonhiet").prop("checked", false);
        w3_close();
    } else {
        initMap();
        jsonKhachhang = '';
        jsonKhachhangbandau = '';
        jsonHinnhanh_cot = '';
        jsonHinnhanh_khang = '';
        jsonHinnhanh_cotbandau = '';
        tramda_clickdouble = new Array();
        vitrimarker_click = '';
        w3_open();
    }

    //hủy 3 button cot-hinhanh-image đi
    centerControlDiv_BienTapEXCEL = document.createElement('div');
    centerControlDiv_BienTapGiaoDien = document.createElement('div');
}

// BAT DAU: DONG BO CMIS
function clickDongboCMIS() {
    if (matram == null || matram == '0') {
        document.getElementById('id02_2').style.display = 'block';
        var obj = $("#text_xacnhan2").text("Bạn có đồng ý lấy dữ liệu CMIS cho điện lực " + madienluc + " này không?"
            + " \nLưu ý: \n1. Khi bạn đồng ý, toàn bộ dữ liệu ở bảng trung gian sẽ bị xóa hết và sẽ được lấy dữ liệu mới nhất từ CMIS về."
            + "\n 2. Vì vậy, dữ liệu nào bạn thu thập ngoài hiện trường mà chưa gửi CMIS, bạn vui lòng gửi nếu cần thiết."
            + " \n3. Chức năng này chỉ có Quản trị ở Điện lực mới sử dụng được.");
        obj.html(obj.html().replace(/\n/g, '<br/>'));
    } else {
        document.getElementById('id02_1').style.display = 'block';
        var obj = $("#text_xacnhan1").text("Bạn có đồng ý lấy dữ liệu CMIS cho trạm " + matram + " này không?"
            + " \nLưu ý: \n1. Khi bạn đồng ý, toàn bộ dữ liệu ở bảng trung gian sẽ bị xóa hết và sẽ được lấy dữ liệu mới nhất từ CMIS về."
            + "\n 2. Vì vậy, dữ liệu nào bạn thu thập ngoài hiện trường mà chưa gửi CMIS, bạn vui lòng gửi nếu cần thiết."
            + " \n3. Chức năng này chỉ có Quản trị ở Điện lực mới sử dụng được.");
        obj.html(obj.html().replace(/\n/g, '<br/>'));
    }
}

function clickDongyDongbo_choTram() {
    var m_taikhoan = localStorage['taikhoan'];
    var m_matkhau = localStorage['matkhau'];
    //console.log("URL: " + `${url}/api/get_DangnhapTTKH_adminDienluc/${taikhoan}/${matkhau}/TTKH_ADMIN_CAPDIENLUC`);
    // Hien tai cho Admin cong ty van lay duoc
    $.get(`${url}/api/get_DangnhapTTKH_adminDienluc/${m_taikhoan}/${m_matkhau}/TTKH_ADMIN_CAPDIENLUC`, function (data) {
        if (data.length) {
            $("#loading").show();
            if (macongty == null || macongty == '0') {
                alert("Mời bạn chọn Công ty")
            } else {
                if (madienluc == null || madienluc == '0') {
                    alert("Mời bạn chọn Điện lực.");
                } else {
                    if (matram == null || matram == '0') {
                        alert("Mời bạn chọn Trạm.");
                    } else {
                        //console.log('URL: ' + `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSKhachhang_byTram?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&NGUOI_NHAP=${taikhoan}&KEY=${GV_KEY_TOANDTB}`);
                        $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSKhachhang_byTram?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&NGUOI_NHAP=${m_taikhoan}&KEY=${GV_KEY_TOANDTB}`, function (data) {
                            $("#loading").hide();
                            document.getElementById('id02_1').style.display = 'none';
                            document.getElementById('btn_xembaocao').click();
                        });

                        $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSHosodientu_byTram?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&KEY=${GV_KEY_TOANDTB}`, function (data) {
                            //console.log("HATHE_setDSHosodientu_byTram: " + data);
                        });

                        $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMTram_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
                            //console.log("HATHE_setDMTram_byMadviqly: " + data);
                        });
                    }
                }
            }
        } else {
            alert("Tài khoản của bạn không phải là Admin cấp Điện lực.\n Vui lòng liên hệ Công ty để lấy mã vạch phân quyền trên thiết bị di động thông minh.")
        }
    });
}

function clickDongyDongbo_choDienluc() {
    var m_taikhoan = localStorage['taikhoan'];
    var m_matkhau = localStorage['matkhau'];
    //console.log("URL: " + `${url}/api/get_DangnhapTTKH_adminDienluc/${taikhoan}/${matkhau}/TTKH_ADMIN_CAPDIENLUC`);
    // Hien tai cho Admin cong ty van lay duoc
    $.get(`${url}/api/get_DangnhapTTKH_adminDienluc/${m_taikhoan}/${m_matkhau}/TTKH_ADMIN_CAPDIENLUC`, function (data) {
        if (data.length) {
            $("#loading").show();
            if (macongty == null || macongty == '0') {
                alert("Mời bạn chọn Công ty")
            } else {
                if (madienluc == null || madienluc == '0') {
                    alert("Mời bạn chọn Điện lực.");
                } else {
                    console.log('URL: ' + `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_dongboDSKhachhang_byTram?MA_DVIQLY=${madienluc}&NGUOI_NHAP=${m_taikhoan}&KEY=${GV_KEY_TOANDTB}`);
                    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_dongboDSKhachhang_byTram?MA_DVIQLY=${madienluc}&NGUOI_NHAP=${m_taikhoan}&KEY=${GV_KEY_TOANDTB}`, function (data) {
                        $("#loading").hide();
                        document.getElementById('id02_2').style.display = 'none';
                        document.getElementById('btn_xembaocao').click();
                    });
                }
            }
        } else {
            alert("Tài khoản của bạn không phải là Admin cấp Điện lực.\n Vui lòng liên hệ Công ty để lấy mã vạch phân quyền trên thiết bị di động thông minh.")
        }
    });
}

// BAT DAU: DONG BO EMEC
function clickDongboEMEC() {
    document.getElementById('id03').style.display = 'block';
    var obj = $("#text_id03_xacnhan").text("Bạn có đồng ý lấy dữ liệu cột, tọa độ, liên kết của EMEC đổ vào dữ liệu của điện lực " + madienluc + " này không?"
        + " \nLưu ý: \n1. Chức năng này sẽ được hủy bỏ khi quá trình hợp nhất dữ liệu của EMEC và CPCITC kết thúc."
        + " \n2. Chức năng này chỉ có Quản trị ở Ứng dụng mới sử dụng được.");
    obj.html(obj.html().replace(/\n/g, '<br/>'));
}

function convert_namthangngay(thoigian) {
    if (thoigian == null || thoigian == '') {
        return "1900-01-01";
    } else {
        var chuoi = thoigian.split("T");
        var chuoitach = chuoi[0].split("-");
        return chuoi[0];
    }
}

function convert_khongchonull(chuoiguivao) {
    if (chuoiguivao == null) {
        return encodeURIComponent('');
    } else {
        return encodeURIComponent(chuoiguivao);
    }
}
// KET THUC: DONG BO CMIS

// BAT DAU: Xem hinh anh cot chi tiet
function getHinhanhchitiet(index, cotorkhang) {
    document.getElementById('id_image').style.display = 'block';
    var n = 1;
    $("#hinhanhslide").empty();
    $("#dotanh").empty();
    if (cotorkhang == 0) {
        $.each(jsonHinnhanh_cot, function (i, f) {
            $("#hinhanhslide").append(
                `<div class="mySlides" style="height:auto; width:auto">
                        <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}" style="height:80%; width:auto">                
                </div>`
            );
            $("#dotanh").append(
                `<span class="dot" onclick="currentSlide(${n++})"></span>`
            );
        });
    } else if (cotorkhang == 1) {
        $.each(jsonHinnhanh_khang, function (i, f) {
            $("#hinhanhslide").append(
                `<div class="mySlides" style="height:auto; width:auto">
                    <img src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}" style="height:80%; width:auto">                
                </div>`
            );
            $("#dotanh").append(
                `<span class="dot" onclick="currentSlide(${n++})"></span>`
            );
        });
    }

    //
    currentSlide(index + 1, cotorkhang);
}

function getHosochitiet(ctrl, ma_khang, ten_hinh) {
    var macongty_sohoa = "";
    var madienluc_sohoa = ""
    if (ma_khang.substring(0, 2) == 'PP' || ma_khang.substring(0, 2) == 'PQ') {
        macongty_sohoa = ma_khang.substring(0, 2);
        madienluc_sohoa = ma_khang.substring(0, 6);
    } else {
        var macongty_sohoa = ma_khang.substring(0, 4);
        var madienluc_sohoa = ma_khang.substring(0, 6);
    }

    var url = "";
    if (macongty == "PC01") {
        url = "http://10.126.0.22/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC02") {
        url = "http://10.125.184.2/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC03") {
        url = "http://10.124.0.9/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PP") {
        url = "http://10.123.99.15:8888/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC05") {
        url = "http://10.122.0.17/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC06") {
        url = "http://qnpc.evncpc.vn/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC07") {
        url = "http://qnpc.evncpc.vn/HOSO_KHACHHANG_DIENTU/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC08") {
        url = "http://10.119.0.8/HoSo_KhachHang_DienTu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PQ") {
        url = "http://file.khpc.vn/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC10") {
        url = "http://10.117.0.38/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC11") {
        url = "http://10.116.0.10/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC12") {
        url = "http://10.115.0.14/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    } else if (macongty == "PC13") {
        url = "http://10.114.0.8/hoso_khachhang_dientu/" + madienluc + "/" + ma_khang + "/" + ten_hinh;
    }

    //console.log("MACONGTY: "+ macongty);
    //console.log("MADIENLUC: "+ madienluc);
    //console.log("URL: " + url);
    $('#obj_hosodientu').attr("data", url);
    $('#obj_hosodientu').show();

    //
    var list = document.getElementById("ul_hosodientu").getElementsByTagName('li');
    for (i = 0; i < list.length; i++) {
        list[i].style.background = '#ffffff';
    }

    ctrl.style.background = '#ddd';
}

// script cho slide ảnh
var slideIndex = 0;
function plusSlides(n) {
    showSlides(slideIndex += n);
    //
    if (cotorkhang == 0) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_cot.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    } else if (cotorkhang == 1) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_khang.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    }
}

var cotorkhang;
function currentSlide(n, m_cotorkhang) {
    cotorkhang = m_cotorkhang
    showSlides(slideIndex = n, cotorkhang);
    //
    if (cotorkhang == 0) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_cot.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    } else if (cotorkhang == 1) {
        if (slideIndex == 1) {
            $('#anh_prev').hide();
        } else if (slideIndex == jsonHinnhanh_khang.length) {
            $('#anh_next').hide();
        } else {
            $('#anh_prev').show();
            $('#anh_next').show();
        }
    }
}

//
function showSlides(n, m_cotorkhang) {
    if (m_cotorkhang == 0) {
        $("#tenhinhanh").text(jsonHinnhanh_cot[n - 1].TEN_HINHANH);
    } else if (m_cotorkhang == 1) {
        $("#tenhinhanh").text(jsonHinnhanh_khang[n - 1].TEN_HINHANH);
    }

    //
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" image_active", "");
    }
    slides[slideIndex - 1].style.display = "block";
    dots[slideIndex - 1].className += " image_active";
}

// A
function clearOverlays() {
    for (var i = 0; i < arrayMarker.length; i++) {
        arrayMarker[i].setMap(null);
    }
    arrayMarker.length = 0;
}
