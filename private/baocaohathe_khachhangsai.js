GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matram;
$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});


//chay mac dinh lan dau
// $("#loading").show();
// //console.log("URL: "+`${url}/api/HATHE_getBCChitietKhachhangsai/CPC/%20/${GV_CAPTONG}`)
// $.get(`${url}/api/HATHE_getBCChitietKhachhangsai/CPC/%20/${GV_CAPTONG}`, function (data) {
//     dulieubaocao = data;
//     $('#tongcong_colwillchangef').html('Tổng cộng: '+data.length);       
//     $.each(data, function (i, f) {
//         $("#tbody").append(
//             `<tr>
//                 <td style='text-align:center'>${f.STT}</td>
//                 <td>${f.MA_DVIQLY}</td>
//                 <td>${f.MA_TRAM}</td>
//                 <td>${f.MA_KHANG}</td>
//                 <td>${f.SO_COT}</td>
//             </tr>`
//         );
//     });
//     $('#dataTable').DataTable({
//         "columns": [
//             { "width": "4%" },
//             { "width": "19%" },
//             { "width": "19%" },
//             { "width": "19%" },
//             { "width": "19%" },
//             { "width": "19%" }
//         ]
//     });
//     $("#loading").hide();
// });
$("#loading").hide();

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: dulieubaocao,
        columns: getColumns(dulieubaocao)
    });
}

// Click nut xem bao cao
function clickXembaocao() {
    $('#tongcong_colwillchangef').html('');
    $('#tongsocot_colwillchangef').html('');
    $('#tongsoanhcot_colwillchangef').html('');
    $('#tongsokhachhang_colwillchangef').html('');
    $('#tongsoanhkhachhang_colwillchangef').html('');
    if (macongty == null || macongty == '0') {
        alert("Mời bạn chọn Công ty");
        $("#dataTable").dataTable().fnDestroy();
        $("#tbody").empty();
        // $("#loading").show();
        // //console.log("URL: "`${url}/api/HATHE_getBCKhachhang/CPC/${GV_CAPTONG}` );
        // $.get(`${url}/api/HATHE_getBCChitietKhachhangsai/CPC/%20/${GV_CAPTONG}`, function (data) {
        //     dulieubaocao = data;
        //     $('#tongcong_colwillchangef').html('Tổng cộng: '+data.length);     
        //     $.each(data, function (i, f) {
        //         $("#tbody").append(
        //             `<tr>
        //                 <td style='text-align:center'>${f.STT}</td>
        //                 <td>${f.MA_DVIQLY}</td>
        //                 <td>${f.MA_TRAM}</td>
        //                 <td>${f.MA_KHANG}</td>
        //                 <td>${f.TEN_KHANG}</td>
        //                 <td>${f.SO_COT}</td>
        //             </tr>`
        //         );
        //     });
        //     $('#dataTable').DataTable({
        //         "columns": [
        //             { "width": "4%" },
        //             { "width": "19%" },
        //             { "width": "19%" },
        //             { "width": "19%" },
        //             { "width": "19%" },
        //             { "width": "19%" }
        //         ]
        //     });
        //     //
        //     $("#loading").hide();
        // });
    } else {
        if (madienluc == null || madienluc == '0') {
            alert("Mời bạn chọn Điện lực")
            $("#dataTable").dataTable().fnDestroy();
            $("#tbody").empty();
            // $("#loading").show();
            // $.get(`${url}/api/HATHE_getBCChitietKhachhangsai/${macongty}/%20/${GV_CAPCONGTY}`, function (data) {
            //     dulieubaocao = data;
            //     $('#tongcong_colwillchangef').html('Tổng cộng: '+data.length);     
            //     $.each(data, function (i, f) {
            //         $("#tbody").append(
            //             `<tr>
            //             <td style='text-align:center'>${f.STT}</td>
            //             <td>${f.MA_DVIQLY}</td>
            //             <td>${f.MA_TRAM}</td>
            //             <td>${f.MA_KHANG}</td>
            //             <td>${f.TEN_KHANG}</td>
            //             <td>${f.SO_COT}</td>
            //         </tr>`
            //         );
            //     });
            //     $('#dataTable').DataTable({
            //         "columns": [
            //             { "width": "4%" },
            //             { "width": "19%" },
            //             { "width": "19%" },
            //             { "width": "19%" },
            //             { "width": "19%" },
            //             { "width": "19%" }
            //         ]
            //     });
            //     //
            //     $("#loading").hide();
            // });
        } else {
            if (matram == null || matram == '0') {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $("#loading").show();
                $.get(`${url}/api/HATHE_getBCChitietKhachhangsai/${madienluc}/%20/${GV_CAPDIENLUC}`, function (data) {
                    dulieubaocao = data;
                    $('#tongcong_colwillchangef').html('Tổng cộng: '+data.length.toLocaleString('en'));     
                    $.each(data, function (i, f) {
                        $("#tbody").append(
                            `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.MA_DVIQLY}</td>
                            <td>${f.MA_TRAM}</td>
                            <td>${f.MA_KHANG}</td>
                            <td>${f.TEN_KHANG}</td>
                            <td>${f.SO_COT}</td>
                        </tr>`
                        );
                    });
                    $('#dataTable').DataTable({
                        "columns": [
                            { "width": "4%" },
                            { "width": "19%" },
                            { "width": "19%" },
                            { "width": "19%" },
                            { "width": "19%" },
                            { "width": "19%" }
                        ]
                    });
                    //
                    $("#loading").hide();
                });
            } else {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $("#loading").show();
                $.get(`${url}/api/HATHE_getBCChitietKhachhangsai/${madienluc}/${matram}/${GV_CAPTRAM}`, function (data) {
                    dulieubaocao = data;
                    $('#tongcong_colwillchangef').html('Tổng cộng: '+data.length.toLocaleString('en'));     
                    $.each(data, function (i, f) {
                        $("#tbody").append(
                            `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.MA_DVIQLY}</td>
                            <td>${f.MA_TRAM}</td>
                            <td>${f.MA_KHANG}</td>
                            <td>${f.TEN_KHANG}</td>
                            <td>${f.SO_COT}</td>
                        </tr>`
                        );
                    });
                    $('#dataTable').DataTable({
                        "columns": [
                            { "width": "4%" },
                            { "width": "19%" },
                            { "width": "19%" },
                            { "width": "19%" },
                            { "width": "19%" },
                            { "width": "19%" }
                        ]
                    });
                    $("#loading").hide();
                });
            }
        }
    }
}
