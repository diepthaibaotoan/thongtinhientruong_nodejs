var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matram;
var nhanvien;
var jsonNhanVien;
var jsonKhachhang;
var jsonKhachhangDuyetTreoThao = [];
var jsonHinnhanh_khang;
var madonvicuanguoidung = '';
var m_taikhoan = localStorage['taikhoan'];

var date = new Date();
var year = date.getFullYear();
var month = date.getMonth();
var chonTatCa = false;

$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
$("#nhanvientreothao").empty();
$("#nhanvientreothao").append(`<option value="">-- NV được phân công --</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

    // Kiem tra user
    $.get(`${url}/api/get_MadviqlycuaNguoidung/${m_taikhoan}`, function (data) {
        $.each(data, function (i, f) {
            madonvicuanguoidung = f.MA_DVIQLY;
            if (madonvicuanguoidung.length == 4 || madonvicuanguoidung == 'PQ' || madonvicuanguoidung == 'PP') {
                $("#congty").select2().val(madonvicuanguoidung);
                $("#congty").trigger('change');
                $("#congty").prop('disabled', true);
            } else if (madonvicuanguoidung.length == 6) {
                if (madonvicuanguoidung.substring(0, 2) == 'PP' || madonvicuanguoidung.substring(0, 2) == 'PQ') {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 2));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                } else {
                    $("#congty").select2().val(madonvicuanguoidung.substring(0, 4));
                    $("#congty").trigger('change');
                    $("#congty").prop('disabled', true);
                    //
                    $("#dienluc").select2().val(madonvicuanguoidung);
                    $("#dienluc").trigger('change');
                    $("#dienluc").prop('disabled', true);
                }
            }
        });

        //Khởi tạo lần đầu
        clickTaoBangDuLieu();
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});

$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

$('#nhanvientreothao').on('change', function () {
    nhanvien = this.value;
    jsonKhachhangDuyetTreoThao = new Array();
    $.each(jsonKhachhang, function (i, f) {
        jsonKhachhang[i].NGUOI_DUOCPHANCONG = nhanvien;
    });
    taoDuLieuChoBang(jsonKhachhang);
});

$("#loading").hide();

function taoDuLieuChoBang(data) {
    $("#dataTable").dataTable().fnDestroy();
    $("#tbody").empty();

    var index = 1;
    $.each(data, function (i, f) {
        $("#tbody").append(
            `<tr>
                    <td style='text-align:center'>${index++}</td>
                    <td style='text-align:center'>
                        <label class="container">
                        <input id="${f.MA_DDO}" type="checkbox" onclick="duyetTreoThao('${f.MA_DDO}')">
                        <span class="checkmark"></span>
                        </label>
                    </td>
                    <td style='text-align:left'>${f.MA_DDO}</td>
                    <td style='text-align:left'>${f.TEN_KHANG}</td>
                    <td style='text-align:left'>${f.DCHI_SDD}</td>
                    <td style='text-align:left'>${f.SO_CTO}</td>
                    <td style='text-align:left'>${f.MA_SOGCS + "-" + f.STT}</td>
                    <td style='text-align:right'>${f.NGUOI_DUOCPHANCONG}</td>
                    <td style='text-align:center'>
                        <button class='button_xem' style="display: table;margin: 0 auto" onclick="getitem('${f.MA_KHANG}')">Xem</button>
                    </td>
                </tr>`
        );
        if (chonTatCa) {
            var temp = document.getElementById(jsonKhachhang[i].MA_DDO);
            temp.checked = true;
        }
    });

    $('#dataTable').DataTable({
        "columns": [
            { "width": "5%" },
            { "width": "2%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "15%" },
            { "width": "5%" },
            { "width": "5%" },
            { "width": "5%" },
            { "width": "5%" }
        ]
    });
}

function taoDuLieuNhanVienTreoThao(jsonNhanVien) {
    $("#nhanvientreothao").empty();
    $("#nhanvientreothao").append(`<option value="">-- NV được phân công --</option>`);
    $.each(jsonNhanVien, function (i, f) {
        $("#nhanvientreothao").append(`<option value="${f.TEN_DANGNHAP}">${f.tenkhaisinh} - ${f.TEN_DANGNHAP}</option>`);
    });
}

// Click nut xem bao cao
function clickTaoBangDuLieu() {
    if (macongty == null || macongty == '0') {
    } else {
        if (madienluc == null || madienluc == '0') {
        } else {
            if (matram == null || matram == '0') {
                if($('.search_thongtinkhachhang_input').val()!="")
                {
                    timKiemNhanh();
                    console.log("đang tìm kiếm nhân viên");                
                }
            } else {
                if($('.search_thongtinkhachhang_input').val()=="")
                {
                    $("#loading").show();
                    $.get(`${url}/api/TTCT_getDSKhachHangChuaDangKyTreoThao_byTram/${madienluc}/${matram}`, function (data) {
                        jsonKhachhang = data;
                        taoDuLieuChoBang(jsonKhachhang);
                        $.get(`${url}/api/TTCT_getDSNhanVien_ByMaDienLuc/${madienluc}`, function (data) {
                            jsonNhanVien = data;
                            taoDuLieuNhanVienTreoThao(jsonNhanVien);
                            $("#loading").hide();
                        });
                    });
                    console.log("đang thực chức năng treo tháo");
                }
                else
                {
                    timKiemNhanh();
                }
               
            }
        }
    }
}

function timKiemNhanh() {
    var str = $('.search_thongtinkhachhang_input').val();
    str = GF_CONVERTTOENGLISH(str);
    if (str.length > 6) {
        if (madienluc == null || madienluc == '0') {
            GF_SHOWTOAST_LONG("Chưa chọn mã điện lực", 1000);
        } else {
            $("#loading").show();
            $.get(`${url}/api/TTCT_getDSKhachHangChuaDangKyTreoThao_byTimKiem/${madienluc}/${str}`, function (data) {
                jsonKhachhang = data;
                taoDuLieuChoBang(jsonKhachhang);
                $.get(`${url}/api/TTCT_getDSNhanVien_ByMaDienLuc/${madienluc}`, function (data) {
                    jsonNhanVien = data;
                    taoDuLieuNhanVienTreoThao(jsonNhanVien);
                    $("#loading").hide();
                });
            });
        }
    }else{
        GF_SHOWTOAST_LONG("Yêu cầu lớn hơn 6 kí tự.", 1000);
    }
}

function duyetTreoThao(madiemdo) {
    var checkBox_ = document.getElementById(madiemdo);
    var khachhangObj = jsonKhachhang.find(function (obj) { return obj.MA_DDO === madiemdo });
    if (checkBox_.checked == true) {
        jsonKhachhangDuyetTreoThao.push(khachhangObj);
    } else {
        jsonKhachhangDuyetTreoThao = GF_removeFunction(jsonKhachhangDuyetTreoThao, "MA_DDO", madiemdo);
    }
}

function xacNhan() {
    if (jsonKhachhangDuyetTreoThao == undefined || jsonKhachhangDuyetTreoThao.length == 0) {
        alert("Bạn chưa chọn khách hàng nào");
    } else if (kiemTraNguoiDuocPhanCong(jsonKhachhangDuyetTreoThao) == false) {
        alert("Bạn chưa chọn nhân viên treo tháo");
    } else {
        xacNhanGuiDuLieu();
    }
}

function chonTatCaKhachHang() {
    var checkBox_ = document.getElementById('isChonTatCaKhachHang');

    if (jsonKhachhang == undefined || jsonKhachhang.length == 0) {
        alert("Không có khách hàng để chọn");
        checkBox_.checked = false;
    } else if (kiemTraNguoiDuocPhanCong(jsonKhachhang) == false) {
        alert("Bạn chưa chọn nhân viên treo tháo");
        checkBox_.checked = false;
    } else {
        if (checkBox_.checked == true) {
            chonTatCa = true;
            jsonKhachhangDuyetTreoThao = new Array();
            for (var i = 0; i < jsonKhachhang.length; i++) {
                jsonKhachhangDuyetTreoThao.push(jsonKhachhang[i]);
            }
        } else {
            chonTatCa = false;
            jsonKhachhangDuyetTreoThao = new Array();
        }
        taoDuLieuChoBang(jsonKhachhang);

    }

}

function kiemTraNguoiDuocPhanCong(jsonArrays) {
    if (jsonArrays.length == 0) return false;
    for (var i = 0; i < jsonArrays.length; i++) {
        if (GF_isValidString(jsonArrays[i].NGUOI_DUOCPHANCONG) == false) return false;
    }
    return true;
}

function xacNhanGuiDuLieu() {
    try {
        if (jsonKhachhangDuyetTreoThao.length == 0) {
            alert("Không có dữ liệu nào để duyệt.");
        } else {
            document.getElementById('id_dktt').style.display = 'block';
            var obj = $("#text_dktt").text("Bạn có đồng ý duyệt phân công nhiệm vụ nhân viên "+ nhanvien + " cho " + jsonKhachhangDuyetTreoThao.length +
                " khách hàng này không?");
            obj.html(obj.html().replace(/\n/g, '<br/>'));
        }
    } catch (e) {
        alert("Không có dữ liệu nào để duyệt nè.")
    }
}

function guiDanhSachPhanCongNhiemVu(jsonArrays, m_i) {
    if (jsonArrays.length > 0) {
        if (jsonArrays[m_i].BUOC_TTHAO == 0 || jsonArrays[m_i].BUOC_TTHAO == 3) {
            $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDSThongtintreothao_byMaddo_TTHT?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&NAM=${year}&THANG=${month}&MA_DDO=${jsonArrays[m_i].MA_DDO}&TINH_TRANG=${TTCT_CONGTODANGTREO}&NGUOI_PHANCONG=${m_taikhoan}&NGUOI_DUOCPHANCONG=${jsonArrays[m_i].NGUOI_DUOCPHANCONG}&BUOCTREOTHAO=${TTCT_TRANGTHAIPHANCONGNHIEMVU}&KEY=${GV_KEY_TOANDTB}`, function (data) {
                m_i++;
                if (jsonArrays.length == m_i) {
                    alert("Bạn gửi dữ liệu thành công.");
                    reload();
                } else {
                    guiDanhSachPhanCongNhiemVu(jsonArrays, m_i);
                }
            });
        } else {
            $("#loading").hide();
            reload();
        }
    } else {
        $("#loading").hide();
        reload();
    }
}

function Dongy() {
    $("#loading").show();
    guiDanhSachPhanCongNhiemVu(jsonKhachhangDuyetTreoThao, 0);
}

function Huy() {
    document.getElementById('id_dktt').style.display = 'none';
}

function reload() {
    $("#loading").hide();
    document.getElementById('isChonTatCaKhachHang').checked = false;
    chonTatCa = false;
    clickTaoBangDuLieu();
    document.getElementById('id_dktt').style.display = 'none';
    jsonKhachhangDuyetTreoThao = new Array();
}
function getitem(makh) {
    localStorage['MA_KHANG_TTHAO'] = makh;
    localStorage['MA_CTY_TTHAO'] = macongty;
    localStorage['MA_DIENLUC_TTHAO'] =madienluc;
    localStorage['MA_TRAM_TTHAO'] = matram;
    var win = window.open("/luoidienhathe", '_blank');
    win.focus();
}

function clickLayCMIS_DMTram() {
    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMTram_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Trạm thành công.")
    });

    //console.log("matram: " + matram);
    if (matram != null && matram != '0') {
        $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSHosodientu_byTram?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&KEY=${GV_KEY_TOANDTB}`, function (data) {
            //console.log("HATHE_setDSHosodientu_byTram: " + data);
        });

        $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSHosodientu_byTram_Soluong?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&KEY=${GV_KEY_TOANDTB}`, function (data) {
            //console.log("HATHE_setDSHosodientu_byTram: " + data);
        });

        $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSKhachhangGiadien_byTram?MA_DVIQLY=${madienluc}&MA_TRAM=${matram}&NGUOI_NHAP=${localStorage['taikhoan']}&KEY=${GV_KEY_TOANDTB}`, function (data) {
            //console.log("HATHE_setDSHosodientu_byTram: " + data);
        });
    }
}

function clickLayCMIS_DMVitritreo() {
    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMVtritreo_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Vị trí treo thành công.")
    });

}

function clickLayCMIS_DMChungloaicongto() {
    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMChungLoaiCongTo_CMIS?KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Chủng loại công tơ thành công.")
    });
}
function clickLayCMIS_DMSogcs() {
    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDMSogcs_byMadviqly?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Sổ ghi chỉ số thành công.");
    });
}
function clickLayCMIS_DMLydotreothao() {
    $("#loading").show();
    console.log(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMLydotreothao?KEY=${GV_KEY_TOANDTB}`);
    $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMLydotreothao?KEY=${GV_KEY_TOANDTB}`, function (data) {

        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Lý do treo tháo thành công.")
    });
}
function clickLayCMIS_DMLoaiDdo() {
    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMLoaiddo?KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Loại điểm đo thành công.")
    });
}
function clickLayCMIS_DMNVienkepchi() {
    $("#loading").show();
    $.get(`https://gismobile.cpc.vn/Service_TTCT.asmx/TTCT_setDMNhanvien?MA_DVIQLY=${madienluc}&KEY=${GV_KEY_TOANDTB}`, function (data) {
        $("#loading").hide();
        alert("Bạn đồng bộ danh mục Nhân viên số thành công.")
    });
}
function clickDongboCMIS(){
    $("#treothao_dialog").show();
}
