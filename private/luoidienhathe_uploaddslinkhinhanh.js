var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var username = localStorage['taikhoan'];
// let matram = localStorage.get('matram');
var macongty = localStorage['macongty_excel'];
var madienluc = localStorage['madienluc_excel'];
var matram = localStorage['matram_excel'];
var height_ = $(window).height()- 250;
document.getElementById("excel_MA_DVIQLY").innerHTML = madienluc + "/" + matram;

/* ----- BAT DAU BIEN TAP -----*/
var bientapControl = 0;
var Array_DSCOT;
var Array_DSHINHANH;
var oFile_COT;
var oFile_HINHANH;
var oFile_ImageUpload;

// BIEN TAP 2
$(function () {
    oFile_HINHANH = document.getElementById('luoidienhathe_input_dshinhanh');
    if (oFile_HINHANH.addEventListener) {
        oFile_HINHANH.addEventListener('change', filePicked_DSHINHANH, false);
    }
});

// Đọc dữ liệu từ file excel--> trả về json
function filePicked_DSHINHANH(oEvent) {
    // Get The File From The Input
    var oFile = oEvent.target.files[0];
    var sFilename = oFile.name;
    // Create A File Reader HTML5
    var reader = new FileReader();
    
    // Ready The Event For When A File Gets Selected
    reader.onload = function (e) {
        try {
            var data = e.target.result;
            var cfb = XLS.CFB.read(data, { type: 'binary' });
            var wb = XLS.parse_xlscfb(cfb);
            // Loop Over Each Sheet
            wb.SheetNames.forEach(function (sheetName) {
                // Obtain The Current Row As CSV
                var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
                var oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);
                $("#tbody-table-dslinkhinhanh").css('height',`${height_}`);
                // $("#my_file_output").html(sCSV);
                // console.log(oJS)
                for (var i = 0; i < oJS.length; i++) {
                    $("#tbody-table-dslinkhinhanh").append(
                        `<tr>
                        <td style='text-align:center;width: 60px'>${(i + 1) + ""}</td>
                        <td style='text-align:left;width: 130px'>${oJS[i].MA_DVIQLY}</td>
                        <td style='text-align:left;width: 175px'>${replaceAll(oJS[i].MA_TRAMNGUON, '', '')}</td>
                        <td style='text-align:left;width: 170px'>${replaceAll(oJS[i].MA_XUATTUYEN, '', '')}</td>
                        <td style='text-align:left;width: 120px'>${replaceAll(oJS[i].MA_TRAM, '', '')}</td>
                        <td style='text-align:left;width: 150px'>${replaceAll(oJS[i].SO_COT, '\'', '')}</td>
                        <td style='text-align:left;width: 150px'>${replaceAll(oJS[i].MA_KHANG, '', '')}</td>
                        <td style='text-align:left;width: 210px'>${replaceAll(oJS[i].LOAI_HINHANH, '', '')}</td>
                        <td style='text-align:left;width: 280px'>${replaceAll(oJS[i].URL_THUMUC, '', '')}</td>
                        <td style='text-align:left;width: 550px'>${replaceAll(oJS[i].TEN_HINHANH, '', '')}</td>
                        <td style='text-align:left;width: 150px'>${replaceAll(oJS[i].NGAY_CHUP, '', '')}</td>
                        <td style='text-align:left;width: 170px'>${replaceAll(oJS[i].NGUOI_NHAP, '', '')}</td>
                        <td style='text-align:left;width: 250px'>${replaceAll(oJS[i].NGUOI_NHAP, '', '')}</td>        
                    </tr>`
                    );
                }

                //
                Array_DSHINHANH = oJS;
            });
        } catch (e) {
            // khong dung dinh dang se thong bao
            alert("File excel DUONGDAN_HINHANH không đúng.");
        }
    };
    // Tell JS To Start Reading The File.. You could delay this if desired
    reader.readAsBinaryString(oFile);
}

//2. Gửi dữ liệu đường dẫn ảnh về server
function guiDuLieu_HINHANH() {

    if (CheckMATRAM(Array_DSHINHANH) == 1) {
        $("#loading").show();
        //
        guiAnh(Array_DSHINHANH, 0);
    }
}

function guiAnh(m_Array_DSHINHANH, m_i) {

    if (m_Array_DSHINHANH[m_i].LOAI_HINHANH == 'HINHANH_HIENTRUONG') {

        if (m_Array_DSHINHANH[m_i].MA_DVIQLY == '' || m_Array_DSHINHANH[m_i].MA_DVIQLY == undefined) {
            m_Array_DSHINHANH[m_i].MA_DVIQLY = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_TRAMNGUON == '' || m_Array_DSHINHANH[m_i].MA_TRAMNGUON == undefined) {
            m_Array_DSHINHANH[m_i].MA_TRAMNGUON = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_XUATTUYEN == '' || m_Array_DSHINHANH[m_i].MA_XUATTUYEN == undefined) {
            m_Array_DSHINHANH[m_i].MA_XUATTUYEN = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_TRAM == '' || m_Array_DSHINHANH[m_i].MA_TRAM == undefined) {
            m_Array_DSHINHANH[m_i].MA_TRAM = '%20';
        }
        if (m_Array_DSHINHANH[m_i].SO_COT == '' || m_Array_DSHINHANH[m_i].SO_COT == undefined) {
            m_Array_DSHINHANH[m_i].SO_COT = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_KHANG == '' || m_Array_DSHINHANH[m_i].MA_KHANG == undefined) {
            m_Array_DSHINHANH[m_i].MA_KHANG = '%20';
        }
        if (m_Array_DSHINHANH[m_i].LOAI_HINHANH == '' || m_Array_DSHINHANH[m_i].LOAI_HINHANH == undefined) {
            m_Array_DSHINHANH[m_i].LOAI_HINHANH = '%20';
        }
        if (m_Array_DSHINHANH[m_i].URL_THUMUC == '' || m_Array_DSHINHANH[m_i].URL_THUMUC == undefined) {
            m_Array_DSHINHANH[m_i].URL_THUMUC = '%20';
        }
        if (m_Array_DSHINHANH[m_i].TEN_HINHANH == '' || m_Array_DSHINHANH[m_i].TEN_HINHANH == undefined) {
            m_Array_DSHINHANH[m_i].TEN_HINHANH = '%20';
        }
        if (m_Array_DSHINHANH[m_i].NGAY_CHUP == '' || m_Array_DSHINHANH[m_i].NGAY_CHUP == undefined) {
            m_Array_DSHINHANH[m_i].NGAY_CHUP = '%20';
        }
        if (m_Array_DSHINHANH[m_i].NGUOI_NHAP == '' || m_Array_DSHINHANH[m_i].NGUOI_NHAP == undefined) {
            m_Array_DSHINHANH[m_i].NGUOI_NHAP = '%20';
        }

        m_Array_DSHINHANH[m_i].SO_COT = replaceAll(m_Array_DSHINHANH[m_i].SO_COT, '/', '%2F');
        m_Array_DSHINHANH[m_i].SO_COT = replaceAll(m_Array_DSHINHANH[m_i].SO_COT, '\'', '%20');
        m_Array_DSHINHANH[m_i].URL_THUMUC = replaceAll(m_Array_DSHINHANH[m_i].URL_THUMUC, '/', '%2F');
        m_Array_DSHINHANH[m_i].NGAY_CHUP = replaceAll(m_Array_DSHINHANH[m_i].NGAY_CHUP, '/', '%2F');
        m_Array_DSHINHANH[m_i].NGAY_CHUP = replaceAll(m_Array_DSHINHANH[m_i].NGAY_CHUP, ':', '%3A');
        m_Array_DSHINHANH[m_i].NGAY_CHUP = replaceAll(m_Array_DSHINHANH[m_i].NGAY_CHUP, ' ', '%20');
        if(m_Array_DSHINHANH[m_i].NGUOI_NHAP.includes('bookmark')){
        } else {
            m_Array_DSHINHANH[m_i].NGUOI_NHAP = username+'_excel';
        }

        var url_InsertDSHINHANH = "/api/HATHE_updateDSHinhAnh_byTram"
            + '/' + m_Array_DSHINHANH[m_i].MA_DVIQLY
            + '/' + m_Array_DSHINHANH[m_i].MA_TRAMNGUON
            + '/' + m_Array_DSHINHANH[m_i].MA_XUATTUYEN
            + '/' + m_Array_DSHINHANH[m_i].MA_TRAM
            + '/' + m_Array_DSHINHANH[m_i].SO_COT.toUpperCase()
            + '/' + m_Array_DSHINHANH[m_i].MA_KHANG
            + '/' + m_Array_DSHINHANH[m_i].LOAI_HINHANH
            + '/' + m_Array_DSHINHANH[m_i].URL_THUMUC
            + '/' + m_Array_DSHINHANH[m_i].TEN_HINHANH
            + '/' + m_Array_DSHINHANH[m_i].NGAY_CHUP
            + '/' + m_Array_DSHINHANH[m_i].NGUOI_NHAP;

        url_InsertDSHINHANH = url + url_InsertDSHINHANH;
        $.get(url_InsertDSHINHANH, function (data) {
            m_i++;
            if (m_Array_DSHINHANH.length == m_i) {
                $("#loading").hide();
                alert("Bạn gửi dữ liệu Đường Dẫn Hình Ảnh thành công.");
                setTimeout(clickDongy, 0);
            } else {
                guiAnh(m_Array_DSHINHANH, m_i);
            }
        });
    } else if (m_Array_DSHINHANH[m_i].LOAI_HINHANH == 'UPDATE') {

        if (m_Array_DSHINHANH[m_i].MA_DVIQLY == '' || m_Array_DSHINHANH[m_i].MA_DVIQLY == undefined) {
            m_Array_DSHINHANH[m_i].MA_DVIQLY = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_TRAMNGUON == '' || m_Array_DSHINHANH[m_i].MA_TRAMNGUON == undefined) {
            m_Array_DSHINHANH[m_i].MA_TRAMNGUON = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_XUATTUYEN == '' || m_Array_DSHINHANH[m_i].MA_XUATTUYEN == undefined) {
            m_Array_DSHINHANH[m_i].MA_XUATTUYEN = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_TRAM == '' || m_Array_DSHINHANH[m_i].MA_TRAM == undefined) {
            m_Array_DSHINHANH[m_i].MA_TRAM = '%20';
        }
        if (m_Array_DSHINHANH[m_i].SO_COT == '' || m_Array_DSHINHANH[m_i].SO_COT == undefined) {
            m_Array_DSHINHANH[m_i].SO_COT = '%20';
        }

        if (m_Array_DSHINHANH[m_i].TEN_HINHANH == '' || m_Array_DSHINHANH[m_i].TEN_HINHANH == undefined) {
            m_Array_DSHINHANH[m_i].TEN_HINHANH = '%20';
        }

        m_Array_DSHINHANH[m_i].SO_COT = replaceAll(m_Array_DSHINHANH[m_i].SO_COT, '/', '%2F');
        m_Array_DSHINHANH[m_i].SO_COT = replaceAll(m_Array_DSHINHANH[m_i].SO_COT, '\'', '%20');


        var url_updateSocotDSHINHANH = "/api/HATHE_updateordeleteDSHinhAnh_byTenhinhanh"
            + '/' + m_Array_DSHINHANH[m_i].MA_DVIQLY
            + '/' + m_Array_DSHINHANH[m_i].MA_TRAMNGUON
            + '/' + m_Array_DSHINHANH[m_i].MA_XUATTUYEN
            + '/' + m_Array_DSHINHANH[m_i].MA_TRAM
            + '/' + m_Array_DSHINHANH[m_i].SO_COT.toUpperCase()
            + '/' + m_Array_DSHINHANH[m_i].TEN_HINHANH
            + '/' + m_Array_DSHINHANH[m_i].LOAI_HINHANH;

        url_updateSocotDSHINHANH = url + url_updateSocotDSHINHANH;
        $.get(url_updateSocotDSHINHANH, function (data) {
            m_i++;
            if (m_Array_DSHINHANH.length == m_i) {
                $("#loading").hide();
                alert("Bạn gửi dữ liệu Đường Dẫn Hình Ảnh thành công.");
                setTimeout(clickDongy, 0);
            } else {
                guiAnh(m_Array_DSHINHANH, m_i);
            }
        });
    } else if (m_Array_DSHINHANH[m_i].LOAI_HINHANH == 'DELETE') {

        if (m_Array_DSHINHANH[m_i].MA_DVIQLY == '' || m_Array_DSHINHANH[m_i].MA_DVIQLY == undefined) {
            m_Array_DSHINHANH[m_i].MA_DVIQLY = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_TRAMNGUON == '' || m_Array_DSHINHANH[m_i].MA_TRAMNGUON == undefined) {
            m_Array_DSHINHANH[m_i].MA_TRAMNGUON = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_XUATTUYEN == '' || m_Array_DSHINHANH[m_i].MA_XUATTUYEN == undefined) {
            m_Array_DSHINHANH[m_i].MA_XUATTUYEN = '%20';
        }
        if (m_Array_DSHINHANH[m_i].MA_TRAM == '' || m_Array_DSHINHANH[m_i].MA_TRAM == undefined) {
            m_Array_DSHINHANH[m_i].MA_TRAM = '%20';
        }
        if (m_Array_DSHINHANH[m_i].SO_COT == '' || m_Array_DSHINHANH[m_i].SO_COT == undefined) {
            m_Array_DSHINHANH[m_i].SO_COT = '%20';
        }

        if (m_Array_DSHINHANH[m_i].TEN_HINHANH == '' || m_Array_DSHINHANH[m_i].TEN_HINHANH == undefined) {
            m_Array_DSHINHANH[m_i].TEN_HINHANH = '%20';
        }

        m_Array_DSHINHANH[m_i].SO_COT = replaceAll(m_Array_DSHINHANH[m_i].SO_COT, '/', '%2F');
        m_Array_DSHINHANH[m_i].SO_COT = replaceAll(m_Array_DSHINHANH[m_i].SO_COT, '\'', '%20');


        var url_updateSocotDSHINHANH = "/api/HATHE_updateordeleteDSHinhAnh_byTenhinhanh"
            + '/' + m_Array_DSHINHANH[m_i].MA_DVIQLY
            + '/' + m_Array_DSHINHANH[m_i].MA_TRAMNGUON
            + '/' + m_Array_DSHINHANH[m_i].MA_XUATTUYEN
            + '/' + m_Array_DSHINHANH[m_i].MA_TRAM
            + '/' + m_Array_DSHINHANH[m_i].SO_COT.toUpperCase()
            + '/' + m_Array_DSHINHANH[m_i].TEN_HINHANH
            + '/' + m_Array_DSHINHANH[m_i].LOAI_HINHANH;

        url_updateSocotDSHINHANH = url + url_updateSocotDSHINHANH;
        $.get(url_updateSocotDSHINHANH, function (data) {
            m_i++;
            if (m_Array_DSHINHANH.length == m_i) {
                $("#loading").hide();
                alert("Bạn gửi dữ liệu Đường Dẫn Hình Ảnh thành công.");
                setTimeout(clickDongy, 0);
            } else {
                guiAnh(m_Array_DSHINHANH, m_i);
            }
        });
    }
}

function xacNhanGuiDuLieu() {
    try {
        if (Array_DSHINHANH.length == 0) {
            alert("Excel không có dữ liệu nào để gửi.");
        } else {
            document.getElementById('id_excelduongdanhinhanh').style.display = 'block';
            var obj = $("#text_excelduongdanhinhanh").text("Bạn có đồng ý gửi dữ liệu cho trạm " + matram + " này không?"
                + " \nLưu ý: \n1. Khi bạn đồng ý, toàn bộ dữ liệu đường dẫn hình ảnh của trạm này sẽ bị xóa hết và được đẩy dữ liệu mới nhất từ file Excel vào."
                + "\n 2. Khi gửi dữ liệu thành công, bạn vui lòng nhấn lại nút 'Xem' để xem dữ liệu mới gửi về."
                + " \n3. Chức năng này chỉ có Quản trị ở Điện lực mới sử dụng được.");
            obj.html(obj.html().replace(/\n/g, '<br/>'));
        }
    } catch (e) {
        alert("Excel không có dữ liệu nào để gửi.")
    }
}

function quayLai() {
    window.close();
}

function clickDongy() {
    window.close();
}

//Kiểm tra MA_DVIQLY va MA_TRAM của user có trùng với file excel không?
function CheckMATRAM(ArrayInfo) {
    var SoMaDienLuc_loi = 0;
    var SoMaTram_loi = 0;

    for (var i = 0; i < ArrayInfo.length; i++) {
        if (ArrayInfo[i].MA_DVIQLY != madienluc) {
            SoMaDienLuc_loi++;
        }
        if (ArrayInfo[i].MA_TRAM != matram) {
            SoMaTram_loi++;
        }
    }


    if (SoMaDienLuc_loi == 0 && SoMaTram_loi == 0) {
        return 1;
    } else {
        alert("Có " + SoMaDienLuc_loi + " MA_DVIQLY và " + SoMaTram_loi + " MA_TRAM bị sai thông tin. \n Bạn vui lòng kiểm tra lại dữ liệu.");
    }

}

function replaceAll(str, find, replace) {
    if (str == undefined) {
        return '';
    } else {
        return str.replace(new RegExp(find, 'g'), replace);
    }
}


// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    //
    $("#loading").show();
    $.get(`${url}/api/HATHE_getDSExcelDuongdanhinhanh_byTram/${madienluc.trim()}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${matram.trim()}`, function (data) {
        $("#loading").hide();
        //console.log(data.length + "---");
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
    });
}
