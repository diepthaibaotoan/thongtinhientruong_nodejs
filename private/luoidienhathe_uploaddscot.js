var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var username = localStorage['taikhoan'];
var macongty = localStorage['macongty_excel'];
var madienluc = localStorage['madienluc_excel'];
var matram = localStorage['matram_excel'];
var height_ = $(window).height()- 250;

document.getElementById("excel_MA_DVIQLY").innerHTML = madienluc + "/" + matram;


/* ----- BAT DAU BIEN TAP -----*/
var bientapControl = 0;
var Array_DSCOT;
var oFile_COT;

// BIEN TAP 1
$(function () {
    oFile_COT = document.getElementById('luoidienhathe_input_dscot');
    if (oFile_COT.addEventListener) {
        oFile_COT.addEventListener('change', filePicked_DSCOT, false);
    }
});

// Override --> Đọc dữ liệu từ file excel--> trả về json
function filePicked_DSCOT(oEvent) {

    // Get The File From The Input
    var oFile = oEvent.target.files[0];
    var sFilename = oFile.name;
    // Create A File Reader HTML5
    var reader = new FileReader();
    
    // Ready The Event For When A File Gets Selected
    reader.onload = function (e) {
        try {
            var data = e.target.result;
            var cfb = XLS.CFB.read(data, { type: 'binary' });
            var wb = XLS.parse_xlscfb(cfb);
            // Loop Over Each Sheet
            $("#tbody-table-dscot").css('height',`${height_}`);
            wb.SheetNames.forEach(function (sheetName) {

                // Obtain The Current Row As CSV
                var sCSV = XLS.utils.make_csv(wb.Sheets[sheetName]);
                var oJS = XLS.utils.sheet_to_row_object_array(wb.Sheets[sheetName]);

                //
                $("#my_file_output").html(sCSV);
                for (var i = 0; i < oJS.length; i++) {
                    $("#tbody-table-dscot").append(
                        `<tr>
                        <td style='text-align:center;width:60px;'>${(i + 1) + ""}</td>
                        <td style='text-align:left;width:135px;'>${oJS[i].MA_DVIQLY}</td>
                        <td style='text-align:left;width:175px'>${replaceAll(oJS[i].MA_TRAMNGUON, '', '')}</td>
                        <td style='text-align:left;width:170px'>${oJS[i].MA_XUATTUYEN}</td>
                        <td style='text-align:left;width:120px'>${oJS[i].MA_TRAM}</td>
                        <td style='text-align:left;width:180px'>${replaceAll(oJS[i].SO_COT, '\'', '')}</td>
                        <td style='text-align:left;width:125px'>${replaceAll(oJS[i].X_COT, '', '')}</td>
                        <td style='text-align:left;width:125px'>${replaceAll(oJS[i].Y_COT, '', '')}</td>
                        <td style='text-align:left;width:140px'>${replaceAll(oJS[i].LK_COT1, '\'', '')}</td>
                        <td style='text-align:left;width:140px'>${replaceAll(oJS[i].LK_COT2, '', '')}</td>
                        <td style='text-align:left;width:150px'>${replaceAll(oJS[i].GHI_CHU, '', '')}</td>
                        <td style='text-align:left;width:140px'>${replaceAll(oJS[i].LOAI_COT, '\'', '')}</td>
                        <td style='text-align:left;width:140px'>${replaceAll(oJS[i].TINH_CHAT, '', '')}</td>
                        <td style='text-align:left;width:140px'>${replaceAll(oJS[i].CHIEU_CAO, '\'', '')}</td>
                        <td style='text-align:left;width:180px'>${replaceAll(oJS[i].NHA_SANXUAT, '\'', '')}</td>
                    </tr>`
                    );
                }
                //
                Array_DSCOT = oJS;
            });
        } catch (e) {
            // khong dung dinh dang se thong bao
            alert("File excel BANDO_LUOIDIEN không đúng.");
        }
    };
    reader.readAsBinaryString(oFile);
}

//1. Gửi dữ liệu cột về server
function GuiDuLieu_COT() {
    //
    if (CheckMATRAM(Array_DSCOT) == 1) {
        $("#loading").show();
        $.get(`${url}/api/HATHE_deleteDSCot_byTram/${madienluc}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${matram}`, function (data) {
            guiCot(Array_DSCOT, 0);
        });
    }
}

function guiCot(m_Array_DSCOT, m_i) {
    //console.log("index: " + m_i);


    if (m_Array_DSCOT[m_i].MA_DVIQLY == undefined || m_Array_DSCOT[m_i].MA_DVIQLY.trim() == '') {
        m_Array_DSCOT[m_i].MA_DVIQLY = '%20'
    }
    if (m_Array_DSCOT[m_i].MA_TRAMNGUON == undefined || m_Array_DSCOT[m_i].MA_TRAMNGUON.trim() == '') {
        m_Array_DSCOT[m_i].MA_TRAMNGUON = '%20'
    }
    if (m_Array_DSCOT[m_i].MA_XUATTUYEN == undefined || m_Array_DSCOT[m_i].MA_XUATTUYEN.trim() == '') {
        m_Array_DSCOT[m_i].MA_XUATTUYEN = '%20'
    }
    if (m_Array_DSCOT[m_i].MA_TRAM == undefined || m_Array_DSCOT[m_i].MA_TRAM.trim() == '') {
        m_Array_DSCOT[m_i].MA_TRAM = '%20'
    }
    if (m_Array_DSCOT[m_i].SO_COT == undefined || m_Array_DSCOT[m_i].SO_COT.trim() == '') {
        m_Array_DSCOT[m_i].SO_COT = '%20'
    }
    if (m_Array_DSCOT[m_i].X_COT == undefined || m_Array_DSCOT[m_i].X_COT.trim() == '') {
        m_Array_DSCOT[m_i].X_COT = '%20'
    }
    if (m_Array_DSCOT[m_i].Y_COT == undefined || m_Array_DSCOT[m_i].Y_COT.trim() == '') {
        m_Array_DSCOT[m_i].Y_COT = '%20'
    }
    if (m_Array_DSCOT[m_i].Z_COT == undefined || m_Array_DSCOT[m_i].Z_COT.trim() == '') {
        m_Array_DSCOT[m_i].Z_COT = '%20'
    }
    if (m_Array_DSCOT[m_i].LK_COT1 == undefined || m_Array_DSCOT[m_i].LK_COT1.trim() == '') {
        m_Array_DSCOT[m_i].LK_COT1 = '%20'
    }
    if (m_Array_DSCOT[m_i].LK_COT2 == undefined || m_Array_DSCOT[m_i].LK_COT2.trim() == '') {
        m_Array_DSCOT[m_i].LK_COT2 = '%20'
    }
    if (m_Array_DSCOT[m_i].LK_COT3 == undefined || m_Array_DSCOT[m_i].LK_COT3.trim() == '') {
        m_Array_DSCOT[m_i].LK_COT3 = '%20'
    }
    if (m_Array_DSCOT[m_i].LK_COT4 == undefined || m_Array_DSCOT[m_i].LK_COT4.trim() == '') {
        m_Array_DSCOT[m_i].LK_COT4 = '%20'
    }
    if (m_Array_DSCOT[m_i].MA_DUONGDAY == undefined || m_Array_DSCOT[m_i].MA_DUONGDAY.trim() == '') {
        m_Array_DSCOT[m_i].MA_DUONGDAY = '%20'
    }
    if (m_Array_DSCOT[m_i].MA_VITRI == undefined || m_Array_DSCOT[m_i].MA_VITRI.trim() == '') {
        m_Array_DSCOT[m_i].MA_VITRI = '%20'
    }
    if (m_Array_DSCOT[m_i].GHI_CHU == undefined || m_Array_DSCOT[m_i].GHI_CHU.trim() == '') {
        m_Array_DSCOT[m_i].GHI_CHU = '%20'
    }
    if (m_Array_DSCOT[m_i].NGUOI_NHAP == undefined || m_Array_DSCOT[m_i].NGUOI_NHAP.trim() == '') {
        m_Array_DSCOT[m_i].NGUOI_NHAP = '%20'
    }
    if (m_Array_DSCOT[m_i].LOAI_COT == undefined || m_Array_DSCOT[m_i].LOAI_COT.trim() == '') {
        m_Array_DSCOT[m_i].LOAI_COT = '%20'
    }
    if (m_Array_DSCOT[m_i].TINH_CHAT == undefined || m_Array_DSCOT[m_i].TINH_CHAT.trim() == '') {
        m_Array_DSCOT[m_i].TINH_CHAT = '%20'
    }
    if (m_Array_DSCOT[m_i].MA_HIEU == undefined || m_Array_DSCOT[m_i].MA_HIEU.trim() == '') {
        m_Array_DSCOT[m_i].MA_HIEU = '%20'
    }
    if (m_Array_DSCOT[m_i].CHIEU_CAO == undefined || m_Array_DSCOT[m_i].CHIEU_CAO.trim() == '') {
        m_Array_DSCOT[m_i].CHIEU_CAO = '%20'
    }
    if (m_Array_DSCOT[m_i].NHA_SANXUAT == undefined || m_Array_DSCOT[m_i].NHA_SANXUAT.trim() == '') {
        m_Array_DSCOT[m_i].NHA_SANXUAT = '%20'
    }

    m_Array_DSCOT[m_i].SO_COT = replaceAll(m_Array_DSCOT[m_i].SO_COT, '/', '%2F');
    m_Array_DSCOT[m_i].SO_COT = replaceAll(m_Array_DSCOT[m_i].SO_COT, '\'', '%20');
    m_Array_DSCOT[m_i].LK_COT1 = replaceAll(m_Array_DSCOT[m_i].LK_COT1, '/', '%2F');
    m_Array_DSCOT[m_i].LK_COT1 = replaceAll(m_Array_DSCOT[m_i].LK_COT1, '\'', '%20');
    m_Array_DSCOT[m_i].LK_COT3 = replaceAll(m_Array_DSCOT[m_i].LK_COT3, '/', '%2F');
    m_Array_DSCOT[m_i].LK_COT3 = replaceAll(m_Array_DSCOT[m_i].LK_COT3, '\'', '%20');
    m_Array_DSCOT[m_i].LK_COT4 = replaceAll(m_Array_DSCOT[m_i].LK_COT4, '/', '%2F');
    m_Array_DSCOT[m_i].LK_COT4 = replaceAll(m_Array_DSCOT[m_i].LK_COT4, '\'', '%20');
    m_Array_DSCOT[m_i].LOAI_COT = replaceAll(m_Array_DSCOT[m_i].LOAI_COT, '\'', '%20');
    m_Array_DSCOT[m_i].CHIEU_CAO = replaceAll(m_Array_DSCOT[m_i].CHIEU_CAO, '\'', '%20');
    m_Array_DSCOT[m_i].NHA_SANXUAT = replaceAll(m_Array_DSCOT[m_i].NHA_SANXUAT, '\'', '%20');
    m_Array_DSCOT[m_i].NGUOI_NHAP = username+'_excel';

    var url_InsertDSCOT = "/api/HATHE_updateDSCot_byTram"
        + '/' + m_Array_DSCOT[m_i].MA_DVIQLY
        + '/' + m_Array_DSCOT[m_i].MA_TRAMNGUON
        + '/' + m_Array_DSCOT[m_i].MA_XUATTUYEN
        + '/' + m_Array_DSCOT[m_i].MA_TRAM
        + '/' + m_Array_DSCOT[m_i].SO_COT.toUpperCase()
        + '/' + m_Array_DSCOT[m_i].X_COT
        + '/' + m_Array_DSCOT[m_i].Y_COT
        + '/' + m_Array_DSCOT[m_i].Z_COT
        + '/' + m_Array_DSCOT[m_i].LK_COT1.toUpperCase()
        + '/' + m_Array_DSCOT[m_i].LK_COT2
        + '/' + m_Array_DSCOT[m_i].LK_COT3.toUpperCase()
        + '/' + m_Array_DSCOT[m_i].LK_COT4.toUpperCase()
        + '/' + m_Array_DSCOT[m_i].MA_DUONGDAY
        + '/' + m_Array_DSCOT[m_i].MA_VITRI
        + '/' + m_Array_DSCOT[m_i].GHI_CHU
        + '/' + m_Array_DSCOT[m_i].NGUOI_NHAP
        + '/' + m_Array_DSCOT[m_i].LOAI_COT
        + '/' + m_Array_DSCOT[m_i].TINH_CHAT
        + '/' + m_Array_DSCOT[m_i].MA_HIEU
        + '/' + m_Array_DSCOT[m_i].CHIEU_CAO
        + '/' + m_Array_DSCOT[m_i].NHA_SANXUAT;

    url_InsertDSCOT = url + url_InsertDSCOT;
    //1. Gửi dữ liệu cột về server
    // console.log(url_InsertDSCOT);
    $.get(url_InsertDSCOT, function (data) {
        m_i++;
        if (Array_DSCOT.length == m_i) {
            $("#loading").hide();
            alert("Bạn gửi dữ liệu Bản Đồ Lưới Điện thành công.");
            setTimeout(clickDongy, 0);
        } else {
            guiCot(Array_DSCOT, m_i);
        }
    });
}

//
function xacNhanGuiDuLieu() {
    try {
        if (Array_DSCOT.length == 0) {
            alert("Excel không có dữ liệu nào để gửi.");
        } else {
            document.getElementById('id_excelcot').style.display = 'block';
            var obj = $("#text_excelcot").text("Bạn có đồng ý gửi dữ liệu cho trạm " + matram + " này không?"
                + " \nLưu ý: \n1. Khi bạn đồng ý, toàn bộ dữ liệu cột của trạm này sẽ bị xóa hết và được đẩy dữ liệu mới nhất từ file Excel vào."
                + "\n 2. Khi gửi dữ liệu thành công, bạn vui lòng nhấn lại nút 'Xem' để xem dữ liệu mới gửi về."
                + " \n3. Chức năng này chỉ có Quản trị ở Điện lực mới sử dụng được.");
            obj.html(obj.html().replace(/\n/g, '<br/>'));
        }
    } catch (e) {
        alert("Excel không có dữ liệu nào để gửi.")
    }
}

function quayLai() {
    window.close();
}

//Kiểm tra MA_DVIQLY va MA_TRAM của user có trùng với file excel không?
function CheckMATRAM(ArrayInfo) {
    var SoMaDienLuc_loi = 0;
    var SoMaTram_loi = 0;
    var so_Xcot = 0;
    var so_Ycot = 0;
    var so_LKcot2 = 0;
    var so_Tinhchat = 0;

    for (var i = 0; i < ArrayInfo.length; i++) {
        if (ArrayInfo[i].MA_DVIQLY != madienluc) {
            SoMaDienLuc_loi++;
        }
        if (ArrayInfo[i].MA_TRAM != matram) {
            SoMaTram_loi++;
        }
        if (ArrayInfo[i].X_COT == null || ArrayInfo[i].X_COT == '') {
            so_Xcot++;
        }
        if (ArrayInfo[i].Y_COT == null || ArrayInfo[i].Y_COT == '') {
            so_Ycot++;
        }
        if (ArrayInfo[i].LK_COT2 == null || ArrayInfo[i].LK_COT2 == '') {
            so_LKcot2++;
        }
        if (ArrayInfo[i].TINH_CHAT == null || ArrayInfo[i].TINH_CHAT == '') {
            so_Tinhchat++;
        }
    }

    if (SoMaDienLuc_loi == 0 && SoMaTram_loi == 0 && so_Xcot == 0 && so_Ycot == 0 && so_LKcot2 == 0 && so_Tinhchat == 0) {
        return 1;
    } else {
        var msg = "Số lượng MA_DVIQLY không hợp lí: " + SoMaDienLuc_loi +
            "Số lượng MA_TRAM không hợp lí: " + SoMaTram_loi +
            "\n Số lượng X không hợp lí: " + so_Xcot +
            "\n Số lượng Y không hợp lí: " + so_Ycot +
            "\n Số lượng LK_COT2 không hợp lí: " + so_LKcot2 +
            "\n Số lượng TINH_CHAT không hợp lí: " + so_Tinhchat +
            "\n Bạn vui lòng kiểm tra lại dữ liệu.";
        alert(msg);
    }

}

function replaceAll(str, find, replace) {
    if (str == undefined) {
        return '';
    } else {
        return str.replace(new RegExp(find, 'g'), replace);
    }
}

function clickDongy() {
    window.close();
}

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    //
    $("#loading").show();
    $.get(`${url}/api/HATHE_getDSExcelBandoluoidien_byTram/${madienluc.trim()}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${matram.trim()}`, function (data) {
        $("#loading").hide();
        //console.log(data.length + "---");
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
    });
}
