GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);

var jsonDonvi;
var macongty, madienluc, matram;
var jsonkhachhang;
var isDate = false;

$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);

// BIEN TAP CHE DO NGAY GIO 
document.querySelector("#today").valueAsDate = new Date();
document.getElementById("today").disabled = true;
var cb_date = document.querySelector("input[name=cb_date]");
cb_date.addEventListener('change', function () {
    if (this.checked) {
        isDate = true;
        document.getElementById("today").disabled = false;
        document.getElementById("today").style.backgroundColor = 'white';
        document.getElementById("congty").disabled = true;
        document.getElementById("dienluc").disabled = true;
        document.getElementById("tram").disabled = true;

    } else {
        isDate = false;
        document.getElementById("today").disabled = true;
        document.getElementById("today").style.backgroundColor = 'beige';
        document.getElementById("congty").disabled = false;
        document.getElementById("dienluc").disabled = false;
        document.getElementById("tram").disabled = false;
    }
});

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

$("#loading").hide();

// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: jsonKhachhang,
        columns: getColumns(jsonKhachhang)
    });
}

// Click nut xem bao cao
function clickXembaocao() {

    madonviquanly_tam = '%20';
    matram_tam = '%20';
    capdonvi = '%20';
    if (macongty == null || macongty == '0') {
        capdonvi = 1;
        madonviquanly_tam = 'CPC';
    } else {
        if (madienluc == null || madienluc == '0') {
            capdonvi = 2;
            madonviquanly_tam = macongty;
        } else {
            if (matram == null || matram == '0') {
                capdonvi = 3;
                madonviquanly_tam = madienluc;
            } else {
                capdonvi = 4;
                madonviquanly_tam = madienluc;
                matram_tam = matram;
            }
        }
    }



    //Nếu bật chế độ ngày!!!!
    if (isDate) {
        $("#loading").show();
        $.get(`${url}/api/HATHE_getBCKhachhang_RaSoatThongTin_TheoNgay/${$('#today').val()}`, function (data) {
            jsonKhachhang = data;
            taodulieuchobang(jsonKhachhang);
            $("#loading").hide();
        });
    } else {
        $("#loading").show();
        console.log("URL: " + `${url}/api/HATHE_getBCKhachhang_RaSoatThongTin/${madonviquanly_tam}/${matram_tam}/${capdonvi}`);
        $.get(`${url}/api/HATHE_getBCKhachhang_RaSoatThongTin/${madonviquanly_tam}/${matram_tam}/${capdonvi}`, function (data) {
            jsonKhachhang = data;
            taodulieuchobang(jsonKhachhang);
            $("#loading").hide();
        });
    }

}

function taodulieuchobang(data) {
    $("#dataTable").dataTable().fnDestroy();
    $("#tbody").empty();
    //
    var tong_SLUONG_KHANG = 0;
    var tong_SLUONG_KHANG_DUOCRASOAT = 0;    // Du so cot co toa do
    var tong_SLUONG_KHANG_DUTHONGTINKHANG = 0;
    var tong_SLUONG_KHANG_COSDT = 0;
    var tong_SLUONG_KHANG_COEMAIL = 0;
    var tong_SLUONG_KHANG_DUTHONGTINDIEMDO = 0;
    var tong_SLUONG_KHANG_DUTHONGTINDHETHONGDODEM = 0;
    var tong_SLUONG_KHANG_DUBAYEUTOTREN = 0;
    $.each(data, function (i, f) {
        var tyle_phantram_full;
        var tyle_phantram;
        tong_SLUONG_KHANG = tong_SLUONG_KHANG + f.SLUONG_KHANG;
        tong_SLUONG_KHANG_DUOCRASOAT = tong_SLUONG_KHANG_DUOCRASOAT + f.SLUONG_KHANG_DUOCRASOAT;
        tong_SLUONG_KHANG_DUTHONGTINKHANG = tong_SLUONG_KHANG_DUTHONGTINKHANG + f.SLUONG_KHANG_DUTHONGTINKHANG;
        tong_SLUONG_KHANG_COEMAIL = tong_SLUONG_KHANG_COEMAIL + f.SLUONG_KHANG_COEMAIL;
        tong_SLUONG_KHANG_COSDT = tong_SLUONG_KHANG_COSDT + f.SLUONG_KHANG_COSDT;
        tong_SLUONG_KHANG_DUTHONGTINDIEMDO = tong_SLUONG_KHANG_DUTHONGTINDIEMDO + f.SLUONG_KHANG_DUTHONGTINDIEMDO;
        tong_SLUONG_KHANG_DUTHONGTINDHETHONGDODEM = tong_SLUONG_KHANG_DUTHONGTINDHETHONGDODEM + f.SLUONG_KHANG_DUTHONGTINDHETHONGDODEM;
        tong_SLUONG_KHANG_DUBAYEUTOTREN = tong_SLUONG_KHANG_DUBAYEUTOTREN + f.SLUONG_KHANG_DUBAYEUTOTREN;
        if (f.SLUONG_KHANG != 0) {
            tyle_phantram_full = (f.SLUONG_KHANG_DUBAYEUTOTREN / f.SLUONG_KHANG) * 100;
            tyle_phantram_full = tyle_phantram_full.toFixed(2);
        } else {
            tyle_phantram_full = 0;
        }
        if (f.SLUONG_KHANG_DUOCRASOAT != 0) {
            tyle_phantram = (f.SLUONG_KHANG_DUBAYEUTOTREN / f.SLUONG_KHANG_DUOCRASOAT) * 100;
            tyle_phantram = tyle_phantram.toFixed(2);
        } else {
            tyle_phantram = 0;
        }
        $("#tbody").append(
            `<tr>
                <td style='text-align:center'>${f.STT}</td>
                <td style='text-align:left'>${f.TEN_DONVI}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG_DUOCRASOAT.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG_DUTHONGTINKHANG.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG_COEMAIL.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG_COSDT.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG_DUTHONGTINDIEMDO.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG_DUTHONGTINDHETHONGDODEM.toLocaleString('en')}</td>
                <td style='text-align:right'>${f.SLUONG_KHANG_DUBAYEUTOTREN.toLocaleString('en')}</td>
                <td style='text-align:right'>${tyle_phantram_full + "%"}</td>
                <td style='text-align:right'>${tyle_phantram + "%"}</td>
            </tr>`
        );
    });

    $('#dataTable').DataTable({

        "columns": [
            { "width": "5%" },
            { "width": "35%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" },
            { "width": "6%" }
        ]
    });
    $('#tendonvi_colwillchangef').html('Tổng cộng:');
    $('#tongsoluongkhachhangcolienketcotCMIS_colwillchangef').html(tong_SLUONG_KHANG.toLocaleString('en'));
    $('#tongsoluongkhachhangcolienketcotTTHT_colwillchangef').html(tong_SLUONG_KHANG_DUOCRASOAT.toLocaleString('en'));
    $('#tongsoluongthongtinkhachhang_colwillchangef').html(tong_SLUONG_KHANG_DUTHONGTINKHANG.toLocaleString('en'));
    $('#tongsoluongthongtinemail_colwillchangef').html(tong_SLUONG_KHANG_COEMAIL.toLocaleString('en'));
    $('#tongsoluongthongtinsodienthoai_colwillchangef').html(tong_SLUONG_KHANG_COSDT.toLocaleString('en'));
    $('#tongsoluongthongtindiemdo_colwillchangef').html(tong_SLUONG_KHANG_DUTHONGTINDIEMDO.toLocaleString('en'));
    $('#tongsoluongthongtinhethongdodem_colwillchangef').html(tong_SLUONG_KHANG_DUTHONGTINDHETHONGDODEM.toLocaleString('en'));
    $('#tongsoluongbathongtintren_colwillchangef').html(tong_SLUONG_KHANG_DUBAYEUTOTREN.toLocaleString('en'));
}

