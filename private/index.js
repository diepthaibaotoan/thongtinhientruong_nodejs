var url = GV_URL;
var header_dangnhap = document.getElementById('header_dangnhap');
var ten_dangnhap = "";
// goi dien server
var socket = io(GV_URL);

// var chatarea_tinnhan = document.getElementById('chatarea_tinnhan');    
var chatbox_tinnhan = document.getElementById('chatbox_tinnhan');
var guitinnhan = document.getElementById('guitinnhan');
chatbox_tinnhan.addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        guitinnhan.click();
    }
});

var textarea_nguoionline = document.getElementById('textarea_nguoionline');
var textarea_suco = document.getElementById('textarea_suco');
var test_guisuco = document.getElementById('test_guisuco');
//var send = document.getElementById('send');
var h = $("#wrapper").height();
var w = $("#wrapper").width();
//console.log("w:" +w);
var mainNav = $("#mainNav").height();
//console.log("mainNav:" +mainNav);
var foo = $("#foo").height();

batTatGiaoDienTrangChu(false);

//console.log("mainNav:" +foo);
$("#a").height(h - mainNav - foo - 30);
$("#a").width(w);
// 1. Lang nghe server tra ve chat
socket.on("Server-send-chat", function (data) {
    var ds_tinnhan = GF_CHATAREA_TINNHAN(data, ten_dangnhap);
    $("#chatarea_tinnhan").append(ds_tinnhan);
    $("#chatarea_tinnhan").scrollTop($("#chatarea_tinnhan")[0].scrollHeight);
});

// 2. Lang nghe server tra ve nguoi dang nhap
socket.on("Server-send-username", function (data) {
    // console.log("HeHe :  "+ data);
    $("#area_danhsachnguoionline").empty();
    var ds_nguoidungonline = GF_DANHSACHNGUOIDUNGONLINE(data);
    $("#area_danhsachnguoionline").append(ds_nguoidungonline);
    $("#text_thanhvienonline").text("Online: " + data.length + "");
    // textarea_nguoionline.scrollTop = textarea_nguoionline.scrollHeight;
});

// 3. Lang nghe server su co
socket.on("Server-send-suco", function (data) {
    // console.log("Data: "+data);
    textarea_suco.value = data;
    textarea_suco.scrollTop = textarea_suco.scrollHeight;
});

$(document).ready(function () {
    if(taikhoan == undefined || taikhoan == null || taikhoan == ''){
        document.getElementById('id_login').style.display = 'block';
    }


    $("#guitinnhan").click(function () {

        var d = new Date();
        var gio = addZero(d.getHours()) + ":" + addZero(d.getMinutes());

        var msg = chatbox_tinnhan.value;
        IsUserCallAI(msg);

        chatbox_tinnhan.value = "";
    });

    function addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
});

// test gui su co
$(document).ready(function () {
    // Đạt start
    initgiaodien();
    initgiaodien_cpcitc();

    $("#test_guisuco").click(function () {
        socket.emit("Client-send-suco", "PC05CC"); // gui len server
        chatbox_suco.value = "";
    });
});

// BAT DAU: DANG NHAP
function clickDangnhap() {
    document.getElementById('id_login').style.display = 'block';
}

var autodangnhap = document.getElementById('dialog_dangnhap_matkhau');
autodangnhap.addEventListener("keyup", function (event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        clickDongyDangnhap();
    }
});

function clickDongyDangnhap() {
    var m_taikhoan = $('#dialog_dangnhap_taikhoan').val();
    var m_matkhau = $('#dialog_dangnhap_matkhau').val();
    if (m_taikhoan == "") {
        alert("Bạn vui lòng nhập tài khoản.")
    } else if (m_matkhau == "") {
        alert("Bạn vui lòng nhập mật khẩu.")
    } else {
        $("#loading").show();
        $.post(`${url}/api/get_DangnhapAD`, {
            TEN_DANGNHAP: m_taikhoan,
            MAT_KHAU: m_matkhau,
            KEY: "CPCIT@2017"
        }, function (result) {
            //
            var n = result.search("true");
            if (n == 0) {
                $("#id_login").hide();
                batTatGiaoDienTrangChu(true);
                // Chi insert khong co update
                $.get(`${url}/api/update_DS_Nguoidung/%20/${m_taikhoan}`, function (data) {
                    $('#header_tentaikhoan').text(m_taikhoan);
                    $('#header_dangnhap').hide(1000);
                    $('#header_dangxuat').show(1000);
                    $('#chatbox_tinnhan').show(1000);
                    $('#guitinnhan').show(1000);
                    //
                    localStorage['taikhoan'] = m_taikhoan; // only strings
                    localStorage['matkhau'] = m_matkhau; // only strings
                    taikhoan = m_taikhoan;
                    matkhau = m_matkhau;
                    socket.emit("Client-send-username", "Window%" + taikhoan); // gui len server

                    //
                    ten_dangnhap = m_taikhoan;
                    $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
                        if (data.length != 0) {
                            data.forEach(function (nguoidungObj) {

                                // 
                                if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_PHANQUYEN) > -1) {
                                    $("#menu_phanquyen").show();
                                } else {
                                    $("#menu_phanquyen").hide();
                                }

                                // Phan quyen thay menu treo thao
                                if (nguoidungObj.CHUC_NANG_TTCT == undefined) {
                                    $("#menu_treothao").hide();
                                } else if (nguoidungObj.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT) > -1) {
                                    $("#menu_treothao").show();
                                } else {
                                    $("#menu_treothao").hide();
                                }
                            })
                        } else {
                            //
                        }
                    });
                });
                $("#mainNav").show();

            } else {
                alert("Bạn nhập tài khoản không đúng, bạn vui lòng nhập lại.")
            }
            $("#loading").hide();
        });
    }
}

// BAT DAU: DANG XUAT
function clickDangxuat() {
    document.getElementById('id_logout').style.display = 'block';
    var obj = $("#text_dangxuat_xacnhan").text("Bạn nhấn đồng ý để đăng xuất khỏi trang EVNCPC TTHT.");
    obj.html(obj.html().replace(/\n/g, '<br/>'));
}

function clickDongyDangxuat() {
    batTatGiaoDienTrangChu(false);

    localStorage['taikhoan'] = "";
    localStorage['matkhau'] = "";
    $('#header_tentaikhoan').text("Chưa đăng nhập");
    document.getElementById('id_logout').style.display = 'none';
    $('#header_dangnhap').show(1000);
    $('#header_dangxuat').hide(1000);
    $('#chatbox_tinnhan').hide(1000);
    $('#guitinnhan').hide(1000);
    socket.emit("Client-send-username", taikhoan);
    $("#menu_phanquyen").hide();
    $("#menu_treothao").hide();
}

// Dang nhap tu dong lan dau
var taikhoan = localStorage['taikhoan'];
var matkhau = localStorage['matkhau'];
if (taikhoan == '' || taikhoan == null || taikhoan == 'undefined') {
    $("#menu_phanquyen").hide();
} else {
    $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${taikhoan}`, function (data) {
        if (data.length != 0) {
            data.forEach(function (nguoidungObj) {
                if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_PHANQUYEN) > -1) {
                    $("#menu_phanquyen").show();
                } else {
                    $("#menu_phanquyen").hide();
                }

                // Phan quyen thay menu treo thao
                if (nguoidungObj.CHUC_NANG_TTCT == undefined) {
                    $("#menu_treothao").hide();
                } else if (nguoidungObj.CHUC_NANG_TTCT.indexOf(ADMIN_TTCT) > -1) {
                    $("#menu_treothao").show();
                } else {
                    $("#menu_treothao").hide();
                }
            })
        } else {
            //a
        }
    });
}
ten_dangnhap = taikhoan;

$("#loading").show();
$.post(`${url}/api/get_DangnhapAD`, {
    TEN_DANGNHAP: taikhoan,
    MAT_KHAU: matkhau,
    KEY: "CPCIT@2017"
}, function (result) {
    //
    var n = result.search("true");
    if (n == 0) {
        batTatGiaoDienTrangChu(true);
        $('#header_tentaikhoan').text(taikhoan);
        $('#header_dangnhap').hide();
        $('#header_dangxuat').show(1000);
        $('#chatbox_tinnhan').show(1000);
        $('#guitinnhan').show(1000);
        //
        socket.emit("Client-send-username", "Window%" + taikhoan); // gui len server

        // Dang nhap lan dau thi dua doan chat hien len
        $.get(`${url}/api/get_DS_Tinnhan`, function (data) {
            var ds_tinnhan = GF_CHATAREA_TINNHAN(data, ten_dangnhap);
            $("#chatarea_tinnhan").append(ds_tinnhan);
            $("#chatarea_tinnhan").scrollTop($("#chatarea_tinnhan")[0].scrollHeight);
        });
    } else {
        $('#header_tentaikhoan').text("Chưa đăng nhập");
        $('#header_dangnhap').show(1000);
        $('#header_dangxuat').hide(1000);
        $('#chatbox_tinnhan').hide(1000);
        $('#guitinnhan').hide(1000);
        //
        $.get(`${url}/api/get_DS_Tinnhan`, function (data) {
            var ds_tinnhan = GF_CHATAREA_TINNHAN(data, ten_dangnhap);
            $("#chatarea_tinnhan").append(ds_tinnhan);
            $("#chatarea_tinnhan").scrollTop($("#chatarea_tinnhan")[0].scrollHeight);
        });
    }
    $("#loading").hide();
});

//
function KiemTraChuoiSDT(sdt) {
    if (sdt.length < 3) return false;
    for (var i = 0; i < sdt.length; i++) {
        if (sdt[i] > '9' || sdt[i] < '0') return false;
    }
    return true;
}

function GF_CONVERTTOENGLISH(stringInput) {
    var a_character = new Array("á", "à", "ả", "ã", "ạ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ");
    var A_character = new Array("A", "Á", "À", "Ả", "Ã", "Ạ", "Ă", "Ă", "Ằ", "Ẳ", "Ẵ", "Ặ", "Â", "Ấ", "Ầ", "Ẩ", "Ẫ", "Ậ");
    var d_character = new Array("đ");
    var D_character = new Array("D", "Đ");
    var e_character = new Array("é", "è", "ẽ", "ẹ", "ê", "ế", "ề", "ể", "ễ", "ệ");
    var E_character = new Array("E", "É", "È", "Ẽ", "Ẹ", "Ê", "Ế", "Ề", "Ể", "Ễ", "Ệ");
    var i_character = new Array("í", "ì", "ỉ", "ĩ", "ị");
    var I_character = new Array("I", "Í", "Ì", "Ỉ", "Ĩ", "Ị");
    var o_character = new Array("ó", "ò", "ỏ", "õ", "ọ", "ô", "ố", "ồ", "ổ", "ỗ", "ộ", "ơ", "ớ", "ờ", "ở", "ỡ", "ợ");
    var O_character = new Array("O", "Ó", "Ò", "Ỏ", "Õ", "Ọ", "Ô", "Ố", "Ồ", "ổ", "Ỗ", "Ộ", "Ơ", "Ớ", "Ờ", "Ở", "Ỡ", "Ợ");
    var u_character = new Array("ú", "ù", "ủ", "ũ", "ụ", "ư", "ứ", "ừ", "ử", "ữ", "ự");
    var U_character = new Array("U", "Ú", "Ù", "Ủ", "Ũ", "Ụ", "Ư", "Ứ", "Ừ", "Ử", "Ữ", "Ự");
    var y_character = new Array("ý", "ỳ", "ỷ", "ỹ", "ỵ");
    var Y_character = new Array("Y", "Ý", "Ỳ", "Ỷ", "Ỹ", "Ỵ");

    for (var i = 0; i < a_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, a_character[i], "a");
    }
    for (var i = 0; i < d_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, d_character[i], "d");
    }
    for (var i = 0; i < e_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, e_character[i], "e");
    }
    for (var i = 0; i < i_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, i_character[i], "i");
    }
    for (var i = 0; i < o_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, o_character[i], "o");
    }
    for (var i = 0; i < u_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, u_character[i], "u");
    }
    for (var i = 0; i < y_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, y_character[i], "y");
    }

    for (var i = 0; i < A_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, A_character[i], "a");
    }
    for (var i = 0; i < D_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, D_character[i], "d");
    }
    for (var i = 0; i < E_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, E_character[i], "e");
    }
    for (var i = 0; i < I_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, I_character[i], "i");
    }
    for (var i = 0; i < O_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, O_character[i], "o");
    }
    for (var i = 0; i < U_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, U_character[i], "u");
    }
    for (var i = 0; i < Y_character.length; i++) {
        stringInput = GF_REPLACEALL(stringInput, Y_character[i], "y");
    }
    stringInput = stringInput.toLowerCase();
    return stringInput;
}

function GF_REPLACEALL(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

function LayDSThongTinNguoiDung_bySDT(sdt) {
    if (KiemTraChuoiSDT(sdt)) {
        $.get(`${url}/api/get_DS_ThongTinNguoiDung_bySDT/${sdt}/`, function (data) {
            var AI_TraLoi = "";
            var i = 1;
            data.forEach(function (User) {
                AI_TraLoi += (i++) + ". " + User.tenkhaisinh + " (" + User.sodienthoai + "), " + User.tenpb + " - " + User.tendv + ".<br/>";
            })
            if (i == 1) {
                AI_TraLoi = "Không tìm thấy thông tin nào theo số điện thoại này cả.Bạn vui lòng kiểm tra lại.";
            }
            if (i > 10) {
                AI_TraLoi += (i++) + ". " + User.tenkhaisinh + " (" + User.sodienthoai + "), " + User.tenpb + " - " + User.tendv + ".<br/>";
            }
            socket.emit("Client-send-chat", 'IT' + "$%" + AI_TraLoi);
        });
    }
}

function LayDSThongTinNguoiDung_khongthamso() {
    $.get(`${url}/api/get_DS_ThongTinNguoiDung_KhongThamSo/`, function (data) {
        var i = 1;
        data.forEach(function (User) {
            if (User.tenkhongdau == null) {
                User.tenkhongdau = GF_CONVERTTOENGLISH(User.tenkhaisinh);
                User.tenkhongdau = GF_REPLACEALL(User.tenkhongdau, ' ', '%20')
                User.tenkhaisinh = GF_REPLACEALL(User.tenkhaisinh, ' ', '%20')
                $.get(`${url}/api/update_DS_ThongTinNguoiDung_tenkhongdau/${User.tenkhongdau}/${User.tenkhaisinh}/${User.NS_ID}/`, function (data) {
                    i++;
                });
            }

        })
    });
}

function LayDSThongTinNguoiDung_byTenkhongdau(tenkhongdau) {
    $.get(`${url}/api/get_DS_ThongTinNguoiDung_bytenkhongdau/${tenkhongdau}/`, function (data) {
        var AI_TraLoi = "";
        var i = 1;
        data.forEach(function (User) {
            AI_TraLoi += (i++) + ". " + User.tenkhaisinh + " (" + User.sodienthoai + "), " + User.tenpb + " - " + User.tendv + ".<br/>";
        })
        if (i == 1) {
            AI_TraLoi = "Không tìm thấy thông tin nào theo tên này cả. Bạn vui lòng kiểm tra lại.";
        }
        if (i > 10) {
            AI_TraLoi = "Dữ liệu trả về quá nhiều. Bạn vui lòng nhập thêm kí tự tìm kiếm.";
        }
        socket.emit("Client-send-chat", 'IT' + "$%" + AI_TraLoi);
    });
}

function LayDSThongTinNguoiDung_byUserName(UserName) {
    $.get(`${url}/api/get_DS_ThongTinNguoiDung_byUserName/${UserName}/`, function (data) {
        var AI_TraLoi = "";
        var i = 1;
        data.forEach(function (User) {
            AI_TraLoi += (i++) + ". " + User.tenkhaisinh + " (" + User.sodienthoai + "), " + User.tenpb + " - " + User.tendv + ".<br/>";
            //console.log(User.tenkhaisinh + " (" + User.sodienthoai + "), " + User.tenpb + " - " + User.tendv + ".<br/>");
        })
        if (i == 1) {
            AI_TraLoi = "Không tìm thấy thông tin nào theo UserName này cả.Bạn vui lòng kiểm tra lại.";
        }
        if (i > 10) {
            AI_TraLoi += (i++) + ". " + User.tenkhaisinh + " (" + User.sodienthoai + "), " + User.tenpb + " - " + User.tendv + ".<br/>";
        }
        socket.emit("Client-send-chat", 'IT' + "$%" + AI_TraLoi);
    });
}


//Hàm lấy chuỗi kí tự người dùng cần tìm kiếm
function TIMCHUOITHONGTIN(startCharacter, finishCharacter, stringInput) {
    var stringArray = new Array();
    var i = 0;
    var stringcopy = stringInput;
    var start = -1;
    var finish = -1;
    while (true) {
        start = stringcopy.indexOf(startCharacter);
        finish = stringcopy.indexOf(finishCharacter);
        if (start < finish && start != -1) {
            var strFind = stringcopy.substring(start + 1, finish);
            stringArray[i++] = strFind;
            var stringcopy = stringcopy.substring(finish + 2, stringcopy.length);
        } else {
            break;
        }
    }
    return stringArray;
}

function DanhSachTimKiemThongTinNguoiDung(stringinput) {
    var chuoitachduoc = TIMCHUOITHONGTIN('[', ']', stringinput);
    for (var i = 0; i < chuoitachduoc.length; i++) {
        chuoitachduoc[i] = chuoitachduoc[i].trim();
        if (KiemTraChuoiSDT(chuoitachduoc[i])) {
            LayDSThongTinNguoiDung_bySDT(chuoitachduoc[i]);
        } else {

            if (KiemTraUserName(chuoitachduoc[i])) {
                LayDSThongTinNguoiDung_byUserName(chuoitachduoc[i]);
            } else {
                var tenkhongdau = GF_CONVERTTOENGLISH(chuoitachduoc[i]);
                LayDSThongTinNguoiDung_byTenkhongdau(tenkhongdau);
            }
        }
    }
}

function IsUserCallAI(msg) {
    var res = TIMCHUOITHONGTIN('@', ':', msg);
    var i = res.length;
    var msg2 = msg;
    if (i > 0) {
        var resUpper = res[0].toUpperCase();
        msg = GF_REPLACEALL(msg, res, resUpper);
        if (msg.indexOf("@IT:") > -1) {

            // console.log(msg+" 1 ");
            socket.emit("Client-send-chat", taikhoan + "$%" + msg);
            DanhSachTimKiemThongTinNguoiDung(msg);

        } else {
            // console.log(msg+" 2 ");
            socket.emit("Client-send-chat", taikhoan + "$%" + msg2);
        }
    } else {
        // console.log(msg+" 3 ");
        socket.emit("Client-send-chat", taikhoan + "$%" + msg2);
    }
}

function KiemTraUserName(UserName) {
    if (UserName.indexOf(" ") == -1) {
        return true;
    }
    return false;
    // console.log("Ki tu trang "+UserName+": "+ UserName.indexOf(" "));
}

function batTatGiaoDienTrangChu(kiemtra) {
    if (kiemtra == true) {
        $("#giaodien_trangchu").show();
        $(".giaodien_index").show();
    }else{
        $("#giaodien_trangchu").hide();
        $(".giaodien_index").hide();
    }
}

// Đạt start
function initgiaodien(){
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'UA-126252662-1');
}

function initgiaodien_cpcitc(){
    window.dataLayer = window.dataLayer || [];
    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());

    gtag('config', 'UA-131039951-1');
}

function hide_float_right() {
    var content = document.getElementById('float_content_right');
    var vid = document.getElementById("videothu");
    var hide = document.getElementById('hide_float_right');
    if (content.style.display == "none") {
        content.style.display = "block";
        vid.muted = false; hide.innerHTML = '<a href="javascript:hide_float_right()">Tắt Video [X]</a>';
    } else {
        content.style.display = "none";
        vid.muted = true; hide.innerHTML = '<a href="javascript:hide_float_right()">Xem Video...</a>';
    }
}

//array links
var randomvideo = new Array()
randomvideo[0] = "https://ttht.cpc.vn/evncpc/evncpc_ttht/2.1%20Gi%E1%BB%9Bi%20thi%E1%BB%87u%20%E1%BB%A9ng%20d%E1%BB%A5ng%20EVNCPC%20TTHT.mp4";
randomvideo[1] = "https://ttht.cpc.vn/evncpc/evncpc_ttht/2.2%20Gi%E1%BB%9Bi%20Thi%E1%BB%87u%20%E1%BB%A8ng%20D%E1%BB%A5ng%20EVNCPC%20TTKH.m4v";

//function gets link src
function getRandomUrl() {
    var myFrame = document.getElementById('videothu');
    myFrame.src = randomvideo[Math.floor(Math.random() * randomvideo.length)];
}
getRandomUrl();

// Đạt finish
