GF_PHANMENU();
var url = GV_URL;
battatgiaodienchinh(localStorage['taikhoan']);
var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matram;
var isDate = false;


$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tram").empty();
$("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);


// BIEN TAP CHE DO NGAY GIO 
document.querySelector("#today").valueAsDate = new Date();
document.getElementById("today").disabled = true;
var cb_date = document.querySelector("input[name=cb_date]");
cb_date.addEventListener('change', function () {
    if (this.checked) {
        isDate = true;
        document.getElementById("today").disabled = false;
        document.getElementById("today").style.backgroundColor = 'white';
        document.getElementById("congty").disabled = true;
        document.getElementById("dienluc").disabled = true;
        document.getElementById("tram").disabled = true;

    } else {
        isDate = false;
        document.getElementById("today").disabled = true;
        document.getElementById("today").style.backgroundColor = 'beige';
        document.getElementById("congty").disabled = false;
        document.getElementById("dienluc").disabled = false;
        document.getElementById("tram").disabled = false;
    }
});

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tram").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm"
});

$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matram = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matram = '0';
    $("#tram").empty();
    $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
    $.get(`${url}/api/mtb_get_d_tram_by_donvi/${madienluc.trim()}/dungtoan`, function (data) {
        data = JSON.parse(data);
        $("#tram").empty();
        $("#tram").append(`<option value="0">-- Tất cả Trạm --</option>`);
        $.each(data, function (i, f) {
            $("#tram").append(`<option value="${f.MA_TRAM}">${f.MA_TRAM} - ${f.TEN_TRAM}</option>`);
        });
    });
});

$('#tram').on('change', function () {
    matram = this.value;
});

//chay mac dinh lan dau
$("#loading").hide();
// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: dulieubaocao,
        columns: getColumns(dulieubaocao)
    });
}

// Click nut xem bao cao
function clickXembaocao() {
    $('#tongcong_colwillchangef').html('');
    $('#tongsocot_colwillchangef').html('');
    $('#tongsocotkhongcoanh_colwillchangef').html('');
    $('#tongsocotcomotanh_colwillchangef').html('');
    $('#tongsocotcohaianh_colwillchangef').html('');

    if (isDate) {
        $("#dataTable").dataTable().fnDestroy();
        $("#tbody").empty();
        $('#colwillchangeh').html('Công Ty');
        $("#loading").show();
        $.get(`${url}/api/HATHE_getBCCot_TheoNgay/${$('#today').val()}`, function (data) {
            dulieubaocao = data;
            $('#tongcong_colwillchangef').html('Tổng cộng:');
            var tongsocot = 0;
            var tongsocotkhongcoanh = 0;
            var tongsocotcomotanh = 0;
            var tongsocotcohaianh = 0;
            var tongsocotcobaanhtrolen = 0;
            $.each(data, function (i, f) {
                tongsocot = tongsocot + f.SLUONG_COT;
                tongsocotkhongcoanh = tongsocotkhongcoanh + f.SLUONG_COTKHONGCOANH;
                tongsocotcomotanh = tongsocotcomotanh + f.SLUONG_COTCOMOTANH;
                tongsocotcohaianh = tongsocotcohaianh + f.SLUONG_COTCOHAIANH;
                tongsocotcobaanhtrolen = tongsocotcobaanhtrolen + f.SLUONG_COTCOBAANHTROLEN;

                var tyle_cothoanthanh;
                if (f.SLUONG_COT != 0) {
                    tyle_cothoanthanh = (f.SLUONG_COTCOBAANHTROLEN / f.SLUONG_COT) * 100;
                    tyle_cothoanthanh = tyle_cothoanthanh.toFixed(2);
                } else {
                    tyle_cothoanthanh = 0;
                }
                $("#tbody").append(
                    `<tr>
                        <td style='text-align:center'>${f.STT}</td>
                        <td>${f.TEN_DONVILOC}</td>
                        <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTKHONGCOANH.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTCOMOTANH.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTCOHAIANH.toLocaleString('en')}</td>
                        <td style='text-align:right'>${f.SLUONG_COTCOBAANHTROLEN.toLocaleString('en')}</td>
                        <td style='text-align:right'>${tyle_cothoanthanh + '%'}</td>
                    </tr>`
                );
            });
            $('#dataTable').DataTable({
                "columns": [
                    { "width": "5%" },
                    { "width": "35%" },
                    { "width": "10%" },
                    { "width": "10%" },
                    { "width": "10%" },
                    { "width": "10%" },
                    { "width": "10%" },
                    { "width": "10%" }
                ]
            });

            $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
            $('#tongsocotkhongcoanh_colwillchangef').html(tongsocotkhongcoanh.toLocaleString('en'));
            $('#tongsocotcomotanh_colwillchangef').html(tongsocotcomotanh.toLocaleString('en'));
            $('#tongsocotcohaianh_colwillchangef').html(tongsocotcohaianh.toLocaleString('en'));
            $('#tongsocotcobaanhtrolen_colwillchangef').html(tongsocotcobaanhtrolen.toLocaleString('en'));

            $("#loading").hide();
        });
    } else {
        if (macongty == null || macongty == '0') {
            $("#dataTable").dataTable().fnDestroy();
            $("#tbody").empty();
            $('#colwillchangeh').html('Công Ty');
            $("#loading").show();
            $.get(`${url}/api/HATHE_getBCCot/CPC/${GV_CAPTONG}`, function (data) {
                dulieubaocao = data;
                $('#tongcong_colwillchangef').html('Tổng cộng:');
                var tongsocot = 0;
                var tongsocotkhongcoanh = 0;
                var tongsocotcomotanh = 0;
                var tongsocotcohaianh = 0;
                var tongsocotcobaanhtrolen = 0;
                $.each(data, function (i, f) {
                    tongsocot = tongsocot + f.SLUONG_COT;
                    tongsocotkhongcoanh = tongsocotkhongcoanh + f.SLUONG_COTKHONGCOANH;
                    tongsocotcomotanh = tongsocotcomotanh + f.SLUONG_COTCOMOTANH;
                    tongsocotcohaianh = tongsocotcohaianh + f.SLUONG_COTCOHAIANH;
                    tongsocotcobaanhtrolen = tongsocotcobaanhtrolen + f.SLUONG_COTCOBAANHTROLEN;

                    var tyle_cothoanthanh;
                    if (f.SLUONG_COT != 0) {
                        tyle_cothoanthanh = (f.SLUONG_COTCOBAANHTROLEN / f.SLUONG_COT) * 100;
                        tyle_cothoanthanh = tyle_cothoanthanh.toFixed(2);
                    } else {
                        tyle_cothoanthanh = 0;
                    }
                    $("#tbody").append(
                        `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.TEN_DONVILOC}</td>
                            <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTKHONGCOANH.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTCOMOTANH.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTCOHAIANH.toLocaleString('en')}</td>
                            <td style='text-align:right'>${f.SLUONG_COTCOBAANHTROLEN.toLocaleString('en')}</td>
                            <td style='text-align:right'>${tyle_cothoanthanh + '%'}</td>
                        </tr>`
                    );
                });
                $('#dataTable').DataTable({
                    "columns": [
                        { "width": "5%" },
                        { "width": "35%" },
                        { "width": "10%" },
                        { "width": "10%" },
                        { "width": "10%" },
                        { "width": "10%" },
                        { "width": "10%" },
                        { "width": "10%" }
                    ]
                });

                $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
                $('#tongsocotkhongcoanh_colwillchangef').html(tongsocotkhongcoanh.toLocaleString('en'));
                $('#tongsocotcomotanh_colwillchangef').html(tongsocotcomotanh.toLocaleString('en'));
                $('#tongsocotcohaianh_colwillchangef').html(tongsocotcohaianh.toLocaleString('en'));
                $('#tongsocotcobaanhtrolen_colwillchangef').html(tongsocotcobaanhtrolen.toLocaleString('en'));

                $("#loading").hide();
            });
        } else {
            if (madienluc == null || madienluc == '0') {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $('#colwillchangeh').html('Điện Lực');
                $("#loading").show();
                $.get(`${url}/api/HATHE_getBCCot/${macongty}/${GV_CAPCONGTY}`, function (data) {
                    dulieubaocao = data;
                    $('#tongcong_colwillchangef').html('Tổng cộng:');
                    var tongsocot = 0;
                    var tongsocotkhongcoanh = 0;
                    var tongsocotcomotanh = 0;
                    var tongsocotcohaianh = 0;
                    var tongsocotcobaanhtrolen = 0;

                    $.each(data, function (i, f) {
                        tongsocot = tongsocot + f.SLUONG_COT;
                        tongsocotkhongcoanh = tongsocotkhongcoanh + f.SLUONG_COTKHONGCOANH;
                        tongsocotcomotanh = tongsocotcomotanh + f.SLUONG_COTCOMOTANH;
                        tongsocotcohaianh = tongsocotcohaianh + f.SLUONG_COTCOHAIANH;
                        tongsocotcobaanhtrolen = tongsocotcobaanhtrolen + f.SLUONG_COTCOBAANHTROLEN;

                        var tyle_cothoanthanh;
                        if (f.SLUONG_COT != 0) {
                            tyle_cothoanthanh = (f.SLUONG_COTCOBAANHTROLEN / f.SLUONG_COT) * 100;
                            tyle_cothoanthanh = tyle_cothoanthanh.toFixed(2);
                        } else {
                            tyle_cothoanthanh = 0;
                        }
                        $("#tbody").append(
                            `<tr>
                                <td style='text-align:center'>${f.STT}</td>
                                <td>${f.TEN_DONVILOC}</td>
                                <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_COTKHONGCOANH.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_COTCOMOTANH.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_COTCOHAIANH.toLocaleString('en')}</td>
                                <td style='text-align:right'>${f.SLUONG_COTCOBAANHTROLEN.toLocaleString('en')}</td>
                                <td style='text-align:right'>${tyle_cothoanthanh + '%'}</td>
                            </tr>`
                        );
                    });
                    $('#dataTable').DataTable({
                        "columns": [
                            { "width": "5%" },
                            { "width": "35%" },
                            { "width": "10%" },
                            { "width": "10%" },
                            { "width": "10%" },
                            { "width": "10%" },
                            { "width": "10%" },
                            { "width": "10%" }
                        ]
                    });

                    $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
                    $('#tongsocotkhongcoanh_colwillchangef').html(tongsocotkhongcoanh.toLocaleString('en'));
                    $('#tongsocotcomotanh_colwillchangef').html(tongsocotcomotanh.toLocaleString('en'));
                    $('#tongsocotcohaianh_colwillchangef').html(tongsocotcohaianh.toLocaleString('en'));
                    $('#tongsocotcobaanhtrolen_colwillchangef').html(tongsocotcobaanhtrolen.toLocaleString('en'));

                    $("#loading").hide();
                });
            } else {
                if (matram == null || matram == '0') {
                    $("#dataTable").dataTable().fnDestroy();
                    $("#tbody").empty();
                    $('#colwillchangeh').html('Trạm');
                    $("#loading").show();
                    $.get(`${url}/api/HATHE_getBCCot/${madienluc}/${GV_CAPDIENLUC}`, function (data) {
                        dulieubaocao = data;
                        $('#tongcong_colwillchangef').html('Tổng cộng:');
                        var tongsocot = 0;
                        var tongsocotkhongcoanh = 0;
                        var tongsocotcomotanh = 0;
                        var tongsocotcohaianh = 0;
                        var tongsocotcobaanhtrolen = 0;
                        $.each(data, function (i, f) {
                            tongsocot = tongsocot + f.SLUONG_COT;
                            tongsocotkhongcoanh = tongsocotkhongcoanh + f.SLUONG_COTKHONGCOANH;
                            tongsocotcomotanh = tongsocotcomotanh + f.SLUONG_COTCOMOTANH;
                            tongsocotcohaianh = tongsocotcohaianh + f.SLUONG_COTCOHAIANH;
                            tongsocotcobaanhtrolen = tongsocotcobaanhtrolen + f.SLUONG_COTCOBAANHTROLEN;

                            var tyle_cothoanthanh;
                            if (f.SLUONG_COT != 0) {
                                tyle_cothoanthanh = (f.SLUONG_COTCOBAANHTROLEN / f.SLUONG_COT) * 100;
                                tyle_cothoanthanh = tyle_cothoanthanh.toFixed(2);
                            } else {
                                tyle_cothoanthanh = 0;
                            }
                            $("#tbody").append(
                                `<tr>
                                    <td style='text-align:center'>${f.STT}</td>
                                    <td>${f.TEN_DONVILOC}</td>
                                    <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTKHONGCOANH.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTCOMOTANH.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTCOHAIANH.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTCOBAANHTROLEN.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${tyle_cothoanthanh + '%'}</td>
                                </tr>`
                            );
                        });
                        $('#dataTable').DataTable({
                            "columns": [
                                { "width": "5%" },
                                { "width": "35%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" }
                            ]
                        });

                        $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
                        $('#tongsocotkhongcoanh_colwillchangef').html(tongsocotkhongcoanh.toLocaleString('en'));
                        $('#tongsocotcomotanh_colwillchangef').html(tongsocotcomotanh.toLocaleString('en'));
                        $('#tongsocotcohaianh_colwillchangef').html(tongsocotcohaianh.toLocaleString('en'));
                        $('#tongsocotcobaanhtrolen_colwillchangef').html(tongsocotcobaanhtrolen.toLocaleString('en'));

                        $("#loading").hide();
                    });
                } else {
                    $("#dataTable").dataTable().fnDestroy();
                    $("#tbody").empty();
                    $('#colwillchangeh').html('Trạm');
                    $("#loading").show();
                    $.get(`${url}/api/HATHE_getBCCot/${matram}/${GV_CAPTRAM}`, function (data) {
                        dulieubaocao = data;
                        $('#tongcong_colwillchangef').html('Tổng cộng:');
                        var tongsocot = 0;
                        var tongsocotkhongcoanh = 0;
                        var tongsocotcomotanh = 0;
                        var tongsocotcohaianh = 0;
                        var tongsocotcobaanhtrolen = 0;

                        $.each(data, function (i, f) {
                            tongsocot = tongsocot + f.SLUONG_COT;
                            tongsocotkhongcoanh = tongsocotkhongcoanh + f.SLUONG_COTKHONGCOANH;
                            tongsocotcomotanh = tongsocotcomotanh + f.SLUONG_COTCOMOTANH;
                            tongsocotcohaianh = tongsocotcohaianh + f.SLUONG_COTCOHAIANH;
                            tongsocotcobaanhtrolen = tongsocotcobaanhtrolen + f.SLUONG_COTCOBAANHTROLEN;

                            var tyle_cothoanthanh;
                            if (f.SLUONG_COT != 0) {
                                tyle_cothoanthanh = (f.SLUONG_COTCOBAANHTROLEN / f.SLUONG_COT) * 100;
                                tyle_cothoanthanh = tyle_cothoanthanh.toFixed(2);
                            } else {
                                tyle_cothoanthanh = 0;
                            }
                            $("#tbody").append(
                                `<tr>
                                    <td style='text-align:center'>${f.STT}</td>
                                    <td>${f.TEN_DONVILOC}</td>
                                    <td style='text-align:right'>${f.SLUONG_COT.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTKHONGCOANH.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTCOMOTANH.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTCOHAIANH.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${f.SLUONG_COTCOBAANHTROLEN.toLocaleString('en')}</td>
                                    <td style='text-align:right'>${tyle_cothoanthanh + '%'}</td>
                                </tr>`
                            );
                        });
                        $('#dataTable').DataTable({
                            "columns": [
                                { "width": "5%" },
                                { "width": "35%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" },
                                { "width": "10%" }
                            ]
                        });

                        $('#tongsocot_colwillchangef').html(tongsocot.toLocaleString('en'));
                        $('#tongsocotkhongcoanh_colwillchangef').html(tongsocotkhongcoanh.toLocaleString('en'));
                        $('#tongsocotcomotanh_colwillchangef').html(tongsocotcomotanh.toLocaleString('en'));
                        $('#tongsocotcohaianh_colwillchangef').html(tongsocotcohaianh.toLocaleString('en'));
                        $('#tongsocotcobaanhtrolen_colwillchangef').html(tongsocotcobaanhtrolen.toLocaleString('en'));

                        $("#loading").hide();
                    });
                }
            }
        }
    }

}
