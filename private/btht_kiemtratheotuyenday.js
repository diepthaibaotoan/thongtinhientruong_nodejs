GF_PHANMENU();
var url = GV_URL;
var jsonDonvi;
var dulieubaocao;
var macongty, madienluc, matramnguon;
$('#header_dangnhap').hide();
$('#header_dangxuat').hide();
$('#header_tentaikhoan').text('');
$("#congty").empty();
$("#congty").append(`<option value="0">-- Tất cả Công ty --</option>`);
$("#dienluc").empty();
$("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
$("#tramnguon").empty();
$("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);

$("#thang").empty();
$("#thang").append(`<option value="0">-- Tháng --</option>`);

$("#phong_ban").empty();
$("#phong_ban").append(`<option value="0">-- Tên Phòng Ban--</option>`);

$.getJSON('/assets/DM_DONVI.json', function (data) {
    jsonDonvi = data;
    $.each(data, function (i, f) {
        if (f.CAP_DONVI == '2')
            $("#congty").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });
});

$("#congty").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Công ty"
});

$("#dienluc").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Điện lực"
});
$("#tramnguon").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Mời bạn chọn Trạm Nguồn"
});

$("#thang").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Tháng"
});

for (var i = 1; i <= 12; i++) {
    $("#thang").append("<option value=" + i + ">" + "Tháng " + i + "</option>");
}

$("#phong_ban").select2({
    allowClear: true,
    width: "resolve",
    placeholder: "Chọn Tên Phòng Ban"
});

$("#phong_ban").append(`<option value="1">KD - Kinh Doanh</option>`);
$("#phong_ban").append(`<option value="2">KT - Kỹ Thuật</option>`);
$("#phong_ban").append(`<option value="3">AT - An Toàn</option>`);
$("#phong_ban").append(`<option value="4">KTGS - Kiểm Tra Giám Sát</option>`);


$('#congty').on('change', function () {
    macongty = this.value;
    madienluc = '0';
    matramnguon = '0';
    $("#dienluc").empty();
    $("#dienluc").append(`<option value="0">-- Tất cả Điện lực --</option>`);
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
    $.each(jsonDonvi, function (i, f) {
        if (f.MA_DVICTREN == macongty)
            $("#dienluc").append(`<option value="${f.MA_DVIQLY}">${f.MA_DVIQLY} - ${f.TEN_DVIQLY}</option>`);
    });

});

$('#dienluc').on('change', function () {
    madienluc = this.value;
    matramnguon = '0';
    $("#tramnguon").empty();
    $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
    $.get(`${url}/api/TRUNGTHE_getDMTramnguon_byMadviqly/${madienluc.trim()}`, function (data) {
        $("#tramnguon").empty();
        $("#tramnguon").append(`<option value="0">-- Tất cả Trạm nguồn --</option>`);
        $.each(data, function (i, f) {
            $("#tramnguon").append(`<option value="${f.MA_TRAMNGUON}">${f.MA_TRAMNGUON} - ${f.TEN_TRAMNGUON}</option>`);
        });
    });
});

$('#tramnguon').on('change', function () {
    matramnguon = this.value;
});

//chay mac dinh lan dau
//$('#colwillchangeh').html('Công Ty');
$("#loading").hide();


// Click nut xem bao cao
function clickXembaocao() {
    if (macongty == null || macongty == '0') {

    } else {
        if (madienluc == null || madienluc == '0') {

        } else {
            if (matramnguon == null || matramnguon == '0') {

            } else {
                $("#dataTable").dataTable().fnDestroy();
                $("#tbody").empty();
                $('#colwillchangeh').html('Trạm');
                $("#loading").show();
                $.get(`${url}/api/TRUNGTHE_getDMXuattuyen_byTramnguon/${madienluc}/${matramnguon}`, function (data) {
                    $.each(data, function (i, f) {
                        $("#tbody").append(
                            `<tr>
                            <td style='text-align:center'>${f.STT}</td>
                            <td>${f.MA_DVIQLY}</td>
                            <td style='text-align:right'>${f.MA_XUATTUYEN}</td>
                            <td style='text-align:right'>`+(20+getRandomInt(50))+`</td>
                            <td style='text-align:right'>`+(20+getRandomInt(50))+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right'>`+getRandomInt(10)+`</td>
                            <td style='text-align:right;color:blue'>https://gismobile.cpc.vn/Img/PC05CC/HINHANH_HIENTRUONG/PC05CC_CC23CA02_AB3ABC123_20180814_103622_276.jpeg</td>
                        </tr>`
                        );
                    });
                    // $('#dataTable').DataTable({
                    //     "columns": [
                    //         { "width": "5%" },
                    //         { "width": "35%" },
                    //         { "width": "5%" },
                    //         { "width": "5%" },
                    //         { "width": "5%" },
                    //         { "width": "5%" }
                    //     ]
                    // });
                    $("#loading").hide();
                });
            }
        }
    }
}


function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}