
const request = require("request");
exports.getIndex = function(req,res,next){
    request({
        url: 'http://localhost:3004/api/get_DS_cot/PC05CC/%20/HA_THE/CC23CA02',
        json: true
      }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            res.render('index',{
                title:'Trang chủ',
                data: body
            });
        }
     });
};

exports.getIndex = function(req,res,next){
    res.render('index',{
        title:'Trang Chủ',
    });
};

exports.lang_en = function(req, res, next){
    res.cookie('languages','en',{maxAge:900000,httOnly:true});
    res.redirect('back');
}

exports.lang_vi = function(req, res, next){
    res.cookie('languages','vi',{maxAge:900000,httOnly:true});
    res.redirect('back');
}