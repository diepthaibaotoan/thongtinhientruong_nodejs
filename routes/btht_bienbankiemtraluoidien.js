var express = require('express');
var router = express.Router();

// Require Controllers Module
var btht_bienbankiemtraluoidienController = require('../controllers/btht_bienbankiemtraluoidienController');
router.get('/btht_bienbankiemtraluoidien', btht_bienbankiemtraluoidienController.getBtht_bienbankiemtraluoidien);

module.exports = router;


