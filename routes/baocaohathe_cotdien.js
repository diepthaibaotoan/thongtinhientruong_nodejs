var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_cotdienController = require('../controllers/baocaohathe_cotdienController');
router.get('/baocaohathe_cotdien', baocaohathe_cotdienController.getBaocaohathe_cotdien);

module.exports = router;
