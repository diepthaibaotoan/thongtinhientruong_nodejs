var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_thieuthongtindodemController = require('../controllers/baocaohathe_khachhang_thieuthongtindodemController');
router.get('/baocaohathe_khachhang_thieuthongtindodem', baocaohathe_khachhang_thieuthongtindodemController.getBaocaohathe_khachhang_thieuthongtindodem);

module.exports = router;


