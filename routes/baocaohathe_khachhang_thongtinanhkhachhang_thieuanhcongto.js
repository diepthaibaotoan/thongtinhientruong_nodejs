var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_thongtinanhkhachhang_thieuanhcongtoController = require('../controllers/baocaohathe_khachhang_thongtinanhkhachhang_thieuanhcongtoController');
router.get('/baocaohathe_khachhang_thongtinanhkhachhang_thieuanhcongto', baocaohathe_khachhang_thongtinanhkhachhang_thieuanhcongtoController.getBaocaohathe_khachhang_thongtinanhkhachhang_thieuanhcongto);

module.exports = router;


