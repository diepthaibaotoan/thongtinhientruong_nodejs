﻿var url_ttkh = "https://ttht.cpc.vn:8080";
var ConfigConnect = require('../config/database');
var express = require('express');
var sleep = require('system-sleep');
var service = express.Router();
const sql = require('mssql');
const request = require("request");
var KEY_PASS = "TOANDTB";

function convert_khongchonull(chuoiguivao) {
    if (chuoiguivao == null) {
        return encodeURIComponent('');
    } else {
        return encodeURIComponent(chuoiguivao);
    }
}

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

// LUOI DIEN HA THE: De lam vi du
// service.get('/api/MTB_HATHE_getDSCot_byTramhathe/:MA_DVIQLY/:MA_TRAMTRUNGGIAN/:MA_XUATTUYEN/:MA_TRAM', function (req, res, next) {
//     const pool = new sql.ConnectionPool(ConfigConnect, err => {
//         if (err) {
//             console.log("Lỗi kết nối database : MTB_HATHE_getDSCot_byTramhathe" + err);
//             res.send(err);
//         }
//         else {
//             pool.request()
//                 .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
//                 .input('MA_TRAMTRUNGGIAN', sql.VarChar, req.param('MA_TRAMTRUNGGIAN'))
//                 .input('MA_XUATTUYEN', sql.VarChar, req.param('MA_XUATTUYEN'))
//                 .input('MA_TRAM', sql.VarChar, req.param('MA_TRAM'))
//                 .execute('MTB_HATHE_getDSCot_byTramhathe', (err, result) => {
//                     if (err) {
//                         res.send(err);
//                     } else {
//                         res.send(result.recordset);
//                     }
//                 })
//         }
//     })
// });

// LUOI DIEN HA THE + LUOI DIEN TRUNG THE
service.get('/api/HATHE_getDSHinhanh_bySocot/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM/:SO_COT', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSHinhanh_bySocot: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.NVarChar, req.params.SO_COT)
                    .execute('HATHE_getDSHinhanh_bySocot', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

service.get('/api/HATHE_getDSHinhanh_byKhang/:MA_DVIQLY/:MA_TRAM/:MA_KHANG', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_Hinhanh_byKhang: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('MA_KHANG', sql.NVarChar, req.params.MA_KHANG)
                    .execute('HATHE_getDSHinhanh_byKhang', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// BAO CAO LUOI DIEN TRUNG THE - TONG QUAN
service.get('/api/TRUNGTHE_getBCTongquan/:MA_DVIQLY/:MA_TRAMNGUON/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getBCTongquan: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('TRUNGTHE_getBCTongquan', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// BAO CAO LUOI DIEN HA THE - TONG QUAN
service.get('/api/HATHE_getBCTongquan/:MA_DVIQLY/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCTongquan: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCTongquan', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// BAO CAO LUOI DIEN HA THE - TONG QUAN
service.get('/api/HATHE_getBCTongquan_TheoNgay/:NGAY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCTongquan_TheoNgay: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('NGAY', sql.VarChar, req.params.NGAY)
                    .execute('HATHE_getBCTongquan_TheoNgay', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// BAO CAO LUOI DIEN HA THE - KHACH HANG
service.get('/api/HATHE_getBCKhachhang/:MA_DVIQLY/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// anh Tri Dung: THONG TIN KHACH HANG
// service.get('/api/mtb_delete_khachhang_by_matram/:MA_DVIQLY/:MA_TRAM/:pass', function (req, res, next) {
//     //
//     var url = `http://cpm.cpc.vn:8080/chamno_android.asmx/mtb_delete_khachhang_by_matram?MA_DVIQLY=${req.param('MA_DVIQLY')}&MA_TRAM=${req.param('MA_TRAM')}&pass=${req.param('pass')}`;
//     console.log("URL: " + url);
//     request(url, function (error, response, body) {
//         if (body != '') {
//             body = body.replace('</string>', '');
//             body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
//             body = body.replace('<string xmlns="http://tempuri.org/">', '');
//             body = body.trim();
//             res.send(body);
//         }
//         else { return ''; }
//     });
// });

// anh Tri Dung: THONG TIN KHACH HANG
service.get('/api/mtb_get_khachhang_by_ma_tram_full_for_gis/:MA_DVIQLY/:MA_TRAM/:pass', function (req, res, next) {
    try {
        var url = `http://cpm.cpc.vn:8080/chamno_android.asmx/mtb_get_khachhang_by_ma_tram_full_for_gis?MA_DVIQLY=${req.params.MA_DVIQLY}&MA_TRAM=${req.params.MA_TRAM}&pass=${req.params.pass}`;
        //console.log("URL: " + url);
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

// BAO CAO LUOI DIEN HA THE
service.get('/api/delete_DS_Thongtinkhachhang_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - delete_DS_Thongtinkhachhang_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('delete_DS_Thongtinkhachhang_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// LUOI DIEN HA THE
service.get('/api/update_DS_Hosodientu/:MA_DVIQLY/:MA_TRAM/:MA_KHANG/:TEN_HINH', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - update_DS_Hosodientu: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('MA_KHANG', sql.VarChar, req.params.MA_KHANG)
                    .input('TEN_HINH', sql.VarChar, req.params.TEN_HINH)
                    .execute('update_DS_Hosodientu', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// THONG TIN KHACH HANG
// service.post('/api/update_DS_Thongtinkhachhang_themThaydoi', function (req, res, next) {
//     var url = `http://gismobile.cpc.vn/Mobile_GIS/Service_GIS_ToanDTB.asmx/update_DS_Thongtinkhachhang_themThaydoi?` +
//         `MA_DVIQLY=${req.body.MA_DVIQLY}` +
//         `&MA_TRAM=${req.body.MA_TRAM}` +
//         `&MA_LO=${req.body.MA_LO}` +
//         `&MA_SOGCS=${req.body.MA_SOGCS}` +
//         `&STT=${req.body.STT}` +
//         `&MA_KHANG=${req.body.MA_KHANG}` +
//         `&MA_DDO=${req.body.MA_DDO}` +
//         `&TEN_KHANG=${req.body.TEN_KHANG}` +
//         `&DCHI_KHANG=${req.body.DCHI_KHANG}` +
//         `&DIEN_THOAI=${req.body.DIEN_THOAI}` +
//         `&EMAIL=${req.body.EMAIL}` +
//         `&DCHI_SDD=${req.body.DCHI_SDD}` +
//         `&X_SDD=${req.body.X_SDD}` +
//         `&Y_SDD=${req.body.Y_SDD}` +
//         `&Z_SDD=${req.body.Z_SDD}` +
//         `&SO_PHA=${req.body.SO_PHA}` +
//         `&PHA=${req.body.PHA}` +
//         `&SO_HO=${req.body.SO_HO}` +
//         `&SO_COT=${req.body.SO_COT}` +
//         `&SO_THUNG=${req.body.SO_THUNG}` +
//         `&VTRI_THUNG=${req.body.VTRI_THUNG}` +
//         `&LOAI_THUNG=${req.body.LOAI_THUNG}` +
//         `&VTRI_CTO=${req.body.VTRI_CTO}` +
//         `&VTRI_TREO=${req.body.VTRI_TREO}` +
//         `&GIA_DIEN=${req.body.GIA_DIEN}` +
//         `&MA_CLOAI_CTO=${req.body.MA_CLOAI_CTO}` +
//         `&NAM_SXUAT_CTO=${req.body.NAM_SXUAT_CTO}` +
//         `&SO_CTO=${req.body.SO_CTO}` +
//         `&NGAY_TREO_CTO=${req.body.NGAY_TREO_CTO}` +
//         `&NGAY_KDINH_CTO=${req.body.NGAY_KDINH_CTO}` +
//         `&HS_NHAN=${req.body.HS_NHAN}` +
//         `&MA_CLOAI_TI1=${req.body.MA_CLOAI_TI1}` +
//         `&NAM_SXUAT_TI1=${req.body.NAM_SXUAT_TI1}` +
//         `&SO_TI1=${req.body.SO_TI1}` +
//         `&NGAY_TREO_TI1=${req.body.NGAY_TREO_TI1}` +
//         `&NGAY_KDINH_TI1=${req.body.NGAY_KDINH_TI1}` +
//         `&MA_CLOAI_TI2=${req.body.MA_CLOAI_TI2}` +
//         `&NAM_SXUAT_TI2=${req.body.NAM_SXUAT_TI2}` +
//         `&SO_TI2=${req.body.SO_TI2}` +
//         `&NGAY_TREO_TI2=${req.body.NGAY_TREO_TI2}` +
//         `&NGAY_KDINH_TI2=${req.body.NGAY_KDINH_TI2}` +
//         `&MA_CLOAI_TI3=${req.body.MA_CLOAI_TI3}` +
//         `&NAM_SXUAT_TI3=${req.body.NAM_SXUAT_TI3}` +
//         `&SO_TI3=${req.body.SO_TI3}` +
//         `&NGAY_TREO_TI3=${req.body.NGAY_TREO_TI3}` +
//         `&NGAY_KDINH_TI3=${req.body.NGAY_KDINH_TI3}` +
//         `&GHI_CHU=${req.body.GHI_CHU}` +
//         `&NGUOI_NHAP=${req.body.NGUOI_NHAP}` +
//         `&THAY_DOI=${req.body.THAY_DOI}` +
//         `&KEY=${req.body.KEY}`;
//     console.log("URL: "+url);
//     request(url, function (error, response, body) {
//         if (body != '') {
//             body = body.replace('</string>', '');
//             body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
//             body = body.replace('<string xmlns="http://tempuri.org/">', '');
//             body = body.trim();
//             res.send(body);
//         }
//         else { return ''; }
//     });
// });

// THONG TIN KHACH HANG: ANH SO HOA ANH TRI DUNG
// service.get('/api/HATHE_setDSHosodientu_byTram/:MA_DVIQLY/:MA_TRAM/:KEY', function (req, res, next) {
//     var url = `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSHosodientu_byTram?MA_DVIQLY=${req.param('MA_DVIQLY')}&MA_TRAM=${req.param('MA_TRAM')}&KEY=${req.param('KEY')}`;
//     request(url, function (error, response, body) {
//         if (body != '') {
//             res.send('1');
//         }
//         else { return ''; }ddd
//     });
// });

// THONG TIN KHACH HANG: DANH MUC TRAM ANH TRI DUNG
// service.get('/api/HATHE_setDMTram_byMadviqly/:MA_DVIQLY/:KEY', function (req, res, next) {
//     var url = `http://gismobile.cpc.vn/Mobile_GIS/Service_GIS_ToanDTB.asmx/HATHE_setDMTram_byMadviqly?MA_DVIQLY=${req.param('MA_DVIQLY')}&KEY=${req.param('KEY')}`;
//     request(url, function (error, response, body) {
//         if (body != '') {
//             res.send('1');
//         }
//         else { return ''; }
//     });
// });

// THONG TIN KHACH HANG: ANH SO HOA ANH TRI DUNG
// service.get('/api/mtb_get_anh_sohoa_by_dienluc/:MA_DVIQLY/:MA_KHANG/:pass', function (req, res, next) {
//     var url = `http://cpm.cpc.vn:8080/chamno_android.asmx/mtb_get_anh_sohoa_by_dienluc?MA_DVIQLY=${req.param('MA_DVIQLY')}&MA_TRAM=${req.param('MA_TRAM')}&pass=${req.param('pass')}`;
//     request(url, function (error, response, body) {
//         if (body != '') {
//             body = body.replace('</string>', '');
//             body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
//             body = body.replace('<string xmlns="http://tempuri.org/">', '');
//             body = body.trim();
//             res.send(body);
//         }
//         else { return ''; }
//     });
// });

// THONG TIN KHACH HANG: DANH SACH KHACH HANG TRI DUNG
service.get('/api/HATHE_setDSKhachhang_byTram/:MA_DVIQLY/:MA_TRAM/:NGUOI_NHAP/:KEY', function (req, res, next) {
    try {
        var url = `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/HATHE_setDSKhachhang_byTram_Cu?MA_DVIQLY=${req.params.MA_DVIQLY}&MA_TRAM=${req.params.MA_TRAM}&NGUOI_NHAP=${req.params.NGUOI_NHAP}&KEY=${req.params.KEY}`;
        request(url, function (error, response, body) {
            if (body) {
                //console.log("AA"+ body);
                //console.log("BB"+ body);
                return res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

// service.get('/api/GetThietBiViTriJSON/:assetid', function (req, res, next) {
//     var url = `https://qlld.cpc.vn/omsws/pmis.asmx/GetThietBiViTriJSON?assetid=${req.param('assetid')}`;
//     request(url, function (error, response, body) {
//         if (body != '') {
//             res.send(body);
//         }
//         else { return ''; }
//     });
// });

// DANG NHAP
service.get('/api/get_DangnhapTTKH_adminDienluc/:TEN_DANGNHAP/:MAT_KHAU/:CHUC_NANG', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DangnhapTTKH_adminDienluc: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .input('MAT_KHAU', sql.VarChar, req.params.MAT_KHAU)
                    .input('CHUC_NANG', sql.VarChar, req.params.CHUC_NANG)
                    .execute('get_DangnhapTTKH_adminDienluc', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// THONG TIN KHACH HANG: ANH SO HOA
// service.get('/api/delete_DS_Hosodientu_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
//     const pool = new sql.ConnectionPool(ConfigConnect, err => {
//         if (err) {
//             console.log("Lỗi kết nối database :- " + err);
//             res.send(err);
//         }
//         else {
//             pool.request()
//                 .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
//                 .input('MA_TRAM', sql.VarChar, req.param('MA_TRAM'))
//                 .execute('delete_DS_Hosodientu_byTram', (err, result) => {
//                     if (err) {
//                         res.send(err);
//                     } else {
//                         res.send(result.recordset);
//                     }
//                 })
//         }
//     })
// });

// THONG TIN KHACH HANG: EMEC
service.get('/api/get_Dulieu_EMEC/:MA_DVIQLY/:KEY', function (req, res, next) {
    //console.log("URL: "+`http://spider.cpc.vn/serspider.asmx/get_ds_gis?madvi:=${req.param('MA_DVIQLY')}&identity=${req.param('KEY')}`);
    try {
        var url = `http://spider.cpc.vn/serspider.asmx/get_ds_gis?madvi=${req.params.MA_DVIQLY}&identity=${req.params.KEY}`;
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

// LUOI DIEN TRUNG THE: anh Vy
var i;
service.get('/api/GetVitriDZJSON/:orgid', function (req, res, next) {
    try {
        var url = `https://qlld.cpc.vn/omsws/pmis.asmx/GetVitriDZJSON?orgid=${req.params.orgid}`;
        request(url, function (error, response, body) {
            if (body) {
                //res.send(body);
                i = 0;
                JSON.parse(body).forEach(function (duongdayorvitriObj) {
                    //console.log("URL: " + duongdayorvitriObj.TYPEID);
                    i++;
                    if (i % 100 == 0) {
                        sleep(1200); // sleep for 10 seconds
                    }
                    if (duongdayorvitriObj.TYPEID == "DUONGDAY") {
                        var url = `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/mtb_update_DM_Duongday_forTenduongday?` +
                            `MA_DVIQLY=${duongdayorvitriObj.ORGID}` +
                            `&MA_TRAMTRUNGGIAN=` +
                            `&MA_XUATTUYEN=` +
                            `&MA_TRAM=` +
                            `&MA_DUONGDAY_CHA=${convert_khongchonull(duongdayorvitriObj.ASSETID_PARENT)}` +
                            `&MA_DUONGDAY=${convert_khongchonull(duongdayorvitriObj.ASSETID)}` +
                            `&TEN_DUONGDAY=${convert_khongchonull(duongdayorvitriObj.ASSETDESC)}` +
                            `&HIEUDIENTHE=${convert_khongchonull(duongdayorvitriObj.ULEVELID)}` +
                            `&NGUOI_NHAP=WEB` +
                            `&KEY=toandtb`;
                        request(url, function (error, response, body) {
                            if (body) {
                                //console.log("DAY DUONG DAY VAO SERVER: " + body);
                            }
                            else { return ''; }
                        });
                    } else if (duongdayorvitriObj.TYPEID == "VITRI") {
                        var url = `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/mtb_update_DM_Vitri_forTenvitri?` +
                            `MA_DVIQLY=${convert_khongchonull(duongdayorvitriObj.ORGID)}` +
                            `&MA_TRAMTRUNGGIAN=` +
                            `&MA_XUATTUYEN=` +
                            `&MA_TRAM=` +
                            `&MA_DUONGDAY=${convert_khongchonull(duongdayorvitriObj.ASSETID_PARENT)}` +
                            `&MA_VITRI=${convert_khongchonull(duongdayorvitriObj.ASSETID)}` +
                            `&TEN_VITRI=${convert_khongchonull(duongdayorvitriObj.ASSETDESC)}` +
                            `&HIEUDIENTHE=${convert_khongchonull(duongdayorvitriObj.ULEVELID)}` +
                            `&NGUOI_NHAP=WEB` +
                            `&KEY=toandtb`;
                        request(url, function (error, response, body) {
                            console.log('error:', error); // Print the error if one occurred
                            console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                            console.log('body:', body); // Print the HTML for the Google homepage.
                        });
                    }
                });
                res.send("1");
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
});

var i_laythietbichocot;
service.get('/api/mtb_get_DM_Vitri_byMaxuattuyen/:MA_DVIQLY/:MA_TRAMTRUNGGIAN/:MA_XUATTUYEN/:KEY', function (req, res, next) {
    try {
        var url = `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/mtb_get_DM_Vitri_byMaxuattuyen?` +
        `MA_DVIQLY=${req.params.MA_DVIQLY}` +
        `&MA_TRAMTRUNGGIAN=${req.params.MA_TRAMTRUNGGIAN}` +
        `&MA_XUATTUYEN=${req.params.MA_XUATTUYEN}` +
        `&KEY=toandtb`;
        //console.log("URL: " + url);
        request(url, function (error, response, body) {
            if (body) {
                i_laythietbichocot = 0;
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                JSON.parse(body).forEach(function (vitriObj) {
                    var url = `https://qlld.cpc.vn/omsws/pmis.asmx/GetThietBiViTriJSON?assetid=${convert_khongchonull(vitriObj.MA_VITRI)}`;
                    //console.log("URL: "+ url);
                    i_laythietbichocot++;
                    if (i_laythietbichocot % 50 == 0) {
                        sleep(1200);
                    }
                    request(url, function (error, response, body) {
                        if (body) {
                            JSON.parse(body).forEach(function (thietbiObj) {
                                sleep(100);
                                var ma_loaithietbi = '';
                                if (thietbiObj.ASSETID.includes("_DDAN_")) {
                                    ma_loaithietbi = 'DAYDAN'
                                } else if (thietbiObj.ASSETID.includes("_CD_")) {
                                    ma_loaithietbi = 'COT'
                                } else if (thietbiObj.ASSETID.includes("_MCOT_")) {
                                    ma_loaithietbi = 'MONGCOT'
                                } else if (thietbiObj.ASSETID.includes("_SU_")) {
                                    ma_loaithietbi = 'SU'
                                } else if (thietbiObj.ASSETID.includes("_XA_")) {
                                    ma_loaithietbi = 'XA'
                                } else if (thietbiObj.ASSETID.includes("_CAPN_")) {
                                    ma_loaithietbi = 'CAPNGAM'
                                } else if (thietbiObj.ASSETID.includes("_DCS_")) {
                                    ma_loaithietbi = 'DAYCHONGSET'
                                } else {
                                    ma_loaithietbi = 'KHAC';
                                }
                                var url = `${url_ttkh}/api/mtb_update_DS_Thietbicuacot_forTenthietbi` +
                                    `/${convert_khongchonull(vitriObj.MA_DVIQLY)}` +
                                    `/${convert_khongchonull(vitriObj.MA_TRAMTRUNGGIAN)}` +
                                    `/${convert_khongchonull(vitriObj.MA_XUATTUYEN)}` +
                                    `/%20` +
                                    `/%20` +
                                    `/${convert_khongchonull(thietbiObj.ASSETID_PARENT)}` +
                                    `/%20` +
                                    `/${convert_khongchonull(ma_loaithietbi)}` +
                                    `/${convert_khongchonull(thietbiObj.ASSETID)}` +
                                    `/${convert_khongchonull(thietbiObj.ASSETDESC + ` - ` + thietbiObj.CATEGORYDESC)}` +
                                    `/WEB` +
                                    `/PMIS`;
                                request(url, function (error, response, body) {
                                    if (body) {
                                        console.log('error:', error); // Print the error if one occurred
                                        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
                                        console.log('body:', body); // Print the HTML for the Google homepage.
                                    }
                                    else { return ''; }
                                });
                            });
                        }
                        else { return ''; }
                    })
                });
                res.send("1");
            }
            else { return ''; }
        });
    } catch(err) {
        return '';
    }
    
});

// LUOI DIEN TRUNG THE
// service.post('/api/mtb_update_DM_Duongday_forTenduongday', function (req, res, next) {
//     // console.log(`http://gismobile.cpc.vn/Mobile_GIS/Service_GIS_ToanDTB.asmx/mtb_update_DM_Duongday_forTenduongday?` +
//     // `MA_DVIQLY=${req.body.MA_DVIQLY}` +
//     // `&MA_TRAMTRUNGGIAN=${req.body.MA_TRAMTRUNGGIAN}` +
//     // `&MA_XUATTUYEN=${req.body.MA_XUATTUYEN}` +
//     // `&MA_TRAM=${req.body.MA_TRAM}` +
//     // `&MA_DUONGDAY_CHA=${req.body.MA_DUONGDAY_CHA}` +
//     // `&MA_DUONGDAY=${req.body.MA_DUONGDAY}` +
//     // `&TEN_DUONGDAY=${req.body.TEN_DUONGDAY}` +
//     // `&HIEUDIENTHE=${req.body.HIEUDIENTHE}` +
//     // `&NGUOI_NHAP=${req.body.NGUOI_NHAP}` +
//     // `&KEY=${req.body.KEY}`);
//     var url = `http://gismobile.cpc.vn/Mobile_GIS/Service_GIS_ToanDTB.asmx/mtb_update_DM_Duongday_forTenduongday?` +
//         `MA_DVIQLY=${req.body.MA_DVIQLY}` +
//         `&MA_TRAMTRUNGGIAN=${req.body.MA_TRAMTRUNGGIAN}` +
//         `&MA_XUATTUYEN=${req.body.MA_XUATTUYEN}` +
//         `&MA_TRAM=${req.body.MA_TRAM}` +
//         `&MA_DUONGDAY_CHA=${req.body.MA_DUONGDAY_CHA}` +
//         `&MA_DUONGDAY=${req.body.MA_DUONGDAY}` +
//         `&TEN_DUONGDAY=${req.body.TEN_DUONGDAY}` +
//         `&HIEUDIENTHE=${req.body.HIEUDIENTHE}` +
//         `&NGUOI_NHAP=${req.body.NGUOI_NHAP}` +
//         `&KEY=${req.body.KEY}`;
//     request(url, function (error, response, body) {
//         if (body != '') {
//             res.send(body);
//         }
//         else { return ''; }
//     });
// });

// service.post('/api/mtb_update_DM_Vitri_forTenvitri', function (req, res, next) {
//     console.log(`http://gismobile.cpc.vn/Mobile_GIS/Service_GIS_ToanDTB.asmx/mtb_update_DM_Vitri_forTenvitri?` +
//         `MA_DVIQLY=${req.body.MA_DVIQLY}` +
//         `&MA_TRAMTRUNGGIAN=${req.body.MA_TRAMTRUNGGIAN}` +
//         `&MA_XUATTUYEN=${req.body.MA_XUATTUYEN}` +
//         `&MA_TRAM=${req.body.MA_TRAM}` +
//         `&MA_DUONGDAY=${req.body.MA_DUONGDAY}` +
//         `&MA_VITRI=${req.body.MA_VITRI}` +
//         `&TEN_VITRI=${req.body.TEN_VITRI}` +
//         `&HIEUDIENTHE=${req.body.HIEUDIENTHE}` +
//         `&NGUOI_NHAP=${req.body.NGUOI_NHAP}` +
//         `&KEY=${req.body.KEY}`);
//     var url = `http://gismobile.cpc.vn/Mobile_GIS/Service_GIS_ToanDTB.asmx/mtb_update_DM_Vitri_forTenvitri?` +
//         `MA_DVIQLY=${req.body.MA_DVIQLY}` +
//         `&MA_TRAMTRUNGGIAN=${req.body.MA_TRAMTRUNGGIAN}` +
//         `&MA_XUATTUYEN=${req.body.MA_XUATTUYEN}` +
//         `&MA_TRAM=${req.body.MA_TRAM}` +
//         `&MA_DUONGDAY=${req.body.MA_DUONGDAY}` +
//         `&MA_VITRI=${req.body.MA_VITRI}` +
//         `&TEN_VITRI=${req.body.TEN_VITRI}` +
//         `&HIEUDIENTHE=${req.body.HIEUDIENTHE}` +
//         `&NGUOI_NHAP=${req.body.NGUOI_NHAP}` +
//         `&KEY=${req.body.KEY}`;
//     request(url, function (error, response, body) {
//         if (body != '') {
//             res.send(body);
//         }
//         else { return ''; }
//     });
// });

// LUOI DIEN TRUNG THE: anh Vy
// service.get('/api/GetThietBiViTriJSON/:assetid', function (req, res, next) {
//     var url = `https://qlld.cpc.vn/omsws/pmis.asmx/GetThietBiViTriJSON?assetid=${req.param('assetid')}`;
//     request(url, function (error, response, body) {
//         if (body != '') {
//             res.send(body);
//         }
//         else { return ''; }
//     });
// });

// LUOI DIEN TRUNG THE
//MA_DVIQLY=PC05CC&MA_TRAMTRUNGGIAN=&MA_XUATTUYEN=&MA_TRAM=&SO_COT=&MA_VITRI=PC05CC_DZ_VT_0019678&MA_KHANG=&MA_LOAITHIETBI=PC05CC_DZ_XA_19678&MA_THIETBI=PC05CC_DZ_XA_19678&TEN_THIETBI=X%C4%90A%20-%20X%C3%A0%2022kV&NGUOI_NHAP=WEB&GHI_CHU=PMIS
service.get('/api/mtb_update_DS_Thietbicuacot_forTenthietbi/:MA_DVIQLY/:MA_TRAMTRUNGGIAN/:MA_XUATTUYEN' +
    '/:MA_TRAM/:SO_COT/:MA_VITRI/:MA_KHANG/:MA_LOAITHIETBI/:MA_THIETBI/:TEN_THIETBI' +
    '/:NGUOI_NHAP/:GHI_CHU', function (req, res, next) {
        try {
            const pool = new sql.ConnectionPool(ConfigConnect, err => {
                if (err) {
                    //console.log("Lỗi kết nối database - mtb_update_DS_Thietbicuacot_forTenthietbi: " + err);
                    res.send(err);
                }
                else {
                    pool.request()
                        .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                        .input('MA_TRAMTRUNGGIAN', sql.VarChar, req.params.MA_TRAMTRUNGGIAN)
                        .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                        .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                        .input('SO_COT', sql.VarChar, req.params.SO_COT)
                        .input('MA_VITRI', sql.VarChar, req.params.MA_VITRI)
                        .input('MA_KHANG', sql.VarChar, req.params.MA_KHANG)
                        .input('MA_LOAITHIETBI', sql.VarChar, req.params.MA_LOAITHIETBI)
                        .input('MA_THIETBI', sql.VarChar, req.params.MA_THIETBI)
                        .input('TEN_THIETBI', sql.NVarChar, req.params.TEN_THIETBI)
                        .input('NGUOI_NHAP', sql.VarChar, req.params.NGUOI_NHAP)
                        .input('GHI_CHU', sql.NVarChar, req.params.GHI_CHU)
                        .execute('mtb_update_DS_Thietbicuacot_forTenthietbi', (err, result) => {
                            pool.close();
                            if (err) {
                                return res.send(err);
                            } else {
                                return res.send(result.recordset);
                            }
                        })
                }
            })
        } catch (err) {
            return res.send(err);
        }
        
    });

// DANG NHAP AD
service.post('/api/get_DangnhapAD', function (req, res, next) {
    // console.log(`https://auit.cpc.vn/DangNhapAD.asmx/CheckLoginAD?` +
    //     `userAD=${req.body.TEN_DANGNHAP}` +
    //     `&passwordAD=${req.body.MAT_KHAU}` +
    //     `&password=${req.body.KEY}`);
    try {
        var url = `https://auit.cpc.vn/DangNhapAD.asmx/CheckLoginAD?` +
        `userAD=${req.body.TEN_DANGNHAP}` +
        `&passwordAD=${convert_khongchonull(req.body.MAT_KHAU)}` +
        `&password=${req.body.KEY}`;
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('<WS_ServiceResponse xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://tempuri.org/">', '');
                body = body.replace('</WS_ServiceResponse>', '');
                body = body.replace('<data />', '');
                body = body.replace('<result>', '');
                body = body.replace('</result>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

// DANG NHAP AD
service.get('/api/update_DS_Nguoidung/:MA_DVIQLY/:TEN_DANGNHAP', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - update_DS_Nguoidung: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .execute('update_DS_Nguoidung', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// DANG NHAP AD ONLINE
service.get('/api/update_DS_Nguoidungonline/:MA_DVIQLY/:TEN_DANGNHAP/:THIET_BI/:TRANG_THAI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - update_DS_Nguoidungonline: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .input('THIET_BI', sql.VarChar, req.params.THIET_BI)
                    .input('TRANG_THAI', sql.VarChar, req.params.TRANG_THAI)
                    .execute('update_DS_Nguoidungonline', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

service.get('/api/update_DS_Nguoidungonline_full/:MA_DVIQLY/:TEN_DANGNHAP/:THIET_BI/:TRANG_THAI/:LAT/:LONG', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - update_DS_Nguoidungonline_full: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .input('THIET_BI', sql.VarChar, req.params.THIET_BI)
                    .input('TRANG_THAI', sql.VarChar, req.params.TRANG_THAI)
                    .input('LAT', sql.Float, req.params.LAT)
                    .input('LONG', sql.Float, req.params.LONG)
                    .execute('update_DS_Nguoidungonline_full', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// DANG NHAP AD ONLINE
service.get('/api/get_DS_Nguoidungonline_byTrangthai/:TRANG_THAI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_Nguoidungonline_byTrangthai: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('TRANG_THAI', sql.VarChar, req.params.TRANG_THAI)
                    .execute('get_DS_Nguoidungonline_byTrangthai', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// BAO CAO SU CO
service.get('/api/mtb_get_DS_Suco_byMadviqly/:MA_DVIQLY/:KEY', function (req, res, next) {
    try {
        var url = `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/mtb_get_DS_Suco_byMadviqly` +
        `?MA_DVIQLY=${req.params.MA_DVIQLY}&KEY=${req.params.KEY}`;
        //console.log("URL: " + url);
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

//  SO HOA HOP DONG
service.get('/api/get_DS_Hosodientu_byMakhang/:MA_DVIQLY/:MA_TRAM/:MA_KHANG', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_Hosodientu_byMakhang: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('MA_KHANG', sql.VarChar, req.params.MA_KHANG)
                    .execute('get_DS_Hosodientu_byMakhang', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

//  CHAT
service.get('/api/set_DS_Tinnhan/:TEN_DANGNHAP/:NOI_DUNG', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - set_DS_Tinnhan: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .input('NOI_DUNG', sql.NVarChar, req.params.NOI_DUNG)
                    .execute('set_DS_Tinnhan', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

//  CHAT
service.get('/api/get_DS_Tinnhan', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_Tinnhan: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .execute('get_DS_Tinnhan', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// NGUOI DUNG
service.get('/api/get_MadviqlycuaNguoidung/:TEN_DANGNHAP', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_MadviqlycuaNguoidung: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .execute('get_MadviqlycuaNguoidung', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// AAAAA: PMIS - LUOI DIEN TRUNG THE
// 1
service.get('/api/TRUNGTHE_getDMTramnguon_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDMTramnguon_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TRUNGTHE_getDMTramnguon_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// 2
service.get('/api/TRUNGTHE_getDMXuattuyen_byTramnguon/:MA_DVIQLY/:MA_TRAMNGUON', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDMXuattuyen_byTramnguon: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .execute('TRUNGTHE_getDMXuattuyen_byTramnguon', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// 3
service.get('/api/TRUNGTHE_getDSCottrungthe_byXuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSCottrungthe_byXuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getDSCottrungthe_byXuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 4
service.get('/api/TRUNGTHE_getDSHinhanh_byXuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSHinhanh_byXuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getDSHinhanh_byXuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 5
service.get('/api/TRUNGTHE_getDSHinhanh_bySocot/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM/:SO_COT', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSHinhanh_bySocot: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.NVarChar, req.params.SO_COT)
                    .execute('TRUNGTHE_getDSHinhanh_bySocot', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// BBBBB: CMIS - LUOI DIEN HA THE
// 1
service.get('/api/HATHE_getDMTram_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDMTram_byMadviqly: " + err);
                return res.send(err);
            } else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('HATHE_getDMTram_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// 2
service.get('/api/HATHE_getDSCot_byTramhathe/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSCot_byTramhathe: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getDSCot_byTramhathe', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// 3
service.get('/api/HATHE_getDSCot_byLkcot2/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSCot_byLkcot2: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('HATHE_getDSCot_byLkcot2', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 4
service.get('/api/HATHE_getBCCot/:MA_DVIQLY/:CAPDONVI/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCCot: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('CAPDONVI', sql.VarChar, req.params.CAPDONVI)
                    .execute('HATHE_getBCCot', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 4
service.get('/api/HATHE_getBCCot_ChitietHinhanh/:MA_DVIQLY/:MA_TRAM/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCCot_ChitietHinhanh: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getBCCot_ChitietHinhanh', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 5
service.get('/api/HATHE_updateDSCot_byTram/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM/:SO_COT/:X_COT/:Y_COT/:Z_COT/:LK_COT1/:LK_COT2/:LK_COT3/:LK_COT4/:MA_DUONGDAY/:MA_VITRI/:GHI_CHU/:NGUOI_NHAP/:LOAI_COT/:TINH_CHAT/:MA_HIEU/:CHIEU_CAO/:NHA_SANXUAT/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                //console.log("Lỗi kết nối database - HATHE_updateDSCot_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .input('X_COT', sql.VarChar, req.params.X_COT)
                    .input('Y_COT', sql.VarChar, req.params.Y_COT)
                    .input('Z_COT', sql.VarChar, req.params.Z_COT)
                    .input('LK_COT1', sql.VarChar, req.params.LK_COT1)
                    .input('LK_COT2', sql.VarChar, req.params.LK_COT2)
                    .input('LK_COT3', sql.VarChar, req.params.LK_COT3)
                    .input('LK_COT4', sql.VarChar, req.params.LK_COT4)
                    .input('MA_DUONGDAY', sql.VarChar, req.params.MA_DUONGDAY)
                    .input('MA_VITRI', sql.VarChar, req.params.MA_VITRI)
                    .input('GHI_CHU', sql.VarChar, req.params.GHI_CHU)
                    .input('NGUOI_NHAP', sql.VarChar, req.params.NGUOI_NHAP)
                    .input('LOAI_COT', sql.VarChar, req.params.LOAI_COT)
                    .input('TINH_CHAT', sql.VarChar, req.params.TINH_CHAT)
                    .input('MA_HIEU', sql.VarChar, req.params.MA_HIEU)
                    .input('CHIEU_CAO', sql.VarChar, req.params.CHIEU_CAO)
                    .input('NHA_SANXUAT', sql.VarChar, req.params.NHA_SANXUAT)
                    .execute('HATHE_updateDSCot_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// 6
service.get('/api/HATHE_updateDSHinhAnh_byTram/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM/:SO_COT/:MA_KHANG/:LOAI_HINHANH/:URL_THUMUC/:TEN_HINHANH/:NGAY_CHUP/:NGUOI_NHAP/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_updateDSHinhAnh_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .input('MA_KHANG', sql.VarChar, req.params.MA_KHANG)
                    .input('LOAI_HINHANH', sql.VarChar, req.params.LOAI_HINHANH)
                    .input('URL_THUMUC', sql.VarChar, req.params.URL_THUMUC)
                    .input('TEN_HINHANH', sql.VarChar, req.params.TEN_HINHANH)
                    .input('NGAY_CHUP', sql.VarChar, req.params.NGAY_CHUP)
                    .input('NGUOI_NHAP', sql.VarChar, req.params.NGUOI_NHAP)
                    .execute('HATHE_updateDSHinhAnh_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 6.1
service.get('/api/HATHE_updateDSHinhAnh/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM/:SO_COT/:MA_KHANG/:LOAI_HINHANH/:URL_THUMUC/:TEN_HINHANH/:NGAY_CHUP/:NGUOI_NHAP/:UNGDUNG_SUDUNG/:GHI_CHU/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_updateDSHinhAnh: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .input('MA_KHANG', sql.VarChar, req.params.MA_KHANG)
                    .input('LOAI_HINHANH', sql.VarChar, req.params.LOAI_HINHANH)
                    .input('URL_THUMUC', sql.VarChar, req.params.URL_THUMUC)
                    .input('TEN_HINHANH', sql.VarChar, req.params.TEN_HINHANH)
                    .input('NGAY_CHUP', sql.VarChar, req.params.NGAY_CHUP)
                    .input('NGUOI_NHAP', sql.VarChar, req.params.NGUOI_NHAP)
                    .input('UNGDUNG_SUDUNG', sql.VarChar, req.params.UNGDUNG_SUDUNG)
                    .input('GHI_CHU', sql.VarChar, req.params.GHI_CHU)
                    .execute('HATHE_updateDSHinhAnh', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 7
service.get('/api/HATHE_getBCChitietKhachhangsai/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCChitietKhachhangsai: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.VarChar, req.params.CAPDONVI)
                    .execute('HATHE_getBCChitietKhachhangsai', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});


// 8
service.get('/api/HATHE_getDSCot_byMadviqlyXY/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:N_X/:N_Y/:S_X/:S_Y', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSCot_byMadviqlyXY: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('N_X', sql.Float, req.params.N_X)
                    .input('N_Y', sql.Float, req.params.N_Y)
                    .input('S_X', sql.Float, req.params.S_X)
                    .input('S_Y', sql.Float, req.params.S_Y)
                    .execute('HATHE_getDSCot_byMadviqlyXY', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 8.1
service.get('/api/HATHE_getSLCot_byMadviqly/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getSLCot_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('HATHE_getSLCot_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 8.2
service.get('/api/HATHE_getDS1Cot_byMadviqly/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDS1Cot_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('HATHE_getDS1Cot_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 9
service.get('/api/HATHE_getDSThongtinkhachhang_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSThongtinkhachhang_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('HATHE_getDSThongtinkhachhang_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 9.1
service.get('/api/HATHE_getSLThongtinkhachhang_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getSLThongtinkhachhang_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('HATHE_getSLThongtinkhachhang_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 10
service.get('/api/HATHE_getDSThongtinkhachhang_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSThongtinkhachhang_byTram:" + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getDSThongtinkhachhang_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 11
service.get('/api/HATHE_getDSThongtinkhachhang_bySocot/:MA_DVIQLY/:MA_TRAM/:SO_COT', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSThongtinkhachhang_bySocot:" + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .execute('HATHE_getDSThongtinkhachhang_bySocot', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 12
service.get('/api/HATHE_getSLHinhanhchoCot_byMadviqly/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getSLHinhanhchoCot_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('HATHE_getSLHinhanhchoCot_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 13
service.get('/api/HATHE_getDSHinhanhchocot_byTram/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSHinhanhchocot_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getDSHinhanhchocot_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 13
service.get('/api/HATHE_getDSHinhanhchocot_byMadviqly/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSHinhanhchocot_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('HATHE_getDSHinhanhchocot_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 14 THUAN: Lấy danh sách thông tin người dùng thông qua số điện thoại
service.get('/api/get_DS_ThongTinNguoiDung_bySDT/:sodienthoai/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_ThongTinNguoiDung_bySDT: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('sodienthoai', sql.NVarChar, req.params.sodienthoai)
                    .execute('get_DS_ThongTinNguoiDung_bySDT', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 15 THUAN: Lấy danh sách tất cả thông tin người dùng 
service.get('/api/get_DS_ThongTinNguoiDung_KhongThamSo/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_ThongTinNguoiDung_KhongThamSo: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .execute('get_DS_ThongTinNguoiDung_KhongThamSo', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 16 THUAN: Update danh sách tenkhongdau của thông tin người dùng 
service.get('/api/update_DS_ThongTinNguoiDung_tenkhongdau/:tenkhongdau/:tenkhaisinh/:NS_ID', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - update_DS_ThongTinNguoiDung_tenkhongdau: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('tenkhongdau', sql.NVarChar, req.params.tenkhongdau)
                    .input('tenkhaisinh', sql.NVarChar, req.params.tenkhaisinh)
                    .input('NS_ID', sql.VarChar, req.params.NS_ID)
                    .execute('update_DS_ThongTinNguoiDung_tenkhongdau', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 17 THUAN: Lấy danh sách thông tin người dùng thông qua tên
service.get('/api/get_DS_ThongTinNguoiDung_bytenkhongdau/:tenkhongdau/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_ThongTinNguoiDung_bytenkhongdau: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('tenkhongdau', sql.NVarChar, req.params.tenkhongdau)
                    .execute('get_DS_ThongTinNguoiDung_bytenkhongdau', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 18 THUAN: Lấy danh sách thông tin người dùng thông qua Username
service.get('/api/get_DS_ThongTinNguoiDung_byUserName/:UserName/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_ThongTinNguoiDung_byUserName: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('UserName', sql.VarChar, req.params.UserName)
                    .execute('get_DS_ThongTinNguoiDung_byUserName', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 19 THUAN: 
service.get('/api/HATHE_getDMToadocongty_byDviqly/:ma_dviqly/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDMToadocongty_byDviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('ma_dviqly', sql.VarChar, req.params.ma_dviqly)
                    .execute('HATHE_getDMToadocongty_byDviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 20 THUAN: 
service.get('/api/HATHE_getDSThongtinkhachhang_byTimkiem/:MA_DVIQLY/:MA_TRAM/:CAPDONVI/:THAMSO/', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSThongtinkhachhang_byTimkiem: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .input('THAMSO', sql.VarChar, req.params.THAMSO)
                    .execute('HATHE_getDSThongtinkhachhang_byTimkiem', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 21
service.get('/api/HATHE_deleteDSCot_byTram/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_deleteDSCot_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_deleteDSCot_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});


// 21.1
service.get('/api/HATHE_deleteDSHinhanh_byTenhinhanh/:MA_DVIQLY/:MA_TRAM/:TEN_HINHANH', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_deleteDSHinhanh_byTenhinhanh: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('TEN_HINHANH', sql.VarChar, req.params.TEN_HINHANH)
                    .execute('HATHE_deleteDSHinhanh_byTenhinhanh', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 22
service.get('/api/HATHE_updateordeleteDSHinhAnh_byTenhinhanh/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM/:SO_COT/:TEN_HINHANH/:KEY_THAYDOI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_updateordeleteDSHinhAnh_byTenhinhanh: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .input('TEN_HINHANH', sql.VarChar, req.params.TEN_HINHANH)
                    .input('KEY_THAYDOI', sql.VarChar, req.params.KEY_THAYDOI)
                    .execute('HATHE_updateordeleteDSHinhAnh_byTenhinhanh', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 23: Hung
service.get('/api/get_DSNguoidung_theoTCT', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DSNguoidung_theoTCT: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .execute('get_DSNguoidung_theoTCT', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 24: Hung
service.get('/api/update_DS_Nguoidung_byChucNang/:TEN_DANGNHAP/:CHUCNANG', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - update_DS_Nguoidung_byChucNang: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .input('CHUCNANG', sql.VarChar, req.params.CHUCNANG)
                    .execute('update_DS_Nguoidung_byChucNang', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 25: Thuan
service.get('/api/HATHE_getTentram_ByMatram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getTentram_ByMatram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getTentram_ByMatram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 26:Toan
// anh Tri Dung: LUOI DIEN HA THE + THONG TIN KHACH HANG + BAO CAO HA THE --> De Lam vi du
service.get('/api/mtb_get_d_tram_by_donvi/:MA_DVIQLY/:pass', function (req, res, next) {
    try {
        var url = `http://cpm.cpc.vn:8080/chamno_android.asmx/mtb_get_d_tram_by_donvi?MA_DVIQLY=${req.params.MA_DVIQLY}&pass=${req.params.pass}`;
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return ''; 
    }
    
});

// 27
service.get('/api/TRUNGTHE_deleteDSCot_byXuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                //console.log("Lỗi kết nối database - TRUNGTHE_deleteDSCot_byXuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_deleteDSCot_byXuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 28
service.get('/api/TRUNGTHE_getDSThietbicuacot_byMavitri/:MA_DVIQLY/:MA_VITRI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSThietbicuacot_byMavitri: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_VITRI', sql.VarChar, req.params.MA_VITRI)
                    .execute('TRUNGTHE_getDSThietbicuacot_byMavitri', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
   
});

// 31
service.get('/api/TRUNGTHE_getDMXuattuyen_byDviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDMXuattuyen_byDviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TRUNGTHE_getDMXuattuyen_byDviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 34
service.get('/api/TRUNGTHE_getDSCot_byXuattuyenXY/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:N_X/:N_Y/:S_X/:S_Y', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSCot_byXuattuyenXY: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('N_X', sql.Float, req.params.N_X)
                    .input('N_Y', sql.Float, req.params.N_Y)
                    .input('S_X', sql.Float, req.params.S_X)
                    .input('S_Y', sql.Float, req.params.S_Y)
                    .execute('TRUNGTHE_getDSCot_byXuattuyenXY', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 35.1
service.get('/api/TRUNGTHE_getDS1Cot_byMaxuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDS1Cot_byMaxuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getDS1Cot_byMaxuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 35.2
service.get('/api/TRUNGTHE_getDS1Cot_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDS1Cot_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TRUNGTHE_getDS1Cot_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// // 36
// service.get('/api/PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap/:MA_DVIQLY/:TEN_DANGNHAP/:CHUC_NANG', function (req, res, next) {
//     const pool = new sql.ConnectionPool(ConfigConnect, err => {
//         if (err) {
//             console.log("Lỗi kết nối database - PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap: " + err);
//             res.send(err);
//         }
//         else {
//             pool.request()
//                 .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
//                 .input('TEN_DANGNHAP', sql.VarChar, req.param('TEN_DANGNHAP'))
//                 .input('CHUC_NANG', sql.NVarChar, req.param('CHUC_NANG'))
//                 .execute('PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap', (err, result) => {
//                     if (err) {
//                         res.send(err);
//                     } else {
//                         res.send(result.recordset);
//                     }
//                 })
//         }
//     })
// });

// 36.1
service.get('/api/PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full/:MA_DVIQLY/:TEN_DANGNHAP/:CHUC_NANG/:NGUOI_NHAP', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .input('CHUC_NANG', sql.NVarChar, req.params.CHUC_NANG)
                    .input('NGUOI_NHAP', sql.NVarChar, req.params.NGUOI_NHAP)
                    .execute('PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 37
service.get('/api/TRUNGTHE_getDSCot_byLkcot2/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSCot_byLkcot2: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getDSCot_byLkcot2', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});


// 38.1
service.get('/api/TRUNGTHE_getSLCot_byMaxuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getSLCot_byMaxuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getSLCot_byMaxuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 38.2
service.get('/api/TRUNGTHE_getSLCot_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getSLCot_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TRUNGTHE_getSLCot_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 39
service.get('/api/TRUNGTHE_getDSThietbicuacotPMIS_byXuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSThietbicuacotPMIS_byXuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getDSThietbicuacotPMIS_byXuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 40
service.get('/api/get_DS_ThongTinNguoiDung_byTendangnhap/:TEN_DANGNHAP', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_ThongTinNguoiDung_byTendangnhap: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .execute('get_DS_ThongTinNguoiDung_byTendangnhap', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 41
service.get('/api/TRUNGTHE_getSLHinhanh_byXuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getSLHinhanh_byXuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getSLHinhanh_byXuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 42
service.get('/api/TRUNGTHE_getSLHinhanh_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getSLHinhanh_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TRUNGTHE_getSLHinhanh_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 42
service.get('/api/TRUNGTHE_getDSHinhanh_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSHinhanh_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TRUNGTHE_getDSHinhanh_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 43
service.get('/api/HATHE_getBCKhachhang_RaSoatThongTin/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_RaSoatThongTin: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_RaSoatThongTin', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 44
service.get('/api/HATHE_getBCKhachhang_TramHoanThanh/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_TramHoanThanh: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_TramHoanThanh', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 45
service.get('/api/HATHE_getBCKhachhang_ThongTinAnhKhachHang/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_ThongTinAnhKhachHang: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_ThongTinAnhKhachHang', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 45--1.1
service.get('/api/HATHE_getBCKhachhang_ThieuAnhKhachHang/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_ThieuAnhKhachHang: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_ThieuAnhKhachHang', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 45--1.2
service.get('/api/HATHE_getBCKhachhang_ThieuAnhCongTo/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_ThieuAnhCongTo: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_ThieuAnhCongTo', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 45-1A
service.get('/api/HATHE_getBCKhachhang_ThongTinAnhKhachHang_TheoNgay/:NGAY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_ThongTinAnhKhachHang_TheoNgay: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('NGAY', sql.VarChar, req.params.NGAY)
                    .execute('HATHE_getBCKhachhang_ThongTinAnhKhachHang_TheoNgay', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 45-2A
service.get('/api/HATHE_getBCKhachhang_RaSoatThongTin_TheoNgay/:NGAY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_RaSoatThongTin_TheoNgay: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('NGAY', sql.VarChar, req.params.NGAY)
                    .execute('HATHE_getBCKhachhang_RaSoatThongTin_TheoNgay', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    } 
})

// 45-2A
service.get('/api/HATHE_getBCKhachhang_TramHoanThanh_TheoNgay/:NGAY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_TramHoanThanh_TheoNgay: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('NGAY', sql.VarChar, req.params.NGAY)
                    .execute('HATHE_getBCKhachhang_TramHoanThanh_TheoNgay', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 45-3A
service.get('/api/HATHE_getBCCot_TheoNgay/:NGAY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCCot_TheoNgay: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('NGAY', sql.VarChar, req.params.NGAY)
                    .execute('HATHE_getBCCot_TheoNgay', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 45-4A
service.get('/api/HATHE_getBCKhachhang_TheoNgay/:NGAY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_TheoNgay: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('NGAY', sql.VarChar, req.params.NGAY)
                    .execute('HATHE_getBCKhachhang_TheoNgay', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 46 BTHT
service.get('/api/BTHT_getVitringuyhiem/:MA_DVIQLY', function (req, res, next) {
    try {
        var url = `https://ktat.cpc.vn/wsKTHT/WebService.asmx/getds_ToaDo_dvi?dvi_cphieu=${req.params.MA_DVIQLY}`;
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

// 47 BTHT
service.get('/api/BTHT_getAnhViTriNguyHiem/:ID', function (req, res, next) {
    try {
        var url = `https://ktat.cpc.vn/wsKTHT/WebService.asmx/getds_anh_dvi?idvtnh=${req.params.ID}`;
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

//
service.get('/api/mtb_get_d_tram_by_donvi/:MA_DVIQLY/:pass', function (req, res, next) {
    try {
        var url = `http://cpm.cpc.vn:8080/chamno_android.asmx/mtb_get_d_tram_by_donvi?MA_DVIQLY=${req.params.MA_DVIQLY}&pass=${req.params.pass}`;
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

// 13
service.get('/api/BTHT_HATHE_getDSHinhanhchoCot_byTram/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - BTHT_HATHE_getDSHinhanhchoCot_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('BTHT_HATHE_getDSHinhanhchoCot_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// LUOI DIEN HA THE + LUOI DIEN TRUNG THE
service.get('/api/BTHT_HATHE_getDSHinhanh_bySocot/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM/:SO_COT', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - BTHT_HATHE_getDSHinhanh_bySocot: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.NVarChar, req.params.SO_COT)
                    .execute('BTHT_HATHE_getDSHinhanh_bySocot', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

//48
service.get('/api/HATHE_getDS_BTHT_DS_VITRIBATTHUONG_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDS_BTHT_DS_VITRIBATTHUONG_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('HATHE_getDS_BTHT_DS_VITRIBATTHUONG_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 49
service.get('/api/HATHE_updateDSKhachhang_Socot_byTram/:MA_DVIQLY/:MA_TRAM/:MA_DDO/:SO_COT', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_updateDSKhachhang_Socot_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('MA_DDO', sql.VarChar, req.params.MA_DDO)
                    .input('SO_COT', sql.NVarChar, req.params.SO_COT)
                    .execute('HATHE_updateDSKhachhang_Socot_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// 49.1
service.get('/api/HATHE_getDSExcelThongtinkhachhang_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSExcelThongtinkhachhang_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getDSExcelThongtinkhachhang_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 49.2
service.get('/api/HATHE_getDSExcelBandoluoidien_byTram/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSExcelBandoluoidien_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getDSExcelBandoluoidien_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 49.3
service.get('/api/HATHE_getDSExcelDuongdanhinhanh_byTram/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN/:MA_TRAM', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getDSExcelDuongdanhinhanh_byTram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .execute('HATHE_getDSExcelDuongdanhinhanh_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 50
service.post('/api/HATHE_updateDSKhachhang_byTram', function (req, res, next) {
    //console.log("URL: " + '/api/HATHE_updateDSKhachhang_byTram/:MA_DVIQLY/:MA_TRAM/:MA_LO/:MA_SOGCS/:STT/:MA_KHANG/:MA_DDO/:TEN_KHANG/:DCHI_KHANG/:DIEN_THOAI/:EMAIL/:DCHI_SDD/:X_SDD/:Y_SDD/:Z_SDD/:SO_PHA/:PHA/:SO_HO/:SO_COT/:SO_THUNG/:VTRI_THUNG/:LOAI_THUNG/:VTRI_CTO/:VTRI_TREO/:GIA_DIEN/:MA_CLOAI_CTO/:NAM_SXUAT_CTO/:SO_CTO/:NGAY_TREO_CTO/:NGAY_KDINH_CTO/:HS_NHAN/:MA_CLOAI_TI1/:NAM_SXUAT_TI1/:SO_TI1/:NGAY_TREO_TI1/:NGAY_KDINH_TI1/:MA_CLOAI_TI2/:NAM_SXUAT_TI2/:SO_TI2/:NGAY_TREO_TI2/:NGAY_KDINH_TI2/:MA_CLOAI_TI3/:NAM_SXUAT_TI3/:SO_TI3/:NGAY_TREO_TI3/:NGAY_KDINH_TI3/:GHI_CHU/:NGUOI_NHAP/:THAY_DOI');
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_updateDSKhachhang_byTram: " + err);
                return res.send(err);
            }
            else {
                console.log(req.body);
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.body.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.body.MA_TRAM)
                    .input('MA_LO', sql.VarChar, req.body.MA_LO)
                    .input('MA_SOGCS', sql.VarChar, req.body.MA_SOGCS)
                    .input('STT', sql.VarChar, req.body.STT)
                    .input('MA_KHANG', sql.VarChar, req.body.MA_KHANG)
                    .input('MA_DDO', sql.VarChar, req.body.MA_DDO)
                    .input('TEN_KHANG', sql.NVarChar, req.body.TEN_KHANG)
                    .input('DCHI_KHANG', sql.NVarChar, req.body.DCHI_KHANG)
                    .input('DIEN_THOAI', sql.VarChar, req.body.DIEN_THOAI)
                    .input('EMAIL', sql.VarChar, req.body.EMAIL)
                    .input('DCHI_SDD', sql.NVarChar, req.body.DCHI_SDD)
                    .input('X_SDD', sql.VarChar, req.body.X_SDD)
                    .input('Y_SDD', sql.VarChar, req.body.Y_SDD)
                    .input('Z_SDD', sql.VarChar, req.body.Z_SDD)
                    .input('SO_PHA', sql.Decimal, req.body.SO_PHA)
                    .input('PHA', sql.NVarChar, req.body.PHA)
                    .input('SO_HO', sql.Decimal, req.body.SO_HO)
                    .input('SO_COT', sql.NVarChar, req.body.SO_COT)
                    .input('SO_THUNG', sql.NVarChar, req.body.SO_THUNG)
                    .input('VTRI_THUNG', sql.Decimal, req.body.VTRI_THUNG)
                    .input('LOAI_THUNG', sql.NVarChar, req.body.LOAI_THUNG)
                    .input('VTRI_CTO', sql.Decimal, req.body.VTRI_CTO)
                    .input('VTRI_TREO', sql.NVarChar, req.body.VTRI_TREO)
                    .input('GIA_DIEN', sql.NVarChar, req.body.GIA_DIEN)
                    .input('MA_CLOAI_CTO', sql.VarChar, req.body.MA_CLOAI_CTO)
                    .input('NAM_SXUAT_CTO', sql.Decimal, req.body.NAM_SXUAT_CTO)
                    .input('SO_CTO', sql.VarChar, req.body.SO_CTO)
                    .input('NGAY_TREO_CTO', sql.VarChar, req.body.NGAY_TREO_CTO)
                    .input('NGAY_KDINH_CTO', sql.VarChar, req.body.NGAY_KDINH_CTO)
                    .input('HS_NHAN', sql.Decimal, req.body.HS_NHAN)
                    .input('MA_CLOAI_TI1', sql.VarChar, req.body.MA_CLOAI_TI1)
                    .input('NAM_SXUAT_TI1', sql.Decimal, req.body.NAM_SXUAT_TI1)
                    .input('SO_TI1', sql.VarChar, req.body.SO_TI1)
                    .input('NGAY_TREO_TI1', sql.VarChar, req.body.NGAY_TREO_TI1)
                    .input('NGAY_KDINH_TI1', sql.VarChar, req.body.NGAY_KDINH_TI1)
                    .input('MA_CLOAI_TI2', sql.VarChar, req.body.MA_CLOAI_TI2)
                    .input('NAM_SXUAT_TI2', sql.Decimal, req.body.NAM_SXUAT_TI2)
                    .input('SO_TI2', sql.VarChar, req.body.SO_TI2)
                    .input('NGAY_TREO_TI2', sql.VarChar, req.body.NGAY_TREO_TI2)
                    .input('NGAY_KDINH_TI2', sql.VarChar, req.body.NGAY_KDINH_TI2)
                    .input('MA_CLOAI_TI3', sql.VarChar, req.body.MA_CLOAI_TI3)
                    .input('NAM_SXUAT_TI3', sql.Decimal, req.body.NAM_SXUAT_TI3)
                    .input('SO_TI3', sql.VarChar, req.body.SO_TI3)
                    .input('NGAY_TREO_TI3', sql.VarChar, req.body.NGAY_TREO_TI3)
                    .input('NGAY_KDINH_TI3', sql.VarChar, req.body.NGAY_KDINH_TI3)
                    .input('GHI_CHU', sql.NVarChar, req.body.GHI_CHU)
                    .input('NGUOI_NHAP', sql.VarChar, req.body.NGUOI_NHAP)
                    .input('THAY_DOI', sql.NVarChar, req.body.THAY_DOI)
                    .execute('HATHE_updateDSKhachhang_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 51
service.get('/api/GetFileSHHDFromServer/:MA_DVIQLY/:MA_KHANG/:TEN_HINH/:KEY', function (req, res, next) {
    try {
        var url = `https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/GetFileSHHDFromServer?MA_DVIQLY=${req.params.MA_DVIQLY}&MA_KHANG=${req.params.MA_KHANG}&TEN_HINH=${req.params.TEN_HINH}&KEY=${req.params.KEY}`;
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

// 52
service.get('/api/TRUNGTHE_getDSExcelBandoluoidien_byXuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSExcelBandoluoidien_byXuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getDSExcelBandoluoidien_byXuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 53
service.get('/api/TRUNGTHE_getDSExcelDuongdanhinhanh_byXuattuyen/:MA_DVIQLY/:MA_TRAMNGUON/:MA_XUATTUYEN', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TRUNGTHE_getDSExcelDuongdanhinhanh_byXuattuyen: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAMNGUON', sql.VarChar, req.params.MA_TRAMNGUON)
                    .input('MA_XUATTUYEN', sql.VarChar, req.params.MA_XUATTUYEN)
                    .execute('TRUNGTHE_getDSExcelDuongdanhinhanh_byXuattuyen', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 54
service.get('/api/get_DS_Nguoidung_byTenDangNhap/:ten_dangnhap', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - get_DS_Nguoidung_byTenDangNhap: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('ten_dangnhap', sql.VarChar, req.params.ten_dangnhap)
                    .execute('get_DS_Nguoidung_byTenDangNhap', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 55--4.1
service.get('/api/HATHE_getBCKhachhang_ThieuThongTinKhachHang/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_ThieuThongTinKhachHang: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_ThieuThongTinKhachHang', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 55--4.2
service.get('/api/HATHE_getBCKhachhang_ThieuThongTinDiemDo/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_ThieuThongTinDiemDo: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_ThieuThongTinDiemDo', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 55--4.3
service.get('/api/HATHE_getBCKhachhang_ThieuThongTinDoDem/:MA_DVIQLY/:MA_TRAM/:CAPDONVI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_getBCKhachhang_ThieuThongTinDoDem: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('CAPDONVI', sql.Int, req.params.CAPDONVI)
                    .execute('HATHE_getBCKhachhang_ThieuThongTinDoDem', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err); 
    }
    
})

// 55--4.3
service.get('/api/HATHE_updateCot_chuyentram/:MA_DVIQLY/:MA_TRAM/:SO_COT/:MA_TRAM_MOI', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_updateCot_chuyentram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .input('MA_TRAM_MOI', sql.VarChar, req.params.MA_TRAM_MOI)
                    .execute('HATHE_updateCot_chuyentram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 56
service.get('/api/QUANLYCAP_getDSHinhanh_byMatramvaLoaiHinhanh/:MA_DVIQLY/:MA_TRAM/:LOAI_HINHANH', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - QUANLYCAP_getDSHinhanh_byMatramvaLoaiHinhanh: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('LOAI_HINHANH', sql.VarChar, req.params.LOAI_HINHANH)
                    .execute('QUANLYCAP_getDSHinhanh_byMatramvaLoaiHinhanh', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

// 57
service.get('/api/QUANLYCAP_getDSHinhanh_bySocotvaLoaiHinhanh/:MA_DVIQLY/:MA_TRAM/:SO_COT/:LOAI_HINHANH', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - QUANLYCAP_getDSHinhanh_bySocotvaLoaiHinhanh: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .input('LOAI_HINHANH', sql.VarChar, req.params.LOAI_HINHANH)
                    .execute('QUANLYCAP_getDSHinhanh_bySocotvaLoaiHinhanh', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
})

// 57
service.get('/api/HATHE_kiemtracot_chuyentram/:MA_DVIQLY/:MA_TRAM/:SO_COT', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_kiemtracot_chuyentram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .execute('HATHE_kiemtracot_chuyentram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
})

//
service.get('/api/hoso_khachhang_dientu/:MA_DVIQLY', function (req, res, next) {
    //
    try {
        var url = `http://10.122.0.17/hoso_khachhang_dientu/PC05CC/PC05CC0000001/PC05CC0000001_3_BBTT.pdf`;
        //console.log("URL: " + url);
        request(url, function (error, response, body) {
            if (body) {
                body = body.replace('</string>', '');
                body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                body = body.replace('<string xmlns="http://tempuri.org/">', '');
                body = body.trim();
                res.send(body);
            }
            else { return ''; }
        });
    } catch (err) {
        return '';
    }
    
});

service.get('/api/HATHE_updateDSCot_toPhanTang_byTram/:MA_DVIQLY/:MA_TRAM/:SO_COT/:X_COT/:Y_COT/:PHAN_TANG', function (req, res, next) {
    //
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - HATHE_kiemtracot_chuyentram: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_TRAM', sql.VarChar, req.params.MA_TRAM)
                    .input('SO_COT', sql.VarChar, req.params.SO_COT)
                    .input('X_COT', sql.VarChar, req.params.X_COT)
                    .input('Y_COT', sql.VarChar, req.params.Y_COT)
                    .input('PHAN_TANG', sql.NVarChar, req.params.PHAN_TANG)
                    .execute('HATHE_updateDSCot_toPhanTang_byTram', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});
module.exports = service;

