var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_tongquanController = require('../controllers/baocaohathe_tongquanController');
router.get('/baocaohathe_tongquan', baocaohathe_tongquanController.getBaocaohathe_tongquan);

module.exports = router;


