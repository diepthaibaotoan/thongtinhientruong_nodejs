var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_thieuthongtinkhachhangController = require('../controllers/baocaohathe_khachhang_thieuthongtinkhachhangController');
router.get('/baocaohathe_khachhang_thieuthongtinkhachhang', baocaohathe_khachhang_thieuthongtinkhachhangController.getBaocaohathe_khachhang_thieuthongtinkhachhang);

module.exports = router;


