var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_thongtinanhkhachhang_thieuanhController = require('../controllers/baocaohathe_khachhang_thongtinanhkhachhang_thieuanhController');
router.get('/baocaohathe_khachhang_thongtinanhkhachhang_thieuanh', baocaohathe_khachhang_thongtinanhkhachhang_thieuanhController.getBaocaohathe_khachhang_thongtinanhkhachhang_thieuanh);

module.exports = router;


