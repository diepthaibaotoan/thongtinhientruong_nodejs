var express = require('express');
var router = express.Router();

// Require Controllers Module
var treothaocongto_cauhinhchuongtrinhController = require('../controllers/treothaocongto_cauhinhchuongtrinhController');
router.get('/treothaocongto_cauhinhchuongtrinh', treothaocongto_cauhinhchuongtrinhController.getTreothaocongto_cauhinhchuongtrinh);

module.exports = router;