var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_thieuthongtindiemdoController = require('../controllers/baocaohathe_khachhang_thieuthongtindiemdoController');
router.get('/baocaohathe_khachhang_thieuthongtindiemdo', baocaohathe_khachhang_thieuthongtindiemdoController.getBaocaohathe_khachhang_thieuthongtindiemdo);

module.exports = router;


