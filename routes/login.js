var expresss = require('express');
var router = expresss.Router();

// Require Controller Module
var login_controller = require('../controllers/loginController');

/* GET user listing */
router.get('/login',login_controller.getLogin);

module.exports = router;
