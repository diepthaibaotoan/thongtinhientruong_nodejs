var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaotrungthe_tongquanController = require('../controllers/baocaotrungthe_tongquanController');
router.get('/baocaotrungthe_tongquan', baocaotrungthe_tongquanController.getBaocaotrungthe_tongquan);

module.exports = router;


