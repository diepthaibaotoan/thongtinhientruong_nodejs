var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_rasoatthongtinController = require('../controllers/baocaohathe_khachhang_rasoatthongtinController');
router.get('/baocaohathe_khachhang_rasoatthongtin', baocaohathe_khachhang_rasoatthongtinController.getBaocaohathe_khachhang_rasoatthongtin);

module.exports = router;


