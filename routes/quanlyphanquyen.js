var express = require('express');
var router = express.Router();

// Require Controllers Module
var quanlyphanquyenController = require('../controllers/quanlyphanquyenController');
router.get('/quanlyphanquyen', quanlyphanquyenController.getQuanlyphanquyen);

module.exports = router;
