const fs = require("fs");
const path = require("path");

module.exports = (app) => {
    // API routes
    fs.readdirSync(path.join(__dirname)).forEach((file) => {
        if(file != 'router.js') {
            app.use('/', require(`../routes/${file.substr(0, file.indexOf('.'))}`));
        }
    });

};
