var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhangsaiController = require('../controllers/baocaohathe_khachhangsaiController');
router.get('/baocaohathe_khachhangsai', baocaohathe_khachhangsaiController.getBaocaohathe_khachhangsai);

module.exports = router;


