var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_thongtinanhkhachhangController = require('../controllers/baocaohathe_khachhang_thongtinanhkhachhangController');
router.get('/baocaohathe_khachhang_thongtinanhkhachhang', baocaohathe_khachhang_thongtinanhkhachhangController.getBaocaohathe_khachhang_thongtinanhkhachhang);

module.exports = router;


