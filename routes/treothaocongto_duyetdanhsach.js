var express = require('express');
var router = express.Router();

// Require Controllers Module
var treothaocongto_duyetdanhsachController = require('../controllers/treothaocongto_duyetdanhsachController');
router.get('/treothaocongto_duyetdanhsach', treothaocongto_duyetdanhsachController.getTreothaocongto_duyetdanhsach);

module.exports = router;
