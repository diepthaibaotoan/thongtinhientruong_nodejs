var express = require('express');
var router = express.Router();

// Require Controllers Module
var indexController = require('../controllers/indexController');
router.get('/', indexController.getIndex);


/* English */
router.get('/en', indexController.lang_en);

/* Viet Nam */
router.get('/vi', indexController.lang_vi);
//
module.exports = router;

//
