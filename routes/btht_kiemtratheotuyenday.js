var express = require('express');
var router = express.Router();

// Require Controllers Module
var btht_kiemtratheotuyendayController = require('../controllers/btht_kiemtratheotuyendayController');
router.get('/btht_kiemtratheotuyenday', btht_kiemtratheotuyendayController.getBtht_kiemtratheotuyenday);

module.exports = router;


