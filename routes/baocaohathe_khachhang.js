var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhangController = require('../controllers/baocaohathe_khachhangController');
router.get('/baocaohathe_khachhang', baocaohathe_khachhangController.getBaocaohathe_khachhang);

module.exports = router;


