var url_ttkh = "https://ttht.cpc.vn:8080";
var ConfigConnect = require('../config/database');
var express = require('express');
var sleep = require('system-sleep');
var service = express.Router();
const sql = require('mssql');
const request = require("request");
var KEY_PASS = "TOANDTB";

function convert_khongchonull(chuoiguivao) {
    if (chuoiguivao == null) {
        return encodeURIComponent('');
    } else {
        return encodeURIComponent(chuoiguivao);
    }
}

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}


// 1
service.get('/api/TTCT_updateDSThongtinkhachhangDoDem_byNguoidangkytt/:MA_DDO/:BUOC_TTHAO/:NGUOI_DUOCPHANCONG', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_updateDSThongtinkhachhangDoDem_byNguoidangkytt: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DDO', sql.VarChar, req.params.MA_DDO)
                    .input('BUOC_TTHAO', sql.Int, req.params.BUOC_TTHAO)
                    .input('NGUOI_DUOCPHANCONG', sql.VarChar, req.params.NGUOI_DUOCPHANCONG)
                    .execute('TTCT_updateDSThongtinkhachhangDoDem_byNguoidangkytt', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
});

// 3
service.get('/api/TTCT_getDSKhachHangChuaDangKyTreoThao_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    const pool = new sql.ConnectionPool(ConfigConnect, err => {
        if (err) {
            console.log("Lỗi kết nối database - TTCT_getDSKhachHangChuaDangKyTreoThao_byTram: " + err);
            res.send(err);
        }
        else {
            pool.request()
                .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
                .input('MA_TRAM', sql.VarChar, req.param('MA_TRAM'))
                .execute('TTCT_getDSKhachHangChuaDangKyTreoThao_byTram', (err, result) => {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send(result.recordset);
                    }
                })
        }
    })
});

// 3.1
service.get('/api/TTCT_getDSKhachHangChuaDangKyTreoThao_byTimKiem/:MA_DVIQLY/:TIM_KIEM', function (req, res, next) {
    const pool = new sql.ConnectionPool(ConfigConnect, err => {
        if (err) {
            console.log("Lỗi kết nối database - TTCT_getDSKhachHangChuaDangKyTreoThao_byTimKiem: " + err);
            res.send(err);
        }
        else {
            pool.request()
                .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
                .input('TIM_KIEM', sql.VarChar, req.param('TIM_KIEM'))
                .execute('TTCT_getDSKhachHangChuaDangKyTreoThao_byTimKiem', (err, result) => {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send(result.recordset);
                    }
                })
        }
    })
});


// 4
service.get('/api/TTCT_updateBuocTreoThaoCongToVeBinhThuong/:MA_DDO/:BUOC_TTHAO', function (req, res, next) {
    const pool = new sql.ConnectionPool(ConfigConnect, err => {
        if (err) {
            console.log("Lỗi kết nối database - TTCT_updateBuocTreoThaoCongToVeBinhThuong: " + err);
            res.send(err);
        }
        else {
            pool.request()
                .input('MA_DDO', sql.VarChar, req.param('MA_DDO'))
                .input('BUOC_TTHAO', sql.Int, req.param('BUOC_TTHAO'))
                .execute('TTCT_updateBuocTreoThaoCongToVeBinhThuong', (err, result) => {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send(result.recordset);
                    }
                })
        }
    })
});

// 5
service.get('/api/TTCT_getDSThongtinkhachhangDaDangKyTreoTHao_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    const pool = new sql.ConnectionPool(ConfigConnect, err => {
        if (err) {
            console.log("Lỗi kết nối database - TTCT_getDSThongtinkhachhangDaDangKyTreoTHao_byTram: " + err);
            res.send(err);
        }
        else {
            pool.request()
                .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
                .input('MA_TRAM', sql.VarChar, req.param('MA_TRAM'))
                .execute('TTCT_getDSThongtinkhachhangDaDangKyTreoTHao_byTram', (err, result) => {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send(result.recordset);
                    }
                })
        }
    })
});

// 7
service.get('/api/TTCT_getDSKhachHangDeThongKe_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    const pool = new sql.ConnectionPool(ConfigConnect, err => {
        if (err) {
            console.log("Lỗi kết nối database - TTCT_updateBuocTreoThaoCongToVeBinhThuong: " + err);
            res.send(err);
        }
        else {
            pool.request()
                .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
                .input('MA_TRAM', sql.VarChar, req.param('MA_TRAM'))
                .execute('TTCT_getDSKhachHangDeThongKe_byTram', (err, result) => {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send(result.recordset);
                    }
                })
        }
    })
});

// 8
service.get('/api/TTCT_getDSKhachHangDangTreoThaoHoacChuanBiGuiCMIS_byTram/:MA_DVIQLY/:MA_TRAM', function (req, res, next) {
    const pool = new sql.ConnectionPool(ConfigConnect, err => {
        if (err) {
            console.log("Lỗi kết nối database - TTCT_getDSKhachHangDangTreoThaoHoacChuanBiGuiCMIS_byTram: " + err);
            res.send(err);
        }
        else {
            pool.request()
                .input('MA_DVIQLY', sql.VarChar, req.param('MA_DVIQLY'))
                .input('MA_TRAM', sql.VarChar, req.param('MA_TRAM'))
                .execute('TTCT_getDSKhachHangDangTreoThaoHoacChuanBiGuiCMIS_byTram', (err, result) => {
                    if (err) {
                        res.send(err);
                    } else {
                        res.send(result.recordset);
                    }
                })
        }
    })
});

// 9
service.get('/api/TTCT_PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full/:MA_DVIQLY/:TEN_DANGNHAP/:CHUC_NANG_TTCT/:NGUOI_NHAP', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('TEN_DANGNHAP', sql.VarChar, req.params.TEN_DANGNHAP)
                    .input('CHUC_NANG_TTCT', sql.NVarChar, req.params.CHUC_NANG_TTCT)
                    .input('NGUOI_NHAP', sql.NVarChar, req.params.NGUOI_NHAP)
                    .execute('TTCT_PQUD_updateDSNGUOIDUNG_byMadviqlyvaTenDangNhap_full', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 10
service.get('/api/TTCT_getDSNhanVien_ByMaDienLuc/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_getDSNhanVien_ByMaDienLuc: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TTCT_getDSNhanVien_ByMaDienLuc', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

// 12
service.get('/api/TTCT_getDSNGUOIDUNG_byMadviqly/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_getDSNGUOIDUNG_byMadviqly: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TTCT_getDSNGUOIDUNG_byMadviqly', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});
service.get('/api/TTCT_getThongtinNhanVien_ByMaDienLuc/:MA_DVIQLY', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_getThongtinNhanVien_ByMaDienLuc: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TTCT_getThongtinNhanVien_ByMaDienLuc', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

service.get('/api/TTCT_updateThongtinNhanVien_ByMaDienLuc/:MA_DVIQLY/:MA_NVIEN/:TEN_DNHAP', function (req, res, next) {
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_getThongtinNhanVien_ByMaDienLuc: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_NVIEN', sql.VarChar, req.params.MA_NVIEN)
                    .input('TEN_DNHAP', sql.VarChar, req.params.TEN_DNHAP)
                    .execute('TTCT_updateThongtinNhanVien_ByMaDienLuc', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

service.get('/api/TTCT_getThongtinGiamDoc_ByMaDienLuc/:MA_DVIQLY', function (req, res, next) {
    //
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_getThongtinGiamDoc_ByMaDienLuc: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .execute('TTCT_getThongtinGiamDoc_ByMaDienLuc', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});

service.get('/api/TTCT_updateThongtinGiamDoc_ByMaDienLuc/:MA_DVIQLY/:MA_NVIEN/:TEN_NVIEN/:TEN_DNHAP', function (req, res, next) {
    //
    try {
        const pool = new sql.ConnectionPool(ConfigConnect, err => {
            if (err) {
                console.log("Lỗi kết nối database - TTCT_updateThongtinGiamDoc_ByMaDienLuc: " + err);
                return res.send(err);
            }
            else {
                pool.request()
                    .input('MA_DVIQLY', sql.VarChar, req.params.MA_DVIQLY)
                    .input('MA_NVIEN', sql.VarChar, req.params.MA_NVIEN)
                    .input('TEN_NVIEN', sql.NVarChar, req.params.TEN_NVIEN)
                    .input('TEN_DNHAP', sql.VarChar, req.params.TEN_DNHAP)
                    .execute('TTCT_updateThongtinGiamDoc_ByMaDienLuc', (err, result) => {
                        pool.close();
                        if (err) {
                            return res.send(err);
                        } else {
                            return res.send(result.recordset);
                        }
                    })
            }
        })
    } catch (err) {
        return res.send(err);
    }
    
});
module.exports = service;

