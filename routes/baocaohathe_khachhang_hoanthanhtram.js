var express = require('express');
var router = express.Router();

// Require Controllers Module
var baocaohathe_khachhang_hoanthanhtramController = require('../controllers/baocaohathe_khachhang_hoanthanhtramController');
router.get('/baocaohathe_khachhang_hoanthanhtram', baocaohathe_khachhang_hoanthanhtramController.getBaocaohathe_khachhang_hoanthanhtram);

module.exports = router;


