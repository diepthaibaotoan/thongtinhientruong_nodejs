var express = require('express');
var router = express.Router();

// Require Controllers Module
var treothaocongto_quanlytreothaoController = require('../controllers/treothaocongto_quanlytreothaoController');
router.get('/treothaocongto_quanlytreothao', treothaocongto_quanlytreothaoController.getTreothaocongto_quanlytreothao);

module.exports = router;
