var express = require('express');
var router = express.Router();

// Require Controllers Module
var treothaocongto_phanquyenController = require('../controllers/treothaocongto_phanquyenController');
router.get('/treothaocongto_phanquyen', treothaocongto_phanquyenController.getTreothaocongto_phanquyen);

module.exports = router;
