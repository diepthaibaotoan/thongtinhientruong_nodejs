var express = require('express');
var router = express.Router();

// Require Controllers Module
var treothaocongto_thongketreothaoController = require('../controllers/treothaocongto_thongketreothaoController');
router.get('/treothaocongto_thongketreothao', treothaocongto_thongketreothaoController.getTreothaocongto_thongketreothao);

module.exports = router;
