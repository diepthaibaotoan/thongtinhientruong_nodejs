// Xuat excel
var dvjson = $("#dvjson");
function clickXuatexcel() {
    if (matram != null && matram != '0') {

        if (localStorage['taikhoan'] == null || localStorage['taikhoan'] == '' || localStorage['taikhoan'] == 'undefined') {
            $("#isBientapExcel").prop("checked", false);
            $("#isTinhKhoangCachCot").prop("checked", false);
            alert("Bạn chưa đăng nhập.");
        } else {
            $("#loading").show();
            $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${localStorage['taikhoan']}`, function (data) {
                $("#loading").hide();
                if (data.length != 0) {
                    data.forEach(function (nguoidungObj) {
                        if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_CMIS_EXCUTE) > -1) {
                            if (macongty == null || macongty == '0') {
                                alert("Mời bạn chọn Công ty")
                            } else {
                                if (madienluc == null || madienluc == '0') {
                                    alert("Mời bạn chọn Điện lực.");
                                } else {
                                    if (matram == null || matram == '0') {
                                        alert("Mời bạn chọn Trạm.");
                                    } else {
                                        $("#id11").show();
                                    }
                                }
                            }
                        } else {
                            alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Hạ Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
                        }
                    })
                } else {
                    alert("Tài khoản này không tồn tại.");
                }

            });
        }
    } else {
        GF_SHOWTOAST_LONG("Chỉ có xem ở cấp Trạm mới có thể xuất Excel.", 400);
    }
}

function XDLE_clickXuatExcelBandoluoidien() {
    //
    $("#loading").show();
    $.get(`${url}/api/HATHE_getDSExcelBandoluoidien_byTram/${madienluc.trim()}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${matram.trim()}`, function (data) {
        $("#loading").hide();
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
        $("#id11").hide();
    });
}

function XDLE_clickXuatExcelThongtinkhachhang() {
    $("#loading").show();
    $.get(`${url}/api/HATHE_getDSExcelThongtinkhachhang_byTram/${madienluc.trim()}/${matram.trim()}`, function (data) {
        $("#loading").hide();
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
    });
    $("#id11").hide();
}

function XDLE_clickXuatExcelDuongdanhinhanh() {
    $("#loading").show();
    $.get(`${url}/api/HATHE_getDSExcelDuongdanhinhanh_byTram/${madienluc.trim()}/${GV_CHUOI_MATRAMNGUON}/${GV_CHUOI_MAXUATTUYEN}/${matram.trim()}`, function (data) {
        $("#loading").hide();
        dvjson.excelexportjs({
            containerid: "dvjson",
            datatype: 'json',
            dataset: data,
            columns: getColumns(data)
        });
    });
    $("#id11").hide();
}

function XDLE_clickXuatHinhanhBandoluoidien() {
    mapToImg('#map .gm-style > [tabindex]', matram);
    $("#id11").hide();
}

function XDLE_clickXuatDuongDayTram() {
    $("#loading").show();
    var data = DDT_convertAllLinesToJsonArray(listCothathe);

    dvjson.excelexportjs({
        containerid: "dvjson",
        datatype: 'json',
        dataset: data,
        columns: getColumns(data)
    });
    
    $("#id11").hide();
    $("#loading").hide();
}