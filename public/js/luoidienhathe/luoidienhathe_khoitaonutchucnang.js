var centerControlDiv_BienTapGiaoDien;
var centerControlDiv_BienTapEXCEL;

/* Ham chinh */
function initNutChucnang() {
    var centerControlDiv_giaodienchinh = document.createElement('div');
    var centerControl = new CenterControlGiaodienchinh(centerControlDiv_giaodienchinh, map);
    centerControlDiv_giaodienchinh.index = 1;
    map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_giaodienchinh);
}

/* 1. Khi initmap se khoi tao ta ca cac nut */
function CenterControlGiaodienchinh(controlDiv, map) {

    /* BAT DAU 1.1: Hien So Cot */
    var controlUICOT_div = document.createElement('div');
    controlUICOT_div.id = 'GNC_hienthicot';
    controlUICOT_div.style.cssFloat = 'left';
    var controlUICOT_GDC = document.createElement('input');
    controlUICOT_GDC.type = 'checkbox';
    controlUICOT_GDC.id = 'isSocot';
    controlUICOT_GDC.name = 'cb_hiensocot';
    controlUICOT_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUICOT_GDC.style.width = '18px';
    controlUICOT_GDC.style.height = '25px';
    controlUICOT_GDC.style.textAlign = 'center';
    controlUICOT_GDC.style.fontSize = '18px';
    controlUICOT_GDC.style.fontWeight = 'bold';

    var controlUICOT_GDC_label = document.createElement('label');
    controlUICOT_GDC_label.htmlFor = 'isSocot';
    controlUICOT_GDC_label.id = 'socot';
    controlUICOT_GDC_label.style.margin = '2px 0px 0px 27px';
    controlUICOT_GDC_label.style.cursor = 'pointer';
    controlUICOT_GDC_label.innerHTML = 'Hiện số cột';

    //add div hien so cot vao map
    controlUICOT_div.onmouseover = function () {
        controlUICOT_div.style.backgroundColor = 'aquamarine';
    };
    controlUICOT_div.onmouseleave = function () {
        if (isHiensocot) {
            controlUICOT_div.style.backgroundColor = 'aquamarine';
        } else {
            controlUICOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        }
    };
    controlUICOT_div.appendChild(controlUICOT_GDC);
    controlUICOT_div.appendChild(controlUICOT_GDC_label);
    controlDiv.appendChild(controlUICOT_div);

    controlUICOT_GDC.addEventListener('change', function () {
        if (this.checked) {
            controlUICOT_div.style.backgroundColor = ' aquamarine';
            isHiensocot = true;
            if ((matram == null || matram == '0') && map.getZoom() <= 15) {
                // khong lam chi
            } else {

                bochuyendoiZoom(map.getZoom());
                for (var i = 0; i < hashMapMarker.size; i++) {
                    hashMapMarker.get(i).setIcon({
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: hashMapMarker.get(i).icon.fillColor,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: hashMapMarker.get(i).icon.madviqly, //1
                        matram: hashMapMarker.get(i).icon.matram, //2
                        tentram: hashMapMarker.get(i).icon.tentram, //3
                        socot: hashMapMarker.get(i).icon.socot, //4
                        lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                        ishien: 1, //5
                        iscot: hashMapMarker.get(i).icon.iscot, //5
                    });

                    //
                    if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else {
                        hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    }
                }
            }
        } else {
            controlUICOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
            isHiensocot = false;

            if ((matram == null || matram == '0') && map.getZoom() <= 15) {
                // khong lam chi
            } else {
                bochuyendoiZoom(map.getZoom());
                for (var i = 0; i < hashMapMarker.size; i++) {
                    hashMapMarker.get(i).setIcon({
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: tron_scale,
                        fillColor: hashMapMarker.get(i).icon.fillColor,
                        fillOpacity: 1,
                        strokeWeight: tron_stroke,
                        origin: new google.maps.Point(0, 0), // origin
                        anchor: new google.maps.Point(0, 0), // anchor
                        labelOrigin: new google.maps.Point(0, -2),
                        madviqly: hashMapMarker.get(i).icon.madviqly, //1
                        matram: hashMapMarker.get(i).icon.matram, //2
                        tentram: hashMapMarker.get(i).icon.tentram, //3
                        socot: hashMapMarker.get(i).icon.socot, //4
                        lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                        ishien: 0, //6
                        iscot: hashMapMarker.get(i).icon.iscot, //5
                    });

                    //
                    if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                        hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    } else {
                        hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
                        hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                    }
                }
            }
        }

    });
    /* KET THUC 1.1: Hien So Cot */

    /* BAT DAU 1.2: Bien tap giao dien */
    var controlUIBTGIAODIEN_div = document.createElement('div');
    controlUIBTGIAODIEN_div.id = 'GND_bientap';
    controlUIBTGIAODIEN_div.style.cssFloat = 'left';
    var controlUIBTGIAODIEN_GDC = document.createElement('input');
    controlUIBTGIAODIEN_GDC.type = 'checkbox';
    controlUIBTGIAODIEN_GDC.id = 'isBientap';
    controlUIBTGIAODIEN_GDC.name = 'cb_bientap';
    controlUIBTGIAODIEN_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUIBTGIAODIEN_GDC.style.width = '18px';
    controlUIBTGIAODIEN_GDC.style.height = '25px';
    controlUIBTGIAODIEN_GDC.style.textAlign = 'center';
    controlUIBTGIAODIEN_GDC.style.fontSize = '18px';
    controlUIBTGIAODIEN_GDC.style.fontWeight = 'bold';

    var controlUIBTGIAODIEN_GDC_label = document.createElement('label');
    controlUIBTGIAODIEN_GDC_label.htmlFor = 'isBientap';
    controlUIBTGIAODIEN_GDC_label.id = 'bientap';
    controlUIBTGIAODIEN_GDC_label.style.margin = '2px 0px 0px 6px';
    controlUIBTGIAODIEN_GDC_label.style.cursor = 'pointer';
    controlUIBTGIAODIEN_GDC_label.innerHTML = 'Biên tập giao diện';

    //
    controlUIBTGIAODIEN_div.onmouseover = function () {
        controlUIBTGIAODIEN_div.style.backgroundColor = 'aquamarine';
    };
    controlUIBTGIAODIEN_div.onmouseleave = function () {
        controlUIBTGIAODIEN_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlUIBTGIAODIEN_div.appendChild(controlUIBTGIAODIEN_GDC);
    controlUIBTGIAODIEN_div.appendChild(controlUIBTGIAODIEN_GDC_label);
    controlDiv.appendChild(controlUIBTGIAODIEN_div);

    controlUIBTGIAODIEN_GDC.addEventListener('change', function () {

        if (this.checked) {
            var m_taikhoan = localStorage['taikhoan'];
            var m_matkhau = localStorage['matkhau'];
            if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
                $("#isBientap").prop("checked", false);
                alert("Bạn chưa đăng nhập.");
            } else {
                $("#loading").show();
                $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
                    if (data.length != 0) {
                        data.forEach(function (nguoidungObj) {
                            if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_CMIS_EXCUTE) !== -1) {
                                if (macongty == null || macongty == '0') {
                                    alert("Mời bạn chọn Công ty.")
                                } else {
                                    if (madienluc == null || madienluc == '0') {
                                        alert("Mời bạn chọn Điện lực.");
                                    } else {
                                        if (matram == null || matram == '0') {
                                            alert("Mời bạn chọn Trạm.");
                                        } else {
                                            //
                                            w3_close();
                                            GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Giao Diện.", 250);

                                            isHiensocot = false;
                                            isBientapExcel = false;
                                            isXemDuongDay = false;
                                            isTinhKhoangCachCot = false;
                                            isDoiTramChoCot = false;
                                            controlUICOT_GDC.checked = true
                                            controlUICOT_GDC_label.click();
                                            controlUIKHOANGCACHCOT_GDC.checked = true
                                            controlUIKHOANGCACHCOT_GDC_label.click();
                                            isBientap = true;
                                            while (controlDiv.firstChild) {
                                                controlDiv.removeChild(controlDiv.firstChild);
                                            }
                                            centerControlDiv_BienTapGiaoDien = document.createElement('div');
                                            var centerControl_giaodien = new CenterControlGiaoDien(centerControlDiv_BienTapGiaoDien, map, controlUIBTGIAODIEN_GDC, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div, controlUIDUONGDICOT_div, controlUIKHOANGCACHCOT_div, controlUICHONNHIEUTRAM_div, controlUIDOITRAMCHOCOT_div);
                                            centerControlDiv_BienTapGiaoDien.index = 1;
                                            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapGiaoDien);

                                            controlUIBTGIAODIEN_GDC.checked = true;

                                            bochuyendoiZoom(map.getZoom());
                                            for (var i = 0; i < hashMapMarker.size; i++) {
                                                hashMapMarker.get(i).setDraggable(true);
                                                hashMapMarker.get(i).setIcon({
                                                    path: hinh_giotnuoc,
                                                    scale: tron_scale / 8,
                                                    fillColor: hashMapMarker.get(i).icon.fillColor,
                                                    fillOpacity: 1,
                                                    strokeWeight: tron_stroke,
                                                    origin: new google.maps.Point(0, 0), // origin
                                                    anchor: new google.maps.Point(0, 0), // anchor
                                                    labelOrigin: new google.maps.Point(0, -50),
                                                    madviqly: hashMapMarker.get(i).icon.madviqly, //1
                                                    matram: hashMapMarker.get(i).icon.matram, //2
                                                    tentram: hashMapMarker.get(i).icon.tentram, //3
                                                    socot: hashMapMarker.get(i).icon.socot, //4
                                                    lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                                                    ishien: 1, //6
                                                    iscot: hashMapMarker.get(i).icon.iscot //7
                                                });

                                                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                                                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                                                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                                                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                                                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot };
                                                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                                                } else {
                                                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: cochu + 'px', text: hashMapMarker.get(i).icon.socot };
                                                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -50);
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                $("#isBientap").prop("checked", false);
                                alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Hạ Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
                            }
                        })
                    } else {
                        $("#isBientap").prop("checked", false);
                        alert("Tài khoản này không tồn tại.");
                    }
                    $("#loading").hide();
                });
            }
        } else {
            //hủy ba nút biên tập
            while (centerControlDiv_BienTapGiaoDien.firstChild) {
                centerControlDiv_BienTapGiaoDien.removeChild(centerControlDiv_BienTapGiaoDien.firstChild);
            }

            controlDiv.appendChild(controlUICOT_div);
            controlDiv.appendChild(controlUIBTGIAODIEN_div);
            controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
            controlDiv.appendChild(controlUIDUONGDICOT_div);
            controlDiv.appendChild(controlUIKHOANGCACHCOT_div);
            controlDiv.appendChild(controlUIDOITRAMCHOCOT_div);
            controlDiv.appendChild(controlUICHONNHIEUTRAM_div);
            centerControlDiv_BienTapGiaoDien = document.createElement('div');
            //
            w3_open();
            GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Giao Diện.", 250);
            isBientap = false;
            bochuyendoiZoom(map.getZoom());
            $("#tieudeI").text("I. Thông Tin Cột Hạ Thế: " + hashMapMarker.size + "");
            for (var i = 0; i < hashMapMarker.size; i++) {
                hashMapMarker.get(i).setDraggable(false);
                hashMapMarker.get(i).setIcon({
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: tron_scale,
                    fillColor: hashMapMarker.get(i).icon.fillColor,
                    fillOpacity: 1,
                    strokeWeight: tron_stroke,
                    origin: new google.maps.Point(0, 0), // origin
                    anchor: new google.maps.Point(0, 0), // anchor
                    labelOrigin: new google.maps.Point(0, -2),
                    madviqly: hashMapMarker.get(i).icon.madviqly, //1
                    matram: hashMapMarker.get(i).icon.matram, //2
                    tentram: hashMapMarker.get(i).icon.tentram, //3
                    socot: hashMapMarker.get(i).icon.socot, //4
                    lkcot2: hashMapMarker.get(i).icon.lkcot2, //5
                    ishien: 0, //6
                    iscot: hashMapMarker.get(i).icon.iscot, //7
                });

                //
                if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCONGCONG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_TIMVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else if (hashMapMarker.get(i).icon.lkcot2 == GV_THUOCTINH_TRAMCHUYENDUNG) {
                    hashMapMarker.get(i).label = { color: GV_COLOR_HONGVIENDEN, fontSize: cochu_chotram + 'px', text: hashMapMarker.get(i).icon.socot + ": " + hashMapMarker.get(i).icon.matram + " - " + hashMapMarker.get(i).icon.tentram };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                } else {
                    hashMapMarker.get(i).label = { color: GV_COLOR_MAUDEN, fontSize: '0px', text: hashMapMarker.get(i).icon.socot };
                    hashMapMarker.get(i).icon.labelOrigin = new google.maps.Point(0, -2);
                }
            }

            // Thiet lap lai tu dau
            $("#ul_khachhang").empty();
            $("#tieudeII").text("II. Thông Tin Khách Hàng: " + jsonKhachhangbandau.length + "");
            jsonKhachhang = jsonKhachhangbandau;
            $.each(jsonKhachhang, function (i, f) {
                $("#ul_khachhang").append(
                    `<li onclick="getitem('${f.MA_KHANG}')">
                        <a style="font-size:12.3px">
                        <b>${f.MA_KHANG}</b>
                        
                        <button aria-label="chi duong" id="icon-box-directions" class="icon-box-directions" data-toggle="tooltip" data-placement="right" title="Chỉ đường" onclick="findDirectionKhachHang('${f.MA_KHANG}',event)"></button>
                        <button aria-label="chi duong" id="searchbox-directions" class="searchbox-directions" data-toggle="tooltip" data-placement="right" title="Tìm vị trí" onclick="getdiadiem('${f.MA_KHANG}',event)"></button>
                        <button  id="display-client" class="display-client" data-toggle="tooltip" data-placement="right"  onclick="getmaKH('${f.MA_KHANG}',event)">CPM</button>
                        </a><br>
                        <a>${f.TEN_KHANG}</a><br>
                        <a>${f.DCHI_KHANG}</a><br>
                    </li>
                    <div style="background-color:rgb(50,190,166);height:1px"></div>`
                );
            });

            $("#hinhanh").empty();
            $("#tieudeIII").text("III. Hình Ảnh Hiện Trường: " + jsonHinnhanh_cotbandau.length + "");
            jsonHinnhanh_cot = jsonHinnhanh_cotbandau;
            $.each(jsonHinnhanh_cot, function (i, f) {
                $("#hinhanh").append(
                    `<div class="gallery" onclick="getHinhanhchitiet(${i}, ${0})">
                     ${displaydatefromdb(f.NGAY_NHAP)}${checknull_anh(f.UNGDUNG_SUDUNG)}
                    <img style="height:${height_}"  src="https://gismobile.cpc.vn/Img/${f.MA_DVIQLY}/HINHANH_HIENTRUONG/${f.TEN_HINHANH}">
            </div>`
                );
            });
        }
    });
    /* KET THUC 1.2: Bien tap giao dien */

    /* BAT DAU 1.3: Bien tap Excel */
    var controlUIBTGIAODIENEXCEL_div = document.createElement('div');
    controlUIBTGIAODIENEXCEL_div.id = 'GND_bientapexcel';
    controlUIBTGIAODIENEXCEL_div.style.cssFloat = 'left';
    var controlUIBTGIAODIENEXCEL_GDC = document.createElement('input');
    controlUIBTGIAODIENEXCEL_GDC.type = 'checkbox';
    controlUIBTGIAODIENEXCEL_GDC.id = 'isBientapExcel';
    controlUIBTGIAODIENEXCEL_GDC.name = 'cb_bientapexcel';
    controlUIBTGIAODIENEXCEL_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUIBTGIAODIENEXCEL_GDC.style.width = '18px';
    controlUIBTGIAODIENEXCEL_GDC.style.height = '25px';
    controlUIBTGIAODIENEXCEL_GDC.style.textAlign = 'center';
    controlUIBTGIAODIENEXCEL_GDC.style.fontSize = '18px';
    controlUIBTGIAODIENEXCEL_GDC.style.fontWeight = 'bold';

    var controlUIBTGIAODIENEXCEL_GDC_label = document.createElement('label');
    controlUIBTGIAODIENEXCEL_GDC_label.htmlFor = 'isBientapExcel';
    controlUIBTGIAODIENEXCEL_GDC_label.id = 'bientapexcel';
    controlUIBTGIAODIENEXCEL_GDC_label.style.margin = '2px 0px 0px 15px';
    controlUIBTGIAODIENEXCEL_GDC_label.innerHTML = 'Biên tập EXCEL';
    controlUIBTGIAODIENEXCEL_GDC_label.style.cursor = 'pointer';

    //
    controlUIBTGIAODIENEXCEL_div.onmouseover = function () {
        controlUIBTGIAODIENEXCEL_div.style.backgroundColor = 'aquamarine';
    };
    controlUIBTGIAODIENEXCEL_div.onmouseleave = function () {
        controlUIBTGIAODIENEXCEL_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlUIBTGIAODIENEXCEL_div.appendChild(controlUIBTGIAODIENEXCEL_GDC);
    controlUIBTGIAODIENEXCEL_div.appendChild(controlUIBTGIAODIENEXCEL_GDC_label);
    controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);

    controlUIBTGIAODIENEXCEL_GDC.addEventListener('change', function () {

        if (this.checked) {
            var m_taikhoan = localStorage['taikhoan'];
            var m_matkhau = localStorage['matkhau'];

            //
            if (m_taikhoan == null || m_taikhoan == '' || m_taikhoan == 'undefined') {
                $("#isBientapExcel").prop("checked", false);
                alert("Bạn chưa đăng nhập.");
            } else {
                //console.log("URL: " + `${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`);
                $("#loading").show();
                $.get(`${url}/api/get_DS_ThongTinNguoiDung_byTendangnhap/${m_taikhoan}`, function (data) {
                    if (data.length != 0) {
                        data.forEach(function (nguoidungObj) {
                            if (nguoidungObj.CHUC_NANG.indexOf(ADMIN_TTHT_CMIS_EXCUTE) !== -1) {
                                if (macongty == null || macongty == '0') {
                                    alert("Mời bạn chọn Công ty")
                                } else {
                                    if (madienluc == null || madienluc == '0') {
                                        alert("Mời bạn chọn Điện lực.");
                                    } else {
                                        if (matram == null || matram == '0') {
                                            alert("Mời bạn chọn Trạm.");
                                        } else {
                                            //
                                            isHiensocot = false;
                                            isBientap = false;
                                            isXemDuongDay = false;
                                            isTinhKhoangCachCot = false;
                                            isDoiTramChoCot = false;
                                            controlUICOT_GDC.checked = true
                                            controlUICOT_GDC_label.click();
                                            controlUIKHOANGCACHCOT_GDC.checked = true
                                            controlUIKHOANGCACHCOT_GDC_label.click();
                                            //
                                            w3_close();
                                            GF_SHOWTOAST_LONG("Bật chế độ Biên Tập Excel.", 250);

                                            //
                                            isBientapExcel = true;
                                            while (controlDiv.firstChild) {
                                                // console.log("CHan qua di");
                                                controlDiv.removeChild(controlDiv.firstChild);
                                            }
                                            centerControlDiv_BienTapEXCEL = document.createElement('div');
                                            var centerControl = new CenterControlExcel(centerControlDiv_BienTapEXCEL, map, controlUIBTGIAODIENEXCEL_GDC, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div, controlUIDUONGDICOT_div, controlUIKHOANGCACHCOT_div, controlUIDOITRAMCHOCOT_div, controlUICHONNHIEUTRAM_div);
                                            centerControlDiv_BienTapEXCEL.index = 1;
                                            map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv_BienTapEXCEL);

                                            controlUIBTGIAODIENEXCEL_GDC.checked = true;
                                        }
                                    }
                                }
                            } else {
                                $("#isBientapExcel").prop("checked", false);
                                alert("Tài khoản của bạn không có quyền thực thi Lưới Điện Hạ Thế.\nVui lòng liên hệ đầu mối công ty hoặc điện lực để được phân quyền.");
                            }
                        })
                    } else {
                        $("#isBientapExcel").prop("checked", false);
                        alert("Tài khoản này không tồn tại.");
                    }
                    $("#loading").hide();
                });
            }
        } else {

            //
            w3_open();
            GF_SHOWTOAST_LONG("Tắt chế độ Biên Tập Excel.", 250);
            isBientapExcel = false;

            while (centerControlDiv_BienTapEXCEL.firstChild) {
                // console.log("CHan qua di");
                centerControlDiv_BienTapEXCEL.removeChild(centerControlDiv_BienTapEXCEL.firstChild);
            }
            controlDiv.appendChild(controlUICOT_div);
            controlDiv.appendChild(controlUIBTGIAODIEN_div);
            controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
            controlDiv.appendChild(controlUIDUONGDICOT_div);
            controlDiv.appendChild(controlUIKHOANGCACHCOT_div);
            controlDiv.appendChild(controlUIDOITRAMCHOCOT_div);
            controlDiv.appendChild(controlUICHONNHIEUTRAM_div);
            centerControlDiv_BienTapEXCEL = document.createElement('div');
            $("#tinhkhoangcachcot").prop("disabled", false);
            $("#isTinhKhoangCachCot").prop("disabled", false);
            isTinhKhoangCachCot = false;
            $("#xemduongday").prop("disabled", false);
            $("#isXemDuongDay").prop("disabled", false);
            isXemDuongDay = false;
        }
    });
    /* KET THUC 1.3: Bien tap Excel */

    // /* BAT DAU 1.4: Xem duong day */
    var controlUIDUONGDICOT_div = document.createElement('div');
    controlUIDUONGDICOT_div.id = 'GND_duongdiday';
    controlUIDUONGDICOT_div.style.cssFloat = 'left';
    var controlUIDUONGDICOT_GDC = document.createElement('input');
    controlUIDUONGDICOT_GDC.type = 'checkbox';
    controlUIDUONGDICOT_GDC.id = 'isXemDuongDay';
    controlUIDUONGDICOT_GDC.name = 'cb_xemduongday';
    controlUIDUONGDICOT_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUIDUONGDICOT_GDC.style.width = '18px';
    controlUIDUONGDICOT_GDC.style.height = '25px';
    controlUIDUONGDICOT_GDC.style.textAlign = 'center';
    controlUIDUONGDICOT_GDC.style.fontSize = '18px';
    controlUIDUONGDICOT_GDC.style.fontWeight = 'bold';

    var controlUIDUONGDICOT_GDC_label = document.createElement('label');
    controlUIDUONGDICOT_GDC_label.htmlFor = 'isXemDuongDay';
    controlUIDUONGDICOT_GDC_label.id = 'xemduongday';
    controlUIDUONGDICOT_GDC_label.style.margin = '2px 0px 0px 21px';
    controlUIDUONGDICOT_GDC_label.style.cursor = 'pointer';
    controlUIDUONGDICOT_GDC_label.innerHTML = 'Đường đi cột';

    controlUIDUONGDICOT_div.appendChild(controlUIDUONGDICOT_GDC);
    controlUIDUONGDICOT_div.appendChild(controlUIDUONGDICOT_GDC_label);
    controlDiv.appendChild(controlUIDUONGDICOT_div);

    controlUIDUONGDICOT_GDC.addEventListener('change', function () {
        list_points = new Array();
        if (this.checked) {
            isXemDuongDay = true;
        } else {
            isXemDuongDay = false;
        }
    });

    controlUIDUONGDICOT_GDC.addEventListener('change', function () {
        list_points = new Array();
        if (this.checked) {
            isXemDuongDay = true;
        } else {
            isXemDuongDay = false;
        }
    });
    /* KET THUC 1.4: Xem duong day */

    /* BAT DAU 1.5: Khoang cach cot */
    var controlUIKHOANGCACHCOT_div = document.createElement('div');
    controlUIKHOANGCACHCOT_div.id = 'GND_khoangcachcot';
    controlUIKHOANGCACHCOT_div.style.cssFloat = 'left';
    var controlUIKHOANGCACHCOT_GDC = document.createElement('input');
    controlUIKHOANGCACHCOT_GDC.type = 'checkbox';
    controlUIKHOANGCACHCOT_GDC.id = 'isTinhKhoangCachCot';
    controlUIKHOANGCACHCOT_GDC.name = 'cb_tinhkhoangcachcot';
    controlUIKHOANGCACHCOT_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUIKHOANGCACHCOT_GDC.style.width = '18px';
    controlUIKHOANGCACHCOT_GDC.style.height = '25px';
    controlUIKHOANGCACHCOT_GDC.style.textAlign = 'center';
    controlUIKHOANGCACHCOT_GDC.style.fontSize = '18px';
    controlUIKHOANGCACHCOT_GDC.style.fontWeight = 'bold';

    var controlUIKHOANGCACHCOT_GDC_label = document.createElement('label');
    controlUIKHOANGCACHCOT_GDC_label.htmlFor = 'isTinhKhoangCachCot';
    controlUIKHOANGCACHCOT_GDC_label.id = 'tinhkhoangcachcot';
    controlUIKHOANGCACHCOT_GDC_label.style.margin = '2px 0px 0px 10px';
    controlUIKHOANGCACHCOT_GDC_label.style.cursor = 'pointer';
    controlUIKHOANGCACHCOT_GDC_label.innerHTML = 'Khoảng cách cột';

    //
    controlUIKHOANGCACHCOT_div.onmouseover = function () {
        controlUIKHOANGCACHCOT_div.style.backgroundColor = 'aquamarine';
    };
    controlUIKHOANGCACHCOT_div.onmouseleave = function () {
        if (isTinhKhoangCachCot) {
            controlUIKHOANGCACHCOT_div.style.backgroundColor = 'aquamarine';
        } else {
            controlUIKHOANGCACHCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        }
    };
    controlUIKHOANGCACHCOT_div.appendChild(controlUIKHOANGCACHCOT_GDC);
    controlUIKHOANGCACHCOT_div.appendChild(controlUIKHOANGCACHCOT_GDC_label);
    controlDiv.appendChild(controlUIKHOANGCACHCOT_div);

    controlUIKHOANGCACHCOT_GDC.addEventListener('change', function () {

        // San tai
        controlUIDOITRAMCHOCOT_GDC.checked = false;
        controlUIDOITRAMCHOCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        isDoiTramChoCot = false;
        $("#thanhnoidung_chuyencot").hide();

        //
        list_points_Distances = new Array();
        totalLine = 0;
        $("#thanhnoidung_khoangcach").empty();
        $("#thanhnoidung_khoangcach").append(`<p style="font-weight: bold;color: blue;padding-top:10px">Tổng độ dài: ${DDT_showTotalLines(listCothathe)}m</p>`);
        $("#thanhnoidung_khoangcach").append(`<span id="tongkhoangcach" style="font-weight: bold;color: red;padding-top:0px">Độ dài: 0m</span><br>`);
        if (this.checked) {
            controlUIKHOANGCACHCOT_div.style.backgroundColor = 'aquamarine';
            $("#thanhnoidung_khoangcach").show();
            isTinhKhoangCachCot = true;
        } else {
            controlUIKHOANGCACHCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
            isTinhKhoangCachCot = false;
            $("#thanhnoidung_khoangcach").hide();
        }
    });
    /* KET THUC 1.5: Khoang cach cot */

    /* BAT DAU 1.6: San tai */
    var controlUIDOITRAMCHOCOT_div = document.createElement('div');
    controlUIDOITRAMCHOCOT_div.id = 'GND_doitramchocot';
    controlUIDOITRAMCHOCOT_div.style.cssFloat = 'left';
    var controlUIDOITRAMCHOCOT_GDC = document.createElement('input');
    controlUIDOITRAMCHOCOT_GDC.type = 'checkbox';
    controlUIDOITRAMCHOCOT_GDC.id = 'isDoiTramChoCot';
    controlUIDOITRAMCHOCOT_GDC.name = 'cb_doitramchocot';
    controlUIDOITRAMCHOCOT_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUIDOITRAMCHOCOT_GDC.style.width = '18px';
    controlUIDOITRAMCHOCOT_GDC.style.height = '25px';
    controlUIDOITRAMCHOCOT_GDC.style.textAlign = 'center';
    controlUIDOITRAMCHOCOT_GDC.style.fontSize = '18px';
    controlUIDOITRAMCHOCOT_GDC.style.fontWeight = 'bold';

    var controlUIDOITRAMCHOCOT_GDC_label = document.createElement('label');
    controlUIDOITRAMCHOCOT_GDC_label.htmlFor = 'isDoiTramChoCot';
    controlUIDOITRAMCHOCOT_GDC_label.id = 'doitramchocot';
    controlUIDOITRAMCHOCOT_GDC_label.style.margin = '2px 0px 0px 24px';
    controlUIDOITRAMCHOCOT_GDC_label.innerHTML = 'San tải TBA';
    controlUIDOITRAMCHOCOT_GDC_label.style.cursor = 'pointer';

    //
    controlUIDOITRAMCHOCOT_div.onmouseover = function () {
        controlUIDOITRAMCHOCOT_div.style.backgroundColor = 'aquamarine';
    };
    controlUIDOITRAMCHOCOT_div.onmouseleave = function () {
        if (isDoiTramChoCot) {
            controlUIDOITRAMCHOCOT_div.style.backgroundColor = 'aquamarine';
        } else {
            controlUIDOITRAMCHOCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        }
    };
    controlUIDOITRAMCHOCOT_div.appendChild(controlUIDOITRAMCHOCOT_GDC);
    controlUIDOITRAMCHOCOT_div.appendChild(controlUIDOITRAMCHOCOT_GDC_label);
    controlDiv.appendChild(controlUIDOITRAMCHOCOT_div);

    controlUIDOITRAMCHOCOT_GDC.addEventListener('change', function () {

        // Khoang cach cot an
        controlUIKHOANGCACHCOT_GDC.checked = false;
        controlUIKHOANGCACHCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        isTinhKhoangCachCot = false;
        $("#thanhnoidung_khoangcach").hide();

        //
        if (this.checked) {
            list_Cot_ChuyenTram = new Array();
            $("#thanhnoidung_chuyencot").empty();
            $("#thanhnoidung_chuyencot").append(
                `<p><span id="socotcanchuyen" style="font-weight: bold;color: red;padding-top:10px">Các cột cần chuyển: 0</span>
                <button class='button_dongy' style="margin-left:15px" onclick="clickChuyencotchotram()">Gửi</button></p>`
            );
            controlUIDOITRAMCHOCOT_div.style.backgroundColor = 'aquamarine';
            $("#thanhnoidung_chuyencot").show();
            isDoiTramChoCot = true;

        } else {
            list_Cot_ChuyenTram = new Array();
            controlUIDOITRAMCHOCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
            isDoiTramChoCot = false;
            $("#thanhnoidung_chuyencot").hide();
            //
        }
    });
    /* KET THUC 1.6: San tai*/

    /* BAT DAU 1.7: Multi tram */
    var controlUICHONNHIEUTRAM_div = document.createElement('div');
    controlUICHONNHIEUTRAM_div.id = 'GND_chonnhieutram';
    controlUICHONNHIEUTRAM_div.style.cssFloat = 'left';
    var controlUICHONNHIEUTRAM_GDC = document.createElement('input');
    controlUICHONNHIEUTRAM_GDC.type = 'checkbox';
    controlUICHONNHIEUTRAM_GDC.id = 'isChonNhieuTram';
    controlUICHONNHIEUTRAM_GDC.name = 'cb_chonnhieutram';
    controlUICHONNHIEUTRAM_GDC.style.borderRadius = '5px 5px 5px 5px';
    controlUICHONNHIEUTRAM_GDC.style.width = '18px';
    controlUICHONNHIEUTRAM_GDC.style.height = '25px';
    controlUICHONNHIEUTRAM_GDC.style.textAlign = 'center';
    controlUICHONNHIEUTRAM_GDC.style.fontSize = '18px';
    controlUICHONNHIEUTRAM_GDC.style.fontWeight = 'bold';

    var controlUICHONNHIEUTRAM_GDC_label = document.createElement('label');
    controlUICHONNHIEUTRAM_GDC_label.htmlFor = 'isChonNhieuTram';
    controlUICHONNHIEUTRAM_GDC_label.id = 'chonnhieutram';
    controlUICHONNHIEUTRAM_GDC_label.style.margin = '2px 0px 0px 13px';
    controlUICHONNHIEUTRAM_GDC_label.innerHTML = 'Xem nhiều trạm';
    controlUICHONNHIEUTRAM_GDC_label.style.cursor = 'pointer';

    //
    controlUICHONNHIEUTRAM_div.onmouseover = function () {
        controlUICHONNHIEUTRAM_div.style.backgroundColor = 'aquamarine';
    };
    controlUICHONNHIEUTRAM_div.onmouseleave = function () {
        if (isChonNhieuTram) {
            controlUICHONNHIEUTRAM_div.style.backgroundColor = 'aquamarine';
        } else {
            controlUICHONNHIEUTRAM_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        }
    };
    controlUICHONNHIEUTRAM_div.appendChild(controlUICHONNHIEUTRAM_GDC);
    controlUICHONNHIEUTRAM_div.appendChild(controlUICHONNHIEUTRAM_GDC_label);
    controlDiv.appendChild(controlUICHONNHIEUTRAM_div);
    controlUICHONNHIEUTRAM_GDC.addEventListener('change', function () {

        // San tai
        controlUIDOITRAMCHOCOT_GDC.checked = false;
        controlUIDOITRAMCHOCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        controlUIDOITRAMCHOCOT_div.hidden = true;
        isDoiTramChoCot = false;
        $("#thanhnoidung_chuyencot").hide();

        // Khoang cach cot an
        controlUIKHOANGCACHCOT_GDC.checked = false;
        controlUIKHOANGCACHCOT_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        isTinhKhoangCachCot = false;
        $("#thanhnoidung_khoangcach").hide();

        //
        controlUIBTGIAODIENEXCEL_div.hidden = true;
        controlUIBTGIAODIEN_div.hidden = true;

        //
        if (this.checked) {
            controlUICHONNHIEUTRAM_div.style.backgroundColor = 'aquamarine';
            Init_listTram(jsonDmTram);
            $("#MultiTram_dialog").show();
            isChonNhieuTram = true;

        } else {
            controlUICHONNHIEUTRAM_div.style.backgroundColor = 'rgba(192,192,192, 0.5)';
            clickXembaocao();
            isChonNhieuTram = false;
        }
    });
    /* KET THUC 1.7: Multi tram */
}

/* 2. Khi vao giao dien chuc nang Bien Tap Giao Dien */
function CenterControlGiaoDien(controlDiv, map, control_temp, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div, controlUIDUONGDICOT_div, controlUIKHOANGCACHCOT_div, controlUICHONNHIEUTRAM_div, controlUIDOITRAMCHOCOT_div) {

    //create button DSCot in google map when choose option "Biên tập"
    var controlSave = document.createElement('button');
    controlSave.id = 'BientapSave';
    controlSave.style.borderRadius = '5px 5px 5px 5px';
    controlSave.style.margin = '5px';
    controlSave.style.border = '1px solid brown';
    controlSave.style.padding = '0px';
    controlSave.style.paddingTop = '1.8px';
    controlSave.style.width = '85px';
    controlSave.style.height = '35px';
    controlSave.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlSave.style.textAlign = 'center';
    controlSave.style.fontSize = '13px';
    controlSave.style.fontWeight = 'bold';
    controlSave.innerHTML = 'Lưu lại';
    controlSave.onclick = function () {
        //console.log("đã click nut Save");
    };
    controlSave.onmouseover = function () {
        controlSave.style.backgroundColor = 'aquamarine';
    };
    controlSave.onmouseleave = function () {
        controlSave.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlSave);

    //create button HSSD in google map when choose option "Biên tập"
    var controlDestroy = document.createElement('button');
    controlDestroy.style.borderRadius = '5px 5px 5px 5px';
    controlDestroy.style.margin = '5px';
    controlDestroy.style.border = '1px solid brown';
    controlDestroy.style.padding = '0px';
    controlDestroy.style.paddingTop = '1.8px';
    controlDestroy.style.width = '85px';
    controlDestroy.style.height = '35px';
    controlDestroy.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlDestroy.style.textAlign = 'center';
    controlDestroy.style.fontSize = '13px';
    controlDestroy.style.fontWeight = 'bold';
    controlDestroy.style.color = 'brown';
    controlDestroy.innerHTML = 'Hủy';
    controlDestroy.onclick = function () {
        while (controlDiv.firstChild) {
            //console.log("CHan qua di");
            controlDiv.removeChild(controlDiv.firstChild);
        }
        controlDiv.appendChild(controlUICOT_div);
        controlDiv.appendChild(controlUIBTGIAODIEN_div);
        controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
        controlDiv.appendChild(controlUIDUONGDICOT_div);
        controlDiv.appendChild(controlUIKHOANGCACHCOT_div);
        controlDiv.appendChild(controlUIDOITRAMCHOCOT_div);
        controlDiv.appendChild(controlUICHONNHIEUTRAM_div);
        control_temp.click();
        isBientap = false;
    };
    controlDestroy.onmouseover = function () {
        controlDestroy.style.backgroundColor = 'aquamarine';
    };
    controlDestroy.onmouseleave = function () {
        controlDestroy.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlDestroy);
}

//* 3. Khi vao giao dien chuc nang Bien Tap Excel */
function CenterControlExcel(controlDiv, map, control_temp, controlUICOT_div, controlUIBTGIAODIEN_div, controlUIBTGIAODIENEXCEL_div, controlUIDUONGDICOT_div, controlUIKHOANGCACHCOT_div, controlUIDOITRAMCHOCOT_div, controlUICHONNHIEUTRAM_div) {

    //create button DSCot in google map when choose option "Biên tập"
    var controlUIEXCELCOT = document.createElement('button');
    controlUIEXCELCOT.id = 'Excelcot';
    controlUIEXCELCOT.style.borderRadius = '5px 5px 5px 5px';
    controlUIEXCELCOT.style.border = '1px solid brown';
    controlUIEXCELCOT.style.margin = '5px';
    controlUIEXCELCOT.style.padding = '0px';
    controlUIEXCELCOT.style.paddingTop = '1.8px';
    controlUIEXCELCOT.style.width = '85px';
    controlUIEXCELCOT.style.height = '35px';
    controlUIEXCELCOT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIEXCELCOT.style.textAlign = 'center';
    controlUIEXCELCOT.style.fontSize = '13px';
    controlUIEXCELCOT.style.fontWeight = 'bold';
    controlUIEXCELCOT.innerHTML = 'Excel Cột';
    controlUIEXCELCOT.onclick = function () {
        localStorage['macongty_excel'] = macongty;
        localStorage['madienluc_excel'] = madienluc;
        localStorage['matram_excel'] = matram;
        var win = window.open("/luoidienhathe_uploaddscot", '_blank');
        win.focus();
    };
    controlUIEXCELCOT.onmouseover = function () {
        controlUIEXCELCOT.style.backgroundColor = 'aquamarine';
    };
    controlUIEXCELCOT.onmouseleave = function () {
        controlUIEXCELCOT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIEXCELCOT);

    //create button DSHinh in google map when choose option "Biên tập"
    var controlUIEXCELHINH = document.createElement('button');
    controlUIEXCELHINH.id = 'Excelhinh';
    controlUIEXCELHINH.style.borderRadius = '5px 5px 5px 5px';
    controlUIEXCELHINH.style.border = '1px solid brown';
    controlUIEXCELHINH.style.margin = '5px';
    controlUIEXCELHINH.style.padding = '0px';
    controlUIEXCELHINH.style.paddingTop = '1.8px';
    controlUIEXCELHINH.style.width = '85px';
    controlUIEXCELHINH.style.height = '35px';
    controlUIEXCELHINH.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIEXCELHINH.style.textAlign = 'center';
    controlUIEXCELHINH.style.fontSize = '13px';
    controlUIEXCELHINH.style.fontWeight = 'bold';
    controlUIEXCELHINH.innerHTML = 'Excel Hình';
    controlUIEXCELHINH.onclick = function () {
        localStorage['macongty_excel'] = macongty;
        localStorage['madienluc_excel'] = madienluc;
        localStorage['matram_excel'] = matram;
        var win = window.open("/luoidienhathe_uploaddslinkhinhanh", '_blank');
        win.focus();
    };
    controlUIEXCELHINH.onmouseover = function () {
        controlUIEXCELHINH.style.backgroundColor = 'aquamarine';
    };
    controlUIEXCELHINH.onmouseleave = function () {
        controlUIEXCELHINH.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIEXCELHINH);

    //create button DSImage in google map when choose option "Biên tập"
    var controlUIEXCELImage = document.createElement('button');
    controlUIEXCELImage.id = 'Excelimage';
    controlUIEXCELImage.style.borderRadius = '5px 5px 5px 5px';
    controlUIEXCELImage.style.border = '1px solid brown';
    controlUIEXCELImage.style.margin = '5px';
    controlUIEXCELImage.style.padding = '0px';
    controlUIEXCELImage.style.paddingTop = '1.8px';
    controlUIEXCELImage.style.width = '85px';
    controlUIEXCELImage.style.height = '35px';
    controlUIEXCELImage.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIEXCELImage.style.textAlign = 'center';
    controlUIEXCELImage.style.fontSize = '13px';
    controlUIEXCELImage.style.fontWeight = 'bold';
    controlUIEXCELImage.innerHTML = 'Hình Ảnh';
    controlUIEXCELImage.onclick = function () {

        var win = window.open("/luoidienhathe_uploadhinhanh", '_blank');
        win.focus();
    };
    controlUIEXCELImage.onmouseover = function () {
        controlUIEXCELImage.style.backgroundColor = 'aquamarine';
    };
    controlUIEXCELImage.onmouseleave = function () {
        controlUIEXCELImage.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIEXCELImage);

    //create button DSKhachHang in google map when choose option "Biên tập"
    var controlUIEXCELKH = document.createElement('button');
    controlUIEXCELKH.id = 'Excelkhachhang';
    controlUIEXCELKH.style.borderRadius = '5px 5px 5px 5px';
    controlUIEXCELKH.style.border = '1px solid brown';
    controlUIEXCELKH.style.margin = '5px';
    controlUIEXCELKH.style.padding = '0px';
    controlUIEXCELKH.style.paddingTop = '1.8px';
    controlUIEXCELKH.style.width = '85px';
    controlUIEXCELKH.style.height = '35px';
    controlUIEXCELKH.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIEXCELKH.style.textAlign = 'center';
    controlUIEXCELKH.style.fontSize = '13px';
    controlUIEXCELKH.style.fontWeight = 'bold';
    controlUIEXCELKH.innerHTML = 'Excel KH';
    controlUIEXCELKH.onclick = function () {
        localStorage['macongty_excel'] = macongty;
        localStorage['madienluc_excel'] = madienluc;
        localStorage['matram_excel'] = matram;
        var win = window.open("/luoidienhathe_uploadthongtinkhachhang", '_blank');
        win.focus();
    };
    controlUIEXCELKH.onmouseover = function () {
        controlUIEXCELKH.style.backgroundColor = 'aquamarine';
    };
    controlUIEXCELKH.onmouseleave = function () {
        controlUIEXCELKH.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    };
    controlDiv.appendChild(controlUIEXCELKH);

    //create button HSSD in google map when choose option "Biên tập"
    var controlUIEXCELHDSD = document.createElement('button');
    controlUIEXCELHDSD.style.borderRadius = '5px 5px 5px 5px';
    controlUIEXCELHDSD.style.border = '1px solid brown';
    controlUIEXCELHDSD.style.margin = '5px';
    controlUIEXCELHDSD.style.padding = '0px';
    controlUIEXCELHDSD.style.paddingTop = '1.8px';
    controlUIEXCELHDSD.style.width = '85px';
    controlUIEXCELHDSD.style.height = '35px';
    controlUIEXCELHDSD.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIEXCELHDSD.style.textAlign = 'center';
    controlUIEXCELHDSD.style.fontSize = '13px';
    controlUIEXCELHDSD.style.fontWeight = 'bold';
    controlUIEXCELHDSD.style.color = 'brown';
    controlUIEXCELHDSD.innerHTML = 'HDSD';
    controlUIEXCELHDSD.onclick = function () {
        var win = window.open("https://ttht.cpc.vn/evncpc/evncpc_ttht/7.%20H%C6%B0%E1%BB%9Bng%20d%E1%BA%ABn%20%C4%91%E1%BA%A9y%20d%E1%BB%AF%20li%E1%BB%87u%20c%E1%BB%99t,%20%C4%91%C6%B0%E1%BB%9Dng%20d%E1%BA%ABn%20h%C3%ACnh%20%E1%BA%A3nh%20v%C3%A0%20h%C3%ACnh%20%E1%BA%A3nh%20l%C3%AAn%20Server%20th%C3%B4ng%20qua%20Web%20EVNCPC%20TTHT/", '_blank');
        win.focus();
    };
    controlUIEXCELHDSD.onmouseover = function () {
        controlUIEXCELHDSD.style.backgroundColor = 'aquamarine';
        controlUIEXCELHDSD.style.color = 'brown';
    };
    controlUIEXCELHDSD.onmouseleave = function () {
        controlUIEXCELHDSD.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        controlUIEXCELHDSD.style.color = 'brown';
    };
    controlDiv.appendChild(controlUIEXCELHDSD);


    var controlUIEXCELOUT = document.createElement('button');
    controlUIEXCELOUT.style.borderRadius = '5px 5px 5px 5px';
    controlUIEXCELOUT.style.border = '1px solid brown';
    controlUIEXCELOUT.style.margin = '5px';
    controlUIEXCELOUT.style.padding = '0px';
    controlUIEXCELOUT.style.paddingTop = '1.8px';
    controlUIEXCELOUT.style.width = '85px';
    controlUIEXCELOUT.style.height = '35px';
    controlUIEXCELOUT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
    controlUIEXCELOUT.style.textAlign = 'center';
    controlUIEXCELOUT.style.fontSize = '13px';
    controlUIEXCELOUT.style.fontWeight = 'bold';
    controlUIEXCELOUT.style.color = 'brown';
    controlUIEXCELOUT.innerHTML = 'Thoát';
    controlUIEXCELOUT.onmouseover = function () {
        controlUIEXCELOUT.style.backgroundColor = 'aquamarine';
        controlUIEXCELOUT.style.color = 'brown';
    };
    controlUIEXCELOUT.onmouseleave = function () {
        controlUIEXCELOUT.style.backgroundColor = 'rgba(192,192,192, 0.5)';
        controlUIEXCELOUT.style.color = 'brown';
    };
    controlUIEXCELOUT.onclick = function () {
        while (controlDiv.firstChild) {
            // console.log("CHan qua di");
            controlDiv.removeChild(controlDiv.firstChild);
        }

        controlDiv.appendChild(controlUICOT_div);
        controlDiv.appendChild(controlUIBTGIAODIEN_div);
        controlDiv.appendChild(controlUIBTGIAODIENEXCEL_div);
        controlDiv.appendChild(controlUIDUONGDICOT_div);
        controlDiv.appendChild(controlUIKHOANGCACHCOT_div);
        controlDiv.appendChild(controlUIDOITRAMCHOCOT_div);
        controlDiv.appendChild(controlUICHONNHIEUTRAM_div);
        control_temp.click();
        isBientapExcel = false;
    };
    controlDiv.appendChild(controlUIEXCELOUT);
}

/* 4. Xac dinh vi tri ban dang o day */
function addYourLocationButton(map) {
    var controlDiv = document.createElement('div');

    var firstChild = document.createElement('button');
    firstChild.style.backgroundColor = '#fff';
    firstChild.style.border = 'none';
    firstChild.style.outline = 'none';
    firstChild.style.width = '28px';
    firstChild.style.height = '28px';
    firstChild.style.borderRadius = '2px';
    firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
    firstChild.style.cursor = 'pointer';
    firstChild.style.marginLeft = '10px';
    firstChild.style.padding = '0px';
    firstChild.title = 'Your Location';
    controlDiv.appendChild(firstChild);

    var secondChild = document.createElement('div');
    secondChild.style.margin = '5px';
    secondChild.style.width = '18px';
    secondChild.style.height = '18px';
    secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-1x.png)';
    secondChild.style.backgroundSize = '180px 18px';
    secondChild.style.backgroundPosition = '0px 0px';
    secondChild.style.backgroundRepeat = 'no-repeat';
    secondChild.id = 'you_location_img';
    firstChild.appendChild(secondChild);

    firstChild.addEventListener('click', function () {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var marker = new google.maps.Marker({
                    position: pos,
                    map: map,
                    title: 'Bạn đang ở đây'
                });
                markeryourlocation.push(marker);
                var infowindow = new google.maps.InfoWindow({
                    content: 'Bạn đang ở đây',
                    position: pos
                });
                // Khi click vào Marker thì hiển thị
                infowindow.open(map, markeryourlocation[0]);
                moveToLocation(pos.lat, pos.lng);
            });
        } else {
            // Browser doesn't support Geolocation
        }
    });

    controlDiv.index = 1;
    map.controls[google.maps.ControlPosition.LEFT_BOTTOM].push(controlDiv);
}

//
function HuyXemNhieuTram() {
    // AAAAA
    document.getElementById('MultiTram_dialog').style.display = 'none';
    isChonNhieuTram = false;
    clickXembaocao();
}

//
function huysocotchotram() {
    document.getElementById('id_chuyentramchocot').style.display = 'none';
    document.getElementById('isDoiTramChoCot').click();
}