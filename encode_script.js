// Require Filesystem module
const fs = require("fs");
const path = require("path");

// Require the Obfuscator Module
const JavaScriptObfuscator = require('javascript-obfuscator');

fs.readdirSync(path.join(__dirname, 'private')).forEach((file) => {
    let pathFileIn = path.join(__dirname, 'private', file);
    let pathFileOut = path.join(__dirname, 'public', 'js', file);
    fs.stat(pathFileIn, (err, stat) => {
        if (err) throw err;
        if(stat.isFile() && path.extname(pathFileIn) == '.js') {
            fs.readFile(pathFileIn, "UTF-8", function(err, data) {
                if (err) throw err;
            
                // Obfuscate content of the JS file
                const obfuscationResult = JavaScriptObfuscator.obfuscate(data);
                
                // Write the obfuscated code into a new file
                fs.writeFile(pathFileOut, obfuscationResult.getObfuscatedCode() , function(err) {
                    if(err) {
                        return console.log(err);
                    }
                
                    console.log("The file was saved!");
                });
            });
        }
    })
});

// // Read the file of your original JavaScript Code as text
// fs.readFile('private/luoidienhathe.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidienhathe.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });
// fs.readFile('private/luoidientrungthe.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidientrungthe.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });
// fs.readFile('private/baocaohathe_cotdien.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_cotdien.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_hoanthanhtram.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_hoanthanhtram.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_rasoatthongtin.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_rasoatthongtin.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_thieuthongtindiemdo.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_thieuthongtindiemdo.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_thieuthongtindodem.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_thieuthongtindodem.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_thieuthongtinkhachhang.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_thieuthongtinkhachhang.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_thongtinanhkhachhang.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_thongtinanhkhachhang.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_thongtinanhkhachhang_thieuanh.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_thongtinanhkhachhang_thieuanh.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhang_thongtinanhkhachhang_thieuanhcongto.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhang_thongtinanhkhachhang_thieuanhcongto.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_khachhangsai.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_khachhangsai.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaohathe_tongquan.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaohathe_tongquan.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/baocaotrungthe_tongquan.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/baocaotrungthe_tongquan.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/bieudo.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/bieudo.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/btht_bandoluoidien.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/btht_bandoluoidien.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/btht_bienbankiemtraluoidien.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/btht_bienbankiemtraluoidien.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/btht_kiemtratheotuyenday.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/btht_kiemtratheotuyenday.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/btht_luoidienhathe.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/btht_luoidienhathe.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/btht_vitribatthuong.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/btht_vitribatthuong.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/global.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/global.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/index.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/index.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidienhathe_duyetdanhsach.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidienhathe_duyetdanhsach.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidienhathe_treothaocongto.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidienhathe_treothaocongto.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });
// fs.readFile('private/luoidienhathe_uploaddscot.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidienhathe_uploaddscot.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidienhathe_uploaddslinkhinhanh.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidienhathe_uploaddslinkhinhanh.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidienhathe_uploadhinhanh.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidienhathe_uploadhinhanh.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidienhathe_uploadthongtinkhachhang.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidienhathe_uploadthongtinkhachhang.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidientrungthe_uploaddscot.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidientrungthe_uploaddscot.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidientrungthe_uploaddslinkhinhanh.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidientrungthe_uploaddslinkhinhanh.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/luoidientrungthe_uploadhinhanh.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/luoidientrungthe_uploadhinhanh.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/quanlyphanquyen.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/quanlyphanquyen.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });

// fs.readFile('private/quanlyvitri.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/quanlyvitri.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });
// fs.readFile('private/thongtinkhachhang.js', "UTF-8", function(err, data) {
//     if (err) {
//         throw err;
//     }

//     // Obfuscate content of the JS file
//     var obfuscationResult = JavaScriptObfuscator.obfuscate(data);
    
//     // Write the obfuscated code into a new file
//     fs.writeFile('public/js/thongtinkhachhang.js', obfuscationResult.getObfuscatedCode() , function(err) {
//         if(err) {
//             return console.log(err);
//         }
    
//         console.log("The file was saved!");
//     });
// });















