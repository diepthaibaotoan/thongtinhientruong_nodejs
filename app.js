﻿var url = "https://ttht.cpc.vn:8080";
// khai báo library
var express = require('express');
var path = require('path');
var bodyParse = require('path');
var JavaScriptObfuscator = require('javascript-obfuscator');
const request = require("request");
var session = require('express-session');
var cors = require('cors');
var flash = require('connect-flash');
const fs = require('fs');
// var mv = require('mv');
const sharp = require('sharp');
const { createCanvas, loadImage } = require('canvas');
//init app
var app = express();
var bodyParser = require('body-parser');
var multer = require('multer');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
app.use(session({ secret: 'toandtb', saveUninitialized: true, resave: true }));
app.use(cors());
app.options('*', cors());

//middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(flash());

//----------------upload image HA_THE
var storageImage = multer.diskStorage({
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    },
    destination: function (req, file, callback) {
        var isCorrectImg = file.originalname[6];
        var madviqly = file.originalname.substring(0, 6);
        if (isCorrectImg == '_') {
            // callback(null, "./uploads/Images/");
            callback(null, "./" + getPathStorageImage(madviqly))
        }
    }
});

var uploadImages = multer({
    storage: storageImage
}).array("imgUploader", 1000);

app.post("/UploadImages", function (req, res) {
    uploadImages(req, res, function (err) {
        if (err) {
            // return res.end("Something went wrong!");
            //   return res.redirect(url);
            req.flash('messageupload', "false");
            res.redirect('/luoidienhathe_uploadhinhanh');
        } else {
            //return res.end("File uploaded sucessfully!.");
            req.flash('messageupload', "true");
            res.redirect('/luoidienhathe_uploadhinhanh');
        }
    });
});

//----------------upload image TRUNG_THE
var storageImage_TRUNGTHE = multer.diskStorage({
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    },
    destination: function (req, file, callback) {
        var isCorrectImg = file.originalname[6];
        var MA_DVIQLY = file.originalname.substring(0, 6);
        if (isCorrectImg == '_') {
            callback(null, "./F:/THUTHAPHIENTRUONG/Img/" + MA_DVIQLY + "/HINHANH_HIENTRUONG/");
        } else {

        }

    }
});

var uploadImages_TRUNGTHE = multer({
    storage: storageImage_TRUNGTHE
}).array("imgUploader", 1000)

app.post("/UploadImages_TRUNGTHE", function (req, res) {
    uploadImages_TRUNGTHE(req, res, function (err) {
        if (err) {
            // return res.end("Something went wrong!");
            //   return res.redirect(url);
            req.flash('messageupload', "false");
            res.redirect('/luoidientrungthe_uploadhinhanh');
        } else {
            //return res.end("File uploaded sucessfully!.");
            req.flash('messageupload', "true");
            res.redirect('/luoidientrungthe_uploadhinhanh');
        }
    });
});

//----------------upload image KHACH_HANG
var storageImage_KH = multer.diskStorage({
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    },
    destination: function (req, file, callback) {
        callback(null, "./" + getMiddlePath());
    }
});

var uploadImages_KH = multer({
    storage: storageImage_KH
}).array("imgUploader_KH", 1000);

app.post("/UploadImages_KH", function (req, res) {
    uploadImages_KH(req, res, async function (err) {
        var new_filename;
        var madviqly = req.body.madviqly;
        var matram = req.body.matram;
        var makhachhang = req.body.makhachhang;
        var taikhoan = req.body.taikhoan;

        //server
        var src = getMiddlePath();
        var dst = getPathStorageImage(madviqly);

        var length = req.files.length;

        //resize all files request
        for (var i = 0; i < length; i++) {
            var type_file = req.files[i].originalname.substring(req.files[i].originalname.indexOf('.') + 1, req.files[i].originalname.length);
            new_filename = setFilename(madviqly, matram, makhachhang, i, type_file);

            await resizePromise(src + req.files[i].originalname, dst + new_filename, 960, 1280).then(result => {
                console.log(result)
            }).catch(error => {
                console.log(error)
            })

            var api = getUrlImage(madviqly, '', 'HA_THE', matram, '', makhachhang, 'HINHANH_HIENTRUONG', madviqly + '/HINHANH_HIENTRUONG', new_filename, '', taikhoan, 'KD', '');
            console.log(api);
            await request(api, function (error, response, body) {
                if (error) console.log('ERROR: ' + err);
            });

        }

        //delete cache after using sharp
        sharp.cache({ items: 200 });
        sharp.cache({ files: 0 });
        sharp.cache(false);

        //delete all files in directory
        for (var i = 0; i < length; i++) {
            await unlinkPromise(src + req.files[i].originalname).then(result => {
                console.log(result)
            }).catch(error => {
                console.log(error)
            })
        }

        if (err) {
            req.flash('messageupload', "false");
            res.redirect('/luoidienhathe_uploadhinhanhkhachhang');
        } else {
            req.flash('messageupload', "true");
            res.redirect('/luoidienhathe_uploadhinhanhkhachhang');
        }
    });
});

app.post('/api/image-edit', async (req, res, next) => {
    try {
        console.log(req.body);
        const infoImageDraf = req.body.infoImageDraf;
        const itemDraws = req.body.itemDraws;
        const madviqly = req.body.madviqly;
        const matram = req.body.matram;
        const makhachhang = req.body.makhachhang;
        const taikhoan = req.body.taikhoan;
        if(!madviqly || !matram || !makhachhang || !taikhoan) throw new Error('Thiếu dữ liệu')
        //server
        
        const dst = getPathStorageImage(madviqly);
        const extFile = infoImageDraf.src.split('.').pop();
        const link = infoImageDraf.src.substring(0, infoImageDraf.src.lastIndexOf('/'));       
        const newFilename = setFilename(madviqly, matram, makhachhang, 1, extFile);

        const image = await loadImage(infoImageDraf.src);
        const canvas = createCanvas(image.width, image.height)
        const ctx = canvas.getContext('2d');
        draw(ctx, image, itemDraws, infoImageDraf, canvas)
        const buf3 = canvas.toBuffer('image/' + extFile, { quality: 1.0 });
        fs.writeFileSync(dst + newFilename, buf3); 

        const api = getUrlImage(madviqly, '', 'HA_THE', matram, '', makhachhang, 'HINHANH_HIENTRUONG', madviqly + '/HINHANH_HIENTRUONG', newFilename, '', taikhoan, 'KD', '');
        request(api, (error, response, body) => {
            if(error) {
                return res.status(500).json({
                    message: error.toString()
                })
            }
            else return res.status(200).json({
                message: 'Lưu thành công',
                src: link + '/'+newFilename
            }) 
        });
    } catch(err) {
        return res.status(500).json({
            message: err.toString()
        })
    }
})

draw = (imgContext, image, itemDraws, infoImageDraf, canvas) => {
    if(!itemDraws) return;
    imgContext.beginPath();
    let isComparse = canvas && infoImageDraf.width != canvas.width;
    let diffWidth = isComparse ? canvas.width / infoImageDraf.width : 1;
    let diffHeight = isComparse ? canvas.height / infoImageDraf.height : 1;
    imgContext.drawImage(image, 0, 0, image.width, image.height); 
    if(itemDraws.rect) {
        let x1 = diffWidth * itemDraws.rect.x1,
            y1 = diffHeight * itemDraws.rect.y1,
            x2 = diffWidth * itemDraws.rect.x2,
            y2 = diffHeight * itemDraws.rect.y2;
        imgContext.rect(x1, y1, x2 - x1, y2 - y1);
        imgContext.lineWidth = diffWidth * 3;
        imgContext.strokeStyle = 'red';
        imgContext.stroke();
    }

    if(itemDraws.text) {
        let fontSize = diffWidth * 20;
        imgContext.font = `bold ${fontSize}px verdana, sans-serif `;
        imgContext.fillStyle = "red";
        for (let i = 0; i < itemDraws.text.length; i++) {
            let text = itemDraws.text[i];
            text.width = imgContext.measureText(text.text).width;
            text.height = diffHeight * 16;
            imgContext.fillText(text.text, diffWidth * text.x, diffHeight * text.y);
        }
    }
    imgContext.closePath();
}

//----------------upload image COT_HATHE
var storageImage_COT = multer.diskStorage({
    filename: function (req, file, callback) {
        callback(null, file.originalname);
    },
    destination: function (req, file, callback) {
        callback(null, "./" + getMiddlePath());
    }
});

var uploadImages_COT = multer({
    storage: storageImage_COT
}).array("imgUploader_COT", 1000);

app.post("/UploadImages_COT", function (req, res) {
    uploadImages_COT(req, res, async function (err) {
        var new_filename;
        var madviqly = req.body.madviqly;
        var matram = req.body.matram;
        var socot = req.body.socot;
        var taikhoan = req.body.taikhoan;

        //server
        var src = getMiddlePath();
        var dst = getPathStorageImage(madviqly);

        var length = req.files.length;

        //resize all files request
        for (var i = 0; i < length; i++) {
            var type_file = req.files[i].originalname.substring(req.files[i].originalname.indexOf('.') + 1, req.files[i].originalname.length);
            new_filename = setFilename(madviqly, matram, replaceAll(socot.trim(), '/', '_'), i, type_file);

            await resizePromise(src + req.files[i].originalname, dst + new_filename, 960, 1280).then(result => {
                console.log(result)
            }).catch(error => {
                console.log(error)
            })

            var api = getUrlImage(madviqly, '', 'HA_THE', matram, socot.trim(), '', 'HINHANH_HIENTRUONG', madviqly + '/HINHANH_HIENTRUONG', new_filename, '', taikhoan, 'KD', '');
            console.log(api);
            await request(api, function (error, response, body) {
                if (error) console.log('ERROR: ' + err);
            });
        }

        //delete cache after using sharp
        sharp.cache({ items: 200 });
        sharp.cache({ files: 0 });
        sharp.cache(false);

        //delete all files in directory
        for (var i = 0; i < length; i++) {
            await unlinkPromise(src + req.files[i].originalname).then(result => {
                console.log(result)
            }).catch(error => {
                console.log(error)
            })
        }

        if (err) {
            req.flash('messageupload', "false");
            res.redirect('/luoidienhathe_uploadhinhanhcot');
        } else {
            req.flash('messageupload', "true");
            res.redirect('/luoidienhathe_uploadhinhanhcot');
        }
    });
});

app.post("/deleteImage",async function (req, res) {
    console.log(req.body.madviqly + "-" + req.body.matram + "-" + req.body.tenhinhanh);

    var madviqly = req.body.madviqly;
    var matram = req.body.matram;
    var filename = req.body.tenhinhanh;

    //delete link image in database
    var api = getUrlDeleteLinkImage(madviqly, matram, filename);
    console.log(api);

    await request(api, function (error, response, body) {
        if (error) console.log('ERROR: ' + err);
    });

    //delete files image in directory
    var path = getPathStorageImage(madviqly);

    await unlinkPromise(path + filename).then(result => {
        console.log(result)
        res.send("success");
    }).catch(error => {
        console.log("delete image: " + error)
        res.send("failed");
    })
})


function unlinkPromise(src) {
    return new Promise((resolve, reject) => {
        fs.unlink(src, function (err) {
            if (err) reject(err)
            console.log('delete successfull')
            resolve(true)
        });
    });
}

function resizePromise(src, dst, width, height) {
    return new Promise((resolve, reject) => {
        sharp(src).resize(width, height).toFile(dst, function (err) {
            if (err) reject(err)
            console.log('resize successfull')
            resolve(true)
        })
    });
}


// View engine setup
app.set('views', path.join(__dirname, 'views')); //chua giao dien 
app.set('view engine', 'ejs');
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
    next();
});

//set public folder
app.use(express.static(path.join(__dirname, 'public'))); // thu muc nay chua file hinh anh, css, hay jquery
app.use(session({ secret: 'toandtb', saveUninitialized: true, resave: true }));
require('./routes/router')(app);

// Dua len server
// var port = 8080;
// const https = require('https');
// const options = {
//     key: fs.readFileSync('key/key.pem'),
//     cert: fs.readFileSync('key/cert.pem')
// };
// var server = https.createServer(options, app).listen(port);
// //Chat
// var io = require("socket.io")(server);

// Chay localhost
 var port = 8080;
 var server = require("http").Server(app);
 server.listen(port);
 //Chat
 var io = require("socket.io")(server);

// 
io.on("connection", function (socket) {

    //console.log('Có người kết nối ' + port + ': ' + socket.id);
    var is_choupdate = true;
    var userName;
    var thietbi;
    var lat;
    var long;

    // lang nghe ngat ket noi
    socket.on("disconnect", function (data) {
        var url_guitrangthaionline = url + "/api/update_DS_Nguoidungonline_full/%20/" +
            userName + "/" + thietbi + "/0/0/0";
        //console.log('Có người ngắt kết nối ' + port + ': ' + url_guitrangthaionline);
        if (userName == undefined || thietbi == undefined || lat == undefined || long == undefined) {
            // khong lam gi ca
        } else {
            console.log("Disconnect " + thietbi + ": " + userName);
            // request(url_guitrangthaionline, function (error, response, body) {
            //     var url_laytrangthaionline = url + "/api/get_DS_Nguoidungonline_byTrangthai/1";
            //     request(url_laytrangthaionline, function (error, response, body) {
            //         try {
            //             var jsondata = JSON.parse(response.body);
            //             io.sockets.emit("Server-send-username", jsondata);
            //         } catch (e) {
            //             console.log('LOI DISCONNECT:', thietbi + "-" + e);
            //         }
            //     });
            // });

            var url_vitri = 'https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/update_DS_Nguoidungvitri?' +
                'MA_DVIQLY=%20' +
                '&TEN_DANGNHAP=' + userName +
                '&THIET_BI=' + thietbi +
                '&TRANG_THAI=false' +
                '&LAT=' + lat +
                '&LONG=' + long +
                '&KEY=toandtb';
            request(url_vitri, function (error, response, body) {
                try {
                    if (body != '') {
                        body = body.replace('</string>', '');
                        body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                        body = body.replace('<string xmlns="http://tempuri.org/">', '');
                        body = body.trim();
                        //console.log("Connect4-" + body);
                        var jsondata = JSON.parse(body);
                        io.sockets.emit("Server-send-username", jsondata);
                    }
                } catch (e) {
                    console.log('LOI DISCONNECT:', thietbi + "-" + e);
                }
            });
        }
    });

    // Lang ngh client gui tin hieu ve
    socket.on("Client-send-chat", function (data) {
        // 1 tra cho tat ca ngay ca chinh no
        //io.sockets.emit("Server-send-chat", data); // gui len server

        // 2 tra cho chinh no thoi
        //socket.emit("Server-send-data", data);

        // 3 tra cho may thang khac
        //socket.broadcast.emit("Server-send-data", data);

        //
        var res = data.split("$%");
        var url_guinoidungchat = url + "/api/set_DS_Tinnhan/" +
            res[0] + "/" + convert_khongchonull(res[1]);
        // console.log("URL: "+ url_guinoidungchat);
        request(url_guinoidungchat, function (error, response, body) {

            var url_laydanhsachtinnhan = url + "/api/get_DS_Tinnhan";
            request(url_laydanhsachtinnhan, function (error, response, body) {
                var jsondata = JSON.parse(response.body);
                io.sockets.emit("Server-send-chat", jsondata); // gui len server 
            });
        });
    });

    // Lang nghe client gui ai dang nhap ve
    socket.on("Client-send-username", function (data) {
        // console.log("Nguoi dang nhap:" + data);
        try {
            var res = data.split("%");
            if (res.length == 4) { // Android tra ve
                // if (is_choupdate) {
                //     thietbi = res[0];
                //     userName = res[1];
                //     lat = res[2];
                //     long = res[3];
                //     var url_vitri = 'https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/update_DS_Nguoidungvitri?' +
                //         'MA_DVIQLY=%20' +
                //         '&TEN_DANGNHAP=' + userName +
                //         '&THIET_BI=' + thietbi +
                //         '&TRANG_THAI=true' +
                //         '&LAT=' + lat +
                //         '&LONG=' + long +
                //         '&KEY=toandtb';
                //     request(url_vitri, function (error, response, body) {
                //         console.log("Connect4-" + thietbi + ": " + userName);
                //         var DateObj = new Date();
                //         var giay_cho = DateObj.getSeconds();
                //         if (giay_cho % 5 == 0) {
                //             try {
                //                 if (body != '') {
                //                     body = body.replace('</string>', '');
                //                     body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                //                     body = body.replace('<string xmlns="http://tempuri.org/">', '');
                //                     body = body.trim();
                //                     //console.log("Connect4-" + body);
                //                     var jsondata = JSON.parse(body);
                //                     io.sockets.emit("Server-send-username", jsondata);
                //                 }
                //             } catch (e) {
                //                 console.log('LOI Connect-Android TRONG:', "" + body);
                //             }
                //         } else {
                //             //
                //         }
                //     });

                //     var DateObj = new Date();
                //     giay =  DateObj.getSeconds();
                //     is_choupdate = false;
                // } else {
                //     var DateObj = new Date();
                //     if (giay == DateObj.getSeconds()) {
                //         //console.log("Connect4-" + giay + "-=giay: " + userName);
                //         is_choupdate = false;
                //     } else {
                //         //console.log("Connect4-" + giay + "-<>giay: " + userName);
                //         is_choupdate = true;
                //     }
                // }
            } else if (res.length == 2) {

                //
                thietbi = res[0];
                userName = res[1];
                lat = 0;
                long = 0;
                var url_vitri = 'https://gismobile.cpc.vn/Service_GIS_ToanDTB.asmx/update_DS_Nguoidungvitri?' +
                    'MA_DVIQLY=%20' +
                    '&TEN_DANGNHAP=' + userName +
                    '&THIET_BI=' + thietbi +
                    '&TRANG_THAI=true' +
                    '&LAT=' + lat +
                    '&LONG=' + long +
                    '&KEY=toandtb';
                request(url_vitri, function (error, response, body) {
                    try {
                        if (body != '') {
                            body = body.replace('</string>', '');
                            body = body.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                            body = body.replace('<string xmlns="http://tempuri.org/">', '');
                            body = body.trim();
                            var jsondata = JSON.parse(body);
                            io.sockets.emit("Server-send-username", jsondata);
                        }
                    } catch (e) {
                        console.log('LOI Connect-Window TRONG:', "" + e);
                    }
                });
            }
        } catch (e) {
            console.log('LOI CONNECT:', "" + e);
        }
    });


    // Lang nghe client gui su co ve
    socket.on("Client-send-suco", function (data) {
        var url_laysuco = url + "/api/mtb_get_DS_Suco_byMadviqly/PC05CC/toandtb";
        request(url_laysuco, function (error, response, body) {
            var ds_suco = '';
            var jsondata = JSON.parse(response.body.replace(/\\/g, ''));
            jsondata.forEach(function (item) {
                if (ds_suco == "") {
                    ds_suco = formatDate(item.THOI_GIAN) + ": " + item.TEN_SUCO;
                } else {
                    ds_suco = ds_suco + "\n" + item.THOI_GIAN + ": " + item.TEN_SUCO;
                }
            });
            // 1 tra cho tat ca ngay ca chinh no
            io.sockets.emit("Server-send-suco", ds_suco); // gui len server
        });
        // 1 tra cho tat ca ngay ca chinh no
        //io.sockets.emit("Server-send-username", data); // gui len server

        // 2 tra cho chinh no thoi
        //socket.emit("Server-send-username", data);

        // 3 tra cho may thang khac
        //socket.broadcast.emit("Server-send-username", data);
    });
});

function formatDate(userDate) {
    var returnDate = new Date(userDate.split('/').join(''));
    var y = returnDate.getFullYear();
    var m = returnDate.getMonth() + 1; // Step 6
    var d = returnDate.getDay();

    y = y.toString();
    m = m.toString();
    d = d.toString();

    if (m.length == 1) {
        m = '0' + m;
    }
    if (d.length == 1) {
        d = '0' + d;
    }

    returnDate = y + m + d;
    return returnDate;
}

function convert_khongchonull(chuoiguivao) {
    if (chuoiguivao == null) {
        return encodeURIComponent('');
    } else {
        return encodeURIComponent(chuoiguivao);
    }
}

function convertURI(str) {
    if (str == undefined || str == undefined || str.trim() == '') return '%20';
    return encodeURIComponent(str);
}

function setFilename(madviqly, matram, name, index, typefile) {
    var today = new Date().getFullYear() + "" + ((new Date().getMonth() + 1) < 10 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1)) + "" + (new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate());
    return madviqly + "_" + matram + "_" + name + "_" + today + "_" + Date.now() + index + "." + typefile;
}

function getUrlImage(madviqly, matramnguon, maxuyettuyen, matram, socot, makhachhang, loaihinhanh, url_thumuc, tenhinhanh, ngaychup, nguoinhap, ungdungsudung, ghichu) {
    ngaychup = new Date().getFullYear() + "-" + ((new Date().getMonth() + 1) < 10 ? '0' + (new Date().getMonth() + 1) : (new Date().getMonth() + 1)) + "-" + (new Date().getDate() < 10 ? '0' + new Date().getDate() : new Date().getDate());
    var api = url + "/api/HATHE_updateDSHinhAnh"
        + '/' + convertURI(madviqly)
        + '/' + convertURI(matramnguon)
        + '/' + convertURI(maxuyettuyen)
        + '/' + convertURI(matram)
        + '/' + convertURI(socot)
        + '/' + convertURI(makhachhang)
        + '/' + convertURI(loaihinhanh)
        + '/' + convertURI(url_thumuc)
        + '/' + convertURI(tenhinhanh)
        + '/' + convertURI(ngaychup)
        + '/' + convertURI(nguoinhap)
        + '/' + convertURI(ungdungsudung)
        + '/' + convertURI(ghichu);

    return api;
}

function getUrlDeleteLinkImage(madviqly, matram, tenhinhanh) {
    var api = url + "/api/HATHE_deleteDSHinhanh_byTenhinhanh"
        + '/' + convertURI(madviqly)
        + '/' + convertURI(matram)
        + '/' + convertURI(tenhinhanh)
    return api;
}

function getPathStorageImage(madviqly) {
    if (madviqly.indexOf("PC01") > -1 || madviqly.indexOf("PC02") > -1 || madviqly.indexOf("PC03") > -1) {
        return "G:/THUTHAPHIENTRUONG/Img/" + madviqly + "/HINHANH_HIENTRUONG/";
    } else if (madviqly.indexOf("PC05") > -1 || madviqly.indexOf("PC06") > -1 || madviqly.indexOf("PC07") > -1) {
        return "H:/THUTHAPHIENTRUONG/Img/" + madviqly + "/HINHANH_HIENTRUONG/";
    } else {
        return "F:/THUTHAPHIENTRUONG/Img/" + madviqly + "/HINHANH_HIENTRUONG/";
    }
}

function getMiddlePath() {
    return "F:/THUTHAPHIENTRUONG/Img/" + 'PPPPPP' + "/HINHANH_HIENTRUONG/";
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}